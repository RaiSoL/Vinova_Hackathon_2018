﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GazeObject : MonoBehaviour {


	public delegate void OnWaitTrigger();
	public static OnWaitTrigger OnWaitEvent;

	public float WaitTime = 5;
	private bool start=false;
	private float timeGo=0;


	// Use this for initialization
	void Start () {
		GazeEvent.OnGazeEnter = OnGazeEventEnter;
		GazeEvent.OnGazeExit = OnGazeEventExit;
		start = false;
		timeGo = 0;
	
	}
	
	// Update is called once per frame
	void Update () {
		if (start) {
			timeGo += Time.deltaTime;
			Image img = GetComponent<Image> ();
			img.fillAmount = (timeGo / WaitTime);
			if (timeGo > WaitTime) {
				start = false;
				if (OnWaitEvent != null) {
					OnWaitEvent ();
					img.fillAmount = 0;
					timeGo = 0;
				}
			}
		}
	}


	void OnGazeEventEnter()
	{
		Image img = GetComponent<Image> ();
		img.transform.localScale = new Vector3 (1, 1, 1);
		start = true;
	}

	void OnGazeEventExit()
	{
		start = false;
		Image img = GetComponent<Image> ();
		img.transform.localScale = new Vector3 (0.0f, 0.0f, 1);
		img.fillAmount = 1;
		timeGo = 0;
	}
}
