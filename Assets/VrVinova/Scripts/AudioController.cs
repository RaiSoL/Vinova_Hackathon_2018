﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {
	public static AudioController instance = null;
	private static AudioSource audioSource;

	void Awake()
	{
		//Check if instance already exists
		if (instance == null)
			//if not, set instance to this
			instance = this;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public AudioSource getAudioSource(){
		return audioSource;
	}
	public void setAudioSource(AudioSource audio){
		audioSource = audio;
	}
}
