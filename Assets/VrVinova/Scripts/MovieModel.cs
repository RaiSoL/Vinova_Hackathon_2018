﻿using System;

[Serializable]
public class MovieModel
{
	public string kind;
	public string etag;
	public VideoIdModel id;
//	public string url;
//	public string imageURL;
//	public string title;
//	public string description;
	public SnippetModel snippet;
	public string videoUrl;
}