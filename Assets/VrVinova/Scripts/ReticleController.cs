﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReticleController : MonoBehaviour {
	public static MeshRenderer rend;
	// Use this for initialization
	void Start () {
		rend = GetComponent<MeshRenderer>();
		rend.enabled = true;
	}

	// Update is called once per frame
	void Update () {

	}

	public static void OnRetEnter ()
	{
		Debug.Log ("Point in");
		if(rend != null)
		rend.enabled = false;
	}

	public static void OnRetExit ()
	{
		Debug.Log ("Point out");
		if(rend != null)
		rend.enabled = true;
	}
}
