﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UniRx.IObservable`1<System.Object>
struct IObservable_1_t1203238302;
// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Object>>
struct IEnumerable_1_t183091191;
// UniRx.IScheduler
struct IScheduler_t411218504;
// UniRx.IObservable`1<System.Object>[]
struct IObservable_1U5BU5D_t3893959211;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t3398609381;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t1859699442;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t777629997;
// System.String
struct String_t;
// System.Exception
struct Exception_t;
// UniRx.IObservable`1<UniRx.EventPattern`1<System.Object>>
struct IObservable_1_t1832587078;
// System.Func`2<System.EventHandler`1<System.Object>,System.Object>
struct Func_2_t663966201;
// UniRx.IObservable`1<UniRx.FrameInterval`1<System.Object>>
struct IObservable_1_t190480876;
// UniRx.IObservable`1<UniRx.IGroupedObservable`2<System.Object,System.Object>>
struct IObservable_1_t655751566;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2447130374;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t892470886;
// UniRx.IObservable`1<UniRx.Notification`1<System.Object>>
struct IObservable_1_t2313894890;
// UniRx.IObservable`1<UniRx.Pair`1<System.Object>>
struct IObservable_1_t1474948339;
// UniRx.IObservable`1<UniRx.TimeInterval`1<System.Object>>
struct IObservable_1_t72460664;
// UniRx.IObservable`1<UniRx.Timestamped`1<System.Object>>
struct IObservable_1_t4638714;
// UniRx.IObservable`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObservable_1_t2768334107;
// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
struct UnityEvent_2_t614268397;
// UniRx.IObservable`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>
struct IObservable_1_t1352113304;
// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
struct UnityEvent_3_t2404744798;
// UniRx.IObservable`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct IObservable_1_t772271949;
// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
struct UnityEvent_4_t4085588227;
// UniRx.AsyncMessageBroker
struct AsyncMessageBroker_t3406290114;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t1485381605;
// System.ObjectDisposedException
struct ObjectDisposedException_t21392786;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
struct Dictionary_2_t1229485932;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// UniRx.IObservable`1<UniRx.Unit>[]
struct IObservable_1U5BU5D_t3038720744;
// System.Func`2<System.Object,UniRx.IObservable`1<UniRx.Unit>>
struct Func_2_t852405815;
// System.Action`2<System.Object,System.Int32>
struct Action_2_t2340848427;
// System.Func`2<System.Action,System.Object>
struct Func_2_t4286427857;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Func`1<System.Collections.IEnumerator>
struct Func_1_t1283030885;
// System.Func`1<System.Object>
struct Func_1_t2509852811;
// System.Func`2<System.Object,System.Collections.IEnumerator>
struct Func_2_t1220308448;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t529408447;
// System.Action
struct Action_t1264377477;
// System.Action`1<System.Exception>
struct Action_1_t1609204844;
// System.Delegate
struct Delegate_t1188392813;
// System.IDisposable
struct IDisposable_t3640265483;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t1841557544;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t400248036;
// System.Action`1<System.Int32>
struct Action_1_t3123413348;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t1185869587;
// System.Action`1<System.Int64>
struct Action_1_t3909034899;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3141536353;
// System.Action`1<System.Single>
struct Action_1_t1569734369;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IObserver_1_t3432324148;
// System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Action_1_t1860522164;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct IObserver_1_t3731211239;
// System.Action`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct Action_1_t2159409255;
// UniRx.IObserver`1<UniRx.Diagnostics.LogEntry>
struct IObserver_1_t2885776692;
// System.Action`1<UniRx.Diagnostics.LogEntry>
struct Action_1_t1313974708;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t811551750;
// System.Action`1<UniRx.Unit>
struct Action_1_t3534717062;
// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_t3632554945;
// System.Action`3<System.Exception,System.Object,System.Object>
struct Action_3_t2220624998;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2470008838;
// System.Action`3<System.Single,System.Object,System.Object>
struct Action_3_t1712484883;
// System.Action`4<System.Int64,System.Object,System.Object,System.Object>
struct Action_4_t327971120;
// System.Action`4<System.Exception,System.Object,System.Object,System.Object>
struct Action_4_t1238479323;
// System.Action`4<System.Object,System.Object,System.Object,System.Object>
struct Action_4_t309985972;
// System.Action`2<System.Boolean,System.Object>
struct Action_2_t2523487705;
// System.Action`2<System.Exception,System.Object>
struct Action_2_t2161070725;
// System.Action`2<System.Int32,System.Object>
struct Action_2_t11315885;
// System.Action`2<System.Int64,System.Object>
struct Action_2_t2185242338;
// System.Action`2<UniRx.Unit,System.Object>
struct Action_2_t1614770371;
// UniRx.ISubject`1<System.Object>
struct ISubject_1_t2874575179;
// UniRx.LazyTask`1<System.Object>
struct LazyTask_1_t2287704132;
// UniRx.Notification`1<System.Object>
struct Notification_1_t4190762752;
// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// UniRx.ObservableYieldInstruction`1<System.Int64>
struct ObservableYieldInstruction_1_t2774328545;
// UniRx.ObservableYieldInstruction`1<System.Object>
struct ObservableYieldInstruction_1_t2117867405;
// UniRx.ObservableYieldInstruction`1<UniRx.Unit>
struct ObservableYieldInstruction_1_t2400010708;
// UniRx.ReactiveCollection`1<System.Object>
struct ReactiveCollection_1_t4090598151;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// UniRx.ReactiveCommand`1<System.Object>
struct ReactiveCommand_1_t1068238646;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t2515387399;
// UniRx.ReactiveDictionary`2<System.Object,System.Object>
struct ReactiveDictionary_2_t820484097;
// UniRx.ReactiveProperty`1<System.Boolean>
struct ReactiveProperty_1_t3626980155;
// UniRx.ReactiveProperty`1<System.Object>
struct ReactiveProperty_1_t2314831058;
// UniRx.ReadOnlyReactiveProperty`1<System.Object>
struct ReadOnlyReactiveProperty_1_t3751549484;
// UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>
struct Tuple_8_t3974872067;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t1074077891;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t682124106;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t3664942305;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t1982102915;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
struct UnityAction_1_t2741065664;
// System.Action`1<UnityEngine.Vector2>
struct Action_1_t2328697118;
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t3283971887;
// System.Action`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Action_1_t522702268;
// UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
struct UnityAction_3_t1557236713;
// System.Action`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>
struct Action_1_t3401448761;
// UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>
struct UnityAction_4_t682480391;
// System.Action`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct Action_1_t2821607406;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3903027533;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct EventFunction_1_t1764640198;
// System.Collections.Generic.IList`1<UnityEngine.Transform>
struct IList_1_t1120718408;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Func`2<System.Int64,System.Int64>
struct Func_2_t2818825014;
// System.Action`2<System.Exception,System.Int64>
struct Action_2_t2817531865;
// System.Func`2<UniRx.CollectionRemoveEvent`1<System.Object>,UniRx.CollectionRemoveEvent`1<System.Object>>
struct Func_2_t3268995430;
// System.Action`2<System.Exception,UniRx.CollectionRemoveEvent`1<System.Object>>
struct Action_2_t1067906221;
// System.Func`2<UniRx.CollectionAddEvent`1<System.Object>,UniRx.CollectionAddEvent`1<System.Object>>
struct Func_2_t278627730;
// System.Action`2<System.Exception,UniRx.CollectionAddEvent`1<System.Object>>
struct Action_2_t769019130;
// System.Func`2<System.Single,System.Single>
struct Func_2_t3660230206;
// System.Action`2<System.Exception,System.Single>
struct Action_2_t478231335;
// System.Func`2<System.Int32,System.Int32>
struct Func_2_t4154244306;
// System.Action`2<System.Exception,System.Int32>
struct Action_2_t2031910314;
// System.Func`2<System.Boolean,System.Boolean>
struct Func_2_t3812758338;
// System.Action`2<System.Exception,System.Boolean>
struct Action_2_t3473219822;
// System.Func`2<System.Collections.Generic.IList`1<System.Boolean>,System.Boolean>
struct Func_2_t3308430047;
// UniRx.Subject`1<System.Object>
struct Subject_1_t3168762551;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t4209139644;
// UniRx.Subject`1<System.Int32>
struct Subject_1_t3039602140;
// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t3450905854;
// UniRx.Subject`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct Subject_1_t2690504464;
// UniRx.Subject`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct Subject_1_t1655735010;
// UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct Subject_1_t2309927415;
// System.Action`2<System.Boolean,UniRx.ReactiveProperty`1<System.Boolean>>
struct Action_2_t3070361696;
// System.Action`2<UniRx.Unit,UniRx.ReactiveCommand`1<UniRx.Unit>>
struct Action_2_t4180013452;
// System.Collections.Generic.IEqualityComparer`1<System.Boolean>
struct IEqualityComparer_1_t2204619983;
// UniRx.Subject`1<System.Boolean>
struct Subject_1_t185944352;
// System.Func`2<UniRx.Unit,UniRx.Unit>
struct Func_2_t1874035210;
// System.Action`2<System.Exception,UniRx.Unit>
struct Action_2_t2443214028;
// System.Func`2<UniRx.Diagnostics.LogEntry,UniRx.Diagnostics.LogEntry>
struct Func_2_t2349466002;
// System.Action`2<System.Exception,UniRx.Diagnostics.LogEntry>
struct Action_2_t222471674;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler>
struct EventFunction_1_t3995630009;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct EventFunction_1_t2867327688;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
struct EventFunction_1_t64614563;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct EventFunction_1_t3256600500;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
struct EventFunction_1_t3111972472;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
struct EventFunction_1_t3587542510;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler>
struct EventFunction_1_t1977848392;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler>
struct EventFunction_1_t972960537;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct EventFunction_1_t3277009892;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
struct EventFunction_1_t2311673543;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
struct EventFunction_1_t2886331738;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
struct EventFunction_1_t2950825503;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>
struct EventFunction_1_t955952873;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler>
struct EventFunction_1_t3373214253;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>
struct EventFunction_1_t3912835512;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler>
struct EventFunction_1_t1475332338;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler>
struct EventFunction_1_t2658898854;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct ObjectPool_1_t231414508;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Func`2<System.Action,UnityEngine.Events.UnityAction>
struct Func_2_t157146996;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t600458651;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.Func`2<System.Object,UniRx.IObservable`1<UniRx.Unit>>[]
struct Func_2U5BU5D_t1724110606;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Collections.Generic.IEqualityComparer`1<System.Type>
struct IEqualityComparer_1_t296309482;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Type,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t3124434336;
// UniRx.IAsyncMessageBroker
struct IAsyncMessageBroker_t503716257;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// UniRx.Subject`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Subject_1_t1776710956;
// UniRx.Subject`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct Subject_1_t4285020456;
// UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct Subject_1_t2075598047;
// UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct Subject_1_t3715269461;
// UniRx.IObservable`1<UniRx.IObservable`1<System.Object>>
struct IObservable_1_t3621337736;
// UniRx.ICancelable
struct ICancelable_t3440398893;
// System.Void
struct Void_t1185182177;
// System.Func`2<UnityEngine.Transform,System.Boolean>
struct Func_2_t1722577774;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// System.Collections.Generic.HashSet`1<System.Type>
struct HashSet_1_t1048894234;
// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t1439315263;
// System.Func`3<System.Int64,UniRx.Unit,System.Int64>
struct Func_3_t2605432362;
// System.Func`3<UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t2376649402;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UniRx.BooleanDisposable
struct BooleanDisposable_t84760918;
// System.Func`2<UniRx.LazyTask,UnityEngine.Coroutine>
struct Func_2_t2678796183;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.EventHandler`1<System.Object>
struct EventHandler_1_t1004265597;

extern RuntimeClass* Observable_t2677550507_il2cpp_TypeInfo_var;
extern const uint32_t Observable_Merge_TisRuntimeObject_m2084972840_MetadataUsageId;
extern RuntimeClass* Scheduler_t1572131957_il2cpp_TypeInfo_var;
extern const uint32_t Observable_Merge_TisRuntimeObject_m2448690970_MetadataUsageId;
extern const uint32_t Observable_Merge_TisRuntimeObject_m1371129573_MetadataUsageId;
extern const uint32_t Observable_Merge_TisRuntimeObject_m2375299036_MetadataUsageId;
extern const uint32_t Observable_Merge_TisRuntimeObject_m2201465073_MetadataUsageId;
extern RuntimeClass* TimeSpan_t881159249_il2cpp_TypeInfo_var;
extern const uint32_t Observable_OnErrorRetry_TisRuntimeObject_TisRuntimeObject_m2365061993_MetadataUsageId;
extern const uint32_t Observable_OnErrorRetry_TisRuntimeObject_TisRuntimeObject_m3057258735_MetadataUsageId;
extern const uint32_t Observable_OnErrorRetry_TisRuntimeObject_TisRuntimeObject_m35022389_MetadataUsageId;
extern const uint32_t Observable_OnErrorRetry_TisRuntimeObject_TisRuntimeObject_m2883033659_MetadataUsageId;
extern const uint32_t Observable_OnErrorRetry_TisRuntimeObject_TisRuntimeObject_m1898518509_MetadataUsageId;
extern const uint32_t Observable_OnErrorRetry_TisRuntimeObject_m3009981768_MetadataUsageId;
extern const uint32_t Observable_Retry_TisRuntimeObject_m1366679961_MetadataUsageId;
extern const uint32_t Observable_Retry_TisRuntimeObject_m2603381968_MetadataUsageId;
extern const uint32_t Observable_Throttle_TisInt64_t3736567304_m1457699944_MetadataUsageId;
extern const uint32_t Observable_Throttle_TisRuntimeObject_m2844368835_MetadataUsageId;
extern const uint32_t Observable_ThrottleFirst_TisRuntimeObject_m1967177714_MetadataUsageId;
extern RuntimeClass* ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral207938285;
extern const uint32_t Observable_ThrottleFirstFrame_TisRuntimeObject_m3270737833_MetadataUsageId;
extern const uint32_t Observable_ThrottleFrame_TisRuntimeObject_m1878905285_MetadataUsageId;
extern const uint32_t Stubs_CatchIgnore_TisRuntimeObject_m2040818366_MetadataUsageId;
extern RuntimeClass* UnityEqualityComparer_t3411193022_il2cpp_TypeInfo_var;
extern const uint32_t Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m3883557576_MetadataUsageId;
extern const uint32_t Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m1594666584_MetadataUsageId;
extern const RuntimeMethod* Nullable_1__ctor_m3940678751_RuntimeMethod_var;
extern const uint32_t Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m56460662_MetadataUsageId;
extern const uint32_t Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_m2801338715_MetadataUsageId;
extern const uint32_t Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_m1316155870_MetadataUsageId;
extern const uint32_t Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_m3471062564_MetadataUsageId;
extern const uint32_t Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_m3516009247_MetadataUsageId;
extern const uint32_t Observable_TimeInterval_TisRuntimeObject_m4203739597_MetadataUsageId;
extern const uint32_t Observable_Timestamp_TisRuntimeObject_m3943482726_MetadataUsageId;
extern const uint32_t UnityEventExtensions_AsObservable_TisRuntimeObject_TisRuntimeObject_m4002206433_MetadataUsageId;
extern const uint32_t UnityEventExtensions_AsObservable_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m2902013217_MetadataUsageId;
extern const uint32_t UnityEventExtensions_AsObservable_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m3617384406_MetadataUsageId;
extern RuntimeClass* ObjectDisposedException_t21392786_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* IObservable_1U5BU5D_t3038720744_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m3038145912_RuntimeMethod_var;
extern String_t* _stringLiteral232837026;
extern const uint32_t AsyncMessageBroker_PublishAsync_TisRuntimeObject_m2308285229_MetadataUsageId;
extern RuntimeClass* MainThreadDispatcher_t3684499304_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Observable_Take_TisUnit_t3362249467_m3583177933_RuntimeMethod_var;
extern const uint32_t Observable_U3CObserveOnMainThread_1U3Em__7_TisRuntimeObject_m3402388184_MetadataUsageId;
extern const uint32_t Observable_U3CObserveOnMainThread_1U3Em__9_TisRuntimeObject_m3367388466_MetadataUsageId;
extern RuntimeClass* Func_1_t1283030885_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Func_1__ctor_m3733315985_RuntimeMethod_var;
extern const uint32_t Observable_SelectMany_TisRuntimeObject_m2392913521_MetadataUsageId;
extern const uint32_t Observable_SelectMany_TisRuntimeObject_m3078398865_MetadataUsageId;
extern const uint32_t Observable_SelectMany_TisRuntimeObject_m3243795571_MetadataUsageId;
extern const uint32_t Observable_SelectMany_TisUnit_t3362249467_m1014653926_MetadataUsageId;
extern RuntimeClass* Stubs_t4137823816_il2cpp_TypeInfo_var;
extern const uint32_t Observer_Create_TisRuntimeObject_m1223195334_MetadataUsageId;
extern const uint32_t Observer_Create_TisRuntimeObject_m2421516608_MetadataUsageId;
extern const uint32_t Observer_Create_TisRuntimeObject_m710702794_MetadataUsageId;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern const uint32_t ObserverExtensions_Synchronize_TisRuntimeObject_m747448223_MetadataUsageId;
extern RuntimeClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral95342995;
extern const uint32_t Notification_CreateOnError_TisRuntimeObject_m3870854833_MetadataUsageId;
extern RuntimeClass* CancellationToken_t1265546479_il2cpp_TypeInfo_var;
extern const uint32_t Observable_ToYieldInstruction_TisInt64_t3736567304_m3018093371_MetadataUsageId;
extern const uint32_t Observable_ToYieldInstruction_TisRuntimeObject_m3235847277_MetadataUsageId;
extern const uint32_t Observable_ToYieldInstruction_TisRuntimeObject_m3857052964_MetadataUsageId;
extern const uint32_t Observable_ToYieldInstruction_TisUnit_t3362249467_m2489366239_MetadataUsageId;
extern const uint32_t Observable_ToYieldInstruction_TisUnit_t3362249467_m2382445313_MetadataUsageId;
extern const uint32_t Observable_StartAsCoroutine_TisInt32_t2950945753_m1004123127_MetadataUsageId;
extern const uint32_t Observable_StartAsCoroutine_TisInt32_t2950945753_m1977739596_MetadataUsageId;
extern const uint32_t Observable_StartAsCoroutine_TisRuntimeObject_m2276369549_MetadataUsageId;
extern const uint32_t Observable_StartAsCoroutine_TisRuntimeObject_m4187518785_MetadataUsageId;
extern const uint32_t Observable_StartAsCoroutine_TisRuntimeObject_m2348039341_MetadataUsageId;
extern const uint32_t Observable_StartAsCoroutine_TisRuntimeObject_m447298803_MetadataUsageId;
extern RuntimeClass* GameObject_t1113636619_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* Component_t1923634451_il2cpp_TypeInfo_var;
extern const uint32_t GameObjectExtensions_GetGameObject_TisRuntimeObject_m291751742_MetadataUsageId;
extern RuntimeClass* ExecuteEvents_t3484638744_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Item_m2402360903_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m3543896146_RuntimeMethod_var;
extern const uint32_t ExecuteEvents_ExecuteHierarchy_TisRuntimeObject_m3266560969_MetadataUsageId;
extern const uint32_t ExecuteEvents_GetEventHandler_TisRuntimeObject_m3687647312_MetadataUsageId;
struct Object_t631007953_marshaled_com;

struct IObservable_1U5BU5D_t3893959211;
struct Func_2U5BU5D_t1724110606;
struct IObservable_1U5BU5D_t3038720744;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SUBSCRIBE_3_T155852850_H
#define SUBSCRIBE_3_T155852850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`3<System.Single,System.Object,System.Object>
struct  Subscribe_3_t155852850  : public RuntimeObject
{
public:
	// TState1 UniRx.Observer/Subscribe`3::state1
	RuntimeObject * ___state1_0;
	// TState2 UniRx.Observer/Subscribe`3::state2
	RuntimeObject * ___state2_1;
	// System.Action`3<T,TState1,TState2> UniRx.Observer/Subscribe`3::onNext
	Action_3_t1712484883 * ___onNext_2;
	// System.Action`3<System.Exception,TState1,TState2> UniRx.Observer/Subscribe`3::onError
	Action_3_t2220624998 * ___onError_3;
	// System.Action`2<TState1,TState2> UniRx.Observer/Subscribe`3::onCompleted
	Action_2_t2470008838 * ___onCompleted_4;
	// System.Int32 UniRx.Observer/Subscribe`3::isStopped
	int32_t ___isStopped_5;

public:
	inline static int32_t get_offset_of_state1_0() { return static_cast<int32_t>(offsetof(Subscribe_3_t155852850, ___state1_0)); }
	inline RuntimeObject * get_state1_0() const { return ___state1_0; }
	inline RuntimeObject ** get_address_of_state1_0() { return &___state1_0; }
	inline void set_state1_0(RuntimeObject * value)
	{
		___state1_0 = value;
		Il2CppCodeGenWriteBarrier((&___state1_0), value);
	}

	inline static int32_t get_offset_of_state2_1() { return static_cast<int32_t>(offsetof(Subscribe_3_t155852850, ___state2_1)); }
	inline RuntimeObject * get_state2_1() const { return ___state2_1; }
	inline RuntimeObject ** get_address_of_state2_1() { return &___state2_1; }
	inline void set_state2_1(RuntimeObject * value)
	{
		___state2_1 = value;
		Il2CppCodeGenWriteBarrier((&___state2_1), value);
	}

	inline static int32_t get_offset_of_onNext_2() { return static_cast<int32_t>(offsetof(Subscribe_3_t155852850, ___onNext_2)); }
	inline Action_3_t1712484883 * get_onNext_2() const { return ___onNext_2; }
	inline Action_3_t1712484883 ** get_address_of_onNext_2() { return &___onNext_2; }
	inline void set_onNext_2(Action_3_t1712484883 * value)
	{
		___onNext_2 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_2), value);
	}

	inline static int32_t get_offset_of_onError_3() { return static_cast<int32_t>(offsetof(Subscribe_3_t155852850, ___onError_3)); }
	inline Action_3_t2220624998 * get_onError_3() const { return ___onError_3; }
	inline Action_3_t2220624998 ** get_address_of_onError_3() { return &___onError_3; }
	inline void set_onError_3(Action_3_t2220624998 * value)
	{
		___onError_3 = value;
		Il2CppCodeGenWriteBarrier((&___onError_3), value);
	}

	inline static int32_t get_offset_of_onCompleted_4() { return static_cast<int32_t>(offsetof(Subscribe_3_t155852850, ___onCompleted_4)); }
	inline Action_2_t2470008838 * get_onCompleted_4() const { return ___onCompleted_4; }
	inline Action_2_t2470008838 ** get_address_of_onCompleted_4() { return &___onCompleted_4; }
	inline void set_onCompleted_4(Action_2_t2470008838 * value)
	{
		___onCompleted_4 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_4), value);
	}

	inline static int32_t get_offset_of_isStopped_5() { return static_cast<int32_t>(offsetof(Subscribe_3_t155852850, ___isStopped_5)); }
	inline int32_t get_isStopped_5() const { return ___isStopped_5; }
	inline int32_t* get_address_of_isStopped_5() { return &___isStopped_5; }
	inline void set_isStopped_5(int32_t value)
	{
		___isStopped_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_3_T155852850_H
#ifndef SUBSCRIBE__1_T2940771805_H
#define SUBSCRIBE__1_T2940771805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe_`1<System.Object>
struct  Subscribe__1_t2940771805  : public RuntimeObject
{
public:
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe_`1::onError
	Action_1_t1609204844 * ___onError_0;
	// System.Action UniRx.Observer/Subscribe_`1::onCompleted
	Action_t1264377477 * ___onCompleted_1;
	// System.Int32 UniRx.Observer/Subscribe_`1::isStopped
	int32_t ___isStopped_2;

public:
	inline static int32_t get_offset_of_onError_0() { return static_cast<int32_t>(offsetof(Subscribe__1_t2940771805, ___onError_0)); }
	inline Action_1_t1609204844 * get_onError_0() const { return ___onError_0; }
	inline Action_1_t1609204844 ** get_address_of_onError_0() { return &___onError_0; }
	inline void set_onError_0(Action_1_t1609204844 * value)
	{
		___onError_0 = value;
		Il2CppCodeGenWriteBarrier((&___onError_0), value);
	}

	inline static int32_t get_offset_of_onCompleted_1() { return static_cast<int32_t>(offsetof(Subscribe__1_t2940771805, ___onCompleted_1)); }
	inline Action_t1264377477 * get_onCompleted_1() const { return ___onCompleted_1; }
	inline Action_t1264377477 ** get_address_of_onCompleted_1() { return &___onCompleted_1; }
	inline void set_onCompleted_1(Action_t1264377477 * value)
	{
		___onCompleted_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_1), value);
	}

	inline static int32_t get_offset_of_isStopped_2() { return static_cast<int32_t>(offsetof(Subscribe__1_t2940771805, ___isStopped_2)); }
	inline int32_t get_isStopped_2() const { return ___isStopped_2; }
	inline int32_t* get_address_of_isStopped_2() { return &___isStopped_2; }
	inline void set_isStopped_2(int32_t value)
	{
		___isStopped_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE__1_T2940771805_H
#ifndef SUBSCRIBE_1_T4250263556_H
#define SUBSCRIBE_1_T4250263556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`1<System.Int64>
struct  Subscribe_1_t4250263556  : public RuntimeObject
{
public:
	// System.Action`1<T> UniRx.Observer/Subscribe`1::onNext
	Action_1_t3909034899 * ___onNext_0;
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe`1::onError
	Action_1_t1609204844 * ___onError_1;
	// System.Action UniRx.Observer/Subscribe`1::onCompleted
	Action_t1264377477 * ___onCompleted_2;
	// System.Int32 UniRx.Observer/Subscribe`1::isStopped
	int32_t ___isStopped_3;

public:
	inline static int32_t get_offset_of_onNext_0() { return static_cast<int32_t>(offsetof(Subscribe_1_t4250263556, ___onNext_0)); }
	inline Action_1_t3909034899 * get_onNext_0() const { return ___onNext_0; }
	inline Action_1_t3909034899 ** get_address_of_onNext_0() { return &___onNext_0; }
	inline void set_onNext_0(Action_1_t3909034899 * value)
	{
		___onNext_0 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_0), value);
	}

	inline static int32_t get_offset_of_onError_1() { return static_cast<int32_t>(offsetof(Subscribe_1_t4250263556, ___onError_1)); }
	inline Action_1_t1609204844 * get_onError_1() const { return ___onError_1; }
	inline Action_1_t1609204844 ** get_address_of_onError_1() { return &___onError_1; }
	inline void set_onError_1(Action_1_t1609204844 * value)
	{
		___onError_1 = value;
		Il2CppCodeGenWriteBarrier((&___onError_1), value);
	}

	inline static int32_t get_offset_of_onCompleted_2() { return static_cast<int32_t>(offsetof(Subscribe_1_t4250263556, ___onCompleted_2)); }
	inline Action_t1264377477 * get_onCompleted_2() const { return ___onCompleted_2; }
	inline Action_t1264377477 ** get_address_of_onCompleted_2() { return &___onCompleted_2; }
	inline void set_onCompleted_2(Action_t1264377477 * value)
	{
		___onCompleted_2 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_2), value);
	}

	inline static int32_t get_offset_of_isStopped_3() { return static_cast<int32_t>(offsetof(Subscribe_1_t4250263556, ___isStopped_3)); }
	inline int32_t get_isStopped_3() const { return ___isStopped_3; }
	inline int32_t* get_address_of_isStopped_3() { return &___isStopped_3; }
	inline void set_isStopped_3(int32_t value)
	{
		___isStopped_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_1_T4250263556_H
#ifndef SUBSCRIBE__1_T3597232945_H
#define SUBSCRIBE__1_T3597232945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe_`1<System.Int64>
struct  Subscribe__1_t3597232945  : public RuntimeObject
{
public:
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe_`1::onError
	Action_1_t1609204844 * ___onError_0;
	// System.Action UniRx.Observer/Subscribe_`1::onCompleted
	Action_t1264377477 * ___onCompleted_1;
	// System.Int32 UniRx.Observer/Subscribe_`1::isStopped
	int32_t ___isStopped_2;

public:
	inline static int32_t get_offset_of_onError_0() { return static_cast<int32_t>(offsetof(Subscribe__1_t3597232945, ___onError_0)); }
	inline Action_1_t1609204844 * get_onError_0() const { return ___onError_0; }
	inline Action_1_t1609204844 ** get_address_of_onError_0() { return &___onError_0; }
	inline void set_onError_0(Action_1_t1609204844 * value)
	{
		___onError_0 = value;
		Il2CppCodeGenWriteBarrier((&___onError_0), value);
	}

	inline static int32_t get_offset_of_onCompleted_1() { return static_cast<int32_t>(offsetof(Subscribe__1_t3597232945, ___onCompleted_1)); }
	inline Action_t1264377477 * get_onCompleted_1() const { return ___onCompleted_1; }
	inline Action_t1264377477 ** get_address_of_onCompleted_1() { return &___onCompleted_1; }
	inline void set_onCompleted_1(Action_t1264377477 * value)
	{
		___onCompleted_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_1), value);
	}

	inline static int32_t get_offset_of_isStopped_2() { return static_cast<int32_t>(offsetof(Subscribe__1_t3597232945, ___isStopped_2)); }
	inline int32_t get_isStopped_2() const { return ___isStopped_2; }
	inline int32_t* get_address_of_isStopped_2() { return &___isStopped_2; }
	inline void set_isStopped_2(int32_t value)
	{
		___isStopped_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE__1_T3597232945_H
#ifndef STUBS_1_T2741129912_H
#define STUBS_1_T2741129912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Stubs`1<System.Int64>
struct  Stubs_1_t2741129912  : public RuntimeObject
{
public:

public:
};

struct Stubs_1_t2741129912_StaticFields
{
public:
	// System.Action`1<T> UniRx.Stubs`1::Ignore
	Action_1_t3909034899 * ___Ignore_0;
	// System.Func`2<T,T> UniRx.Stubs`1::Identity
	Func_2_t2818825014 * ___Identity_1;
	// System.Action`2<System.Exception,T> UniRx.Stubs`1::Throw
	Action_2_t2817531865 * ___Throw_2;

public:
	inline static int32_t get_offset_of_Ignore_0() { return static_cast<int32_t>(offsetof(Stubs_1_t2741129912_StaticFields, ___Ignore_0)); }
	inline Action_1_t3909034899 * get_Ignore_0() const { return ___Ignore_0; }
	inline Action_1_t3909034899 ** get_address_of_Ignore_0() { return &___Ignore_0; }
	inline void set_Ignore_0(Action_1_t3909034899 * value)
	{
		___Ignore_0 = value;
		Il2CppCodeGenWriteBarrier((&___Ignore_0), value);
	}

	inline static int32_t get_offset_of_Identity_1() { return static_cast<int32_t>(offsetof(Stubs_1_t2741129912_StaticFields, ___Identity_1)); }
	inline Func_2_t2818825014 * get_Identity_1() const { return ___Identity_1; }
	inline Func_2_t2818825014 ** get_address_of_Identity_1() { return &___Identity_1; }
	inline void set_Identity_1(Func_2_t2818825014 * value)
	{
		___Identity_1 = value;
		Il2CppCodeGenWriteBarrier((&___Identity_1), value);
	}

	inline static int32_t get_offset_of_Throw_2() { return static_cast<int32_t>(offsetof(Stubs_1_t2741129912_StaticFields, ___Throw_2)); }
	inline Action_2_t2817531865 * get_Throw_2() const { return ___Throw_2; }
	inline Action_2_t2817531865 ** get_address_of_Throw_2() { return &___Throw_2; }
	inline void set_Throw_2(Action_2_t2817531865 * value)
	{
		___Throw_2 = value;
		Il2CppCodeGenWriteBarrier((&___Throw_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STUBS_1_T2741129912_H
#ifndef SUBSCRIBE_1_T3464642005_H
#define SUBSCRIBE_1_T3464642005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`1<System.Int32>
struct  Subscribe_1_t3464642005  : public RuntimeObject
{
public:
	// System.Action`1<T> UniRx.Observer/Subscribe`1::onNext
	Action_1_t3123413348 * ___onNext_0;
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe`1::onError
	Action_1_t1609204844 * ___onError_1;
	// System.Action UniRx.Observer/Subscribe`1::onCompleted
	Action_t1264377477 * ___onCompleted_2;
	// System.Int32 UniRx.Observer/Subscribe`1::isStopped
	int32_t ___isStopped_3;

public:
	inline static int32_t get_offset_of_onNext_0() { return static_cast<int32_t>(offsetof(Subscribe_1_t3464642005, ___onNext_0)); }
	inline Action_1_t3123413348 * get_onNext_0() const { return ___onNext_0; }
	inline Action_1_t3123413348 ** get_address_of_onNext_0() { return &___onNext_0; }
	inline void set_onNext_0(Action_1_t3123413348 * value)
	{
		___onNext_0 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_0), value);
	}

	inline static int32_t get_offset_of_onError_1() { return static_cast<int32_t>(offsetof(Subscribe_1_t3464642005, ___onError_1)); }
	inline Action_1_t1609204844 * get_onError_1() const { return ___onError_1; }
	inline Action_1_t1609204844 ** get_address_of_onError_1() { return &___onError_1; }
	inline void set_onError_1(Action_1_t1609204844 * value)
	{
		___onError_1 = value;
		Il2CppCodeGenWriteBarrier((&___onError_1), value);
	}

	inline static int32_t get_offset_of_onCompleted_2() { return static_cast<int32_t>(offsetof(Subscribe_1_t3464642005, ___onCompleted_2)); }
	inline Action_t1264377477 * get_onCompleted_2() const { return ___onCompleted_2; }
	inline Action_t1264377477 ** get_address_of_onCompleted_2() { return &___onCompleted_2; }
	inline void set_onCompleted_2(Action_t1264377477 * value)
	{
		___onCompleted_2 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_2), value);
	}

	inline static int32_t get_offset_of_isStopped_3() { return static_cast<int32_t>(offsetof(Subscribe_1_t3464642005, ___isStopped_3)); }
	inline int32_t get_isStopped_3() const { return ___isStopped_3; }
	inline int32_t* get_address_of_isStopped_3() { return &___isStopped_3; }
	inline void set_isStopped_3(int32_t value)
	{
		___isStopped_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_1_T3464642005_H
#ifndef SUBSCRIBE__1_T2811611394_H
#define SUBSCRIBE__1_T2811611394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe_`1<System.Int32>
struct  Subscribe__1_t2811611394  : public RuntimeObject
{
public:
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe_`1::onError
	Action_1_t1609204844 * ___onError_0;
	// System.Action UniRx.Observer/Subscribe_`1::onCompleted
	Action_t1264377477 * ___onCompleted_1;
	// System.Int32 UniRx.Observer/Subscribe_`1::isStopped
	int32_t ___isStopped_2;

public:
	inline static int32_t get_offset_of_onError_0() { return static_cast<int32_t>(offsetof(Subscribe__1_t2811611394, ___onError_0)); }
	inline Action_1_t1609204844 * get_onError_0() const { return ___onError_0; }
	inline Action_1_t1609204844 ** get_address_of_onError_0() { return &___onError_0; }
	inline void set_onError_0(Action_1_t1609204844 * value)
	{
		___onError_0 = value;
		Il2CppCodeGenWriteBarrier((&___onError_0), value);
	}

	inline static int32_t get_offset_of_onCompleted_1() { return static_cast<int32_t>(offsetof(Subscribe__1_t2811611394, ___onCompleted_1)); }
	inline Action_t1264377477 * get_onCompleted_1() const { return ___onCompleted_1; }
	inline Action_t1264377477 ** get_address_of_onCompleted_1() { return &___onCompleted_1; }
	inline void set_onCompleted_1(Action_t1264377477 * value)
	{
		___onCompleted_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_1), value);
	}

	inline static int32_t get_offset_of_isStopped_2() { return static_cast<int32_t>(offsetof(Subscribe__1_t2811611394, ___isStopped_2)); }
	inline int32_t get_isStopped_2() const { return ___isStopped_2; }
	inline int32_t* get_address_of_isStopped_2() { return &___isStopped_2; }
	inline void set_isStopped_2(int32_t value)
	{
		___isStopped_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE__1_T2811611394_H
#ifndef SUBSCRIBE_1_T3593802416_H
#define SUBSCRIBE_1_T3593802416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`1<System.Object>
struct  Subscribe_1_t3593802416  : public RuntimeObject
{
public:
	// System.Action`1<T> UniRx.Observer/Subscribe`1::onNext
	Action_1_t3252573759 * ___onNext_0;
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe`1::onError
	Action_1_t1609204844 * ___onError_1;
	// System.Action UniRx.Observer/Subscribe`1::onCompleted
	Action_t1264377477 * ___onCompleted_2;
	// System.Int32 UniRx.Observer/Subscribe`1::isStopped
	int32_t ___isStopped_3;

public:
	inline static int32_t get_offset_of_onNext_0() { return static_cast<int32_t>(offsetof(Subscribe_1_t3593802416, ___onNext_0)); }
	inline Action_1_t3252573759 * get_onNext_0() const { return ___onNext_0; }
	inline Action_1_t3252573759 ** get_address_of_onNext_0() { return &___onNext_0; }
	inline void set_onNext_0(Action_1_t3252573759 * value)
	{
		___onNext_0 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_0), value);
	}

	inline static int32_t get_offset_of_onError_1() { return static_cast<int32_t>(offsetof(Subscribe_1_t3593802416, ___onError_1)); }
	inline Action_1_t1609204844 * get_onError_1() const { return ___onError_1; }
	inline Action_1_t1609204844 ** get_address_of_onError_1() { return &___onError_1; }
	inline void set_onError_1(Action_1_t1609204844 * value)
	{
		___onError_1 = value;
		Il2CppCodeGenWriteBarrier((&___onError_1), value);
	}

	inline static int32_t get_offset_of_onCompleted_2() { return static_cast<int32_t>(offsetof(Subscribe_1_t3593802416, ___onCompleted_2)); }
	inline Action_t1264377477 * get_onCompleted_2() const { return ___onCompleted_2; }
	inline Action_t1264377477 ** get_address_of_onCompleted_2() { return &___onCompleted_2; }
	inline void set_onCompleted_2(Action_t1264377477 * value)
	{
		___onCompleted_2 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_2), value);
	}

	inline static int32_t get_offset_of_isStopped_3() { return static_cast<int32_t>(offsetof(Subscribe_1_t3593802416, ___isStopped_3)); }
	inline int32_t get_isStopped_3() const { return ___isStopped_3; }
	inline int32_t* get_address_of_isStopped_3() { return &___isStopped_3; }
	inline void set_isStopped_3(int32_t value)
	{
		___isStopped_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_1_T3593802416_H
#ifndef SUBSCRIBE__1_T1847607301_H
#define SUBSCRIBE__1_T1847607301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe_`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct  Subscribe__1_t1847607301  : public RuntimeObject
{
public:
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe_`1::onError
	Action_1_t1609204844 * ___onError_0;
	// System.Action UniRx.Observer/Subscribe_`1::onCompleted
	Action_t1264377477 * ___onCompleted_1;
	// System.Int32 UniRx.Observer/Subscribe_`1::isStopped
	int32_t ___isStopped_2;

public:
	inline static int32_t get_offset_of_onError_0() { return static_cast<int32_t>(offsetof(Subscribe__1_t1847607301, ___onError_0)); }
	inline Action_1_t1609204844 * get_onError_0() const { return ___onError_0; }
	inline Action_1_t1609204844 ** get_address_of_onError_0() { return &___onError_0; }
	inline void set_onError_0(Action_1_t1609204844 * value)
	{
		___onError_0 = value;
		Il2CppCodeGenWriteBarrier((&___onError_0), value);
	}

	inline static int32_t get_offset_of_onCompleted_1() { return static_cast<int32_t>(offsetof(Subscribe__1_t1847607301, ___onCompleted_1)); }
	inline Action_t1264377477 * get_onCompleted_1() const { return ___onCompleted_1; }
	inline Action_t1264377477 ** get_address_of_onCompleted_1() { return &___onCompleted_1; }
	inline void set_onCompleted_1(Action_t1264377477 * value)
	{
		___onCompleted_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_1), value);
	}

	inline static int32_t get_offset_of_isStopped_2() { return static_cast<int32_t>(offsetof(Subscribe__1_t1847607301, ___isStopped_2)); }
	inline int32_t get_isStopped_2() const { return ___isStopped_2; }
	inline int32_t* get_address_of_isStopped_2() { return &___isStopped_2; }
	inline void set_isStopped_2(int32_t value)
	{
		___isStopped_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE__1_T1847607301_H
#ifndef STUBS_1_T991504268_H
#define STUBS_1_T991504268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Stubs`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct  Stubs_1_t991504268  : public RuntimeObject
{
public:

public:
};

struct Stubs_1_t991504268_StaticFields
{
public:
	// System.Action`1<T> UniRx.Stubs`1::Ignore
	Action_1_t2159409255 * ___Ignore_0;
	// System.Func`2<T,T> UniRx.Stubs`1::Identity
	Func_2_t3268995430 * ___Identity_1;
	// System.Action`2<System.Exception,T> UniRx.Stubs`1::Throw
	Action_2_t1067906221 * ___Throw_2;

public:
	inline static int32_t get_offset_of_Ignore_0() { return static_cast<int32_t>(offsetof(Stubs_1_t991504268_StaticFields, ___Ignore_0)); }
	inline Action_1_t2159409255 * get_Ignore_0() const { return ___Ignore_0; }
	inline Action_1_t2159409255 ** get_address_of_Ignore_0() { return &___Ignore_0; }
	inline void set_Ignore_0(Action_1_t2159409255 * value)
	{
		___Ignore_0 = value;
		Il2CppCodeGenWriteBarrier((&___Ignore_0), value);
	}

	inline static int32_t get_offset_of_Identity_1() { return static_cast<int32_t>(offsetof(Stubs_1_t991504268_StaticFields, ___Identity_1)); }
	inline Func_2_t3268995430 * get_Identity_1() const { return ___Identity_1; }
	inline Func_2_t3268995430 ** get_address_of_Identity_1() { return &___Identity_1; }
	inline void set_Identity_1(Func_2_t3268995430 * value)
	{
		___Identity_1 = value;
		Il2CppCodeGenWriteBarrier((&___Identity_1), value);
	}

	inline static int32_t get_offset_of_Throw_2() { return static_cast<int32_t>(offsetof(Stubs_1_t991504268_StaticFields, ___Throw_2)); }
	inline Action_2_t1067906221 * get_Throw_2() const { return ___Throw_2; }
	inline Action_2_t1067906221 ** get_address_of_Throw_2() { return &___Throw_2; }
	inline void set_Throw_2(Action_2_t1067906221 * value)
	{
		___Throw_2 = value;
		Il2CppCodeGenWriteBarrier((&___Throw_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STUBS_1_T991504268_H
#ifndef SUBSCRIBE_1_T2201750821_H
#define SUBSCRIBE_1_T2201750821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`1<UniRx.CollectionAddEvent`1<System.Object>>
struct  Subscribe_1_t2201750821  : public RuntimeObject
{
public:
	// System.Action`1<T> UniRx.Observer/Subscribe`1::onNext
	Action_1_t1860522164 * ___onNext_0;
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe`1::onError
	Action_1_t1609204844 * ___onError_1;
	// System.Action UniRx.Observer/Subscribe`1::onCompleted
	Action_t1264377477 * ___onCompleted_2;
	// System.Int32 UniRx.Observer/Subscribe`1::isStopped
	int32_t ___isStopped_3;

public:
	inline static int32_t get_offset_of_onNext_0() { return static_cast<int32_t>(offsetof(Subscribe_1_t2201750821, ___onNext_0)); }
	inline Action_1_t1860522164 * get_onNext_0() const { return ___onNext_0; }
	inline Action_1_t1860522164 ** get_address_of_onNext_0() { return &___onNext_0; }
	inline void set_onNext_0(Action_1_t1860522164 * value)
	{
		___onNext_0 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_0), value);
	}

	inline static int32_t get_offset_of_onError_1() { return static_cast<int32_t>(offsetof(Subscribe_1_t2201750821, ___onError_1)); }
	inline Action_1_t1609204844 * get_onError_1() const { return ___onError_1; }
	inline Action_1_t1609204844 ** get_address_of_onError_1() { return &___onError_1; }
	inline void set_onError_1(Action_1_t1609204844 * value)
	{
		___onError_1 = value;
		Il2CppCodeGenWriteBarrier((&___onError_1), value);
	}

	inline static int32_t get_offset_of_onCompleted_2() { return static_cast<int32_t>(offsetof(Subscribe_1_t2201750821, ___onCompleted_2)); }
	inline Action_t1264377477 * get_onCompleted_2() const { return ___onCompleted_2; }
	inline Action_t1264377477 ** get_address_of_onCompleted_2() { return &___onCompleted_2; }
	inline void set_onCompleted_2(Action_t1264377477 * value)
	{
		___onCompleted_2 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_2), value);
	}

	inline static int32_t get_offset_of_isStopped_3() { return static_cast<int32_t>(offsetof(Subscribe_1_t2201750821, ___isStopped_3)); }
	inline int32_t get_isStopped_3() const { return ___isStopped_3; }
	inline int32_t* get_address_of_isStopped_3() { return &___isStopped_3; }
	inline void set_isStopped_3(int32_t value)
	{
		___isStopped_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_1_T2201750821_H
#ifndef SUBSCRIBE__1_T1548720210_H
#define SUBSCRIBE__1_T1548720210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe_`1<UniRx.CollectionAddEvent`1<System.Object>>
struct  Subscribe__1_t1548720210  : public RuntimeObject
{
public:
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe_`1::onError
	Action_1_t1609204844 * ___onError_0;
	// System.Action UniRx.Observer/Subscribe_`1::onCompleted
	Action_t1264377477 * ___onCompleted_1;
	// System.Int32 UniRx.Observer/Subscribe_`1::isStopped
	int32_t ___isStopped_2;

public:
	inline static int32_t get_offset_of_onError_0() { return static_cast<int32_t>(offsetof(Subscribe__1_t1548720210, ___onError_0)); }
	inline Action_1_t1609204844 * get_onError_0() const { return ___onError_0; }
	inline Action_1_t1609204844 ** get_address_of_onError_0() { return &___onError_0; }
	inline void set_onError_0(Action_1_t1609204844 * value)
	{
		___onError_0 = value;
		Il2CppCodeGenWriteBarrier((&___onError_0), value);
	}

	inline static int32_t get_offset_of_onCompleted_1() { return static_cast<int32_t>(offsetof(Subscribe__1_t1548720210, ___onCompleted_1)); }
	inline Action_t1264377477 * get_onCompleted_1() const { return ___onCompleted_1; }
	inline Action_t1264377477 ** get_address_of_onCompleted_1() { return &___onCompleted_1; }
	inline void set_onCompleted_1(Action_t1264377477 * value)
	{
		___onCompleted_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_1), value);
	}

	inline static int32_t get_offset_of_isStopped_2() { return static_cast<int32_t>(offsetof(Subscribe__1_t1548720210, ___isStopped_2)); }
	inline int32_t get_isStopped_2() const { return ___isStopped_2; }
	inline int32_t* get_address_of_isStopped_2() { return &___isStopped_2; }
	inline void set_isStopped_2(int32_t value)
	{
		___isStopped_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE__1_T1548720210_H
#ifndef STUBS_1_T692617177_H
#define STUBS_1_T692617177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Stubs`1<UniRx.CollectionAddEvent`1<System.Object>>
struct  Stubs_1_t692617177  : public RuntimeObject
{
public:

public:
};

struct Stubs_1_t692617177_StaticFields
{
public:
	// System.Action`1<T> UniRx.Stubs`1::Ignore
	Action_1_t1860522164 * ___Ignore_0;
	// System.Func`2<T,T> UniRx.Stubs`1::Identity
	Func_2_t278627730 * ___Identity_1;
	// System.Action`2<System.Exception,T> UniRx.Stubs`1::Throw
	Action_2_t769019130 * ___Throw_2;

public:
	inline static int32_t get_offset_of_Ignore_0() { return static_cast<int32_t>(offsetof(Stubs_1_t692617177_StaticFields, ___Ignore_0)); }
	inline Action_1_t1860522164 * get_Ignore_0() const { return ___Ignore_0; }
	inline Action_1_t1860522164 ** get_address_of_Ignore_0() { return &___Ignore_0; }
	inline void set_Ignore_0(Action_1_t1860522164 * value)
	{
		___Ignore_0 = value;
		Il2CppCodeGenWriteBarrier((&___Ignore_0), value);
	}

	inline static int32_t get_offset_of_Identity_1() { return static_cast<int32_t>(offsetof(Stubs_1_t692617177_StaticFields, ___Identity_1)); }
	inline Func_2_t278627730 * get_Identity_1() const { return ___Identity_1; }
	inline Func_2_t278627730 ** get_address_of_Identity_1() { return &___Identity_1; }
	inline void set_Identity_1(Func_2_t278627730 * value)
	{
		___Identity_1 = value;
		Il2CppCodeGenWriteBarrier((&___Identity_1), value);
	}

	inline static int32_t get_offset_of_Throw_2() { return static_cast<int32_t>(offsetof(Stubs_1_t692617177_StaticFields, ___Throw_2)); }
	inline Action_2_t769019130 * get_Throw_2() const { return ___Throw_2; }
	inline Action_2_t769019130 ** get_address_of_Throw_2() { return &___Throw_2; }
	inline void set_Throw_2(Action_2_t769019130 * value)
	{
		___Throw_2 = value;
		Il2CppCodeGenWriteBarrier((&___Throw_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STUBS_1_T692617177_H
#ifndef SUBSCRIBE_1_T1910963026_H
#define SUBSCRIBE_1_T1910963026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`1<System.Single>
struct  Subscribe_1_t1910963026  : public RuntimeObject
{
public:
	// System.Action`1<T> UniRx.Observer/Subscribe`1::onNext
	Action_1_t1569734369 * ___onNext_0;
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe`1::onError
	Action_1_t1609204844 * ___onError_1;
	// System.Action UniRx.Observer/Subscribe`1::onCompleted
	Action_t1264377477 * ___onCompleted_2;
	// System.Int32 UniRx.Observer/Subscribe`1::isStopped
	int32_t ___isStopped_3;

public:
	inline static int32_t get_offset_of_onNext_0() { return static_cast<int32_t>(offsetof(Subscribe_1_t1910963026, ___onNext_0)); }
	inline Action_1_t1569734369 * get_onNext_0() const { return ___onNext_0; }
	inline Action_1_t1569734369 ** get_address_of_onNext_0() { return &___onNext_0; }
	inline void set_onNext_0(Action_1_t1569734369 * value)
	{
		___onNext_0 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_0), value);
	}

	inline static int32_t get_offset_of_onError_1() { return static_cast<int32_t>(offsetof(Subscribe_1_t1910963026, ___onError_1)); }
	inline Action_1_t1609204844 * get_onError_1() const { return ___onError_1; }
	inline Action_1_t1609204844 ** get_address_of_onError_1() { return &___onError_1; }
	inline void set_onError_1(Action_1_t1609204844 * value)
	{
		___onError_1 = value;
		Il2CppCodeGenWriteBarrier((&___onError_1), value);
	}

	inline static int32_t get_offset_of_onCompleted_2() { return static_cast<int32_t>(offsetof(Subscribe_1_t1910963026, ___onCompleted_2)); }
	inline Action_t1264377477 * get_onCompleted_2() const { return ___onCompleted_2; }
	inline Action_t1264377477 ** get_address_of_onCompleted_2() { return &___onCompleted_2; }
	inline void set_onCompleted_2(Action_t1264377477 * value)
	{
		___onCompleted_2 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_2), value);
	}

	inline static int32_t get_offset_of_isStopped_3() { return static_cast<int32_t>(offsetof(Subscribe_1_t1910963026, ___isStopped_3)); }
	inline int32_t get_isStopped_3() const { return ___isStopped_3; }
	inline int32_t* get_address_of_isStopped_3() { return &___isStopped_3; }
	inline void set_isStopped_3(int32_t value)
	{
		___isStopped_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_1_T1910963026_H
#ifndef SUBSCRIBE__1_T1257932415_H
#define SUBSCRIBE__1_T1257932415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe_`1<System.Single>
struct  Subscribe__1_t1257932415  : public RuntimeObject
{
public:
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe_`1::onError
	Action_1_t1609204844 * ___onError_0;
	// System.Action UniRx.Observer/Subscribe_`1::onCompleted
	Action_t1264377477 * ___onCompleted_1;
	// System.Int32 UniRx.Observer/Subscribe_`1::isStopped
	int32_t ___isStopped_2;

public:
	inline static int32_t get_offset_of_onError_0() { return static_cast<int32_t>(offsetof(Subscribe__1_t1257932415, ___onError_0)); }
	inline Action_1_t1609204844 * get_onError_0() const { return ___onError_0; }
	inline Action_1_t1609204844 ** get_address_of_onError_0() { return &___onError_0; }
	inline void set_onError_0(Action_1_t1609204844 * value)
	{
		___onError_0 = value;
		Il2CppCodeGenWriteBarrier((&___onError_0), value);
	}

	inline static int32_t get_offset_of_onCompleted_1() { return static_cast<int32_t>(offsetof(Subscribe__1_t1257932415, ___onCompleted_1)); }
	inline Action_t1264377477 * get_onCompleted_1() const { return ___onCompleted_1; }
	inline Action_t1264377477 ** get_address_of_onCompleted_1() { return &___onCompleted_1; }
	inline void set_onCompleted_1(Action_t1264377477 * value)
	{
		___onCompleted_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_1), value);
	}

	inline static int32_t get_offset_of_isStopped_2() { return static_cast<int32_t>(offsetof(Subscribe__1_t1257932415, ___isStopped_2)); }
	inline int32_t get_isStopped_2() const { return ___isStopped_2; }
	inline int32_t* get_address_of_isStopped_2() { return &___isStopped_2; }
	inline void set_isStopped_2(int32_t value)
	{
		___isStopped_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE__1_T1257932415_H
#ifndef STUBS_1_T401829382_H
#define STUBS_1_T401829382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Stubs`1<System.Single>
struct  Stubs_1_t401829382  : public RuntimeObject
{
public:

public:
};

struct Stubs_1_t401829382_StaticFields
{
public:
	// System.Action`1<T> UniRx.Stubs`1::Ignore
	Action_1_t1569734369 * ___Ignore_0;
	// System.Func`2<T,T> UniRx.Stubs`1::Identity
	Func_2_t3660230206 * ___Identity_1;
	// System.Action`2<System.Exception,T> UniRx.Stubs`1::Throw
	Action_2_t478231335 * ___Throw_2;

public:
	inline static int32_t get_offset_of_Ignore_0() { return static_cast<int32_t>(offsetof(Stubs_1_t401829382_StaticFields, ___Ignore_0)); }
	inline Action_1_t1569734369 * get_Ignore_0() const { return ___Ignore_0; }
	inline Action_1_t1569734369 ** get_address_of_Ignore_0() { return &___Ignore_0; }
	inline void set_Ignore_0(Action_1_t1569734369 * value)
	{
		___Ignore_0 = value;
		Il2CppCodeGenWriteBarrier((&___Ignore_0), value);
	}

	inline static int32_t get_offset_of_Identity_1() { return static_cast<int32_t>(offsetof(Stubs_1_t401829382_StaticFields, ___Identity_1)); }
	inline Func_2_t3660230206 * get_Identity_1() const { return ___Identity_1; }
	inline Func_2_t3660230206 ** get_address_of_Identity_1() { return &___Identity_1; }
	inline void set_Identity_1(Func_2_t3660230206 * value)
	{
		___Identity_1 = value;
		Il2CppCodeGenWriteBarrier((&___Identity_1), value);
	}

	inline static int32_t get_offset_of_Throw_2() { return static_cast<int32_t>(offsetof(Stubs_1_t401829382_StaticFields, ___Throw_2)); }
	inline Action_2_t478231335 * get_Throw_2() const { return ___Throw_2; }
	inline Action_2_t478231335 ** get_address_of_Throw_2() { return &___Throw_2; }
	inline void set_Throw_2(Action_2_t478231335 * value)
	{
		___Throw_2 = value;
		Il2CppCodeGenWriteBarrier((&___Throw_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STUBS_1_T401829382_H
#ifndef STUBS_1_T1955508361_H
#define STUBS_1_T1955508361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Stubs`1<System.Int32>
struct  Stubs_1_t1955508361  : public RuntimeObject
{
public:

public:
};

struct Stubs_1_t1955508361_StaticFields
{
public:
	// System.Action`1<T> UniRx.Stubs`1::Ignore
	Action_1_t3123413348 * ___Ignore_0;
	// System.Func`2<T,T> UniRx.Stubs`1::Identity
	Func_2_t4154244306 * ___Identity_1;
	// System.Action`2<System.Exception,T> UniRx.Stubs`1::Throw
	Action_2_t2031910314 * ___Throw_2;

public:
	inline static int32_t get_offset_of_Ignore_0() { return static_cast<int32_t>(offsetof(Stubs_1_t1955508361_StaticFields, ___Ignore_0)); }
	inline Action_1_t3123413348 * get_Ignore_0() const { return ___Ignore_0; }
	inline Action_1_t3123413348 ** get_address_of_Ignore_0() { return &___Ignore_0; }
	inline void set_Ignore_0(Action_1_t3123413348 * value)
	{
		___Ignore_0 = value;
		Il2CppCodeGenWriteBarrier((&___Ignore_0), value);
	}

	inline static int32_t get_offset_of_Identity_1() { return static_cast<int32_t>(offsetof(Stubs_1_t1955508361_StaticFields, ___Identity_1)); }
	inline Func_2_t4154244306 * get_Identity_1() const { return ___Identity_1; }
	inline Func_2_t4154244306 ** get_address_of_Identity_1() { return &___Identity_1; }
	inline void set_Identity_1(Func_2_t4154244306 * value)
	{
		___Identity_1 = value;
		Il2CppCodeGenWriteBarrier((&___Identity_1), value);
	}

	inline static int32_t get_offset_of_Throw_2() { return static_cast<int32_t>(offsetof(Stubs_1_t1955508361_StaticFields, ___Throw_2)); }
	inline Action_2_t2031910314 * get_Throw_2() const { return ___Throw_2; }
	inline Action_2_t2031910314 ** get_address_of_Throw_2() { return &___Throw_2; }
	inline void set_Throw_2(Action_2_t2031910314 * value)
	{
		___Throw_2 = value;
		Il2CppCodeGenWriteBarrier((&___Throw_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STUBS_1_T1955508361_H
#ifndef OBSERVER_T4122299270_H
#define OBSERVER_T4122299270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer
struct  Observer_t4122299270  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVER_T4122299270_H
#ifndef U3CSELECTMANYU3EC__ANONSTOREY26_1_T1609749638_H
#define U3CSELECTMANYU3EC__ANONSTOREY26_1_T1609749638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<SelectMany>c__AnonStorey26`1<UniRx.Unit>
struct  U3CSelectManyU3Ec__AnonStorey26_1_t1609749638  : public RuntimeObject
{
public:
	// System.Func`1<System.Collections.IEnumerator> UniRx.Observable/<SelectMany>c__AnonStorey26`1::selector
	Func_1_t1283030885 * ___selector_0;

public:
	inline static int32_t get_offset_of_selector_0() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey26_1_t1609749638, ___selector_0)); }
	inline Func_1_t1283030885 * get_selector_0() const { return ___selector_0; }
	inline Func_1_t1283030885 ** get_address_of_selector_0() { return &___selector_0; }
	inline void set_selector_0(Func_1_t1283030885 * value)
	{
		___selector_0 = value;
		Il2CppCodeGenWriteBarrier((&___selector_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSELECTMANYU3EC__ANONSTOREY26_1_T1609749638_H
#ifndef U3CSELECTMANYU3EC__ANONSTOREY27_1_T3896420927_H
#define U3CSELECTMANYU3EC__ANONSTOREY27_1_T3896420927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<SelectMany>c__AnonStorey27`1<System.Object>
struct  U3CSelectManyU3Ec__AnonStorey27_1_t3896420927  : public RuntimeObject
{
public:
	// System.Func`2<T,System.Collections.IEnumerator> UniRx.Observable/<SelectMany>c__AnonStorey27`1::selector
	Func_2_t1220308448 * ___selector_0;

public:
	inline static int32_t get_offset_of_selector_0() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey27_1_t3896420927, ___selector_0)); }
	inline Func_2_t1220308448 * get_selector_0() const { return ___selector_0; }
	inline Func_2_t1220308448 ** get_address_of_selector_0() { return &___selector_0; }
	inline void set_selector_0(Func_2_t1220308448 * value)
	{
		___selector_0 = value;
		Il2CppCodeGenWriteBarrier((&___selector_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSELECTMANYU3EC__ANONSTOREY27_1_T3896420927_H
#ifndef U3CASOBSERVABLEU3EC__ANONSTOREY6_4_T2752624349_H
#define U3CASOBSERVABLEU3EC__ANONSTOREY6_4_T2752624349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey6`4<System.Object,System.Object,System.Object,System.Object>
struct  U3CAsObservableU3Ec__AnonStorey6_4_t2752624349  : public RuntimeObject
{
public:
	// System.Action`1<UniRx.Tuple`4<T0,T1,T2,T3>> UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey6`4::h
	Action_1_t2821607406 * ___h_0;

public:
	inline static int32_t get_offset_of_h_0() { return static_cast<int32_t>(offsetof(U3CAsObservableU3Ec__AnonStorey6_4_t2752624349, ___h_0)); }
	inline Action_1_t2821607406 * get_h_0() const { return ___h_0; }
	inline Action_1_t2821607406 ** get_address_of_h_0() { return &___h_0; }
	inline void set_h_0(Action_1_t2821607406 * value)
	{
		___h_0 = value;
		Il2CppCodeGenWriteBarrier((&___h_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CASOBSERVABLEU3EC__ANONSTOREY6_4_T2752624349_H
#ifndef U3CSELECTMANYU3EC__ANONSTOREY26_1_T1327606335_H
#define U3CSELECTMANYU3EC__ANONSTOREY26_1_T1327606335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<SelectMany>c__AnonStorey26`1<System.Object>
struct  U3CSelectManyU3Ec__AnonStorey26_1_t1327606335  : public RuntimeObject
{
public:
	// System.Func`1<System.Collections.IEnumerator> UniRx.Observable/<SelectMany>c__AnonStorey26`1::selector
	Func_1_t1283030885 * ___selector_0;

public:
	inline static int32_t get_offset_of_selector_0() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey26_1_t1327606335, ___selector_0)); }
	inline Func_1_t1283030885 * get_selector_0() const { return ___selector_0; }
	inline Func_1_t1283030885 ** get_address_of_selector_0() { return &___selector_0; }
	inline void set_selector_0(Func_1_t1283030885 * value)
	{
		___selector_0 = value;
		Il2CppCodeGenWriteBarrier((&___selector_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSELECTMANYU3EC__ANONSTOREY26_1_T1327606335_H
#ifndef GAMEOBJECTEXTENSIONS_T3873029940_H
#define GAMEOBJECTEXTENSIONS_T3873029940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions
struct  GameObjectExtensions_t3873029940  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTEXTENSIONS_T3873029940_H
#ifndef U3CSELECTMANYU3EC__ANONSTOREY25_1_T3053759039_H
#define U3CSELECTMANYU3EC__ANONSTOREY25_1_T3053759039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<SelectMany>c__AnonStorey25`1<System.Object>
struct  U3CSelectManyU3Ec__AnonStorey25_1_t3053759039  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator UniRx.Observable/<SelectMany>c__AnonStorey25`1::coroutine
	RuntimeObject* ___coroutine_0;

public:
	inline static int32_t get_offset_of_coroutine_0() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey25_1_t3053759039, ___coroutine_0)); }
	inline RuntimeObject* get_coroutine_0() const { return ___coroutine_0; }
	inline RuntimeObject** get_address_of_coroutine_0() { return &___coroutine_0; }
	inline void set_coroutine_0(RuntimeObject* value)
	{
		___coroutine_0 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSELECTMANYU3EC__ANONSTOREY25_1_T3053759039_H
#ifndef U3CASOBSERVABLEU3EC__ANONSTOREY4_3_T3097154774_H
#define U3CASOBSERVABLEU3EC__ANONSTOREY4_3_T3097154774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey4`3<System.Object,System.Object,System.Object>
struct  U3CAsObservableU3Ec__AnonStorey4_3_t3097154774  : public RuntimeObject
{
public:
	// System.Action`1<UniRx.Tuple`3<T0,T1,T2>> UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey4`3::h
	Action_1_t3401448761 * ___h_0;

public:
	inline static int32_t get_offset_of_h_0() { return static_cast<int32_t>(offsetof(U3CAsObservableU3Ec__AnonStorey4_3_t3097154774, ___h_0)); }
	inline Action_1_t3401448761 * get_h_0() const { return ___h_0; }
	inline Action_1_t3401448761 ** get_address_of_h_0() { return &___h_0; }
	inline void set_h_0(Action_1_t3401448761 * value)
	{
		___h_0 = value;
		Il2CppCodeGenWriteBarrier((&___h_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CASOBSERVABLEU3EC__ANONSTOREY4_3_T3097154774_H
#ifndef SUBSCRIBE_1_T610984217_H
#define SUBSCRIBE_1_T610984217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`1<System.Boolean>
struct  Subscribe_1_t610984217  : public RuntimeObject
{
public:
	// System.Action`1<T> UniRx.Observer/Subscribe`1::onNext
	Action_1_t269755560 * ___onNext_0;
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe`1::onError
	Action_1_t1609204844 * ___onError_1;
	// System.Action UniRx.Observer/Subscribe`1::onCompleted
	Action_t1264377477 * ___onCompleted_2;
	// System.Int32 UniRx.Observer/Subscribe`1::isStopped
	int32_t ___isStopped_3;

public:
	inline static int32_t get_offset_of_onNext_0() { return static_cast<int32_t>(offsetof(Subscribe_1_t610984217, ___onNext_0)); }
	inline Action_1_t269755560 * get_onNext_0() const { return ___onNext_0; }
	inline Action_1_t269755560 ** get_address_of_onNext_0() { return &___onNext_0; }
	inline void set_onNext_0(Action_1_t269755560 * value)
	{
		___onNext_0 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_0), value);
	}

	inline static int32_t get_offset_of_onError_1() { return static_cast<int32_t>(offsetof(Subscribe_1_t610984217, ___onError_1)); }
	inline Action_1_t1609204844 * get_onError_1() const { return ___onError_1; }
	inline Action_1_t1609204844 ** get_address_of_onError_1() { return &___onError_1; }
	inline void set_onError_1(Action_1_t1609204844 * value)
	{
		___onError_1 = value;
		Il2CppCodeGenWriteBarrier((&___onError_1), value);
	}

	inline static int32_t get_offset_of_onCompleted_2() { return static_cast<int32_t>(offsetof(Subscribe_1_t610984217, ___onCompleted_2)); }
	inline Action_t1264377477 * get_onCompleted_2() const { return ___onCompleted_2; }
	inline Action_t1264377477 ** get_address_of_onCompleted_2() { return &___onCompleted_2; }
	inline void set_onCompleted_2(Action_t1264377477 * value)
	{
		___onCompleted_2 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_2), value);
	}

	inline static int32_t get_offset_of_isStopped_3() { return static_cast<int32_t>(offsetof(Subscribe_1_t610984217, ___isStopped_3)); }
	inline int32_t get_isStopped_3() const { return ___isStopped_3; }
	inline int32_t* get_address_of_isStopped_3() { return &___isStopped_3; }
	inline void set_isStopped_3(int32_t value)
	{
		___isStopped_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_1_T610984217_H
#ifndef SUBSCRIBE__1_T4252920902_H
#define SUBSCRIBE__1_T4252920902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe_`1<System.Boolean>
struct  Subscribe__1_t4252920902  : public RuntimeObject
{
public:
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe_`1::onError
	Action_1_t1609204844 * ___onError_0;
	// System.Action UniRx.Observer/Subscribe_`1::onCompleted
	Action_t1264377477 * ___onCompleted_1;
	// System.Int32 UniRx.Observer/Subscribe_`1::isStopped
	int32_t ___isStopped_2;

public:
	inline static int32_t get_offset_of_onError_0() { return static_cast<int32_t>(offsetof(Subscribe__1_t4252920902, ___onError_0)); }
	inline Action_1_t1609204844 * get_onError_0() const { return ___onError_0; }
	inline Action_1_t1609204844 ** get_address_of_onError_0() { return &___onError_0; }
	inline void set_onError_0(Action_1_t1609204844 * value)
	{
		___onError_0 = value;
		Il2CppCodeGenWriteBarrier((&___onError_0), value);
	}

	inline static int32_t get_offset_of_onCompleted_1() { return static_cast<int32_t>(offsetof(Subscribe__1_t4252920902, ___onCompleted_1)); }
	inline Action_t1264377477 * get_onCompleted_1() const { return ___onCompleted_1; }
	inline Action_t1264377477 ** get_address_of_onCompleted_1() { return &___onCompleted_1; }
	inline void set_onCompleted_1(Action_t1264377477 * value)
	{
		___onCompleted_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_1), value);
	}

	inline static int32_t get_offset_of_isStopped_2() { return static_cast<int32_t>(offsetof(Subscribe__1_t4252920902, ___isStopped_2)); }
	inline int32_t get_isStopped_2() const { return ___isStopped_2; }
	inline int32_t* get_address_of_isStopped_2() { return &___isStopped_2; }
	inline void set_isStopped_2(int32_t value)
	{
		___isStopped_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE__1_T4252920902_H
#ifndef STUBS_1_T3396817869_H
#define STUBS_1_T3396817869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Stubs`1<System.Boolean>
struct  Stubs_1_t3396817869  : public RuntimeObject
{
public:

public:
};

struct Stubs_1_t3396817869_StaticFields
{
public:
	// System.Action`1<T> UniRx.Stubs`1::Ignore
	Action_1_t269755560 * ___Ignore_0;
	// System.Func`2<T,T> UniRx.Stubs`1::Identity
	Func_2_t3812758338 * ___Identity_1;
	// System.Action`2<System.Exception,T> UniRx.Stubs`1::Throw
	Action_2_t3473219822 * ___Throw_2;

public:
	inline static int32_t get_offset_of_Ignore_0() { return static_cast<int32_t>(offsetof(Stubs_1_t3396817869_StaticFields, ___Ignore_0)); }
	inline Action_1_t269755560 * get_Ignore_0() const { return ___Ignore_0; }
	inline Action_1_t269755560 ** get_address_of_Ignore_0() { return &___Ignore_0; }
	inline void set_Ignore_0(Action_1_t269755560 * value)
	{
		___Ignore_0 = value;
		Il2CppCodeGenWriteBarrier((&___Ignore_0), value);
	}

	inline static int32_t get_offset_of_Identity_1() { return static_cast<int32_t>(offsetof(Stubs_1_t3396817869_StaticFields, ___Identity_1)); }
	inline Func_2_t3812758338 * get_Identity_1() const { return ___Identity_1; }
	inline Func_2_t3812758338 ** get_address_of_Identity_1() { return &___Identity_1; }
	inline void set_Identity_1(Func_2_t3812758338 * value)
	{
		___Identity_1 = value;
		Il2CppCodeGenWriteBarrier((&___Identity_1), value);
	}

	inline static int32_t get_offset_of_Throw_2() { return static_cast<int32_t>(offsetof(Stubs_1_t3396817869_StaticFields, ___Throw_2)); }
	inline Action_2_t3473219822 * get_Throw_2() const { return ___Throw_2; }
	inline Action_2_t3473219822 ** get_address_of_Throw_2() { return &___Throw_2; }
	inline void set_Throw_2(Action_2_t3473219822 * value)
	{
		___Throw_2 = value;
		Il2CppCodeGenWriteBarrier((&___Throw_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STUBS_1_T3396817869_H
#ifndef REACTIVEPROPERTYEXTENSIONS_T2855915014_H
#define REACTIVEPROPERTYEXTENSIONS_T2855915014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactivePropertyExtensions
struct  ReactivePropertyExtensions_t2855915014  : public RuntimeObject
{
public:

public:
};

struct ReactivePropertyExtensions_t2855915014_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.IList`1<System.Boolean>,System.Boolean> UniRx.ReactivePropertyExtensions::<>f__am$cache0
	Func_2_t3308430047 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<System.Collections.Generic.IList`1<System.Boolean>,System.Boolean> UniRx.ReactivePropertyExtensions::<>f__am$cache1
	Func_2_t3308430047 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(ReactivePropertyExtensions_t2855915014_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t3308430047 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t3308430047 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t3308430047 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(ReactivePropertyExtensions_t2855915014_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t3308430047 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t3308430047 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t3308430047 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTYEXTENSIONS_T2855915014_H
#ifndef ANONYMOUSOBSERVER_1_T66846250_H
#define ANONYMOUSOBSERVER_1_T66846250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/AnonymousObserver`1<System.Object>
struct  AnonymousObserver_1_t66846250  : public RuntimeObject
{
public:
	// System.Action`1<T> UniRx.Observer/AnonymousObserver`1::onNext
	Action_1_t3252573759 * ___onNext_0;
	// System.Action`1<System.Exception> UniRx.Observer/AnonymousObserver`1::onError
	Action_1_t1609204844 * ___onError_1;
	// System.Action UniRx.Observer/AnonymousObserver`1::onCompleted
	Action_t1264377477 * ___onCompleted_2;
	// System.Int32 UniRx.Observer/AnonymousObserver`1::isStopped
	int32_t ___isStopped_3;

public:
	inline static int32_t get_offset_of_onNext_0() { return static_cast<int32_t>(offsetof(AnonymousObserver_1_t66846250, ___onNext_0)); }
	inline Action_1_t3252573759 * get_onNext_0() const { return ___onNext_0; }
	inline Action_1_t3252573759 ** get_address_of_onNext_0() { return &___onNext_0; }
	inline void set_onNext_0(Action_1_t3252573759 * value)
	{
		___onNext_0 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_0), value);
	}

	inline static int32_t get_offset_of_onError_1() { return static_cast<int32_t>(offsetof(AnonymousObserver_1_t66846250, ___onError_1)); }
	inline Action_1_t1609204844 * get_onError_1() const { return ___onError_1; }
	inline Action_1_t1609204844 ** get_address_of_onError_1() { return &___onError_1; }
	inline void set_onError_1(Action_1_t1609204844 * value)
	{
		___onError_1 = value;
		Il2CppCodeGenWriteBarrier((&___onError_1), value);
	}

	inline static int32_t get_offset_of_onCompleted_2() { return static_cast<int32_t>(offsetof(AnonymousObserver_1_t66846250, ___onCompleted_2)); }
	inline Action_t1264377477 * get_onCompleted_2() const { return ___onCompleted_2; }
	inline Action_t1264377477 ** get_address_of_onCompleted_2() { return &___onCompleted_2; }
	inline void set_onCompleted_2(Action_t1264377477 * value)
	{
		___onCompleted_2 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_2), value);
	}

	inline static int32_t get_offset_of_isStopped_3() { return static_cast<int32_t>(offsetof(AnonymousObserver_1_t66846250, ___isStopped_3)); }
	inline int32_t get_isStopped_3() const { return ___isStopped_3; }
	inline int32_t* get_address_of_isStopped_3() { return &___isStopped_3; }
	inline void set_isStopped_3(int32_t value)
	{
		___isStopped_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANONYMOUSOBSERVER_1_T66846250_H
#ifndef EMPTYONNEXTANONYMOUSOBSERVER_1_T2179921185_H
#define EMPTYONNEXTANONYMOUSOBSERVER_1_T2179921185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/EmptyOnNextAnonymousObserver`1<System.Object>
struct  EmptyOnNextAnonymousObserver_1_t2179921185  : public RuntimeObject
{
public:
	// System.Action`1<System.Exception> UniRx.Observer/EmptyOnNextAnonymousObserver`1::onError
	Action_1_t1609204844 * ___onError_0;
	// System.Action UniRx.Observer/EmptyOnNextAnonymousObserver`1::onCompleted
	Action_t1264377477 * ___onCompleted_1;
	// System.Int32 UniRx.Observer/EmptyOnNextAnonymousObserver`1::isStopped
	int32_t ___isStopped_2;

public:
	inline static int32_t get_offset_of_onError_0() { return static_cast<int32_t>(offsetof(EmptyOnNextAnonymousObserver_1_t2179921185, ___onError_0)); }
	inline Action_1_t1609204844 * get_onError_0() const { return ___onError_0; }
	inline Action_1_t1609204844 ** get_address_of_onError_0() { return &___onError_0; }
	inline void set_onError_0(Action_1_t1609204844 * value)
	{
		___onError_0 = value;
		Il2CppCodeGenWriteBarrier((&___onError_0), value);
	}

	inline static int32_t get_offset_of_onCompleted_1() { return static_cast<int32_t>(offsetof(EmptyOnNextAnonymousObserver_1_t2179921185, ___onCompleted_1)); }
	inline Action_t1264377477 * get_onCompleted_1() const { return ___onCompleted_1; }
	inline Action_t1264377477 ** get_address_of_onCompleted_1() { return &___onCompleted_1; }
	inline void set_onCompleted_1(Action_t1264377477 * value)
	{
		___onCompleted_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_1), value);
	}

	inline static int32_t get_offset_of_isStopped_2() { return static_cast<int32_t>(offsetof(EmptyOnNextAnonymousObserver_1_t2179921185, ___isStopped_2)); }
	inline int32_t get_isStopped_2() const { return ___isStopped_2; }
	inline int32_t* get_address_of_isStopped_2() { return &___isStopped_2; }
	inline void set_isStopped_2(int32_t value)
	{
		___isStopped_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYONNEXTANONYMOUSOBSERVER_1_T2179921185_H
#ifndef U3CASOBSERVABLEU3EC__ANONSTOREY2_2_T1336485635_H
#define U3CASOBSERVABLEU3EC__ANONSTOREY2_2_T1336485635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey2`2<System.Object,System.Object>
struct  U3CAsObservableU3Ec__AnonStorey2_2_t1336485635  : public RuntimeObject
{
public:
	// System.Action`1<UniRx.Tuple`2<T0,T1>> UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey2`2::h
	Action_1_t522702268 * ___h_0;

public:
	inline static int32_t get_offset_of_h_0() { return static_cast<int32_t>(offsetof(U3CAsObservableU3Ec__AnonStorey2_2_t1336485635, ___h_0)); }
	inline Action_1_t522702268 * get_h_0() const { return ___h_0; }
	inline Action_1_t522702268 ** get_address_of_h_0() { return &___h_0; }
	inline void set_h_0(Action_1_t522702268 * value)
	{
		___h_0 = value;
		Il2CppCodeGenWriteBarrier((&___h_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CASOBSERVABLEU3EC__ANONSTOREY2_2_T1336485635_H
#ifndef SUBSCRIBE_1_T2500637912_H
#define SUBSCRIBE_1_T2500637912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct  Subscribe_1_t2500637912  : public RuntimeObject
{
public:
	// System.Action`1<T> UniRx.Observer/Subscribe`1::onNext
	Action_1_t2159409255 * ___onNext_0;
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe`1::onError
	Action_1_t1609204844 * ___onError_1;
	// System.Action UniRx.Observer/Subscribe`1::onCompleted
	Action_t1264377477 * ___onCompleted_2;
	// System.Int32 UniRx.Observer/Subscribe`1::isStopped
	int32_t ___isStopped_3;

public:
	inline static int32_t get_offset_of_onNext_0() { return static_cast<int32_t>(offsetof(Subscribe_1_t2500637912, ___onNext_0)); }
	inline Action_1_t2159409255 * get_onNext_0() const { return ___onNext_0; }
	inline Action_1_t2159409255 ** get_address_of_onNext_0() { return &___onNext_0; }
	inline void set_onNext_0(Action_1_t2159409255 * value)
	{
		___onNext_0 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_0), value);
	}

	inline static int32_t get_offset_of_onError_1() { return static_cast<int32_t>(offsetof(Subscribe_1_t2500637912, ___onError_1)); }
	inline Action_1_t1609204844 * get_onError_1() const { return ___onError_1; }
	inline Action_1_t1609204844 ** get_address_of_onError_1() { return &___onError_1; }
	inline void set_onError_1(Action_1_t1609204844 * value)
	{
		___onError_1 = value;
		Il2CppCodeGenWriteBarrier((&___onError_1), value);
	}

	inline static int32_t get_offset_of_onCompleted_2() { return static_cast<int32_t>(offsetof(Subscribe_1_t2500637912, ___onCompleted_2)); }
	inline Action_t1264377477 * get_onCompleted_2() const { return ___onCompleted_2; }
	inline Action_t1264377477 ** get_address_of_onCompleted_2() { return &___onCompleted_2; }
	inline void set_onCompleted_2(Action_t1264377477 * value)
	{
		___onCompleted_2 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_2), value);
	}

	inline static int32_t get_offset_of_isStopped_3() { return static_cast<int32_t>(offsetof(Subscribe_1_t2500637912, ___isStopped_3)); }
	inline int32_t get_isStopped_3() const { return ___isStopped_3; }
	inline int32_t* get_address_of_isStopped_3() { return &___isStopped_3; }
	inline void set_isStopped_3(int32_t value)
	{
		___isStopped_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_1_T2500637912_H
#ifndef READONLYREACTIVEPROPERTY_1_T3751549484_H
#define READONLYREACTIVEPROPERTY_1_T3751549484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReadOnlyReactiveProperty`1<System.Object>
struct  ReadOnlyReactiveProperty_1_t3751549484  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReadOnlyReactiveProperty`1::distinctUntilChanged
	bool ___distinctUntilChanged_1;
	// System.Boolean UniRx.ReadOnlyReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_2;
	// System.Boolean UniRx.ReadOnlyReactiveProperty`1::isDisposed
	bool ___isDisposed_3;
	// System.Exception UniRx.ReadOnlyReactiveProperty`1::lastException
	Exception_t * ___lastException_4;
	// T UniRx.ReadOnlyReactiveProperty`1::value
	RuntimeObject * ___value_5;
	// UniRx.Subject`1<T> UniRx.ReadOnlyReactiveProperty`1::publisher
	Subject_1_t3168762551 * ___publisher_6;
	// System.IDisposable UniRx.ReadOnlyReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_7;
	// System.Boolean UniRx.ReadOnlyReactiveProperty`1::isSourceCompleted
	bool ___isSourceCompleted_8;

public:
	inline static int32_t get_offset_of_distinctUntilChanged_1() { return static_cast<int32_t>(offsetof(ReadOnlyReactiveProperty_1_t3751549484, ___distinctUntilChanged_1)); }
	inline bool get_distinctUntilChanged_1() const { return ___distinctUntilChanged_1; }
	inline bool* get_address_of_distinctUntilChanged_1() { return &___distinctUntilChanged_1; }
	inline void set_distinctUntilChanged_1(bool value)
	{
		___distinctUntilChanged_1 = value;
	}

	inline static int32_t get_offset_of_canPublishValueOnSubscribe_2() { return static_cast<int32_t>(offsetof(ReadOnlyReactiveProperty_1_t3751549484, ___canPublishValueOnSubscribe_2)); }
	inline bool get_canPublishValueOnSubscribe_2() const { return ___canPublishValueOnSubscribe_2; }
	inline bool* get_address_of_canPublishValueOnSubscribe_2() { return &___canPublishValueOnSubscribe_2; }
	inline void set_canPublishValueOnSubscribe_2(bool value)
	{
		___canPublishValueOnSubscribe_2 = value;
	}

	inline static int32_t get_offset_of_isDisposed_3() { return static_cast<int32_t>(offsetof(ReadOnlyReactiveProperty_1_t3751549484, ___isDisposed_3)); }
	inline bool get_isDisposed_3() const { return ___isDisposed_3; }
	inline bool* get_address_of_isDisposed_3() { return &___isDisposed_3; }
	inline void set_isDisposed_3(bool value)
	{
		___isDisposed_3 = value;
	}

	inline static int32_t get_offset_of_lastException_4() { return static_cast<int32_t>(offsetof(ReadOnlyReactiveProperty_1_t3751549484, ___lastException_4)); }
	inline Exception_t * get_lastException_4() const { return ___lastException_4; }
	inline Exception_t ** get_address_of_lastException_4() { return &___lastException_4; }
	inline void set_lastException_4(Exception_t * value)
	{
		___lastException_4 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_4), value);
	}

	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(ReadOnlyReactiveProperty_1_t3751549484, ___value_5)); }
	inline RuntimeObject * get_value_5() const { return ___value_5; }
	inline RuntimeObject ** get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(RuntimeObject * value)
	{
		___value_5 = value;
		Il2CppCodeGenWriteBarrier((&___value_5), value);
	}

	inline static int32_t get_offset_of_publisher_6() { return static_cast<int32_t>(offsetof(ReadOnlyReactiveProperty_1_t3751549484, ___publisher_6)); }
	inline Subject_1_t3168762551 * get_publisher_6() const { return ___publisher_6; }
	inline Subject_1_t3168762551 ** get_address_of_publisher_6() { return &___publisher_6; }
	inline void set_publisher_6(Subject_1_t3168762551 * value)
	{
		___publisher_6 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_6), value);
	}

	inline static int32_t get_offset_of_sourceConnection_7() { return static_cast<int32_t>(offsetof(ReadOnlyReactiveProperty_1_t3751549484, ___sourceConnection_7)); }
	inline RuntimeObject* get_sourceConnection_7() const { return ___sourceConnection_7; }
	inline RuntimeObject** get_address_of_sourceConnection_7() { return &___sourceConnection_7; }
	inline void set_sourceConnection_7(RuntimeObject* value)
	{
		___sourceConnection_7 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_7), value);
	}

	inline static int32_t get_offset_of_isSourceCompleted_8() { return static_cast<int32_t>(offsetof(ReadOnlyReactiveProperty_1_t3751549484, ___isSourceCompleted_8)); }
	inline bool get_isSourceCompleted_8() const { return ___isSourceCompleted_8; }
	inline bool* get_address_of_isSourceCompleted_8() { return &___isSourceCompleted_8; }
	inline void set_isSourceCompleted_8(bool value)
	{
		___isSourceCompleted_8 = value;
	}
};

struct ReadOnlyReactiveProperty_1_t3751549484_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReadOnlyReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReadOnlyReactiveProperty_1_t3751549484_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYREACTIVEPROPERTY_1_T3751549484_H
#ifndef TIMESTAMPED_T555880859_H
#define TIMESTAMPED_T555880859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Timestamped
struct  Timestamped_t555880859  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPED_T555880859_H
#ifndef NOTIFICATION_1_T4190762752_H
#define NOTIFICATION_1_T4190762752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Notification`1<System.Object>
struct  Notification_1_t4190762752  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATION_1_T4190762752_H
#ifndef NOTIFICATION_T2788794668_H
#define NOTIFICATION_T2788794668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Notification
struct  Notification_t2788794668  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATION_T2788794668_H
#ifndef LAZYTASKEXTENSIONS_T3183993102_H
#define LAZYTASKEXTENSIONS_T3183993102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.LazyTaskExtensions
struct  LazyTaskExtensions_t3183993102  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAZYTASKEXTENSIONS_T3183993102_H
#ifndef TUPLE_T1757971014_H
#define TUPLE_T1757971014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple
struct  Tuple_t1757971014  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUPLE_T1757971014_H
#ifndef SUBJECTEXTENSIONS_T2134319452_H
#define SUBJECTEXTENSIONS_T2134319452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.SubjectExtensions
struct  SubjectExtensions_t2134319452  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBJECTEXTENSIONS_T2134319452_H
#ifndef REACTIVEPROPERTY_1_T2314831058_H
#define REACTIVEPROPERTY_1_T2314831058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<System.Object>
struct  ReactiveProperty_1_t2314831058  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	RuntimeObject * ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t3168762551 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2314831058, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2314831058, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2314831058, ___value_3)); }
	inline RuntimeObject * get_value_3() const { return ___value_3; }
	inline RuntimeObject ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(RuntimeObject * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2314831058, ___publisher_4)); }
	inline Subject_1_t3168762551 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t3168762551 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t3168762551 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2314831058, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2314831058, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t2314831058_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2314831058_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T2314831058_H
#ifndef DICTIONARY_2_T132545152_H
#define DICTIONARY_2_T132545152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct  Dictionary_2_t132545152  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	ObjectU5BU5D_t2843939325* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ObjectU5BU5D_t2843939325* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___keySlots_6)); }
	inline ObjectU5BU5D_t2843939325* get_keySlots_6() const { return ___keySlots_6; }
	inline ObjectU5BU5D_t2843939325** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(ObjectU5BU5D_t2843939325* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___valueSlots_7)); }
	inline ObjectU5BU5D_t2843939325* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ObjectU5BU5D_t2843939325** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ObjectU5BU5D_t2843939325* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t132545152_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t4209139644 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t132545152_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t4209139644 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t4209139644 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t4209139644 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T132545152_H
#ifndef REACTIVEDICTIONARY_2_T820484097_H
#define REACTIVEDICTIONARY_2_T820484097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveDictionary`2<System.Object,System.Object>
struct  ReactiveDictionary_2_t820484097  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveDictionary`2::isDisposed
	bool ___isDisposed_0;
	// System.Collections.Generic.Dictionary`2<TKey,TValue> UniRx.ReactiveDictionary`2::inner
	Dictionary_2_t132545152 * ___inner_1;
	// System.Boolean UniRx.ReactiveDictionary`2::disposedValue
	bool ___disposedValue_2;
	// UniRx.Subject`1<System.Int32> UniRx.ReactiveDictionary`2::countChanged
	Subject_1_t3039602140 * ___countChanged_3;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ReactiveDictionary`2::collectionReset
	Subject_1_t3450905854 * ___collectionReset_4;
	// UniRx.Subject`1<UniRx.DictionaryAddEvent`2<TKey,TValue>> UniRx.ReactiveDictionary`2::dictionaryAdd
	Subject_1_t2690504464 * ___dictionaryAdd_5;
	// UniRx.Subject`1<UniRx.DictionaryRemoveEvent`2<TKey,TValue>> UniRx.ReactiveDictionary`2::dictionaryRemove
	Subject_1_t1655735010 * ___dictionaryRemove_6;
	// UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<TKey,TValue>> UniRx.ReactiveDictionary`2::dictionaryReplace
	Subject_1_t2309927415 * ___dictionaryReplace_7;

public:
	inline static int32_t get_offset_of_isDisposed_0() { return static_cast<int32_t>(offsetof(ReactiveDictionary_2_t820484097, ___isDisposed_0)); }
	inline bool get_isDisposed_0() const { return ___isDisposed_0; }
	inline bool* get_address_of_isDisposed_0() { return &___isDisposed_0; }
	inline void set_isDisposed_0(bool value)
	{
		___isDisposed_0 = value;
	}

	inline static int32_t get_offset_of_inner_1() { return static_cast<int32_t>(offsetof(ReactiveDictionary_2_t820484097, ___inner_1)); }
	inline Dictionary_2_t132545152 * get_inner_1() const { return ___inner_1; }
	inline Dictionary_2_t132545152 ** get_address_of_inner_1() { return &___inner_1; }
	inline void set_inner_1(Dictionary_2_t132545152 * value)
	{
		___inner_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_1), value);
	}

	inline static int32_t get_offset_of_disposedValue_2() { return static_cast<int32_t>(offsetof(ReactiveDictionary_2_t820484097, ___disposedValue_2)); }
	inline bool get_disposedValue_2() const { return ___disposedValue_2; }
	inline bool* get_address_of_disposedValue_2() { return &___disposedValue_2; }
	inline void set_disposedValue_2(bool value)
	{
		___disposedValue_2 = value;
	}

	inline static int32_t get_offset_of_countChanged_3() { return static_cast<int32_t>(offsetof(ReactiveDictionary_2_t820484097, ___countChanged_3)); }
	inline Subject_1_t3039602140 * get_countChanged_3() const { return ___countChanged_3; }
	inline Subject_1_t3039602140 ** get_address_of_countChanged_3() { return &___countChanged_3; }
	inline void set_countChanged_3(Subject_1_t3039602140 * value)
	{
		___countChanged_3 = value;
		Il2CppCodeGenWriteBarrier((&___countChanged_3), value);
	}

	inline static int32_t get_offset_of_collectionReset_4() { return static_cast<int32_t>(offsetof(ReactiveDictionary_2_t820484097, ___collectionReset_4)); }
	inline Subject_1_t3450905854 * get_collectionReset_4() const { return ___collectionReset_4; }
	inline Subject_1_t3450905854 ** get_address_of_collectionReset_4() { return &___collectionReset_4; }
	inline void set_collectionReset_4(Subject_1_t3450905854 * value)
	{
		___collectionReset_4 = value;
		Il2CppCodeGenWriteBarrier((&___collectionReset_4), value);
	}

	inline static int32_t get_offset_of_dictionaryAdd_5() { return static_cast<int32_t>(offsetof(ReactiveDictionary_2_t820484097, ___dictionaryAdd_5)); }
	inline Subject_1_t2690504464 * get_dictionaryAdd_5() const { return ___dictionaryAdd_5; }
	inline Subject_1_t2690504464 ** get_address_of_dictionaryAdd_5() { return &___dictionaryAdd_5; }
	inline void set_dictionaryAdd_5(Subject_1_t2690504464 * value)
	{
		___dictionaryAdd_5 = value;
		Il2CppCodeGenWriteBarrier((&___dictionaryAdd_5), value);
	}

	inline static int32_t get_offset_of_dictionaryRemove_6() { return static_cast<int32_t>(offsetof(ReactiveDictionary_2_t820484097, ___dictionaryRemove_6)); }
	inline Subject_1_t1655735010 * get_dictionaryRemove_6() const { return ___dictionaryRemove_6; }
	inline Subject_1_t1655735010 ** get_address_of_dictionaryRemove_6() { return &___dictionaryRemove_6; }
	inline void set_dictionaryRemove_6(Subject_1_t1655735010 * value)
	{
		___dictionaryRemove_6 = value;
		Il2CppCodeGenWriteBarrier((&___dictionaryRemove_6), value);
	}

	inline static int32_t get_offset_of_dictionaryReplace_7() { return static_cast<int32_t>(offsetof(ReactiveDictionary_2_t820484097, ___dictionaryReplace_7)); }
	inline Subject_1_t2309927415 * get_dictionaryReplace_7() const { return ___dictionaryReplace_7; }
	inline Subject_1_t2309927415 ** get_address_of_dictionaryReplace_7() { return &___dictionaryReplace_7; }
	inline void set_dictionaryReplace_7(Subject_1_t2309927415 * value)
	{
		___dictionaryReplace_7 = value;
		Il2CppCodeGenWriteBarrier((&___dictionaryReplace_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEDICTIONARY_2_T820484097_H
#ifndef REACTIVEDICTIONARYEXTENSIONS_T2909995766_H
#define REACTIVEDICTIONARYEXTENSIONS_T2909995766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveDictionaryExtensions
struct  ReactiveDictionaryExtensions_t2909995766  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEDICTIONARYEXTENSIONS_T2909995766_H
#ifndef REACTIVECOMMAND_1_T1068238646_H
#define REACTIVECOMMAND_1_T1068238646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveCommand`1<System.Object>
struct  ReactiveCommand_1_t1068238646  : public RuntimeObject
{
public:
	// UniRx.Subject`1<T> UniRx.ReactiveCommand`1::trigger
	Subject_1_t3168762551 * ___trigger_0;
	// System.IDisposable UniRx.ReactiveCommand`1::canExecuteSubscription
	RuntimeObject* ___canExecuteSubscription_1;
	// UniRx.ReactiveProperty`1<System.Boolean> UniRx.ReactiveCommand`1::canExecute
	ReactiveProperty_1_t3626980155 * ___canExecute_2;
	// System.Boolean UniRx.ReactiveCommand`1::<IsDisposed>k__BackingField
	bool ___U3CIsDisposedU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_trigger_0() { return static_cast<int32_t>(offsetof(ReactiveCommand_1_t1068238646, ___trigger_0)); }
	inline Subject_1_t3168762551 * get_trigger_0() const { return ___trigger_0; }
	inline Subject_1_t3168762551 ** get_address_of_trigger_0() { return &___trigger_0; }
	inline void set_trigger_0(Subject_1_t3168762551 * value)
	{
		___trigger_0 = value;
		Il2CppCodeGenWriteBarrier((&___trigger_0), value);
	}

	inline static int32_t get_offset_of_canExecuteSubscription_1() { return static_cast<int32_t>(offsetof(ReactiveCommand_1_t1068238646, ___canExecuteSubscription_1)); }
	inline RuntimeObject* get_canExecuteSubscription_1() const { return ___canExecuteSubscription_1; }
	inline RuntimeObject** get_address_of_canExecuteSubscription_1() { return &___canExecuteSubscription_1; }
	inline void set_canExecuteSubscription_1(RuntimeObject* value)
	{
		___canExecuteSubscription_1 = value;
		Il2CppCodeGenWriteBarrier((&___canExecuteSubscription_1), value);
	}

	inline static int32_t get_offset_of_canExecute_2() { return static_cast<int32_t>(offsetof(ReactiveCommand_1_t1068238646, ___canExecute_2)); }
	inline ReactiveProperty_1_t3626980155 * get_canExecute_2() const { return ___canExecute_2; }
	inline ReactiveProperty_1_t3626980155 ** get_address_of_canExecute_2() { return &___canExecute_2; }
	inline void set_canExecute_2(ReactiveProperty_1_t3626980155 * value)
	{
		___canExecute_2 = value;
		Il2CppCodeGenWriteBarrier((&___canExecute_2), value);
	}

	inline static int32_t get_offset_of_U3CIsDisposedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ReactiveCommand_1_t1068238646, ___U3CIsDisposedU3Ek__BackingField_3)); }
	inline bool get_U3CIsDisposedU3Ek__BackingField_3() const { return ___U3CIsDisposedU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsDisposedU3Ek__BackingField_3() { return &___U3CIsDisposedU3Ek__BackingField_3; }
	inline void set_U3CIsDisposedU3Ek__BackingField_3(bool value)
	{
		___U3CIsDisposedU3Ek__BackingField_3 = value;
	}
};

struct ReactiveCommand_1_t1068238646_StaticFields
{
public:
	// System.Action`2<System.Boolean,UniRx.ReactiveProperty`1<System.Boolean>> UniRx.ReactiveCommand`1::<>f__am$cache0
	Action_2_t3070361696 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(ReactiveCommand_1_t1068238646_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Action_2_t3070361696 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Action_2_t3070361696 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Action_2_t3070361696 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVECOMMAND_1_T1068238646_H
#ifndef REACTIVECOMMANDEXTENSIONS_T1223375871_H
#define REACTIVECOMMANDEXTENSIONS_T1223375871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveCommandExtensions
struct  ReactiveCommandExtensions_t1223375871  : public RuntimeObject
{
public:

public:
};

struct ReactiveCommandExtensions_t1223375871_StaticFields
{
public:
	// System.Action`2<UniRx.Unit,UniRx.ReactiveCommand`1<UniRx.Unit>> UniRx.ReactiveCommandExtensions::<>f__am$cache0
	Action_2_t4180013452 * ___U3CU3Ef__amU24cache0_0;
	// System.Action`2<UniRx.Unit,UniRx.ReactiveCommand`1<UniRx.Unit>> UniRx.ReactiveCommandExtensions::<>f__am$cache1
	Action_2_t4180013452 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(ReactiveCommandExtensions_t1223375871_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Action_2_t4180013452 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Action_2_t4180013452 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Action_2_t4180013452 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(ReactiveCommandExtensions_t1223375871_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Action_2_t4180013452 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Action_2_t4180013452 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Action_2_t4180013452 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVECOMMANDEXTENSIONS_T1223375871_H
#ifndef REACTIVECOLLECTIONEXTENSIONS_T1881372181_H
#define REACTIVECOLLECTIONEXTENSIONS_T1881372181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveCollectionExtensions
struct  ReactiveCollectionExtensions_t1881372181  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVECOLLECTIONEXTENSIONS_T1881372181_H
#ifndef REACTIVEPROPERTY_1_T3626980155_H
#define REACTIVEPROPERTY_1_T3626980155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<System.Boolean>
struct  ReactiveProperty_1_t3626980155  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	bool ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t185944352 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t3626980155, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t3626980155, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t3626980155, ___value_3)); }
	inline bool get_value_3() const { return ___value_3; }
	inline bool* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(bool value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t3626980155, ___publisher_4)); }
	inline Subject_1_t185944352 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t185944352 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t185944352 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t3626980155, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t3626980155, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t3626980155_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t3626980155_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T3626980155_H
#ifndef SYNCHRONIZEDOBSERVER_1_T3807712003_H
#define SYNCHRONIZEDOBSERVER_1_T3807712003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SynchronizedObserver`1<System.Object>
struct  SynchronizedObserver_1_t3807712003  : public RuntimeObject
{
public:
	// UniRx.IObserver`1<T> UniRx.Operators.SynchronizedObserver`1::observer
	RuntimeObject* ___observer_0;
	// System.Object UniRx.Operators.SynchronizedObserver`1::gate
	RuntimeObject * ___gate_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(SynchronizedObserver_1_t3807712003, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((&___observer_0), value);
	}

	inline static int32_t get_offset_of_gate_1() { return static_cast<int32_t>(offsetof(SynchronizedObserver_1_t3807712003, ___gate_1)); }
	inline RuntimeObject * get_gate_1() const { return ___gate_1; }
	inline RuntimeObject ** get_address_of_gate_1() { return &___gate_1; }
	inline void set_gate_1(RuntimeObject * value)
	{
		___gate_1 = value;
		Il2CppCodeGenWriteBarrier((&___gate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCHRONIZEDOBSERVER_1_T3807712003_H
#ifndef SUBSCRIBE_3_T2075922912_H
#define SUBSCRIBE_3_T2075922912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`3<System.Object,System.Object,System.Object>
struct  Subscribe_3_t2075922912  : public RuntimeObject
{
public:
	// TState1 UniRx.Observer/Subscribe`3::state1
	RuntimeObject * ___state1_0;
	// TState2 UniRx.Observer/Subscribe`3::state2
	RuntimeObject * ___state2_1;
	// System.Action`3<T,TState1,TState2> UniRx.Observer/Subscribe`3::onNext
	Action_3_t3632554945 * ___onNext_2;
	// System.Action`3<System.Exception,TState1,TState2> UniRx.Observer/Subscribe`3::onError
	Action_3_t2220624998 * ___onError_3;
	// System.Action`2<TState1,TState2> UniRx.Observer/Subscribe`3::onCompleted
	Action_2_t2470008838 * ___onCompleted_4;
	// System.Int32 UniRx.Observer/Subscribe`3::isStopped
	int32_t ___isStopped_5;

public:
	inline static int32_t get_offset_of_state1_0() { return static_cast<int32_t>(offsetof(Subscribe_3_t2075922912, ___state1_0)); }
	inline RuntimeObject * get_state1_0() const { return ___state1_0; }
	inline RuntimeObject ** get_address_of_state1_0() { return &___state1_0; }
	inline void set_state1_0(RuntimeObject * value)
	{
		___state1_0 = value;
		Il2CppCodeGenWriteBarrier((&___state1_0), value);
	}

	inline static int32_t get_offset_of_state2_1() { return static_cast<int32_t>(offsetof(Subscribe_3_t2075922912, ___state2_1)); }
	inline RuntimeObject * get_state2_1() const { return ___state2_1; }
	inline RuntimeObject ** get_address_of_state2_1() { return &___state2_1; }
	inline void set_state2_1(RuntimeObject * value)
	{
		___state2_1 = value;
		Il2CppCodeGenWriteBarrier((&___state2_1), value);
	}

	inline static int32_t get_offset_of_onNext_2() { return static_cast<int32_t>(offsetof(Subscribe_3_t2075922912, ___onNext_2)); }
	inline Action_3_t3632554945 * get_onNext_2() const { return ___onNext_2; }
	inline Action_3_t3632554945 ** get_address_of_onNext_2() { return &___onNext_2; }
	inline void set_onNext_2(Action_3_t3632554945 * value)
	{
		___onNext_2 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_2), value);
	}

	inline static int32_t get_offset_of_onError_3() { return static_cast<int32_t>(offsetof(Subscribe_3_t2075922912, ___onError_3)); }
	inline Action_3_t2220624998 * get_onError_3() const { return ___onError_3; }
	inline Action_3_t2220624998 ** get_address_of_onError_3() { return &___onError_3; }
	inline void set_onError_3(Action_3_t2220624998 * value)
	{
		___onError_3 = value;
		Il2CppCodeGenWriteBarrier((&___onError_3), value);
	}

	inline static int32_t get_offset_of_onCompleted_4() { return static_cast<int32_t>(offsetof(Subscribe_3_t2075922912, ___onCompleted_4)); }
	inline Action_2_t2470008838 * get_onCompleted_4() const { return ___onCompleted_4; }
	inline Action_2_t2470008838 ** get_address_of_onCompleted_4() { return &___onCompleted_4; }
	inline void set_onCompleted_4(Action_2_t2470008838 * value)
	{
		___onCompleted_4 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_4), value);
	}

	inline static int32_t get_offset_of_isStopped_5() { return static_cast<int32_t>(offsetof(Subscribe_3_t2075922912, ___isStopped_5)); }
	inline int32_t get_isStopped_5() const { return ___isStopped_5; }
	inline int32_t* get_address_of_isStopped_5() { return &___isStopped_5; }
	inline void set_isStopped_5(int32_t value)
	{
		___isStopped_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_3_T2075922912_H
#ifndef SUBSCRIBE_1_T3875945719_H
#define SUBSCRIBE_1_T3875945719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`1<UniRx.Unit>
struct  Subscribe_1_t3875945719  : public RuntimeObject
{
public:
	// System.Action`1<T> UniRx.Observer/Subscribe`1::onNext
	Action_1_t3534717062 * ___onNext_0;
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe`1::onError
	Action_1_t1609204844 * ___onError_1;
	// System.Action UniRx.Observer/Subscribe`1::onCompleted
	Action_t1264377477 * ___onCompleted_2;
	// System.Int32 UniRx.Observer/Subscribe`1::isStopped
	int32_t ___isStopped_3;

public:
	inline static int32_t get_offset_of_onNext_0() { return static_cast<int32_t>(offsetof(Subscribe_1_t3875945719, ___onNext_0)); }
	inline Action_1_t3534717062 * get_onNext_0() const { return ___onNext_0; }
	inline Action_1_t3534717062 ** get_address_of_onNext_0() { return &___onNext_0; }
	inline void set_onNext_0(Action_1_t3534717062 * value)
	{
		___onNext_0 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_0), value);
	}

	inline static int32_t get_offset_of_onError_1() { return static_cast<int32_t>(offsetof(Subscribe_1_t3875945719, ___onError_1)); }
	inline Action_1_t1609204844 * get_onError_1() const { return ___onError_1; }
	inline Action_1_t1609204844 ** get_address_of_onError_1() { return &___onError_1; }
	inline void set_onError_1(Action_1_t1609204844 * value)
	{
		___onError_1 = value;
		Il2CppCodeGenWriteBarrier((&___onError_1), value);
	}

	inline static int32_t get_offset_of_onCompleted_2() { return static_cast<int32_t>(offsetof(Subscribe_1_t3875945719, ___onCompleted_2)); }
	inline Action_t1264377477 * get_onCompleted_2() const { return ___onCompleted_2; }
	inline Action_t1264377477 ** get_address_of_onCompleted_2() { return &___onCompleted_2; }
	inline void set_onCompleted_2(Action_t1264377477 * value)
	{
		___onCompleted_2 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_2), value);
	}

	inline static int32_t get_offset_of_isStopped_3() { return static_cast<int32_t>(offsetof(Subscribe_1_t3875945719, ___isStopped_3)); }
	inline int32_t get_isStopped_3() const { return ___isStopped_3; }
	inline int32_t* get_address_of_isStopped_3() { return &___isStopped_3; }
	inline void set_isStopped_3(int32_t value)
	{
		___isStopped_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_1_T3875945719_H
#ifndef SUBSCRIBE__1_T3222915108_H
#define SUBSCRIBE__1_T3222915108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe_`1<UniRx.Unit>
struct  Subscribe__1_t3222915108  : public RuntimeObject
{
public:
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe_`1::onError
	Action_1_t1609204844 * ___onError_0;
	// System.Action UniRx.Observer/Subscribe_`1::onCompleted
	Action_t1264377477 * ___onCompleted_1;
	// System.Int32 UniRx.Observer/Subscribe_`1::isStopped
	int32_t ___isStopped_2;

public:
	inline static int32_t get_offset_of_onError_0() { return static_cast<int32_t>(offsetof(Subscribe__1_t3222915108, ___onError_0)); }
	inline Action_1_t1609204844 * get_onError_0() const { return ___onError_0; }
	inline Action_1_t1609204844 ** get_address_of_onError_0() { return &___onError_0; }
	inline void set_onError_0(Action_1_t1609204844 * value)
	{
		___onError_0 = value;
		Il2CppCodeGenWriteBarrier((&___onError_0), value);
	}

	inline static int32_t get_offset_of_onCompleted_1() { return static_cast<int32_t>(offsetof(Subscribe__1_t3222915108, ___onCompleted_1)); }
	inline Action_t1264377477 * get_onCompleted_1() const { return ___onCompleted_1; }
	inline Action_t1264377477 ** get_address_of_onCompleted_1() { return &___onCompleted_1; }
	inline void set_onCompleted_1(Action_t1264377477 * value)
	{
		___onCompleted_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_1), value);
	}

	inline static int32_t get_offset_of_isStopped_2() { return static_cast<int32_t>(offsetof(Subscribe__1_t3222915108, ___isStopped_2)); }
	inline int32_t get_isStopped_2() const { return ___isStopped_2; }
	inline int32_t* get_address_of_isStopped_2() { return &___isStopped_2; }
	inline void set_isStopped_2(int32_t value)
	{
		___isStopped_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE__1_T3222915108_H
#ifndef STUBS_1_T2366812075_H
#define STUBS_1_T2366812075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Stubs`1<UniRx.Unit>
struct  Stubs_1_t2366812075  : public RuntimeObject
{
public:

public:
};

struct Stubs_1_t2366812075_StaticFields
{
public:
	// System.Action`1<T> UniRx.Stubs`1::Ignore
	Action_1_t3534717062 * ___Ignore_0;
	// System.Func`2<T,T> UniRx.Stubs`1::Identity
	Func_2_t1874035210 * ___Identity_1;
	// System.Action`2<System.Exception,T> UniRx.Stubs`1::Throw
	Action_2_t2443214028 * ___Throw_2;

public:
	inline static int32_t get_offset_of_Ignore_0() { return static_cast<int32_t>(offsetof(Stubs_1_t2366812075_StaticFields, ___Ignore_0)); }
	inline Action_1_t3534717062 * get_Ignore_0() const { return ___Ignore_0; }
	inline Action_1_t3534717062 ** get_address_of_Ignore_0() { return &___Ignore_0; }
	inline void set_Ignore_0(Action_1_t3534717062 * value)
	{
		___Ignore_0 = value;
		Il2CppCodeGenWriteBarrier((&___Ignore_0), value);
	}

	inline static int32_t get_offset_of_Identity_1() { return static_cast<int32_t>(offsetof(Stubs_1_t2366812075_StaticFields, ___Identity_1)); }
	inline Func_2_t1874035210 * get_Identity_1() const { return ___Identity_1; }
	inline Func_2_t1874035210 ** get_address_of_Identity_1() { return &___Identity_1; }
	inline void set_Identity_1(Func_2_t1874035210 * value)
	{
		___Identity_1 = value;
		Il2CppCodeGenWriteBarrier((&___Identity_1), value);
	}

	inline static int32_t get_offset_of_Throw_2() { return static_cast<int32_t>(offsetof(Stubs_1_t2366812075_StaticFields, ___Throw_2)); }
	inline Action_2_t2443214028 * get_Throw_2() const { return ___Throw_2; }
	inline Action_2_t2443214028 ** get_address_of_Throw_2() { return &___Throw_2; }
	inline void set_Throw_2(Action_2_t2443214028 * value)
	{
		___Throw_2 = value;
		Il2CppCodeGenWriteBarrier((&___Throw_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STUBS_1_T2366812075_H
#ifndef SUBSCRIBE_1_T1655203365_H
#define SUBSCRIBE_1_T1655203365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`1<UniRx.Diagnostics.LogEntry>
struct  Subscribe_1_t1655203365  : public RuntimeObject
{
public:
	// System.Action`1<T> UniRx.Observer/Subscribe`1::onNext
	Action_1_t1313974708 * ___onNext_0;
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe`1::onError
	Action_1_t1609204844 * ___onError_1;
	// System.Action UniRx.Observer/Subscribe`1::onCompleted
	Action_t1264377477 * ___onCompleted_2;
	// System.Int32 UniRx.Observer/Subscribe`1::isStopped
	int32_t ___isStopped_3;

public:
	inline static int32_t get_offset_of_onNext_0() { return static_cast<int32_t>(offsetof(Subscribe_1_t1655203365, ___onNext_0)); }
	inline Action_1_t1313974708 * get_onNext_0() const { return ___onNext_0; }
	inline Action_1_t1313974708 ** get_address_of_onNext_0() { return &___onNext_0; }
	inline void set_onNext_0(Action_1_t1313974708 * value)
	{
		___onNext_0 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_0), value);
	}

	inline static int32_t get_offset_of_onError_1() { return static_cast<int32_t>(offsetof(Subscribe_1_t1655203365, ___onError_1)); }
	inline Action_1_t1609204844 * get_onError_1() const { return ___onError_1; }
	inline Action_1_t1609204844 ** get_address_of_onError_1() { return &___onError_1; }
	inline void set_onError_1(Action_1_t1609204844 * value)
	{
		___onError_1 = value;
		Il2CppCodeGenWriteBarrier((&___onError_1), value);
	}

	inline static int32_t get_offset_of_onCompleted_2() { return static_cast<int32_t>(offsetof(Subscribe_1_t1655203365, ___onCompleted_2)); }
	inline Action_t1264377477 * get_onCompleted_2() const { return ___onCompleted_2; }
	inline Action_t1264377477 ** get_address_of_onCompleted_2() { return &___onCompleted_2; }
	inline void set_onCompleted_2(Action_t1264377477 * value)
	{
		___onCompleted_2 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_2), value);
	}

	inline static int32_t get_offset_of_isStopped_3() { return static_cast<int32_t>(offsetof(Subscribe_1_t1655203365, ___isStopped_3)); }
	inline int32_t get_isStopped_3() const { return ___isStopped_3; }
	inline int32_t* get_address_of_isStopped_3() { return &___isStopped_3; }
	inline void set_isStopped_3(int32_t value)
	{
		___isStopped_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_1_T1655203365_H
#ifndef SUBSCRIBE__1_T1002172754_H
#define SUBSCRIBE__1_T1002172754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe_`1<UniRx.Diagnostics.LogEntry>
struct  Subscribe__1_t1002172754  : public RuntimeObject
{
public:
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe_`1::onError
	Action_1_t1609204844 * ___onError_0;
	// System.Action UniRx.Observer/Subscribe_`1::onCompleted
	Action_t1264377477 * ___onCompleted_1;
	// System.Int32 UniRx.Observer/Subscribe_`1::isStopped
	int32_t ___isStopped_2;

public:
	inline static int32_t get_offset_of_onError_0() { return static_cast<int32_t>(offsetof(Subscribe__1_t1002172754, ___onError_0)); }
	inline Action_1_t1609204844 * get_onError_0() const { return ___onError_0; }
	inline Action_1_t1609204844 ** get_address_of_onError_0() { return &___onError_0; }
	inline void set_onError_0(Action_1_t1609204844 * value)
	{
		___onError_0 = value;
		Il2CppCodeGenWriteBarrier((&___onError_0), value);
	}

	inline static int32_t get_offset_of_onCompleted_1() { return static_cast<int32_t>(offsetof(Subscribe__1_t1002172754, ___onCompleted_1)); }
	inline Action_t1264377477 * get_onCompleted_1() const { return ___onCompleted_1; }
	inline Action_t1264377477 ** get_address_of_onCompleted_1() { return &___onCompleted_1; }
	inline void set_onCompleted_1(Action_t1264377477 * value)
	{
		___onCompleted_1 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_1), value);
	}

	inline static int32_t get_offset_of_isStopped_2() { return static_cast<int32_t>(offsetof(Subscribe__1_t1002172754, ___isStopped_2)); }
	inline int32_t get_isStopped_2() const { return ___isStopped_2; }
	inline int32_t* get_address_of_isStopped_2() { return &___isStopped_2; }
	inline void set_isStopped_2(int32_t value)
	{
		___isStopped_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE__1_T1002172754_H
#ifndef STUBS_1_T146069721_H
#define STUBS_1_T146069721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Stubs`1<UniRx.Diagnostics.LogEntry>
struct  Stubs_1_t146069721  : public RuntimeObject
{
public:

public:
};

struct Stubs_1_t146069721_StaticFields
{
public:
	// System.Action`1<T> UniRx.Stubs`1::Ignore
	Action_1_t1313974708 * ___Ignore_0;
	// System.Func`2<T,T> UniRx.Stubs`1::Identity
	Func_2_t2349466002 * ___Identity_1;
	// System.Action`2<System.Exception,T> UniRx.Stubs`1::Throw
	Action_2_t222471674 * ___Throw_2;

public:
	inline static int32_t get_offset_of_Ignore_0() { return static_cast<int32_t>(offsetof(Stubs_1_t146069721_StaticFields, ___Ignore_0)); }
	inline Action_1_t1313974708 * get_Ignore_0() const { return ___Ignore_0; }
	inline Action_1_t1313974708 ** get_address_of_Ignore_0() { return &___Ignore_0; }
	inline void set_Ignore_0(Action_1_t1313974708 * value)
	{
		___Ignore_0 = value;
		Il2CppCodeGenWriteBarrier((&___Ignore_0), value);
	}

	inline static int32_t get_offset_of_Identity_1() { return static_cast<int32_t>(offsetof(Stubs_1_t146069721_StaticFields, ___Identity_1)); }
	inline Func_2_t2349466002 * get_Identity_1() const { return ___Identity_1; }
	inline Func_2_t2349466002 ** get_address_of_Identity_1() { return &___Identity_1; }
	inline void set_Identity_1(Func_2_t2349466002 * value)
	{
		___Identity_1 = value;
		Il2CppCodeGenWriteBarrier((&___Identity_1), value);
	}

	inline static int32_t get_offset_of_Throw_2() { return static_cast<int32_t>(offsetof(Stubs_1_t146069721_StaticFields, ___Throw_2)); }
	inline Action_2_t222471674 * get_Throw_2() const { return ___Throw_2; }
	inline Action_2_t222471674 ** get_address_of_Throw_2() { return &___Throw_2; }
	inline void set_Throw_2(Action_2_t222471674 * value)
	{
		___Throw_2 = value;
		Il2CppCodeGenWriteBarrier((&___Throw_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STUBS_1_T146069721_H
#ifndef SUBSCRIBE_4_T2932287952_H
#define SUBSCRIBE_4_T2932287952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`4<System.Int64,System.Object,System.Object,System.Object>
struct  Subscribe_4_t2932287952  : public RuntimeObject
{
public:
	// TState1 UniRx.Observer/Subscribe`4::state1
	RuntimeObject * ___state1_0;
	// TState2 UniRx.Observer/Subscribe`4::state2
	RuntimeObject * ___state2_1;
	// TState3 UniRx.Observer/Subscribe`4::state3
	RuntimeObject * ___state3_2;
	// System.Action`4<T,TState1,TState2,TState3> UniRx.Observer/Subscribe`4::onNext
	Action_4_t327971120 * ___onNext_3;
	// System.Action`4<System.Exception,TState1,TState2,TState3> UniRx.Observer/Subscribe`4::onError
	Action_4_t1238479323 * ___onError_4;
	// System.Action`3<TState1,TState2,TState3> UniRx.Observer/Subscribe`4::onCompleted
	Action_3_t3632554945 * ___onCompleted_5;
	// System.Int32 UniRx.Observer/Subscribe`4::isStopped
	int32_t ___isStopped_6;

public:
	inline static int32_t get_offset_of_state1_0() { return static_cast<int32_t>(offsetof(Subscribe_4_t2932287952, ___state1_0)); }
	inline RuntimeObject * get_state1_0() const { return ___state1_0; }
	inline RuntimeObject ** get_address_of_state1_0() { return &___state1_0; }
	inline void set_state1_0(RuntimeObject * value)
	{
		___state1_0 = value;
		Il2CppCodeGenWriteBarrier((&___state1_0), value);
	}

	inline static int32_t get_offset_of_state2_1() { return static_cast<int32_t>(offsetof(Subscribe_4_t2932287952, ___state2_1)); }
	inline RuntimeObject * get_state2_1() const { return ___state2_1; }
	inline RuntimeObject ** get_address_of_state2_1() { return &___state2_1; }
	inline void set_state2_1(RuntimeObject * value)
	{
		___state2_1 = value;
		Il2CppCodeGenWriteBarrier((&___state2_1), value);
	}

	inline static int32_t get_offset_of_state3_2() { return static_cast<int32_t>(offsetof(Subscribe_4_t2932287952, ___state3_2)); }
	inline RuntimeObject * get_state3_2() const { return ___state3_2; }
	inline RuntimeObject ** get_address_of_state3_2() { return &___state3_2; }
	inline void set_state3_2(RuntimeObject * value)
	{
		___state3_2 = value;
		Il2CppCodeGenWriteBarrier((&___state3_2), value);
	}

	inline static int32_t get_offset_of_onNext_3() { return static_cast<int32_t>(offsetof(Subscribe_4_t2932287952, ___onNext_3)); }
	inline Action_4_t327971120 * get_onNext_3() const { return ___onNext_3; }
	inline Action_4_t327971120 ** get_address_of_onNext_3() { return &___onNext_3; }
	inline void set_onNext_3(Action_4_t327971120 * value)
	{
		___onNext_3 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_3), value);
	}

	inline static int32_t get_offset_of_onError_4() { return static_cast<int32_t>(offsetof(Subscribe_4_t2932287952, ___onError_4)); }
	inline Action_4_t1238479323 * get_onError_4() const { return ___onError_4; }
	inline Action_4_t1238479323 ** get_address_of_onError_4() { return &___onError_4; }
	inline void set_onError_4(Action_4_t1238479323 * value)
	{
		___onError_4 = value;
		Il2CppCodeGenWriteBarrier((&___onError_4), value);
	}

	inline static int32_t get_offset_of_onCompleted_5() { return static_cast<int32_t>(offsetof(Subscribe_4_t2932287952, ___onCompleted_5)); }
	inline Action_3_t3632554945 * get_onCompleted_5() const { return ___onCompleted_5; }
	inline Action_3_t3632554945 ** get_address_of_onCompleted_5() { return &___onCompleted_5; }
	inline void set_onCompleted_5(Action_3_t3632554945 * value)
	{
		___onCompleted_5 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_5), value);
	}

	inline static int32_t get_offset_of_isStopped_6() { return static_cast<int32_t>(offsetof(Subscribe_4_t2932287952, ___isStopped_6)); }
	inline int32_t get_isStopped_6() const { return ___isStopped_6; }
	inline int32_t* get_address_of_isStopped_6() { return &___isStopped_6; }
	inline void set_isStopped_6(int32_t value)
	{
		___isStopped_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_4_T2932287952_H
#ifndef OBSERVEREXTENSIONS_T2168473419_H
#define OBSERVEREXTENSIONS_T2168473419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObserverExtensions
struct  ObserverExtensions_t2168473419  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVEREXTENSIONS_T2168473419_H
#ifndef SUBSCRIBE_2_T349840897_H
#define SUBSCRIBE_2_T349840897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`2<UniRx.Unit,System.Object>
struct  Subscribe_2_t349840897  : public RuntimeObject
{
public:
	// TState UniRx.Observer/Subscribe`2::state
	RuntimeObject * ___state_0;
	// System.Action`2<T,TState> UniRx.Observer/Subscribe`2::onNext
	Action_2_t1614770371 * ___onNext_1;
	// System.Action`2<System.Exception,TState> UniRx.Observer/Subscribe`2::onError
	Action_2_t2161070725 * ___onError_2;
	// System.Action`1<TState> UniRx.Observer/Subscribe`2::onCompleted
	Action_1_t3252573759 * ___onCompleted_3;
	// System.Int32 UniRx.Observer/Subscribe`2::isStopped
	int32_t ___isStopped_4;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(Subscribe_2_t349840897, ___state_0)); }
	inline RuntimeObject * get_state_0() const { return ___state_0; }
	inline RuntimeObject ** get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(RuntimeObject * value)
	{
		___state_0 = value;
		Il2CppCodeGenWriteBarrier((&___state_0), value);
	}

	inline static int32_t get_offset_of_onNext_1() { return static_cast<int32_t>(offsetof(Subscribe_2_t349840897, ___onNext_1)); }
	inline Action_2_t1614770371 * get_onNext_1() const { return ___onNext_1; }
	inline Action_2_t1614770371 ** get_address_of_onNext_1() { return &___onNext_1; }
	inline void set_onNext_1(Action_2_t1614770371 * value)
	{
		___onNext_1 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_1), value);
	}

	inline static int32_t get_offset_of_onError_2() { return static_cast<int32_t>(offsetof(Subscribe_2_t349840897, ___onError_2)); }
	inline Action_2_t2161070725 * get_onError_2() const { return ___onError_2; }
	inline Action_2_t2161070725 ** get_address_of_onError_2() { return &___onError_2; }
	inline void set_onError_2(Action_2_t2161070725 * value)
	{
		___onError_2 = value;
		Il2CppCodeGenWriteBarrier((&___onError_2), value);
	}

	inline static int32_t get_offset_of_onCompleted_3() { return static_cast<int32_t>(offsetof(Subscribe_2_t349840897, ___onCompleted_3)); }
	inline Action_1_t3252573759 * get_onCompleted_3() const { return ___onCompleted_3; }
	inline Action_1_t3252573759 ** get_address_of_onCompleted_3() { return &___onCompleted_3; }
	inline void set_onCompleted_3(Action_1_t3252573759 * value)
	{
		___onCompleted_3 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_3), value);
	}

	inline static int32_t get_offset_of_isStopped_4() { return static_cast<int32_t>(offsetof(Subscribe_2_t349840897, ___isStopped_4)); }
	inline int32_t get_isStopped_4() const { return ___isStopped_4; }
	inline int32_t* get_address_of_isStopped_4() { return &___isStopped_4; }
	inline void set_isStopped_4(int32_t value)
	{
		___isStopped_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_2_T349840897_H
#ifndef SUBSCRIBE_2_T1205079364_H
#define SUBSCRIBE_2_T1205079364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`2<System.Object,System.Object>
struct  Subscribe_2_t1205079364  : public RuntimeObject
{
public:
	// TState UniRx.Observer/Subscribe`2::state
	RuntimeObject * ___state_0;
	// System.Action`2<T,TState> UniRx.Observer/Subscribe`2::onNext
	Action_2_t2470008838 * ___onNext_1;
	// System.Action`2<System.Exception,TState> UniRx.Observer/Subscribe`2::onError
	Action_2_t2161070725 * ___onError_2;
	// System.Action`1<TState> UniRx.Observer/Subscribe`2::onCompleted
	Action_1_t3252573759 * ___onCompleted_3;
	// System.Int32 UniRx.Observer/Subscribe`2::isStopped
	int32_t ___isStopped_4;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(Subscribe_2_t1205079364, ___state_0)); }
	inline RuntimeObject * get_state_0() const { return ___state_0; }
	inline RuntimeObject ** get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(RuntimeObject * value)
	{
		___state_0 = value;
		Il2CppCodeGenWriteBarrier((&___state_0), value);
	}

	inline static int32_t get_offset_of_onNext_1() { return static_cast<int32_t>(offsetof(Subscribe_2_t1205079364, ___onNext_1)); }
	inline Action_2_t2470008838 * get_onNext_1() const { return ___onNext_1; }
	inline Action_2_t2470008838 ** get_address_of_onNext_1() { return &___onNext_1; }
	inline void set_onNext_1(Action_2_t2470008838 * value)
	{
		___onNext_1 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_1), value);
	}

	inline static int32_t get_offset_of_onError_2() { return static_cast<int32_t>(offsetof(Subscribe_2_t1205079364, ___onError_2)); }
	inline Action_2_t2161070725 * get_onError_2() const { return ___onError_2; }
	inline Action_2_t2161070725 ** get_address_of_onError_2() { return &___onError_2; }
	inline void set_onError_2(Action_2_t2161070725 * value)
	{
		___onError_2 = value;
		Il2CppCodeGenWriteBarrier((&___onError_2), value);
	}

	inline static int32_t get_offset_of_onCompleted_3() { return static_cast<int32_t>(offsetof(Subscribe_2_t1205079364, ___onCompleted_3)); }
	inline Action_1_t3252573759 * get_onCompleted_3() const { return ___onCompleted_3; }
	inline Action_1_t3252573759 ** get_address_of_onCompleted_3() { return &___onCompleted_3; }
	inline void set_onCompleted_3(Action_1_t3252573759 * value)
	{
		___onCompleted_3 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_3), value);
	}

	inline static int32_t get_offset_of_isStopped_4() { return static_cast<int32_t>(offsetof(Subscribe_2_t1205079364, ___isStopped_4)); }
	inline int32_t get_isStopped_4() const { return ___isStopped_4; }
	inline int32_t* get_address_of_isStopped_4() { return &___isStopped_4; }
	inline void set_isStopped_4(int32_t value)
	{
		___isStopped_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_2_T1205079364_H
#ifndef SUBSCRIBE_2_T920312864_H
#define SUBSCRIBE_2_T920312864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`2<System.Int64,System.Object>
struct  Subscribe_2_t920312864  : public RuntimeObject
{
public:
	// TState UniRx.Observer/Subscribe`2::state
	RuntimeObject * ___state_0;
	// System.Action`2<T,TState> UniRx.Observer/Subscribe`2::onNext
	Action_2_t2185242338 * ___onNext_1;
	// System.Action`2<System.Exception,TState> UniRx.Observer/Subscribe`2::onError
	Action_2_t2161070725 * ___onError_2;
	// System.Action`1<TState> UniRx.Observer/Subscribe`2::onCompleted
	Action_1_t3252573759 * ___onCompleted_3;
	// System.Int32 UniRx.Observer/Subscribe`2::isStopped
	int32_t ___isStopped_4;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(Subscribe_2_t920312864, ___state_0)); }
	inline RuntimeObject * get_state_0() const { return ___state_0; }
	inline RuntimeObject ** get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(RuntimeObject * value)
	{
		___state_0 = value;
		Il2CppCodeGenWriteBarrier((&___state_0), value);
	}

	inline static int32_t get_offset_of_onNext_1() { return static_cast<int32_t>(offsetof(Subscribe_2_t920312864, ___onNext_1)); }
	inline Action_2_t2185242338 * get_onNext_1() const { return ___onNext_1; }
	inline Action_2_t2185242338 ** get_address_of_onNext_1() { return &___onNext_1; }
	inline void set_onNext_1(Action_2_t2185242338 * value)
	{
		___onNext_1 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_1), value);
	}

	inline static int32_t get_offset_of_onError_2() { return static_cast<int32_t>(offsetof(Subscribe_2_t920312864, ___onError_2)); }
	inline Action_2_t2161070725 * get_onError_2() const { return ___onError_2; }
	inline Action_2_t2161070725 ** get_address_of_onError_2() { return &___onError_2; }
	inline void set_onError_2(Action_2_t2161070725 * value)
	{
		___onError_2 = value;
		Il2CppCodeGenWriteBarrier((&___onError_2), value);
	}

	inline static int32_t get_offset_of_onCompleted_3() { return static_cast<int32_t>(offsetof(Subscribe_2_t920312864, ___onCompleted_3)); }
	inline Action_1_t3252573759 * get_onCompleted_3() const { return ___onCompleted_3; }
	inline Action_1_t3252573759 ** get_address_of_onCompleted_3() { return &___onCompleted_3; }
	inline void set_onCompleted_3(Action_1_t3252573759 * value)
	{
		___onCompleted_3 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_3), value);
	}

	inline static int32_t get_offset_of_isStopped_4() { return static_cast<int32_t>(offsetof(Subscribe_2_t920312864, ___isStopped_4)); }
	inline int32_t get_isStopped_4() const { return ___isStopped_4; }
	inline int32_t* get_address_of_isStopped_4() { return &___isStopped_4; }
	inline void set_isStopped_4(int32_t value)
	{
		___isStopped_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_2_T920312864_H
#ifndef SUBSCRIBE_2_T3041353707_H
#define SUBSCRIBE_2_T3041353707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`2<System.Int32,System.Object>
struct  Subscribe_2_t3041353707  : public RuntimeObject
{
public:
	// TState UniRx.Observer/Subscribe`2::state
	RuntimeObject * ___state_0;
	// System.Action`2<T,TState> UniRx.Observer/Subscribe`2::onNext
	Action_2_t11315885 * ___onNext_1;
	// System.Action`2<System.Exception,TState> UniRx.Observer/Subscribe`2::onError
	Action_2_t2161070725 * ___onError_2;
	// System.Action`1<TState> UniRx.Observer/Subscribe`2::onCompleted
	Action_1_t3252573759 * ___onCompleted_3;
	// System.Int32 UniRx.Observer/Subscribe`2::isStopped
	int32_t ___isStopped_4;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(Subscribe_2_t3041353707, ___state_0)); }
	inline RuntimeObject * get_state_0() const { return ___state_0; }
	inline RuntimeObject ** get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(RuntimeObject * value)
	{
		___state_0 = value;
		Il2CppCodeGenWriteBarrier((&___state_0), value);
	}

	inline static int32_t get_offset_of_onNext_1() { return static_cast<int32_t>(offsetof(Subscribe_2_t3041353707, ___onNext_1)); }
	inline Action_2_t11315885 * get_onNext_1() const { return ___onNext_1; }
	inline Action_2_t11315885 ** get_address_of_onNext_1() { return &___onNext_1; }
	inline void set_onNext_1(Action_2_t11315885 * value)
	{
		___onNext_1 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_1), value);
	}

	inline static int32_t get_offset_of_onError_2() { return static_cast<int32_t>(offsetof(Subscribe_2_t3041353707, ___onError_2)); }
	inline Action_2_t2161070725 * get_onError_2() const { return ___onError_2; }
	inline Action_2_t2161070725 ** get_address_of_onError_2() { return &___onError_2; }
	inline void set_onError_2(Action_2_t2161070725 * value)
	{
		___onError_2 = value;
		Il2CppCodeGenWriteBarrier((&___onError_2), value);
	}

	inline static int32_t get_offset_of_onCompleted_3() { return static_cast<int32_t>(offsetof(Subscribe_2_t3041353707, ___onCompleted_3)); }
	inline Action_1_t3252573759 * get_onCompleted_3() const { return ___onCompleted_3; }
	inline Action_1_t3252573759 ** get_address_of_onCompleted_3() { return &___onCompleted_3; }
	inline void set_onCompleted_3(Action_1_t3252573759 * value)
	{
		___onCompleted_3 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_3), value);
	}

	inline static int32_t get_offset_of_isStopped_4() { return static_cast<int32_t>(offsetof(Subscribe_2_t3041353707, ___isStopped_4)); }
	inline int32_t get_isStopped_4() const { return ___isStopped_4; }
	inline int32_t* get_address_of_isStopped_4() { return &___isStopped_4; }
	inline void set_isStopped_4(int32_t value)
	{
		___isStopped_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_2_T3041353707_H
#ifndef SUBSCRIBE_2_T1258558231_H
#define SUBSCRIBE_2_T1258558231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`2<System.Boolean,System.Object>
struct  Subscribe_2_t1258558231  : public RuntimeObject
{
public:
	// TState UniRx.Observer/Subscribe`2::state
	RuntimeObject * ___state_0;
	// System.Action`2<T,TState> UniRx.Observer/Subscribe`2::onNext
	Action_2_t2523487705 * ___onNext_1;
	// System.Action`2<System.Exception,TState> UniRx.Observer/Subscribe`2::onError
	Action_2_t2161070725 * ___onError_2;
	// System.Action`1<TState> UniRx.Observer/Subscribe`2::onCompleted
	Action_1_t3252573759 * ___onCompleted_3;
	// System.Int32 UniRx.Observer/Subscribe`2::isStopped
	int32_t ___isStopped_4;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(Subscribe_2_t1258558231, ___state_0)); }
	inline RuntimeObject * get_state_0() const { return ___state_0; }
	inline RuntimeObject ** get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(RuntimeObject * value)
	{
		___state_0 = value;
		Il2CppCodeGenWriteBarrier((&___state_0), value);
	}

	inline static int32_t get_offset_of_onNext_1() { return static_cast<int32_t>(offsetof(Subscribe_2_t1258558231, ___onNext_1)); }
	inline Action_2_t2523487705 * get_onNext_1() const { return ___onNext_1; }
	inline Action_2_t2523487705 ** get_address_of_onNext_1() { return &___onNext_1; }
	inline void set_onNext_1(Action_2_t2523487705 * value)
	{
		___onNext_1 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_1), value);
	}

	inline static int32_t get_offset_of_onError_2() { return static_cast<int32_t>(offsetof(Subscribe_2_t1258558231, ___onError_2)); }
	inline Action_2_t2161070725 * get_onError_2() const { return ___onError_2; }
	inline Action_2_t2161070725 ** get_address_of_onError_2() { return &___onError_2; }
	inline void set_onError_2(Action_2_t2161070725 * value)
	{
		___onError_2 = value;
		Il2CppCodeGenWriteBarrier((&___onError_2), value);
	}

	inline static int32_t get_offset_of_onCompleted_3() { return static_cast<int32_t>(offsetof(Subscribe_2_t1258558231, ___onCompleted_3)); }
	inline Action_1_t3252573759 * get_onCompleted_3() const { return ___onCompleted_3; }
	inline Action_1_t3252573759 ** get_address_of_onCompleted_3() { return &___onCompleted_3; }
	inline void set_onCompleted_3(Action_1_t3252573759 * value)
	{
		___onCompleted_3 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_3), value);
	}

	inline static int32_t get_offset_of_isStopped_4() { return static_cast<int32_t>(offsetof(Subscribe_2_t1258558231, ___isStopped_4)); }
	inline int32_t get_isStopped_4() const { return ___isStopped_4; }
	inline int32_t* get_address_of_isStopped_4() { return &___isStopped_4; }
	inline void set_isStopped_4(int32_t value)
	{
		___isStopped_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_2_T1258558231_H
#ifndef SUBSCRIBE_4_T2914302804_H
#define SUBSCRIBE_4_T2914302804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe`4<System.Object,System.Object,System.Object,System.Object>
struct  Subscribe_4_t2914302804  : public RuntimeObject
{
public:
	// TState1 UniRx.Observer/Subscribe`4::state1
	RuntimeObject * ___state1_0;
	// TState2 UniRx.Observer/Subscribe`4::state2
	RuntimeObject * ___state2_1;
	// TState3 UniRx.Observer/Subscribe`4::state3
	RuntimeObject * ___state3_2;
	// System.Action`4<T,TState1,TState2,TState3> UniRx.Observer/Subscribe`4::onNext
	Action_4_t309985972 * ___onNext_3;
	// System.Action`4<System.Exception,TState1,TState2,TState3> UniRx.Observer/Subscribe`4::onError
	Action_4_t1238479323 * ___onError_4;
	// System.Action`3<TState1,TState2,TState3> UniRx.Observer/Subscribe`4::onCompleted
	Action_3_t3632554945 * ___onCompleted_5;
	// System.Int32 UniRx.Observer/Subscribe`4::isStopped
	int32_t ___isStopped_6;

public:
	inline static int32_t get_offset_of_state1_0() { return static_cast<int32_t>(offsetof(Subscribe_4_t2914302804, ___state1_0)); }
	inline RuntimeObject * get_state1_0() const { return ___state1_0; }
	inline RuntimeObject ** get_address_of_state1_0() { return &___state1_0; }
	inline void set_state1_0(RuntimeObject * value)
	{
		___state1_0 = value;
		Il2CppCodeGenWriteBarrier((&___state1_0), value);
	}

	inline static int32_t get_offset_of_state2_1() { return static_cast<int32_t>(offsetof(Subscribe_4_t2914302804, ___state2_1)); }
	inline RuntimeObject * get_state2_1() const { return ___state2_1; }
	inline RuntimeObject ** get_address_of_state2_1() { return &___state2_1; }
	inline void set_state2_1(RuntimeObject * value)
	{
		___state2_1 = value;
		Il2CppCodeGenWriteBarrier((&___state2_1), value);
	}

	inline static int32_t get_offset_of_state3_2() { return static_cast<int32_t>(offsetof(Subscribe_4_t2914302804, ___state3_2)); }
	inline RuntimeObject * get_state3_2() const { return ___state3_2; }
	inline RuntimeObject ** get_address_of_state3_2() { return &___state3_2; }
	inline void set_state3_2(RuntimeObject * value)
	{
		___state3_2 = value;
		Il2CppCodeGenWriteBarrier((&___state3_2), value);
	}

	inline static int32_t get_offset_of_onNext_3() { return static_cast<int32_t>(offsetof(Subscribe_4_t2914302804, ___onNext_3)); }
	inline Action_4_t309985972 * get_onNext_3() const { return ___onNext_3; }
	inline Action_4_t309985972 ** get_address_of_onNext_3() { return &___onNext_3; }
	inline void set_onNext_3(Action_4_t309985972 * value)
	{
		___onNext_3 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_3), value);
	}

	inline static int32_t get_offset_of_onError_4() { return static_cast<int32_t>(offsetof(Subscribe_4_t2914302804, ___onError_4)); }
	inline Action_4_t1238479323 * get_onError_4() const { return ___onError_4; }
	inline Action_4_t1238479323 ** get_address_of_onError_4() { return &___onError_4; }
	inline void set_onError_4(Action_4_t1238479323 * value)
	{
		___onError_4 = value;
		Il2CppCodeGenWriteBarrier((&___onError_4), value);
	}

	inline static int32_t get_offset_of_onCompleted_5() { return static_cast<int32_t>(offsetof(Subscribe_4_t2914302804, ___onCompleted_5)); }
	inline Action_3_t3632554945 * get_onCompleted_5() const { return ___onCompleted_5; }
	inline Action_3_t3632554945 ** get_address_of_onCompleted_5() { return &___onCompleted_5; }
	inline void set_onCompleted_5(Action_3_t3632554945 * value)
	{
		___onCompleted_5 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleted_5), value);
	}

	inline static int32_t get_offset_of_isStopped_6() { return static_cast<int32_t>(offsetof(Subscribe_4_t2914302804, ___isStopped_6)); }
	inline int32_t get_isStopped_6() const { return ___isStopped_6; }
	inline int32_t* get_address_of_isStopped_6() { return &___isStopped_6; }
	inline void set_isStopped_6(int32_t value)
	{
		___isStopped_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIBE_4_T2914302804_H
#ifndef EXECUTEEVENTS_T3484638744_H
#define EXECUTEEVENTS_T3484638744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents
struct  ExecuteEvents_t3484638744  : public RuntimeObject
{
public:

public:
};

struct ExecuteEvents_t3484638744_StaticFields
{
public:
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerEnterHandler
	EventFunction_1_t3995630009 * ___s_PointerEnterHandler_0;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerExitHandler
	EventFunction_1_t2867327688 * ___s_PointerExitHandler_1;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerDownHandler
	EventFunction_1_t64614563 * ___s_PointerDownHandler_2;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerUpHandler
	EventFunction_1_t3256600500 * ___s_PointerUpHandler_3;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerClickHandler
	EventFunction_1_t3111972472 * ___s_PointerClickHandler_4;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_InitializePotentialDragHandler
	EventFunction_1_t3587542510 * ___s_InitializePotentialDragHandler_5;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_BeginDragHandler
	EventFunction_1_t1977848392 * ___s_BeginDragHandler_6;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_DragHandler
	EventFunction_1_t972960537 * ___s_DragHandler_7;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_EndDragHandler
	EventFunction_1_t3277009892 * ___s_EndDragHandler_8;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::s_DropHandler
	EventFunction_1_t2311673543 * ___s_DropHandler_9;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::s_ScrollHandler
	EventFunction_1_t2886331738 * ___s_ScrollHandler_10;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::s_UpdateSelectedHandler
	EventFunction_1_t2950825503 * ___s_UpdateSelectedHandler_11;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::s_SelectHandler
	EventFunction_1_t955952873 * ___s_SelectHandler_12;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::s_DeselectHandler
	EventFunction_1_t3373214253 * ___s_DeselectHandler_13;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::s_MoveHandler
	EventFunction_1_t3912835512 * ___s_MoveHandler_14;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::s_SubmitHandler
	EventFunction_1_t1475332338 * ___s_SubmitHandler_15;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::s_CancelHandler
	EventFunction_1_t2658898854 * ___s_CancelHandler_16;
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>> UnityEngine.EventSystems.ExecuteEvents::s_HandlerListPool
	ObjectPool_1_t231414508 * ___s_HandlerListPool_17;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityEngine.EventSystems.ExecuteEvents::s_InternalTransformList
	List_1_t777473367 * ___s_InternalTransformList_18;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache0
	EventFunction_1_t3995630009 * ___U3CU3Ef__mgU24cache0_19;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache1
	EventFunction_1_t2867327688 * ___U3CU3Ef__mgU24cache1_20;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache2
	EventFunction_1_t64614563 * ___U3CU3Ef__mgU24cache2_21;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache3
	EventFunction_1_t3256600500 * ___U3CU3Ef__mgU24cache3_22;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache4
	EventFunction_1_t3111972472 * ___U3CU3Ef__mgU24cache4_23;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache5
	EventFunction_1_t3587542510 * ___U3CU3Ef__mgU24cache5_24;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache6
	EventFunction_1_t1977848392 * ___U3CU3Ef__mgU24cache6_25;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache7
	EventFunction_1_t972960537 * ___U3CU3Ef__mgU24cache7_26;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache8
	EventFunction_1_t3277009892 * ___U3CU3Ef__mgU24cache8_27;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache9
	EventFunction_1_t2311673543 * ___U3CU3Ef__mgU24cache9_28;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheA
	EventFunction_1_t2886331738 * ___U3CU3Ef__mgU24cacheA_29;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheB
	EventFunction_1_t2950825503 * ___U3CU3Ef__mgU24cacheB_30;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheC
	EventFunction_1_t955952873 * ___U3CU3Ef__mgU24cacheC_31;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheD
	EventFunction_1_t3373214253 * ___U3CU3Ef__mgU24cacheD_32;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheE
	EventFunction_1_t3912835512 * ___U3CU3Ef__mgU24cacheE_33;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheF
	EventFunction_1_t1475332338 * ___U3CU3Ef__mgU24cacheF_34;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache10
	EventFunction_1_t2658898854 * ___U3CU3Ef__mgU24cache10_35;

public:
	inline static int32_t get_offset_of_s_PointerEnterHandler_0() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerEnterHandler_0)); }
	inline EventFunction_1_t3995630009 * get_s_PointerEnterHandler_0() const { return ___s_PointerEnterHandler_0; }
	inline EventFunction_1_t3995630009 ** get_address_of_s_PointerEnterHandler_0() { return &___s_PointerEnterHandler_0; }
	inline void set_s_PointerEnterHandler_0(EventFunction_1_t3995630009 * value)
	{
		___s_PointerEnterHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerEnterHandler_0), value);
	}

	inline static int32_t get_offset_of_s_PointerExitHandler_1() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerExitHandler_1)); }
	inline EventFunction_1_t2867327688 * get_s_PointerExitHandler_1() const { return ___s_PointerExitHandler_1; }
	inline EventFunction_1_t2867327688 ** get_address_of_s_PointerExitHandler_1() { return &___s_PointerExitHandler_1; }
	inline void set_s_PointerExitHandler_1(EventFunction_1_t2867327688 * value)
	{
		___s_PointerExitHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerExitHandler_1), value);
	}

	inline static int32_t get_offset_of_s_PointerDownHandler_2() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerDownHandler_2)); }
	inline EventFunction_1_t64614563 * get_s_PointerDownHandler_2() const { return ___s_PointerDownHandler_2; }
	inline EventFunction_1_t64614563 ** get_address_of_s_PointerDownHandler_2() { return &___s_PointerDownHandler_2; }
	inline void set_s_PointerDownHandler_2(EventFunction_1_t64614563 * value)
	{
		___s_PointerDownHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerDownHandler_2), value);
	}

	inline static int32_t get_offset_of_s_PointerUpHandler_3() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerUpHandler_3)); }
	inline EventFunction_1_t3256600500 * get_s_PointerUpHandler_3() const { return ___s_PointerUpHandler_3; }
	inline EventFunction_1_t3256600500 ** get_address_of_s_PointerUpHandler_3() { return &___s_PointerUpHandler_3; }
	inline void set_s_PointerUpHandler_3(EventFunction_1_t3256600500 * value)
	{
		___s_PointerUpHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerUpHandler_3), value);
	}

	inline static int32_t get_offset_of_s_PointerClickHandler_4() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerClickHandler_4)); }
	inline EventFunction_1_t3111972472 * get_s_PointerClickHandler_4() const { return ___s_PointerClickHandler_4; }
	inline EventFunction_1_t3111972472 ** get_address_of_s_PointerClickHandler_4() { return &___s_PointerClickHandler_4; }
	inline void set_s_PointerClickHandler_4(EventFunction_1_t3111972472 * value)
	{
		___s_PointerClickHandler_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerClickHandler_4), value);
	}

	inline static int32_t get_offset_of_s_InitializePotentialDragHandler_5() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_InitializePotentialDragHandler_5)); }
	inline EventFunction_1_t3587542510 * get_s_InitializePotentialDragHandler_5() const { return ___s_InitializePotentialDragHandler_5; }
	inline EventFunction_1_t3587542510 ** get_address_of_s_InitializePotentialDragHandler_5() { return &___s_InitializePotentialDragHandler_5; }
	inline void set_s_InitializePotentialDragHandler_5(EventFunction_1_t3587542510 * value)
	{
		___s_InitializePotentialDragHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_InitializePotentialDragHandler_5), value);
	}

	inline static int32_t get_offset_of_s_BeginDragHandler_6() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_BeginDragHandler_6)); }
	inline EventFunction_1_t1977848392 * get_s_BeginDragHandler_6() const { return ___s_BeginDragHandler_6; }
	inline EventFunction_1_t1977848392 ** get_address_of_s_BeginDragHandler_6() { return &___s_BeginDragHandler_6; }
	inline void set_s_BeginDragHandler_6(EventFunction_1_t1977848392 * value)
	{
		___s_BeginDragHandler_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_BeginDragHandler_6), value);
	}

	inline static int32_t get_offset_of_s_DragHandler_7() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_DragHandler_7)); }
	inline EventFunction_1_t972960537 * get_s_DragHandler_7() const { return ___s_DragHandler_7; }
	inline EventFunction_1_t972960537 ** get_address_of_s_DragHandler_7() { return &___s_DragHandler_7; }
	inline void set_s_DragHandler_7(EventFunction_1_t972960537 * value)
	{
		___s_DragHandler_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_DragHandler_7), value);
	}

	inline static int32_t get_offset_of_s_EndDragHandler_8() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_EndDragHandler_8)); }
	inline EventFunction_1_t3277009892 * get_s_EndDragHandler_8() const { return ___s_EndDragHandler_8; }
	inline EventFunction_1_t3277009892 ** get_address_of_s_EndDragHandler_8() { return &___s_EndDragHandler_8; }
	inline void set_s_EndDragHandler_8(EventFunction_1_t3277009892 * value)
	{
		___s_EndDragHandler_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_EndDragHandler_8), value);
	}

	inline static int32_t get_offset_of_s_DropHandler_9() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_DropHandler_9)); }
	inline EventFunction_1_t2311673543 * get_s_DropHandler_9() const { return ___s_DropHandler_9; }
	inline EventFunction_1_t2311673543 ** get_address_of_s_DropHandler_9() { return &___s_DropHandler_9; }
	inline void set_s_DropHandler_9(EventFunction_1_t2311673543 * value)
	{
		___s_DropHandler_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_DropHandler_9), value);
	}

	inline static int32_t get_offset_of_s_ScrollHandler_10() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_ScrollHandler_10)); }
	inline EventFunction_1_t2886331738 * get_s_ScrollHandler_10() const { return ___s_ScrollHandler_10; }
	inline EventFunction_1_t2886331738 ** get_address_of_s_ScrollHandler_10() { return &___s_ScrollHandler_10; }
	inline void set_s_ScrollHandler_10(EventFunction_1_t2886331738 * value)
	{
		___s_ScrollHandler_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_ScrollHandler_10), value);
	}

	inline static int32_t get_offset_of_s_UpdateSelectedHandler_11() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_UpdateSelectedHandler_11)); }
	inline EventFunction_1_t2950825503 * get_s_UpdateSelectedHandler_11() const { return ___s_UpdateSelectedHandler_11; }
	inline EventFunction_1_t2950825503 ** get_address_of_s_UpdateSelectedHandler_11() { return &___s_UpdateSelectedHandler_11; }
	inline void set_s_UpdateSelectedHandler_11(EventFunction_1_t2950825503 * value)
	{
		___s_UpdateSelectedHandler_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_UpdateSelectedHandler_11), value);
	}

	inline static int32_t get_offset_of_s_SelectHandler_12() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_SelectHandler_12)); }
	inline EventFunction_1_t955952873 * get_s_SelectHandler_12() const { return ___s_SelectHandler_12; }
	inline EventFunction_1_t955952873 ** get_address_of_s_SelectHandler_12() { return &___s_SelectHandler_12; }
	inline void set_s_SelectHandler_12(EventFunction_1_t955952873 * value)
	{
		___s_SelectHandler_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_SelectHandler_12), value);
	}

	inline static int32_t get_offset_of_s_DeselectHandler_13() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_DeselectHandler_13)); }
	inline EventFunction_1_t3373214253 * get_s_DeselectHandler_13() const { return ___s_DeselectHandler_13; }
	inline EventFunction_1_t3373214253 ** get_address_of_s_DeselectHandler_13() { return &___s_DeselectHandler_13; }
	inline void set_s_DeselectHandler_13(EventFunction_1_t3373214253 * value)
	{
		___s_DeselectHandler_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_DeselectHandler_13), value);
	}

	inline static int32_t get_offset_of_s_MoveHandler_14() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_MoveHandler_14)); }
	inline EventFunction_1_t3912835512 * get_s_MoveHandler_14() const { return ___s_MoveHandler_14; }
	inline EventFunction_1_t3912835512 ** get_address_of_s_MoveHandler_14() { return &___s_MoveHandler_14; }
	inline void set_s_MoveHandler_14(EventFunction_1_t3912835512 * value)
	{
		___s_MoveHandler_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_MoveHandler_14), value);
	}

	inline static int32_t get_offset_of_s_SubmitHandler_15() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_SubmitHandler_15)); }
	inline EventFunction_1_t1475332338 * get_s_SubmitHandler_15() const { return ___s_SubmitHandler_15; }
	inline EventFunction_1_t1475332338 ** get_address_of_s_SubmitHandler_15() { return &___s_SubmitHandler_15; }
	inline void set_s_SubmitHandler_15(EventFunction_1_t1475332338 * value)
	{
		___s_SubmitHandler_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_SubmitHandler_15), value);
	}

	inline static int32_t get_offset_of_s_CancelHandler_16() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_CancelHandler_16)); }
	inline EventFunction_1_t2658898854 * get_s_CancelHandler_16() const { return ___s_CancelHandler_16; }
	inline EventFunction_1_t2658898854 ** get_address_of_s_CancelHandler_16() { return &___s_CancelHandler_16; }
	inline void set_s_CancelHandler_16(EventFunction_1_t2658898854 * value)
	{
		___s_CancelHandler_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_CancelHandler_16), value);
	}

	inline static int32_t get_offset_of_s_HandlerListPool_17() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_HandlerListPool_17)); }
	inline ObjectPool_1_t231414508 * get_s_HandlerListPool_17() const { return ___s_HandlerListPool_17; }
	inline ObjectPool_1_t231414508 ** get_address_of_s_HandlerListPool_17() { return &___s_HandlerListPool_17; }
	inline void set_s_HandlerListPool_17(ObjectPool_1_t231414508 * value)
	{
		___s_HandlerListPool_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_HandlerListPool_17), value);
	}

	inline static int32_t get_offset_of_s_InternalTransformList_18() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_InternalTransformList_18)); }
	inline List_1_t777473367 * get_s_InternalTransformList_18() const { return ___s_InternalTransformList_18; }
	inline List_1_t777473367 ** get_address_of_s_InternalTransformList_18() { return &___s_InternalTransformList_18; }
	inline void set_s_InternalTransformList_18(List_1_t777473367 * value)
	{
		___s_InternalTransformList_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalTransformList_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_19() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache0_19)); }
	inline EventFunction_1_t3995630009 * get_U3CU3Ef__mgU24cache0_19() const { return ___U3CU3Ef__mgU24cache0_19; }
	inline EventFunction_1_t3995630009 ** get_address_of_U3CU3Ef__mgU24cache0_19() { return &___U3CU3Ef__mgU24cache0_19; }
	inline void set_U3CU3Ef__mgU24cache0_19(EventFunction_1_t3995630009 * value)
	{
		___U3CU3Ef__mgU24cache0_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_20() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache1_20)); }
	inline EventFunction_1_t2867327688 * get_U3CU3Ef__mgU24cache1_20() const { return ___U3CU3Ef__mgU24cache1_20; }
	inline EventFunction_1_t2867327688 ** get_address_of_U3CU3Ef__mgU24cache1_20() { return &___U3CU3Ef__mgU24cache1_20; }
	inline void set_U3CU3Ef__mgU24cache1_20(EventFunction_1_t2867327688 * value)
	{
		___U3CU3Ef__mgU24cache1_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_21() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache2_21)); }
	inline EventFunction_1_t64614563 * get_U3CU3Ef__mgU24cache2_21() const { return ___U3CU3Ef__mgU24cache2_21; }
	inline EventFunction_1_t64614563 ** get_address_of_U3CU3Ef__mgU24cache2_21() { return &___U3CU3Ef__mgU24cache2_21; }
	inline void set_U3CU3Ef__mgU24cache2_21(EventFunction_1_t64614563 * value)
	{
		___U3CU3Ef__mgU24cache2_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_22() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache3_22)); }
	inline EventFunction_1_t3256600500 * get_U3CU3Ef__mgU24cache3_22() const { return ___U3CU3Ef__mgU24cache3_22; }
	inline EventFunction_1_t3256600500 ** get_address_of_U3CU3Ef__mgU24cache3_22() { return &___U3CU3Ef__mgU24cache3_22; }
	inline void set_U3CU3Ef__mgU24cache3_22(EventFunction_1_t3256600500 * value)
	{
		___U3CU3Ef__mgU24cache3_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_23() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache4_23)); }
	inline EventFunction_1_t3111972472 * get_U3CU3Ef__mgU24cache4_23() const { return ___U3CU3Ef__mgU24cache4_23; }
	inline EventFunction_1_t3111972472 ** get_address_of_U3CU3Ef__mgU24cache4_23() { return &___U3CU3Ef__mgU24cache4_23; }
	inline void set_U3CU3Ef__mgU24cache4_23(EventFunction_1_t3111972472 * value)
	{
		___U3CU3Ef__mgU24cache4_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_24() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache5_24)); }
	inline EventFunction_1_t3587542510 * get_U3CU3Ef__mgU24cache5_24() const { return ___U3CU3Ef__mgU24cache5_24; }
	inline EventFunction_1_t3587542510 ** get_address_of_U3CU3Ef__mgU24cache5_24() { return &___U3CU3Ef__mgU24cache5_24; }
	inline void set_U3CU3Ef__mgU24cache5_24(EventFunction_1_t3587542510 * value)
	{
		___U3CU3Ef__mgU24cache5_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_25() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache6_25)); }
	inline EventFunction_1_t1977848392 * get_U3CU3Ef__mgU24cache6_25() const { return ___U3CU3Ef__mgU24cache6_25; }
	inline EventFunction_1_t1977848392 ** get_address_of_U3CU3Ef__mgU24cache6_25() { return &___U3CU3Ef__mgU24cache6_25; }
	inline void set_U3CU3Ef__mgU24cache6_25(EventFunction_1_t1977848392 * value)
	{
		___U3CU3Ef__mgU24cache6_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_26() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache7_26)); }
	inline EventFunction_1_t972960537 * get_U3CU3Ef__mgU24cache7_26() const { return ___U3CU3Ef__mgU24cache7_26; }
	inline EventFunction_1_t972960537 ** get_address_of_U3CU3Ef__mgU24cache7_26() { return &___U3CU3Ef__mgU24cache7_26; }
	inline void set_U3CU3Ef__mgU24cache7_26(EventFunction_1_t972960537 * value)
	{
		___U3CU3Ef__mgU24cache7_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_27() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache8_27)); }
	inline EventFunction_1_t3277009892 * get_U3CU3Ef__mgU24cache8_27() const { return ___U3CU3Ef__mgU24cache8_27; }
	inline EventFunction_1_t3277009892 ** get_address_of_U3CU3Ef__mgU24cache8_27() { return &___U3CU3Ef__mgU24cache8_27; }
	inline void set_U3CU3Ef__mgU24cache8_27(EventFunction_1_t3277009892 * value)
	{
		___U3CU3Ef__mgU24cache8_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_28() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache9_28)); }
	inline EventFunction_1_t2311673543 * get_U3CU3Ef__mgU24cache9_28() const { return ___U3CU3Ef__mgU24cache9_28; }
	inline EventFunction_1_t2311673543 ** get_address_of_U3CU3Ef__mgU24cache9_28() { return &___U3CU3Ef__mgU24cache9_28; }
	inline void set_U3CU3Ef__mgU24cache9_28(EventFunction_1_t2311673543 * value)
	{
		___U3CU3Ef__mgU24cache9_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_29() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheA_29)); }
	inline EventFunction_1_t2886331738 * get_U3CU3Ef__mgU24cacheA_29() const { return ___U3CU3Ef__mgU24cacheA_29; }
	inline EventFunction_1_t2886331738 ** get_address_of_U3CU3Ef__mgU24cacheA_29() { return &___U3CU3Ef__mgU24cacheA_29; }
	inline void set_U3CU3Ef__mgU24cacheA_29(EventFunction_1_t2886331738 * value)
	{
		___U3CU3Ef__mgU24cacheA_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_30() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheB_30)); }
	inline EventFunction_1_t2950825503 * get_U3CU3Ef__mgU24cacheB_30() const { return ___U3CU3Ef__mgU24cacheB_30; }
	inline EventFunction_1_t2950825503 ** get_address_of_U3CU3Ef__mgU24cacheB_30() { return &___U3CU3Ef__mgU24cacheB_30; }
	inline void set_U3CU3Ef__mgU24cacheB_30(EventFunction_1_t2950825503 * value)
	{
		___U3CU3Ef__mgU24cacheB_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_31() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheC_31)); }
	inline EventFunction_1_t955952873 * get_U3CU3Ef__mgU24cacheC_31() const { return ___U3CU3Ef__mgU24cacheC_31; }
	inline EventFunction_1_t955952873 ** get_address_of_U3CU3Ef__mgU24cacheC_31() { return &___U3CU3Ef__mgU24cacheC_31; }
	inline void set_U3CU3Ef__mgU24cacheC_31(EventFunction_1_t955952873 * value)
	{
		___U3CU3Ef__mgU24cacheC_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_32() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheD_32)); }
	inline EventFunction_1_t3373214253 * get_U3CU3Ef__mgU24cacheD_32() const { return ___U3CU3Ef__mgU24cacheD_32; }
	inline EventFunction_1_t3373214253 ** get_address_of_U3CU3Ef__mgU24cacheD_32() { return &___U3CU3Ef__mgU24cacheD_32; }
	inline void set_U3CU3Ef__mgU24cacheD_32(EventFunction_1_t3373214253 * value)
	{
		___U3CU3Ef__mgU24cacheD_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheE_33() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheE_33)); }
	inline EventFunction_1_t3912835512 * get_U3CU3Ef__mgU24cacheE_33() const { return ___U3CU3Ef__mgU24cacheE_33; }
	inline EventFunction_1_t3912835512 ** get_address_of_U3CU3Ef__mgU24cacheE_33() { return &___U3CU3Ef__mgU24cacheE_33; }
	inline void set_U3CU3Ef__mgU24cacheE_33(EventFunction_1_t3912835512 * value)
	{
		___U3CU3Ef__mgU24cacheE_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheE_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheF_34() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheF_34)); }
	inline EventFunction_1_t1475332338 * get_U3CU3Ef__mgU24cacheF_34() const { return ___U3CU3Ef__mgU24cacheF_34; }
	inline EventFunction_1_t1475332338 ** get_address_of_U3CU3Ef__mgU24cacheF_34() { return &___U3CU3Ef__mgU24cacheF_34; }
	inline void set_U3CU3Ef__mgU24cacheF_34(EventFunction_1_t1475332338 * value)
	{
		___U3CU3Ef__mgU24cacheF_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheF_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache10_35() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache10_35)); }
	inline EventFunction_1_t2658898854 * get_U3CU3Ef__mgU24cache10_35() const { return ___U3CU3Ef__mgU24cache10_35; }
	inline EventFunction_1_t2658898854 ** get_address_of_U3CU3Ef__mgU24cache10_35() { return &___U3CU3Ef__mgU24cache10_35; }
	inline void set_U3CU3Ef__mgU24cache10_35(EventFunction_1_t2658898854 * value)
	{
		___U3CU3Ef__mgU24cache10_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache10_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECUTEEVENTS_T3484638744_H
#ifndef U3CASOBSERVABLEU3EC__ANONSTOREY5_3_T4293553857_H
#define U3CASOBSERVABLEU3EC__ANONSTOREY5_3_T4293553857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey5`3<System.Object,System.Object,System.Object>
struct  U3CAsObservableU3Ec__AnonStorey5_3_t4293553857  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityEvent`3<T0,T1,T2> UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey5`3::unityEvent
	UnityEvent_3_t2404744798 * ___unityEvent_0;

public:
	inline static int32_t get_offset_of_unityEvent_0() { return static_cast<int32_t>(offsetof(U3CAsObservableU3Ec__AnonStorey5_3_t4293553857, ___unityEvent_0)); }
	inline UnityEvent_3_t2404744798 * get_unityEvent_0() const { return ___unityEvent_0; }
	inline UnityEvent_3_t2404744798 ** get_address_of_unityEvent_0() { return &___unityEvent_0; }
	inline void set_unityEvent_0(UnityEvent_3_t2404744798 * value)
	{
		___unityEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___unityEvent_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CASOBSERVABLEU3EC__ANONSTOREY5_3_T4293553857_H
#ifndef OPERATOROBSERVABLEBASE_1_T344286290_H
#define OPERATOROBSERVABLEBASE_1_T344286290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1<UniRx.Pair`1<System.Object>>
struct  OperatorObservableBase_1_t344286290  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t344286290, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVABLEBASE_1_T344286290_H
#ifndef OPERATOROBSERVABLEBASE_1_T1183232841_H
#define OPERATOROBSERVABLEBASE_1_T1183232841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1<UniRx.Notification`1<System.Object>>
struct  OperatorObservableBase_1_t1183232841  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t1183232841, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVABLEBASE_1_T1183232841_H
#ifndef OPERATOROBSERVABLEBASE_1_T3820056813_H
#define OPERATOROBSERVABLEBASE_1_T3820056813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1<UniRx.IGroupedObservable`2<System.Object,System.Object>>
struct  OperatorObservableBase_1_t3820056813  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t3820056813, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVABLEBASE_1_T3820056813_H
#ifndef OPERATOROBSERVABLEBASE_1_T3354786123_H
#define OPERATOROBSERVABLEBASE_1_T3354786123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1<UniRx.FrameInterval`1<System.Object>>
struct  OperatorObservableBase_1_t3354786123  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t3354786123, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVABLEBASE_1_T3354786123_H
#ifndef OPERATOROBSERVERBASE_2_T59931874_H
#define OPERATOROBSERVERBASE_2_T59931874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObserverBase`2<System.Object,System.Object>
struct  OperatorObserverBase_2_t59931874  : public RuntimeObject
{
public:
	// UniRx.IObserver`1<TResult> modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.Operators.OperatorObserverBase`2::observer
	RuntimeObject* ___observer_0;
	// System.IDisposable UniRx.Operators.OperatorObserverBase`2::cancel
	RuntimeObject* ___cancel_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t59931874, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((&___observer_0), value);
	}

	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t59931874, ___cancel_1)); }
	inline RuntimeObject* get_cancel_1() const { return ___cancel_1; }
	inline RuntimeObject** get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(RuntimeObject* value)
	{
		___cancel_1 = value;
		Il2CppCodeGenWriteBarrier((&___cancel_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVERBASE_2_T59931874_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef U3CASOBSERVABLEU3EC__ANONSTOREY7_4_T135207142_H
#define U3CASOBSERVABLEU3EC__ANONSTOREY7_4_T135207142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey7`4<System.Object,System.Object,System.Object,System.Object>
struct  U3CAsObservableU3Ec__AnonStorey7_4_t135207142  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityEvent`4<T0,T1,T2,T3> UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey7`4::unityEvent
	UnityEvent_4_t4085588227 * ___unityEvent_0;

public:
	inline static int32_t get_offset_of_unityEvent_0() { return static_cast<int32_t>(offsetof(U3CAsObservableU3Ec__AnonStorey7_4_t135207142, ___unityEvent_0)); }
	inline UnityEvent_4_t4085588227 * get_unityEvent_0() const { return ___unityEvent_0; }
	inline UnityEvent_4_t4085588227 ** get_address_of_unityEvent_0() { return &___unityEvent_0; }
	inline void set_unityEvent_0(UnityEvent_4_t4085588227 * value)
	{
		___unityEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___unityEvent_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CASOBSERVABLEU3EC__ANONSTOREY7_4_T135207142_H
#ifndef STUBS_1_T2084668772_H
#define STUBS_1_T2084668772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Stubs`1<System.Object>
struct  Stubs_1_t2084668772  : public RuntimeObject
{
public:

public:
};

struct Stubs_1_t2084668772_StaticFields
{
public:
	// System.Action`1<T> UniRx.Stubs`1::Ignore
	Action_1_t3252573759 * ___Ignore_0;
	// System.Func`2<T,T> UniRx.Stubs`1::Identity
	Func_2_t2447130374 * ___Identity_1;
	// System.Action`2<System.Exception,T> UniRx.Stubs`1::Throw
	Action_2_t2161070725 * ___Throw_2;

public:
	inline static int32_t get_offset_of_Ignore_0() { return static_cast<int32_t>(offsetof(Stubs_1_t2084668772_StaticFields, ___Ignore_0)); }
	inline Action_1_t3252573759 * get_Ignore_0() const { return ___Ignore_0; }
	inline Action_1_t3252573759 ** get_address_of_Ignore_0() { return &___Ignore_0; }
	inline void set_Ignore_0(Action_1_t3252573759 * value)
	{
		___Ignore_0 = value;
		Il2CppCodeGenWriteBarrier((&___Ignore_0), value);
	}

	inline static int32_t get_offset_of_Identity_1() { return static_cast<int32_t>(offsetof(Stubs_1_t2084668772_StaticFields, ___Identity_1)); }
	inline Func_2_t2447130374 * get_Identity_1() const { return ___Identity_1; }
	inline Func_2_t2447130374 ** get_address_of_Identity_1() { return &___Identity_1; }
	inline void set_Identity_1(Func_2_t2447130374 * value)
	{
		___Identity_1 = value;
		Il2CppCodeGenWriteBarrier((&___Identity_1), value);
	}

	inline static int32_t get_offset_of_Throw_2() { return static_cast<int32_t>(offsetof(Stubs_1_t2084668772_StaticFields, ___Throw_2)); }
	inline Action_2_t2161070725 * get_Throw_2() const { return ___Throw_2; }
	inline Action_2_t2161070725 ** get_address_of_Throw_2() { return &___Throw_2; }
	inline void set_Throw_2(Action_2_t2161070725 * value)
	{
		___Throw_2 = value;
		Il2CppCodeGenWriteBarrier((&___Throw_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STUBS_1_T2084668772_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef UNITYEVENTEXTENSIONS_T3448704846_H
#define UNITYEVENTEXTENSIONS_T3448704846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEventExtensions
struct  UnityEventExtensions_t3448704846  : public RuntimeObject
{
public:

public:
};

struct UnityEventExtensions_t3448704846_StaticFields
{
public:
	// System.Func`2<System.Action,UnityEngine.Events.UnityAction> UniRx.UnityEventExtensions::<>f__am$cache0
	Func_2_t157146996 * ___U3CU3Ef__amU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(UnityEventExtensions_t3448704846_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t157146996 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t157146996 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t157146996 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTEXTENSIONS_T3448704846_H
#ifndef COLLECTION_1_T2024462082_H
#define COLLECTION_1_T2024462082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<System.Object>
struct  Collection_1_t2024462082  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::list
	RuntimeObject* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1::syncRoot
	RuntimeObject * ___syncRoot_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Collection_1_t2024462082, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_t2024462082, ___syncRoot_1)); }
	inline RuntimeObject * get_syncRoot_1() const { return ___syncRoot_1; }
	inline RuntimeObject ** get_address_of_syncRoot_1() { return &___syncRoot_1; }
	inline void set_syncRoot_1(RuntimeObject * value)
	{
		___syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_T2024462082_H
#ifndef U3CASOBSERVABLEU3EC__ANONSTOREY3_2_T3918319924_H
#define U3CASOBSERVABLEU3EC__ANONSTOREY3_2_T3918319924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey3`2<System.Object,System.Object>
struct  U3CAsObservableU3Ec__AnonStorey3_2_t3918319924  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityEvent`2<T0,T1> UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey3`2::unityEvent
	UnityEvent_2_t614268397 * ___unityEvent_0;

public:
	inline static int32_t get_offset_of_unityEvent_0() { return static_cast<int32_t>(offsetof(U3CAsObservableU3Ec__AnonStorey3_2_t3918319924, ___unityEvent_0)); }
	inline UnityEvent_2_t614268397 * get_unityEvent_0() const { return ___unityEvent_0; }
	inline UnityEvent_2_t614268397 ** get_address_of_unityEvent_0() { return &___unityEvent_0; }
	inline void set_unityEvent_0(UnityEvent_2_t614268397 * value)
	{
		___unityEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___unityEvent_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CASOBSERVABLEU3EC__ANONSTOREY3_2_T3918319924_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef OPERATOROBSERVABLEBASE_1_T3168943961_H
#define OPERATOROBSERVABLEBASE_1_T3168943961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1<UniRx.Timestamped`1<System.Object>>
struct  OperatorObservableBase_1_t3168943961  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t3168943961, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVABLEBASE_1_T3168943961_H
#ifndef OPERATOROBSERVABLEBASE_1_T3236765911_H
#define OPERATOROBSERVABLEBASE_1_T3236765911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1<UniRx.TimeInterval`1<System.Object>>
struct  OperatorObservableBase_1_t3236765911  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t3236765911, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVABLEBASE_1_T3236765911_H
#ifndef OPERATOROBSERVABLEBASE_1_T701925029_H
#define OPERATOROBSERVABLEBASE_1_T701925029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1<UniRx.EventPattern`1<System.Object>>
struct  OperatorObservableBase_1_t701925029  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t701925029, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVABLEBASE_1_T701925029_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef IMMUTABLELIST_1_T1553399856_H
#define IMMUTABLELIST_1_T1553399856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.ImmutableList`1<System.Func`2<System.Object,UniRx.IObservable`1<UniRx.Unit>>>
struct  ImmutableList_1_t1553399856  : public RuntimeObject
{
public:
	// T[] UniRx.InternalUtil.ImmutableList`1::data
	Func_2U5BU5D_t1724110606* ___data_1;

public:
	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(ImmutableList_1_t1553399856, ___data_1)); }
	inline Func_2U5BU5D_t1724110606* get_data_1() const { return ___data_1; }
	inline Func_2U5BU5D_t1724110606** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(Func_2U5BU5D_t1724110606* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

struct ImmutableList_1_t1553399856_StaticFields
{
public:
	// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1::Empty
	ImmutableList_1_t1553399856 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(ImmutableList_1_t1553399856_StaticFields, ___Empty_0)); }
	inline ImmutableList_1_t1553399856 * get_Empty_0() const { return ___Empty_0; }
	inline ImmutableList_1_t1553399856 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(ImmutableList_1_t1553399856 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMMUTABLELIST_1_T1553399856_H
#ifndef LIST_1_T777473367_H
#define LIST_1_T777473367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Transform>
struct  List_1_t777473367  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TransformU5BU5D_t807237628* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t777473367, ____items_1)); }
	inline TransformU5BU5D_t807237628* get__items_1() const { return ____items_1; }
	inline TransformU5BU5D_t807237628** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TransformU5BU5D_t807237628* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t777473367, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t777473367, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t777473367_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TransformU5BU5D_t807237628* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t777473367_StaticFields, ___EmptyArray_4)); }
	inline TransformU5BU5D_t807237628* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TransformU5BU5D_t807237628** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TransformU5BU5D_t807237628* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T777473367_H
#ifndef DICTIONARY_2_T1229485932_H
#define DICTIONARY_2_T1229485932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
struct  Dictionary_2_t1229485932  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	TypeU5BU5D_t3940880105* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ObjectU5BU5D_t2843939325* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1229485932, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1229485932, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1229485932, ___keySlots_6)); }
	inline TypeU5BU5D_t3940880105* get_keySlots_6() const { return ___keySlots_6; }
	inline TypeU5BU5D_t3940880105** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(TypeU5BU5D_t3940880105* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1229485932, ___valueSlots_7)); }
	inline ObjectU5BU5D_t2843939325* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ObjectU5BU5D_t2843939325** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ObjectU5BU5D_t2843939325* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1229485932, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1229485932, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1229485932, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1229485932, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1229485932, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1229485932, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1229485932, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1229485932_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t3124434336 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1229485932_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t3124434336 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t3124434336 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t3124434336 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1229485932_H
#ifndef OPERATOROBSERVABLEBASE_1_T354719556_H
#define OPERATOROBSERVABLEBASE_1_T354719556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1<UniRx.Unit>
struct  OperatorObservableBase_1_t354719556  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t354719556, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVABLEBASE_1_T354719556_H
#ifndef SCHEDULER_T1572131957_H
#define SCHEDULER_T1572131957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler
struct  Scheduler_t1572131957  : public RuntimeObject
{
public:

public:
};

struct Scheduler_t1572131957_StaticFields
{
public:
	// UniRx.IScheduler UniRx.Scheduler::CurrentThread
	RuntimeObject* ___CurrentThread_0;
	// UniRx.IScheduler UniRx.Scheduler::Immediate
	RuntimeObject* ___Immediate_1;
	// UniRx.IScheduler UniRx.Scheduler::ThreadPool
	RuntimeObject* ___ThreadPool_2;
	// UniRx.IScheduler UniRx.Scheduler::mainThread
	RuntimeObject* ___mainThread_3;
	// UniRx.IScheduler UniRx.Scheduler::mainThreadIgnoreTimeScale
	RuntimeObject* ___mainThreadIgnoreTimeScale_4;
	// UniRx.IScheduler UniRx.Scheduler::mainThreadFixedUpdate
	RuntimeObject* ___mainThreadFixedUpdate_5;
	// UniRx.IScheduler UniRx.Scheduler::mainThreadEndOfFrame
	RuntimeObject* ___mainThreadEndOfFrame_6;

public:
	inline static int32_t get_offset_of_CurrentThread_0() { return static_cast<int32_t>(offsetof(Scheduler_t1572131957_StaticFields, ___CurrentThread_0)); }
	inline RuntimeObject* get_CurrentThread_0() const { return ___CurrentThread_0; }
	inline RuntimeObject** get_address_of_CurrentThread_0() { return &___CurrentThread_0; }
	inline void set_CurrentThread_0(RuntimeObject* value)
	{
		___CurrentThread_0 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentThread_0), value);
	}

	inline static int32_t get_offset_of_Immediate_1() { return static_cast<int32_t>(offsetof(Scheduler_t1572131957_StaticFields, ___Immediate_1)); }
	inline RuntimeObject* get_Immediate_1() const { return ___Immediate_1; }
	inline RuntimeObject** get_address_of_Immediate_1() { return &___Immediate_1; }
	inline void set_Immediate_1(RuntimeObject* value)
	{
		___Immediate_1 = value;
		Il2CppCodeGenWriteBarrier((&___Immediate_1), value);
	}

	inline static int32_t get_offset_of_ThreadPool_2() { return static_cast<int32_t>(offsetof(Scheduler_t1572131957_StaticFields, ___ThreadPool_2)); }
	inline RuntimeObject* get_ThreadPool_2() const { return ___ThreadPool_2; }
	inline RuntimeObject** get_address_of_ThreadPool_2() { return &___ThreadPool_2; }
	inline void set_ThreadPool_2(RuntimeObject* value)
	{
		___ThreadPool_2 = value;
		Il2CppCodeGenWriteBarrier((&___ThreadPool_2), value);
	}

	inline static int32_t get_offset_of_mainThread_3() { return static_cast<int32_t>(offsetof(Scheduler_t1572131957_StaticFields, ___mainThread_3)); }
	inline RuntimeObject* get_mainThread_3() const { return ___mainThread_3; }
	inline RuntimeObject** get_address_of_mainThread_3() { return &___mainThread_3; }
	inline void set_mainThread_3(RuntimeObject* value)
	{
		___mainThread_3 = value;
		Il2CppCodeGenWriteBarrier((&___mainThread_3), value);
	}

	inline static int32_t get_offset_of_mainThreadIgnoreTimeScale_4() { return static_cast<int32_t>(offsetof(Scheduler_t1572131957_StaticFields, ___mainThreadIgnoreTimeScale_4)); }
	inline RuntimeObject* get_mainThreadIgnoreTimeScale_4() const { return ___mainThreadIgnoreTimeScale_4; }
	inline RuntimeObject** get_address_of_mainThreadIgnoreTimeScale_4() { return &___mainThreadIgnoreTimeScale_4; }
	inline void set_mainThreadIgnoreTimeScale_4(RuntimeObject* value)
	{
		___mainThreadIgnoreTimeScale_4 = value;
		Il2CppCodeGenWriteBarrier((&___mainThreadIgnoreTimeScale_4), value);
	}

	inline static int32_t get_offset_of_mainThreadFixedUpdate_5() { return static_cast<int32_t>(offsetof(Scheduler_t1572131957_StaticFields, ___mainThreadFixedUpdate_5)); }
	inline RuntimeObject* get_mainThreadFixedUpdate_5() const { return ___mainThreadFixedUpdate_5; }
	inline RuntimeObject** get_address_of_mainThreadFixedUpdate_5() { return &___mainThreadFixedUpdate_5; }
	inline void set_mainThreadFixedUpdate_5(RuntimeObject* value)
	{
		___mainThreadFixedUpdate_5 = value;
		Il2CppCodeGenWriteBarrier((&___mainThreadFixedUpdate_5), value);
	}

	inline static int32_t get_offset_of_mainThreadEndOfFrame_6() { return static_cast<int32_t>(offsetof(Scheduler_t1572131957_StaticFields, ___mainThreadEndOfFrame_6)); }
	inline RuntimeObject* get_mainThreadEndOfFrame_6() const { return ___mainThreadEndOfFrame_6; }
	inline RuntimeObject** get_address_of_mainThreadEndOfFrame_6() { return &___mainThreadEndOfFrame_6; }
	inline void set_mainThreadEndOfFrame_6(RuntimeObject* value)
	{
		___mainThreadEndOfFrame_6 = value;
		Il2CppCodeGenWriteBarrier((&___mainThreadEndOfFrame_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEDULER_T1572131957_H
#ifndef OPERATOROBSERVABLEBASE_1_T72576253_H
#define OPERATOROBSERVABLEBASE_1_T72576253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1<System.Object>
struct  OperatorObservableBase_1_t72576253  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t72576253, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVABLEBASE_1_T72576253_H
#ifndef ABSTRACTEVENTDATA_T4171500731_H
#define ABSTRACTEVENTDATA_T4171500731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t4171500731  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t4171500731, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEVENTDATA_T4171500731_H
#ifndef ANONYMOUSSUBJECT_2_T3772911333_H
#define ANONYMOUSSUBJECT_2_T3772911333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.SubjectExtensions/AnonymousSubject`2<System.Object,System.Object>
struct  AnonymousSubject_2_t3772911333  : public RuntimeObject
{
public:
	// UniRx.IObserver`1<T> UniRx.SubjectExtensions/AnonymousSubject`2::observer
	RuntimeObject* ___observer_0;
	// UniRx.IObservable`1<U> UniRx.SubjectExtensions/AnonymousSubject`2::observable
	RuntimeObject* ___observable_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(AnonymousSubject_2_t3772911333, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((&___observer_0), value);
	}

	inline static int32_t get_offset_of_observable_1() { return static_cast<int32_t>(offsetof(AnonymousSubject_2_t3772911333, ___observable_1)); }
	inline RuntimeObject* get_observable_1() const { return ___observable_1; }
	inline RuntimeObject** get_address_of_observable_1() { return &___observable_1; }
	inline void set_observable_1(RuntimeObject* value)
	{
		___observable_1 = value;
		Il2CppCodeGenWriteBarrier((&___observable_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANONYMOUSSUBJECT_2_T3772911333_H
#ifndef STUBS_T4137823816_H
#define STUBS_T4137823816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Stubs
struct  Stubs_t4137823816  : public RuntimeObject
{
public:

public:
};

struct Stubs_t4137823816_StaticFields
{
public:
	// System.Action UniRx.Stubs::Nop
	Action_t1264377477 * ___Nop_0;
	// System.Action`1<System.Exception> UniRx.Stubs::Throw
	Action_1_t1609204844 * ___Throw_1;

public:
	inline static int32_t get_offset_of_Nop_0() { return static_cast<int32_t>(offsetof(Stubs_t4137823816_StaticFields, ___Nop_0)); }
	inline Action_t1264377477 * get_Nop_0() const { return ___Nop_0; }
	inline Action_t1264377477 ** get_address_of_Nop_0() { return &___Nop_0; }
	inline void set_Nop_0(Action_t1264377477 * value)
	{
		___Nop_0 = value;
		Il2CppCodeGenWriteBarrier((&___Nop_0), value);
	}

	inline static int32_t get_offset_of_Throw_1() { return static_cast<int32_t>(offsetof(Stubs_t4137823816_StaticFields, ___Throw_1)); }
	inline Action_1_t1609204844 * get_Throw_1() const { return ___Throw_1; }
	inline Action_1_t1609204844 ** get_address_of_Throw_1() { return &___Throw_1; }
	inline void set_Throw_1(Action_1_t1609204844 * value)
	{
		___Throw_1 = value;
		Il2CppCodeGenWriteBarrier((&___Throw_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STUBS_T4137823816_H
#ifndef OPERATOROBSERVABLEBASE_1_T729037393_H
#define OPERATOROBSERVABLEBASE_1_T729037393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1<System.Int64>
struct  OperatorObservableBase_1_t729037393  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t729037393, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVABLEBASE_1_T729037393_H
#ifndef ASYNCMESSAGEBROKER_T3406290114_H
#define ASYNCMESSAGEBROKER_T3406290114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.AsyncMessageBroker
struct  AsyncMessageBroker_t3406290114  : public RuntimeObject
{
public:
	// System.Boolean UniRx.AsyncMessageBroker::isDisposed
	bool ___isDisposed_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Object> UniRx.AsyncMessageBroker::notifiers
	Dictionary_2_t1229485932 * ___notifiers_2;

public:
	inline static int32_t get_offset_of_isDisposed_1() { return static_cast<int32_t>(offsetof(AsyncMessageBroker_t3406290114, ___isDisposed_1)); }
	inline bool get_isDisposed_1() const { return ___isDisposed_1; }
	inline bool* get_address_of_isDisposed_1() { return &___isDisposed_1; }
	inline void set_isDisposed_1(bool value)
	{
		___isDisposed_1 = value;
	}

	inline static int32_t get_offset_of_notifiers_2() { return static_cast<int32_t>(offsetof(AsyncMessageBroker_t3406290114, ___notifiers_2)); }
	inline Dictionary_2_t1229485932 * get_notifiers_2() const { return ___notifiers_2; }
	inline Dictionary_2_t1229485932 ** get_address_of_notifiers_2() { return &___notifiers_2; }
	inline void set_notifiers_2(Dictionary_2_t1229485932 * value)
	{
		___notifiers_2 = value;
		Il2CppCodeGenWriteBarrier((&___notifiers_2), value);
	}
};

struct AsyncMessageBroker_t3406290114_StaticFields
{
public:
	// UniRx.IAsyncMessageBroker UniRx.AsyncMessageBroker::Default
	RuntimeObject* ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(AsyncMessageBroker_t3406290114_StaticFields, ___Default_0)); }
	inline RuntimeObject* get_Default_0() const { return ___Default_0; }
	inline RuntimeObject** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(RuntimeObject* value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((&___Default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCMESSAGEBROKER_T3406290114_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef COLLECTIONREMOVEEVENT_1_T1986941660_H
#define COLLECTIONREMOVEEVENT_1_T1986941660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CollectionRemoveEvent`1<System.Object>
struct  CollectionRemoveEvent_1_t1986941660 
{
public:
	union
	{
		struct
		{
			// System.Int32 UniRx.CollectionRemoveEvent`1::<Index>k__BackingField
			int32_t ___U3CIndexU3Ek__BackingField_0;
			// T UniRx.CollectionRemoveEvent`1::<Value>k__BackingField
			RuntimeObject * ___U3CValueU3Ek__BackingField_1;
		};
		uint8_t CollectionRemoveEvent_1_t1575056731__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CIndexU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CollectionRemoveEvent_1_t1986941660, ___U3CIndexU3Ek__BackingField_0)); }
	inline int32_t get_U3CIndexU3Ek__BackingField_0() const { return ___U3CIndexU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIndexU3Ek__BackingField_0() { return &___U3CIndexU3Ek__BackingField_0; }
	inline void set_U3CIndexU3Ek__BackingField_0(int32_t value)
	{
		___U3CIndexU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CollectionRemoveEvent_1_t1986941660, ___U3CValueU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONREMOVEEVENT_1_T1986941660_H
#ifndef FROMEVENTPATTERNOBSERVABLE_2_T3314993416_H
#define FROMEVENTPATTERNOBSERVABLE_2_T3314993416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FromEventPatternObservable`2<System.Object,System.Object>
struct  FromEventPatternObservable_2_t3314993416  : public OperatorObservableBase_1_t701925029
{
public:
	// System.Func`2<System.EventHandler`1<TEventArgs>,TDelegate> UniRx.Operators.FromEventPatternObservable`2::conversion
	Func_2_t663966201 * ___conversion_1;
	// System.Action`1<TDelegate> UniRx.Operators.FromEventPatternObservable`2::addHandler
	Action_1_t3252573759 * ___addHandler_2;
	// System.Action`1<TDelegate> UniRx.Operators.FromEventPatternObservable`2::removeHandler
	Action_1_t3252573759 * ___removeHandler_3;

public:
	inline static int32_t get_offset_of_conversion_1() { return static_cast<int32_t>(offsetof(FromEventPatternObservable_2_t3314993416, ___conversion_1)); }
	inline Func_2_t663966201 * get_conversion_1() const { return ___conversion_1; }
	inline Func_2_t663966201 ** get_address_of_conversion_1() { return &___conversion_1; }
	inline void set_conversion_1(Func_2_t663966201 * value)
	{
		___conversion_1 = value;
		Il2CppCodeGenWriteBarrier((&___conversion_1), value);
	}

	inline static int32_t get_offset_of_addHandler_2() { return static_cast<int32_t>(offsetof(FromEventPatternObservable_2_t3314993416, ___addHandler_2)); }
	inline Action_1_t3252573759 * get_addHandler_2() const { return ___addHandler_2; }
	inline Action_1_t3252573759 ** get_address_of_addHandler_2() { return &___addHandler_2; }
	inline void set_addHandler_2(Action_1_t3252573759 * value)
	{
		___addHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___addHandler_2), value);
	}

	inline static int32_t get_offset_of_removeHandler_3() { return static_cast<int32_t>(offsetof(FromEventPatternObservable_2_t3314993416, ___removeHandler_3)); }
	inline Action_1_t3252573759 * get_removeHandler_3() const { return ___removeHandler_3; }
	inline Action_1_t3252573759 ** get_address_of_removeHandler_3() { return &___removeHandler_3; }
	inline void set_removeHandler_3(Action_1_t3252573759 * value)
	{
		___removeHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___removeHandler_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMEVENTPATTERNOBSERVABLE_2_T3314993416_H
#ifndef TUPLE_3_T3228981166_H
#define TUPLE_3_T3228981166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple`3<System.Object,System.Object,System.Object>
struct  Tuple_3_t3228981166 
{
public:
	// T1 UniRx.Tuple`3::item1
	RuntimeObject * ___item1_0;
	// T2 UniRx.Tuple`3::item2
	RuntimeObject * ___item2_1;
	// T3 UniRx.Tuple`3::item3
	RuntimeObject * ___item3_2;

public:
	inline static int32_t get_offset_of_item1_0() { return static_cast<int32_t>(offsetof(Tuple_3_t3228981166, ___item1_0)); }
	inline RuntimeObject * get_item1_0() const { return ___item1_0; }
	inline RuntimeObject ** get_address_of_item1_0() { return &___item1_0; }
	inline void set_item1_0(RuntimeObject * value)
	{
		___item1_0 = value;
		Il2CppCodeGenWriteBarrier((&___item1_0), value);
	}

	inline static int32_t get_offset_of_item2_1() { return static_cast<int32_t>(offsetof(Tuple_3_t3228981166, ___item2_1)); }
	inline RuntimeObject * get_item2_1() const { return ___item2_1; }
	inline RuntimeObject ** get_address_of_item2_1() { return &___item2_1; }
	inline void set_item2_1(RuntimeObject * value)
	{
		___item2_1 = value;
		Il2CppCodeGenWriteBarrier((&___item2_1), value);
	}

	inline static int32_t get_offset_of_item3_2() { return static_cast<int32_t>(offsetof(Tuple_3_t3228981166, ___item3_2)); }
	inline RuntimeObject * get_item3_2() const { return ___item3_2; }
	inline RuntimeObject ** get_address_of_item3_2() { return &___item3_2; }
	inline void set_item3_2(RuntimeObject * value)
	{
		___item3_2 = value;
		Il2CppCodeGenWriteBarrier((&___item3_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUPLE_3_T3228981166_H
#ifndef TUPLE_5_T2125722268_H
#define TUPLE_5_T2125722268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Tuple_5_t2125722268 
{
public:
	// T1 UniRx.Tuple`5::item1
	RuntimeObject * ___item1_0;
	// T2 UniRx.Tuple`5::item2
	RuntimeObject * ___item2_1;
	// T3 UniRx.Tuple`5::item3
	RuntimeObject * ___item3_2;
	// T4 UniRx.Tuple`5::item4
	RuntimeObject * ___item4_3;
	// T5 UniRx.Tuple`5::item5
	RuntimeObject * ___item5_4;

public:
	inline static int32_t get_offset_of_item1_0() { return static_cast<int32_t>(offsetof(Tuple_5_t2125722268, ___item1_0)); }
	inline RuntimeObject * get_item1_0() const { return ___item1_0; }
	inline RuntimeObject ** get_address_of_item1_0() { return &___item1_0; }
	inline void set_item1_0(RuntimeObject * value)
	{
		___item1_0 = value;
		Il2CppCodeGenWriteBarrier((&___item1_0), value);
	}

	inline static int32_t get_offset_of_item2_1() { return static_cast<int32_t>(offsetof(Tuple_5_t2125722268, ___item2_1)); }
	inline RuntimeObject * get_item2_1() const { return ___item2_1; }
	inline RuntimeObject ** get_address_of_item2_1() { return &___item2_1; }
	inline void set_item2_1(RuntimeObject * value)
	{
		___item2_1 = value;
		Il2CppCodeGenWriteBarrier((&___item2_1), value);
	}

	inline static int32_t get_offset_of_item3_2() { return static_cast<int32_t>(offsetof(Tuple_5_t2125722268, ___item3_2)); }
	inline RuntimeObject * get_item3_2() const { return ___item3_2; }
	inline RuntimeObject ** get_address_of_item3_2() { return &___item3_2; }
	inline void set_item3_2(RuntimeObject * value)
	{
		___item3_2 = value;
		Il2CppCodeGenWriteBarrier((&___item3_2), value);
	}

	inline static int32_t get_offset_of_item4_3() { return static_cast<int32_t>(offsetof(Tuple_5_t2125722268, ___item4_3)); }
	inline RuntimeObject * get_item4_3() const { return ___item4_3; }
	inline RuntimeObject ** get_address_of_item4_3() { return &___item4_3; }
	inline void set_item4_3(RuntimeObject * value)
	{
		___item4_3 = value;
		Il2CppCodeGenWriteBarrier((&___item4_3), value);
	}

	inline static int32_t get_offset_of_item5_4() { return static_cast<int32_t>(offsetof(Tuple_5_t2125722268, ___item5_4)); }
	inline RuntimeObject * get_item5_4() const { return ___item5_4; }
	inline RuntimeObject ** get_address_of_item5_4() { return &___item5_4; }
	inline void set_item5_4(RuntimeObject * value)
	{
		___item5_4 = value;
		Il2CppCodeGenWriteBarrier((&___item5_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUPLE_5_T2125722268_H
#ifndef COLLECTIONADDEVENT_1_T1688054569_H
#define COLLECTIONADDEVENT_1_T1688054569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CollectionAddEvent`1<System.Object>
struct  CollectionAddEvent_1_t1688054569 
{
public:
	union
	{
		struct
		{
			// System.Int32 UniRx.CollectionAddEvent`1::<Index>k__BackingField
			int32_t ___U3CIndexU3Ek__BackingField_0;
			// T UniRx.CollectionAddEvent`1::<Value>k__BackingField
			RuntimeObject * ___U3CValueU3Ek__BackingField_1;
		};
		uint8_t CollectionAddEvent_1_t607257456__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CIndexU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CollectionAddEvent_1_t1688054569, ___U3CIndexU3Ek__BackingField_0)); }
	inline int32_t get_U3CIndexU3Ek__BackingField_0() const { return ___U3CIndexU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIndexU3Ek__BackingField_0() { return &___U3CIndexU3Ek__BackingField_0; }
	inline void set_U3CIndexU3Ek__BackingField_0(int32_t value)
	{
		___U3CIndexU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CollectionAddEvent_1_t1688054569, ___U3CValueU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONADDEVENT_1_T1688054569_H
#ifndef TUPLE_4_T2649139811_H
#define TUPLE_4_T2649139811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>
struct  Tuple_4_t2649139811 
{
public:
	// T1 UniRx.Tuple`4::item1
	RuntimeObject * ___item1_0;
	// T2 UniRx.Tuple`4::item2
	RuntimeObject * ___item2_1;
	// T3 UniRx.Tuple`4::item3
	RuntimeObject * ___item3_2;
	// T4 UniRx.Tuple`4::item4
	RuntimeObject * ___item4_3;

public:
	inline static int32_t get_offset_of_item1_0() { return static_cast<int32_t>(offsetof(Tuple_4_t2649139811, ___item1_0)); }
	inline RuntimeObject * get_item1_0() const { return ___item1_0; }
	inline RuntimeObject ** get_address_of_item1_0() { return &___item1_0; }
	inline void set_item1_0(RuntimeObject * value)
	{
		___item1_0 = value;
		Il2CppCodeGenWriteBarrier((&___item1_0), value);
	}

	inline static int32_t get_offset_of_item2_1() { return static_cast<int32_t>(offsetof(Tuple_4_t2649139811, ___item2_1)); }
	inline RuntimeObject * get_item2_1() const { return ___item2_1; }
	inline RuntimeObject ** get_address_of_item2_1() { return &___item2_1; }
	inline void set_item2_1(RuntimeObject * value)
	{
		___item2_1 = value;
		Il2CppCodeGenWriteBarrier((&___item2_1), value);
	}

	inline static int32_t get_offset_of_item3_2() { return static_cast<int32_t>(offsetof(Tuple_4_t2649139811, ___item3_2)); }
	inline RuntimeObject * get_item3_2() const { return ___item3_2; }
	inline RuntimeObject ** get_address_of_item3_2() { return &___item3_2; }
	inline void set_item3_2(RuntimeObject * value)
	{
		___item3_2 = value;
		Il2CppCodeGenWriteBarrier((&___item3_2), value);
	}

	inline static int32_t get_offset_of_item4_3() { return static_cast<int32_t>(offsetof(Tuple_4_t2649139811, ___item4_3)); }
	inline RuntimeObject * get_item4_3() const { return ___item4_3; }
	inline RuntimeObject ** get_address_of_item4_3() { return &___item4_3; }
	inline void set_item4_3(RuntimeObject * value)
	{
		___item4_3 = value;
		Il2CppCodeGenWriteBarrier((&___item4_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUPLE_4_T2649139811_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef TUPLE_2_T350234673_H
#define TUPLE_2_T350234673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple`2<System.Object,System.Object>
struct  Tuple_2_t350234673 
{
public:
	// T1 UniRx.Tuple`2::item1
	RuntimeObject * ___item1_0;
	// T2 UniRx.Tuple`2::item2
	RuntimeObject * ___item2_1;

public:
	inline static int32_t get_offset_of_item1_0() { return static_cast<int32_t>(offsetof(Tuple_2_t350234673, ___item1_0)); }
	inline RuntimeObject * get_item1_0() const { return ___item1_0; }
	inline RuntimeObject ** get_address_of_item1_0() { return &___item1_0; }
	inline void set_item1_0(RuntimeObject * value)
	{
		___item1_0 = value;
		Il2CppCodeGenWriteBarrier((&___item1_0), value);
	}

	inline static int32_t get_offset_of_item2_1() { return static_cast<int32_t>(offsetof(Tuple_2_t350234673, ___item2_1)); }
	inline RuntimeObject * get_item2_1() const { return ___item2_1; }
	inline RuntimeObject ** get_address_of_item2_1() { return &___item2_1; }
	inline void set_item2_1(RuntimeObject * value)
	{
		___item2_1 = value;
		Il2CppCodeGenWriteBarrier((&___item2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUPLE_2_T350234673_H
#ifndef UNIT_T3362249467_H
#define UNIT_T3362249467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Unit
struct  Unit_t3362249467 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Unit_t3362249467__padding[1];
	};

public:
};

struct Unit_t3362249467_StaticFields
{
public:
	// UniRx.Unit UniRx.Unit::default
	Unit_t3362249467  ___default_0;

public:
	inline static int32_t get_offset_of_default_0() { return static_cast<int32_t>(offsetof(Unit_t3362249467_StaticFields, ___default_0)); }
	inline Unit_t3362249467  get_default_0() const { return ___default_0; }
	inline Unit_t3362249467 * get_address_of_default_0() { return &___default_0; }
	inline void set_default_0(Unit_t3362249467  value)
	{
		___default_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIT_T3362249467_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef REACTIVECOLLECTION_1_T4090598151_H
#define REACTIVECOLLECTION_1_T4090598151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveCollection`1<System.Object>
struct  ReactiveCollection_1_t4090598151  : public Collection_1_t2024462082
{
public:
	// System.Boolean UniRx.ReactiveCollection`1::isDisposed
	bool ___isDisposed_2;
	// UniRx.Subject`1<System.Int32> UniRx.ReactiveCollection`1::countChanged
	Subject_1_t3039602140 * ___countChanged_3;
	// UniRx.Subject`1<UniRx.Unit> UniRx.ReactiveCollection`1::collectionReset
	Subject_1_t3450905854 * ___collectionReset_4;
	// UniRx.Subject`1<UniRx.CollectionAddEvent`1<T>> UniRx.ReactiveCollection`1::collectionAdd
	Subject_1_t1776710956 * ___collectionAdd_5;
	// UniRx.Subject`1<UniRx.CollectionMoveEvent`1<T>> UniRx.ReactiveCollection`1::collectionMove
	Subject_1_t4285020456 * ___collectionMove_6;
	// UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<T>> UniRx.ReactiveCollection`1::collectionRemove
	Subject_1_t2075598047 * ___collectionRemove_7;
	// UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<T>> UniRx.ReactiveCollection`1::collectionReplace
	Subject_1_t3715269461 * ___collectionReplace_8;
	// System.Boolean UniRx.ReactiveCollection`1::disposedValue
	bool ___disposedValue_9;

public:
	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveCollection_1_t4090598151, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_countChanged_3() { return static_cast<int32_t>(offsetof(ReactiveCollection_1_t4090598151, ___countChanged_3)); }
	inline Subject_1_t3039602140 * get_countChanged_3() const { return ___countChanged_3; }
	inline Subject_1_t3039602140 ** get_address_of_countChanged_3() { return &___countChanged_3; }
	inline void set_countChanged_3(Subject_1_t3039602140 * value)
	{
		___countChanged_3 = value;
		Il2CppCodeGenWriteBarrier((&___countChanged_3), value);
	}

	inline static int32_t get_offset_of_collectionReset_4() { return static_cast<int32_t>(offsetof(ReactiveCollection_1_t4090598151, ___collectionReset_4)); }
	inline Subject_1_t3450905854 * get_collectionReset_4() const { return ___collectionReset_4; }
	inline Subject_1_t3450905854 ** get_address_of_collectionReset_4() { return &___collectionReset_4; }
	inline void set_collectionReset_4(Subject_1_t3450905854 * value)
	{
		___collectionReset_4 = value;
		Il2CppCodeGenWriteBarrier((&___collectionReset_4), value);
	}

	inline static int32_t get_offset_of_collectionAdd_5() { return static_cast<int32_t>(offsetof(ReactiveCollection_1_t4090598151, ___collectionAdd_5)); }
	inline Subject_1_t1776710956 * get_collectionAdd_5() const { return ___collectionAdd_5; }
	inline Subject_1_t1776710956 ** get_address_of_collectionAdd_5() { return &___collectionAdd_5; }
	inline void set_collectionAdd_5(Subject_1_t1776710956 * value)
	{
		___collectionAdd_5 = value;
		Il2CppCodeGenWriteBarrier((&___collectionAdd_5), value);
	}

	inline static int32_t get_offset_of_collectionMove_6() { return static_cast<int32_t>(offsetof(ReactiveCollection_1_t4090598151, ___collectionMove_6)); }
	inline Subject_1_t4285020456 * get_collectionMove_6() const { return ___collectionMove_6; }
	inline Subject_1_t4285020456 ** get_address_of_collectionMove_6() { return &___collectionMove_6; }
	inline void set_collectionMove_6(Subject_1_t4285020456 * value)
	{
		___collectionMove_6 = value;
		Il2CppCodeGenWriteBarrier((&___collectionMove_6), value);
	}

	inline static int32_t get_offset_of_collectionRemove_7() { return static_cast<int32_t>(offsetof(ReactiveCollection_1_t4090598151, ___collectionRemove_7)); }
	inline Subject_1_t2075598047 * get_collectionRemove_7() const { return ___collectionRemove_7; }
	inline Subject_1_t2075598047 ** get_address_of_collectionRemove_7() { return &___collectionRemove_7; }
	inline void set_collectionRemove_7(Subject_1_t2075598047 * value)
	{
		___collectionRemove_7 = value;
		Il2CppCodeGenWriteBarrier((&___collectionRemove_7), value);
	}

	inline static int32_t get_offset_of_collectionReplace_8() { return static_cast<int32_t>(offsetof(ReactiveCollection_1_t4090598151, ___collectionReplace_8)); }
	inline Subject_1_t3715269461 * get_collectionReplace_8() const { return ___collectionReplace_8; }
	inline Subject_1_t3715269461 ** get_address_of_collectionReplace_8() { return &___collectionReplace_8; }
	inline void set_collectionReplace_8(Subject_1_t3715269461 * value)
	{
		___collectionReplace_8 = value;
		Il2CppCodeGenWriteBarrier((&___collectionReplace_8), value);
	}

	inline static int32_t get_offset_of_disposedValue_9() { return static_cast<int32_t>(offsetof(ReactiveCollection_1_t4090598151, ___disposedValue_9)); }
	inline bool get_disposedValue_9() const { return ___disposedValue_9; }
	inline bool* get_address_of_disposedValue_9() { return &___disposedValue_9; }
	inline void set_disposedValue_9(bool value)
	{
		___disposedValue_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVECOLLECTION_1_T4090598151_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef MERGEOBSERVABLE_1_T1765447768_H
#define MERGEOBSERVABLE_1_T1765447768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.MergeObservable`1<System.Object>
struct  MergeObservable_1_t1765447768  : public OperatorObservableBase_1_t72576253
{
public:
	// UniRx.IObservable`1<UniRx.IObservable`1<T>> UniRx.Operators.MergeObservable`1::sources
	RuntimeObject* ___sources_1;
	// System.Int32 UniRx.Operators.MergeObservable`1::maxConcurrent
	int32_t ___maxConcurrent_2;

public:
	inline static int32_t get_offset_of_sources_1() { return static_cast<int32_t>(offsetof(MergeObservable_1_t1765447768, ___sources_1)); }
	inline RuntimeObject* get_sources_1() const { return ___sources_1; }
	inline RuntimeObject** get_address_of_sources_1() { return &___sources_1; }
	inline void set_sources_1(RuntimeObject* value)
	{
		___sources_1 = value;
		Il2CppCodeGenWriteBarrier((&___sources_1), value);
	}

	inline static int32_t get_offset_of_maxConcurrent_2() { return static_cast<int32_t>(offsetof(MergeObservable_1_t1765447768, ___maxConcurrent_2)); }
	inline int32_t get_maxConcurrent_2() const { return ___maxConcurrent_2; }
	inline int32_t* get_address_of_maxConcurrent_2() { return &___maxConcurrent_2; }
	inline void set_maxConcurrent_2(int32_t value)
	{
		___maxConcurrent_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MERGEOBSERVABLE_1_T1765447768_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef CANCELLATIONTOKEN_T1265546479_H
#define CANCELLATIONTOKEN_T1265546479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CancellationToken
struct  CancellationToken_t1265546479 
{
public:
	// UniRx.ICancelable UniRx.CancellationToken::source
	RuntimeObject* ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(CancellationToken_t1265546479, ___source_0)); }
	inline RuntimeObject* get_source_0() const { return ___source_0; }
	inline RuntimeObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(RuntimeObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}
};

struct CancellationToken_t1265546479_StaticFields
{
public:
	// UniRx.CancellationToken UniRx.CancellationToken::Empty
	CancellationToken_t1265546479  ___Empty_1;
	// UniRx.CancellationToken UniRx.CancellationToken::None
	CancellationToken_t1265546479  ___None_2;

public:
	inline static int32_t get_offset_of_Empty_1() { return static_cast<int32_t>(offsetof(CancellationToken_t1265546479_StaticFields, ___Empty_1)); }
	inline CancellationToken_t1265546479  get_Empty_1() const { return ___Empty_1; }
	inline CancellationToken_t1265546479 * get_address_of_Empty_1() { return &___Empty_1; }
	inline void set_Empty_1(CancellationToken_t1265546479  value)
	{
		___Empty_1 = value;
	}

	inline static int32_t get_offset_of_None_2() { return static_cast<int32_t>(offsetof(CancellationToken_t1265546479_StaticFields, ___None_2)); }
	inline CancellationToken_t1265546479  get_None_2() const { return ___None_2; }
	inline CancellationToken_t1265546479 * get_address_of_None_2() { return &___None_2; }
	inline void set_None_2(CancellationToken_t1265546479  value)
	{
		___None_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UniRx.CancellationToken
struct CancellationToken_t1265546479_marshaled_pinvoke
{
	RuntimeObject* ___source_0;
};
// Native definition for COM marshalling of UniRx.CancellationToken
struct CancellationToken_t1265546479_marshaled_com
{
	RuntimeObject* ___source_0;
};
#endif // CANCELLATIONTOKEN_T1265546479_H
#ifndef ANONYMOUSSUBJECT_1_T1300284131_H
#define ANONYMOUSSUBJECT_1_T1300284131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.SubjectExtensions/AnonymousSubject`1<System.Object>
struct  AnonymousSubject_1_t1300284131  : public AnonymousSubject_2_t3772911333
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANONYMOUSSUBJECT_1_T1300284131_H
#ifndef TUPLE_1_T4248013280_H
#define TUPLE_1_T4248013280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple`1<System.Object>
struct  Tuple_1_t4248013280 
{
public:
	// T1 UniRx.Tuple`1::item1
	RuntimeObject * ___item1_0;

public:
	inline static int32_t get_offset_of_item1_0() { return static_cast<int32_t>(offsetof(Tuple_1_t4248013280, ___item1_0)); }
	inline RuntimeObject * get_item1_0() const { return ___item1_0; }
	inline RuntimeObject ** get_address_of_item1_0() { return &___item1_0; }
	inline void set_item1_0(RuntimeObject * value)
	{
		___item1_0 = value;
		Il2CppCodeGenWriteBarrier((&___item1_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUPLE_1_T4248013280_H
#ifndef SCANOBSERVABLE_1_T1685372452_H
#define SCANOBSERVABLE_1_T1685372452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ScanObservable`1<System.Object>
struct  ScanObservable_1_t1685372452  : public OperatorObservableBase_1_t72576253
{
public:
	// UniRx.IObservable`1<TSource> UniRx.Operators.ScanObservable`1::source
	RuntimeObject* ___source_1;
	// System.Func`3<TSource,TSource,TSource> UniRx.Operators.ScanObservable`1::accumulator
	Func_3_t3398609381 * ___accumulator_2;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(ScanObservable_1_t1685372452, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_accumulator_2() { return static_cast<int32_t>(offsetof(ScanObservable_1_t1685372452, ___accumulator_2)); }
	inline Func_3_t3398609381 * get_accumulator_2() const { return ___accumulator_2; }
	inline Func_3_t3398609381 ** get_address_of_accumulator_2() { return &___accumulator_2; }
	inline void set_accumulator_2(Func_3_t3398609381 * value)
	{
		___accumulator_2 = value;
		Il2CppCodeGenWriteBarrier((&___accumulator_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANOBSERVABLE_1_T1685372452_H
#ifndef ONCOMPLETEDNOTIFICATION_T2296134618_H
#define ONCOMPLETEDNOTIFICATION_T2296134618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Notification`1/OnCompletedNotification<System.Object>
struct  OnCompletedNotification_t2296134618  : public Notification_1_t4190762752
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCOMPLETEDNOTIFICATION_T2296134618_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ONERRORNOTIFICATION_T1865595679_H
#define ONERRORNOTIFICATION_T1865595679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Notification`1/OnErrorNotification<System.Object>
struct  OnErrorNotification_t1865595679  : public Notification_1_t4190762752
{
public:
	// System.Exception UniRx.Notification`1/OnErrorNotification::exception
	Exception_t * ___exception_0;

public:
	inline static int32_t get_offset_of_exception_0() { return static_cast<int32_t>(offsetof(OnErrorNotification_t1865595679, ___exception_0)); }
	inline Exception_t * get_exception_0() const { return ___exception_0; }
	inline Exception_t ** get_address_of_exception_0() { return &___exception_0; }
	inline void set_exception_0(Exception_t * value)
	{
		___exception_0 = value;
		Il2CppCodeGenWriteBarrier((&___exception_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONERRORNOTIFICATION_T1865595679_H
#ifndef ONNEXTNOTIFICATION_T3334954344_H
#define ONNEXTNOTIFICATION_T3334954344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Notification`1/OnNextNotification<System.Object>
struct  OnNextNotification_t3334954344  : public Notification_1_t4190762752
{
public:
	// T UniRx.Notification`1/OnNextNotification::value
	RuntimeObject * ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(OnNextNotification_t3334954344, ___value_0)); }
	inline RuntimeObject * get_value_0() const { return ___value_0; }
	inline RuntimeObject ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(RuntimeObject * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONNEXTNOTIFICATION_T3334954344_H
#ifndef TUPLE_6_T2833421365_H
#define TUPLE_6_T2833421365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Tuple_6_t2833421365 
{
public:
	// T1 UniRx.Tuple`6::item1
	RuntimeObject * ___item1_0;
	// T2 UniRx.Tuple`6::item2
	RuntimeObject * ___item2_1;
	// T3 UniRx.Tuple`6::item3
	RuntimeObject * ___item3_2;
	// T4 UniRx.Tuple`6::item4
	RuntimeObject * ___item4_3;
	// T5 UniRx.Tuple`6::item5
	RuntimeObject * ___item5_4;
	// T6 UniRx.Tuple`6::item6
	RuntimeObject * ___item6_5;

public:
	inline static int32_t get_offset_of_item1_0() { return static_cast<int32_t>(offsetof(Tuple_6_t2833421365, ___item1_0)); }
	inline RuntimeObject * get_item1_0() const { return ___item1_0; }
	inline RuntimeObject ** get_address_of_item1_0() { return &___item1_0; }
	inline void set_item1_0(RuntimeObject * value)
	{
		___item1_0 = value;
		Il2CppCodeGenWriteBarrier((&___item1_0), value);
	}

	inline static int32_t get_offset_of_item2_1() { return static_cast<int32_t>(offsetof(Tuple_6_t2833421365, ___item2_1)); }
	inline RuntimeObject * get_item2_1() const { return ___item2_1; }
	inline RuntimeObject ** get_address_of_item2_1() { return &___item2_1; }
	inline void set_item2_1(RuntimeObject * value)
	{
		___item2_1 = value;
		Il2CppCodeGenWriteBarrier((&___item2_1), value);
	}

	inline static int32_t get_offset_of_item3_2() { return static_cast<int32_t>(offsetof(Tuple_6_t2833421365, ___item3_2)); }
	inline RuntimeObject * get_item3_2() const { return ___item3_2; }
	inline RuntimeObject ** get_address_of_item3_2() { return &___item3_2; }
	inline void set_item3_2(RuntimeObject * value)
	{
		___item3_2 = value;
		Il2CppCodeGenWriteBarrier((&___item3_2), value);
	}

	inline static int32_t get_offset_of_item4_3() { return static_cast<int32_t>(offsetof(Tuple_6_t2833421365, ___item4_3)); }
	inline RuntimeObject * get_item4_3() const { return ___item4_3; }
	inline RuntimeObject ** get_address_of_item4_3() { return &___item4_3; }
	inline void set_item4_3(RuntimeObject * value)
	{
		___item4_3 = value;
		Il2CppCodeGenWriteBarrier((&___item4_3), value);
	}

	inline static int32_t get_offset_of_item5_4() { return static_cast<int32_t>(offsetof(Tuple_6_t2833421365, ___item5_4)); }
	inline RuntimeObject * get_item5_4() const { return ___item5_4; }
	inline RuntimeObject ** get_address_of_item5_4() { return &___item5_4; }
	inline void set_item5_4(RuntimeObject * value)
	{
		___item5_4 = value;
		Il2CppCodeGenWriteBarrier((&___item5_4), value);
	}

	inline static int32_t get_offset_of_item6_5() { return static_cast<int32_t>(offsetof(Tuple_6_t2833421365, ___item6_5)); }
	inline RuntimeObject * get_item6_5() const { return ___item6_5; }
	inline RuntimeObject ** get_address_of_item6_5() { return &___item6_5; }
	inline void set_item6_5(RuntimeObject * value)
	{
		___item6_5 = value;
		Il2CppCodeGenWriteBarrier((&___item6_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUPLE_6_T2833421365_H
#ifndef UNITYEVENT_4_T4085588227_H
#define UNITYEVENT_4_T4085588227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
struct  UnityEvent_4_t4085588227  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`4::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_4_t4085588227, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_4_T4085588227_H
#ifndef UNITYEVENT_3_T2404744798_H
#define UNITYEVENT_3_T2404744798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
struct  UnityEvent_3_t2404744798  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t2404744798, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T2404744798_H
#ifndef DESCENDANTSENUMERABLE_T4167108050_H
#define DESCENDANTSENUMERABLE_T4167108050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/DescendantsEnumerable
struct  DescendantsEnumerable_t4167108050 
{
public:
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/DescendantsEnumerable::origin
	GameObject_t1113636619 * ___origin_1;
	// System.Boolean Unity.Linq.GameObjectExtensions/DescendantsEnumerable::withSelf
	bool ___withSelf_2;
	// System.Func`2<UnityEngine.Transform,System.Boolean> Unity.Linq.GameObjectExtensions/DescendantsEnumerable::descendIntoChildren
	Func_2_t1722577774 * ___descendIntoChildren_3;

public:
	inline static int32_t get_offset_of_origin_1() { return static_cast<int32_t>(offsetof(DescendantsEnumerable_t4167108050, ___origin_1)); }
	inline GameObject_t1113636619 * get_origin_1() const { return ___origin_1; }
	inline GameObject_t1113636619 ** get_address_of_origin_1() { return &___origin_1; }
	inline void set_origin_1(GameObject_t1113636619 * value)
	{
		___origin_1 = value;
		Il2CppCodeGenWriteBarrier((&___origin_1), value);
	}

	inline static int32_t get_offset_of_withSelf_2() { return static_cast<int32_t>(offsetof(DescendantsEnumerable_t4167108050, ___withSelf_2)); }
	inline bool get_withSelf_2() const { return ___withSelf_2; }
	inline bool* get_address_of_withSelf_2() { return &___withSelf_2; }
	inline void set_withSelf_2(bool value)
	{
		___withSelf_2 = value;
	}

	inline static int32_t get_offset_of_descendIntoChildren_3() { return static_cast<int32_t>(offsetof(DescendantsEnumerable_t4167108050, ___descendIntoChildren_3)); }
	inline Func_2_t1722577774 * get_descendIntoChildren_3() const { return ___descendIntoChildren_3; }
	inline Func_2_t1722577774 ** get_address_of_descendIntoChildren_3() { return &___descendIntoChildren_3; }
	inline void set_descendIntoChildren_3(Func_2_t1722577774 * value)
	{
		___descendIntoChildren_3 = value;
		Il2CppCodeGenWriteBarrier((&___descendIntoChildren_3), value);
	}
};

struct DescendantsEnumerable_t4167108050_StaticFields
{
public:
	// System.Func`2<UnityEngine.Transform,System.Boolean> Unity.Linq.GameObjectExtensions/DescendantsEnumerable::alwaysTrue
	Func_2_t1722577774 * ___alwaysTrue_0;

public:
	inline static int32_t get_offset_of_alwaysTrue_0() { return static_cast<int32_t>(offsetof(DescendantsEnumerable_t4167108050_StaticFields, ___alwaysTrue_0)); }
	inline Func_2_t1722577774 * get_alwaysTrue_0() const { return ___alwaysTrue_0; }
	inline Func_2_t1722577774 ** get_address_of_alwaysTrue_0() { return &___alwaysTrue_0; }
	inline void set_alwaysTrue_0(Func_2_t1722577774 * value)
	{
		___alwaysTrue_0 = value;
		Il2CppCodeGenWriteBarrier((&___alwaysTrue_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Linq.GameObjectExtensions/DescendantsEnumerable
struct DescendantsEnumerable_t4167108050_marshaled_pinvoke
{
	GameObject_t1113636619 * ___origin_1;
	int32_t ___withSelf_2;
	Il2CppMethodPointer ___descendIntoChildren_3;
};
// Native definition for COM marshalling of Unity.Linq.GameObjectExtensions/DescendantsEnumerable
struct DescendantsEnumerable_t4167108050_marshaled_com
{
	GameObject_t1113636619 * ___origin_1;
	int32_t ___withSelf_2;
	Il2CppMethodPointer ___descendIntoChildren_3;
};
#endif // DESCENDANTSENUMERABLE_T4167108050_H
#ifndef UNITYEVENT_2_T614268397_H
#define UNITYEVENT_2_T614268397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
struct  UnityEvent_2_t614268397  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t614268397, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T614268397_H
#ifndef CHILDRENENUMERABLE_T145725860_H
#define CHILDRENENUMERABLE_T145725860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/ChildrenEnumerable
struct  ChildrenEnumerable_t145725860 
{
public:
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/ChildrenEnumerable::origin
	GameObject_t1113636619 * ___origin_0;
	// System.Boolean Unity.Linq.GameObjectExtensions/ChildrenEnumerable::withSelf
	bool ___withSelf_1;

public:
	inline static int32_t get_offset_of_origin_0() { return static_cast<int32_t>(offsetof(ChildrenEnumerable_t145725860, ___origin_0)); }
	inline GameObject_t1113636619 * get_origin_0() const { return ___origin_0; }
	inline GameObject_t1113636619 ** get_address_of_origin_0() { return &___origin_0; }
	inline void set_origin_0(GameObject_t1113636619 * value)
	{
		___origin_0 = value;
		Il2CppCodeGenWriteBarrier((&___origin_0), value);
	}

	inline static int32_t get_offset_of_withSelf_1() { return static_cast<int32_t>(offsetof(ChildrenEnumerable_t145725860, ___withSelf_1)); }
	inline bool get_withSelf_1() const { return ___withSelf_1; }
	inline bool* get_address_of_withSelf_1() { return &___withSelf_1; }
	inline void set_withSelf_1(bool value)
	{
		___withSelf_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Linq.GameObjectExtensions/ChildrenEnumerable
struct ChildrenEnumerable_t145725860_marshaled_pinvoke
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
// Native definition for COM marshalling of Unity.Linq.GameObjectExtensions/ChildrenEnumerable
struct ChildrenEnumerable_t145725860_marshaled_com
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
#endif // CHILDRENENUMERABLE_T145725860_H
#ifndef TIMESTAMPOBSERVABLE_1_T1502464332_H
#define TIMESTAMPOBSERVABLE_1_T1502464332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimestampObservable`1<System.Object>
struct  TimestampObservable_1_t1502464332  : public OperatorObservableBase_1_t3168943961
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.TimestampObservable`1::source
	RuntimeObject* ___source_1;
	// UniRx.IScheduler UniRx.Operators.TimestampObservable`1::scheduler
	RuntimeObject* ___scheduler_2;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(TimestampObservable_1_t1502464332, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_scheduler_2() { return static_cast<int32_t>(offsetof(TimestampObservable_1_t1502464332, ___scheduler_2)); }
	inline RuntimeObject* get_scheduler_2() const { return ___scheduler_2; }
	inline RuntimeObject** get_address_of_scheduler_2() { return &___scheduler_2; }
	inline void set_scheduler_2(RuntimeObject* value)
	{
		___scheduler_2 = value;
		Il2CppCodeGenWriteBarrier((&___scheduler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPOBSERVABLE_1_T1502464332_H
#ifndef TIMEINTERVALOBSERVABLE_1_T696285847_H
#define TIMEINTERVALOBSERVABLE_1_T696285847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimeIntervalObservable`1<System.Object>
struct  TimeIntervalObservable_1_t696285847  : public OperatorObservableBase_1_t3236765911
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.TimeIntervalObservable`1::source
	RuntimeObject* ___source_1;
	// UniRx.IScheduler UniRx.Operators.TimeIntervalObservable`1::scheduler
	RuntimeObject* ___scheduler_2;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(TimeIntervalObservable_1_t696285847, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_scheduler_2() { return static_cast<int32_t>(offsetof(TimeIntervalObservable_1_t696285847, ___scheduler_2)); }
	inline RuntimeObject* get_scheduler_2() const { return ___scheduler_2; }
	inline RuntimeObject** get_address_of_scheduler_2() { return &___scheduler_2; }
	inline void set_scheduler_2(RuntimeObject* value)
	{
		___scheduler_2 = value;
		Il2CppCodeGenWriteBarrier((&___scheduler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEINTERVALOBSERVABLE_1_T696285847_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef FROMEVENTOBSERVABLE_1_T3138708542_H
#define FROMEVENTOBSERVABLE_1_T3138708542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FromEventObservable`1<System.Object>
struct  FromEventObservable_1_t3138708542  : public OperatorObservableBase_1_t354719556
{
public:
	// System.Func`2<System.Action,TDelegate> UniRx.Operators.FromEventObservable`1::conversion
	Func_2_t4286427857 * ___conversion_1;
	// System.Action`1<TDelegate> UniRx.Operators.FromEventObservable`1::addHandler
	Action_1_t3252573759 * ___addHandler_2;
	// System.Action`1<TDelegate> UniRx.Operators.FromEventObservable`1::removeHandler
	Action_1_t3252573759 * ___removeHandler_3;

public:
	inline static int32_t get_offset_of_conversion_1() { return static_cast<int32_t>(offsetof(FromEventObservable_1_t3138708542, ___conversion_1)); }
	inline Func_2_t4286427857 * get_conversion_1() const { return ___conversion_1; }
	inline Func_2_t4286427857 ** get_address_of_conversion_1() { return &___conversion_1; }
	inline void set_conversion_1(Func_2_t4286427857 * value)
	{
		___conversion_1 = value;
		Il2CppCodeGenWriteBarrier((&___conversion_1), value);
	}

	inline static int32_t get_offset_of_addHandler_2() { return static_cast<int32_t>(offsetof(FromEventObservable_1_t3138708542, ___addHandler_2)); }
	inline Action_1_t3252573759 * get_addHandler_2() const { return ___addHandler_2; }
	inline Action_1_t3252573759 ** get_address_of_addHandler_2() { return &___addHandler_2; }
	inline void set_addHandler_2(Action_1_t3252573759 * value)
	{
		___addHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___addHandler_2), value);
	}

	inline static int32_t get_offset_of_removeHandler_3() { return static_cast<int32_t>(offsetof(FromEventObservable_1_t3138708542, ___removeHandler_3)); }
	inline Action_1_t3252573759 * get_removeHandler_3() const { return ___removeHandler_3; }
	inline Action_1_t3252573759 ** get_address_of_removeHandler_3() { return &___removeHandler_3; }
	inline void set_removeHandler_3(Action_1_t3252573759 * value)
	{
		___removeHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___removeHandler_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMEVENTOBSERVABLE_1_T3138708542_H
#ifndef FOREACHASYNCOBSERVABLE_1_T3073042236_H
#define FOREACHASYNCOBSERVABLE_1_T3073042236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ForEachAsyncObservable`1<System.Object>
struct  ForEachAsyncObservable_1_t3073042236  : public OperatorObservableBase_1_t354719556
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.ForEachAsyncObservable`1::source
	RuntimeObject* ___source_1;
	// System.Action`1<T> UniRx.Operators.ForEachAsyncObservable`1::onNext
	Action_1_t3252573759 * ___onNext_2;
	// System.Action`2<T,System.Int32> UniRx.Operators.ForEachAsyncObservable`1::onNextWithIndex
	Action_2_t2340848427 * ___onNextWithIndex_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(ForEachAsyncObservable_1_t3073042236, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_onNext_2() { return static_cast<int32_t>(offsetof(ForEachAsyncObservable_1_t3073042236, ___onNext_2)); }
	inline Action_1_t3252573759 * get_onNext_2() const { return ___onNext_2; }
	inline Action_1_t3252573759 ** get_address_of_onNext_2() { return &___onNext_2; }
	inline void set_onNext_2(Action_1_t3252573759 * value)
	{
		___onNext_2 = value;
		Il2CppCodeGenWriteBarrier((&___onNext_2), value);
	}

	inline static int32_t get_offset_of_onNextWithIndex_3() { return static_cast<int32_t>(offsetof(ForEachAsyncObservable_1_t3073042236, ___onNextWithIndex_3)); }
	inline Action_2_t2340848427 * get_onNextWithIndex_3() const { return ___onNextWithIndex_3; }
	inline Action_2_t2340848427 ** get_address_of_onNextWithIndex_3() { return &___onNextWithIndex_3; }
	inline void set_onNextWithIndex_3(Action_2_t2340848427 * value)
	{
		___onNextWithIndex_3 = value;
		Il2CppCodeGenWriteBarrier((&___onNextWithIndex_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOREACHASYNCOBSERVABLE_1_T3073042236_H
#ifndef ASUNITOBSERVABLEOBSERVABLE_1_T1939971722_H
#define ASUNITOBSERVABLEOBSERVABLE_1_T1939971722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.AsUnitObservableObservable`1<System.Object>
struct  AsUnitObservableObservable_1_t1939971722  : public OperatorObservableBase_1_t354719556
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.AsUnitObservableObservable`1::source
	RuntimeObject* ___source_1;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(AsUnitObservableObservable_1_t1939971722, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASUNITOBSERVABLEOBSERVABLE_1_T1939971722_H
#ifndef ASSINGLEUNITOBSERVABLEOBSERVABLE_1_T2095035369_H
#define ASSINGLEUNITOBSERVABLEOBSERVABLE_1_T2095035369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.AsSingleUnitObservableObservable`1<System.Object>
struct  AsSingleUnitObservableObservable_1_t2095035369  : public OperatorObservableBase_1_t354719556
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.AsSingleUnitObservableObservable`1::source
	RuntimeObject* ___source_1;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(AsSingleUnitObservableObservable_1_t2095035369, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSINGLEUNITOBSERVABLEOBSERVABLE_1_T2095035369_H
#ifndef BASEEVENTDATA_T3903027533_H
#define BASEEVENTDATA_T3903027533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t3903027533  : public AbstractEventData_t4171500731
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t3903027533, ___m_EventSystem_1)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEVENTDATA_T3903027533_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef FRAMETIMEINTERVALOBSERVABLE_1_T3088423318_H
#define FRAMETIMEINTERVALOBSERVABLE_1_T3088423318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FrameTimeIntervalObservable`1<System.Object>
struct  FrameTimeIntervalObservable_1_t3088423318  : public OperatorObservableBase_1_t3236765911
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.FrameTimeIntervalObservable`1::source
	RuntimeObject* ___source_1;
	// System.Boolean UniRx.Operators.FrameTimeIntervalObservable`1::ignoreTimeScale
	bool ___ignoreTimeScale_2;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(FrameTimeIntervalObservable_1_t3088423318, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_ignoreTimeScale_2() { return static_cast<int32_t>(offsetof(FrameTimeIntervalObservable_1_t3088423318, ___ignoreTimeScale_2)); }
	inline bool get_ignoreTimeScale_2() const { return ___ignoreTimeScale_2; }
	inline bool* get_address_of_ignoreTimeScale_2() { return &___ignoreTimeScale_2; }
	inline void set_ignoreTimeScale_2(bool value)
	{
		___ignoreTimeScale_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMETIMEINTERVALOBSERVABLE_1_T3088423318_H
#ifndef ANCESTORSENUMERABLE_T953073017_H
#define ANCESTORSENUMERABLE_T953073017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/AncestorsEnumerable
struct  AncestorsEnumerable_t953073017 
{
public:
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/AncestorsEnumerable::origin
	GameObject_t1113636619 * ___origin_0;
	// System.Boolean Unity.Linq.GameObjectExtensions/AncestorsEnumerable::withSelf
	bool ___withSelf_1;

public:
	inline static int32_t get_offset_of_origin_0() { return static_cast<int32_t>(offsetof(AncestorsEnumerable_t953073017, ___origin_0)); }
	inline GameObject_t1113636619 * get_origin_0() const { return ___origin_0; }
	inline GameObject_t1113636619 ** get_address_of_origin_0() { return &___origin_0; }
	inline void set_origin_0(GameObject_t1113636619 * value)
	{
		___origin_0 = value;
		Il2CppCodeGenWriteBarrier((&___origin_0), value);
	}

	inline static int32_t get_offset_of_withSelf_1() { return static_cast<int32_t>(offsetof(AncestorsEnumerable_t953073017, ___withSelf_1)); }
	inline bool get_withSelf_1() const { return ___withSelf_1; }
	inline bool* get_address_of_withSelf_1() { return &___withSelf_1; }
	inline void set_withSelf_1(bool value)
	{
		___withSelf_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Linq.GameObjectExtensions/AncestorsEnumerable
struct AncestorsEnumerable_t953073017_marshaled_pinvoke
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
// Native definition for COM marshalling of Unity.Linq.GameObjectExtensions/AncestorsEnumerable
struct AncestorsEnumerable_t953073017_marshaled_com
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
#endif // ANCESTORSENUMERABLE_T953073017_H
#ifndef FRAMEINTERVALOBSERVABLE_1_T2769986100_H
#define FRAMEINTERVALOBSERVABLE_1_T2769986100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FrameIntervalObservable`1<System.Object>
struct  FrameIntervalObservable_1_t2769986100  : public OperatorObservableBase_1_t3354786123
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.FrameIntervalObservable`1::source
	RuntimeObject* ___source_1;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(FrameIntervalObservable_1_t2769986100, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEINTERVALOBSERVABLE_1_T2769986100_H
#ifndef NULLABLE_1_T378540539_H
#define NULLABLE_1_T378540539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t378540539 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T378540539_H
#ifndef AUTODETACHOBSERVER_1_T1556989284_H
#define AUTODETACHOBSERVER_1_T1556989284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/AutoDetachObserver`1<System.Object>
struct  AutoDetachObserver_1_t1556989284  : public OperatorObserverBase_2_t59931874
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTODETACHOBSERVER_1_T1556989284_H
#ifndef AFTERSELFENUMERABLE_T4038755258_H
#define AFTERSELFENUMERABLE_T4038755258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/AfterSelfEnumerable
struct  AfterSelfEnumerable_t4038755258 
{
public:
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/AfterSelfEnumerable::origin
	GameObject_t1113636619 * ___origin_0;
	// System.Boolean Unity.Linq.GameObjectExtensions/AfterSelfEnumerable::withSelf
	bool ___withSelf_1;

public:
	inline static int32_t get_offset_of_origin_0() { return static_cast<int32_t>(offsetof(AfterSelfEnumerable_t4038755258, ___origin_0)); }
	inline GameObject_t1113636619 * get_origin_0() const { return ___origin_0; }
	inline GameObject_t1113636619 ** get_address_of_origin_0() { return &___origin_0; }
	inline void set_origin_0(GameObject_t1113636619 * value)
	{
		___origin_0 = value;
		Il2CppCodeGenWriteBarrier((&___origin_0), value);
	}

	inline static int32_t get_offset_of_withSelf_1() { return static_cast<int32_t>(offsetof(AfterSelfEnumerable_t4038755258, ___withSelf_1)); }
	inline bool get_withSelf_1() const { return ___withSelf_1; }
	inline bool* get_address_of_withSelf_1() { return &___withSelf_1; }
	inline void set_withSelf_1(bool value)
	{
		___withSelf_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Linq.GameObjectExtensions/AfterSelfEnumerable
struct AfterSelfEnumerable_t4038755258_marshaled_pinvoke
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
// Native definition for COM marshalling of Unity.Linq.GameObjectExtensions/AfterSelfEnumerable
struct AfterSelfEnumerable_t4038755258_marshaled_com
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
#endif // AFTERSELFENUMERABLE_T4038755258_H
#ifndef PAIRWISEOBSERVABLE_1_T1009914496_H
#define PAIRWISEOBSERVABLE_1_T1009914496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.PairwiseObservable`1<System.Object>
struct  PairwiseObservable_1_t1009914496  : public OperatorObservableBase_1_t344286290
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.PairwiseObservable`1::source
	RuntimeObject* ___source_1;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(PairwiseObservable_1_t1009914496, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIRWISEOBSERVABLE_1_T1009914496_H
#ifndef BEFORESELFENUMERABLE_T2845913035_H
#define BEFORESELFENUMERABLE_T2845913035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable
struct  BeforeSelfEnumerable_t2845913035 
{
public:
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable::origin
	GameObject_t1113636619 * ___origin_0;
	// System.Boolean Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable::withSelf
	bool ___withSelf_1;

public:
	inline static int32_t get_offset_of_origin_0() { return static_cast<int32_t>(offsetof(BeforeSelfEnumerable_t2845913035, ___origin_0)); }
	inline GameObject_t1113636619 * get_origin_0() const { return ___origin_0; }
	inline GameObject_t1113636619 ** get_address_of_origin_0() { return &___origin_0; }
	inline void set_origin_0(GameObject_t1113636619 * value)
	{
		___origin_0 = value;
		Il2CppCodeGenWriteBarrier((&___origin_0), value);
	}

	inline static int32_t get_offset_of_withSelf_1() { return static_cast<int32_t>(offsetof(BeforeSelfEnumerable_t2845913035, ___withSelf_1)); }
	inline bool get_withSelf_1() const { return ___withSelf_1; }
	inline bool* get_address_of_withSelf_1() { return &___withSelf_1; }
	inline void set_withSelf_1(bool value)
	{
		___withSelf_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable
struct BeforeSelfEnumerable_t2845913035_marshaled_pinvoke
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
// Native definition for COM marshalling of Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable
struct BeforeSelfEnumerable_t2845913035_marshaled_com
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
#endif // BEFORESELFENUMERABLE_T2845913035_H
#ifndef TUPLE_7_T2257463594_H
#define TUPLE_7_T2257463594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Tuple_7_t2257463594 
{
public:
	// T1 UniRx.Tuple`7::item1
	RuntimeObject * ___item1_0;
	// T2 UniRx.Tuple`7::item2
	RuntimeObject * ___item2_1;
	// T3 UniRx.Tuple`7::item3
	RuntimeObject * ___item3_2;
	// T4 UniRx.Tuple`7::item4
	RuntimeObject * ___item4_3;
	// T5 UniRx.Tuple`7::item5
	RuntimeObject * ___item5_4;
	// T6 UniRx.Tuple`7::item6
	RuntimeObject * ___item6_5;
	// T7 UniRx.Tuple`7::item7
	RuntimeObject * ___item7_6;

public:
	inline static int32_t get_offset_of_item1_0() { return static_cast<int32_t>(offsetof(Tuple_7_t2257463594, ___item1_0)); }
	inline RuntimeObject * get_item1_0() const { return ___item1_0; }
	inline RuntimeObject ** get_address_of_item1_0() { return &___item1_0; }
	inline void set_item1_0(RuntimeObject * value)
	{
		___item1_0 = value;
		Il2CppCodeGenWriteBarrier((&___item1_0), value);
	}

	inline static int32_t get_offset_of_item2_1() { return static_cast<int32_t>(offsetof(Tuple_7_t2257463594, ___item2_1)); }
	inline RuntimeObject * get_item2_1() const { return ___item2_1; }
	inline RuntimeObject ** get_address_of_item2_1() { return &___item2_1; }
	inline void set_item2_1(RuntimeObject * value)
	{
		___item2_1 = value;
		Il2CppCodeGenWriteBarrier((&___item2_1), value);
	}

	inline static int32_t get_offset_of_item3_2() { return static_cast<int32_t>(offsetof(Tuple_7_t2257463594, ___item3_2)); }
	inline RuntimeObject * get_item3_2() const { return ___item3_2; }
	inline RuntimeObject ** get_address_of_item3_2() { return &___item3_2; }
	inline void set_item3_2(RuntimeObject * value)
	{
		___item3_2 = value;
		Il2CppCodeGenWriteBarrier((&___item3_2), value);
	}

	inline static int32_t get_offset_of_item4_3() { return static_cast<int32_t>(offsetof(Tuple_7_t2257463594, ___item4_3)); }
	inline RuntimeObject * get_item4_3() const { return ___item4_3; }
	inline RuntimeObject ** get_address_of_item4_3() { return &___item4_3; }
	inline void set_item4_3(RuntimeObject * value)
	{
		___item4_3 = value;
		Il2CppCodeGenWriteBarrier((&___item4_3), value);
	}

	inline static int32_t get_offset_of_item5_4() { return static_cast<int32_t>(offsetof(Tuple_7_t2257463594, ___item5_4)); }
	inline RuntimeObject * get_item5_4() const { return ___item5_4; }
	inline RuntimeObject ** get_address_of_item5_4() { return &___item5_4; }
	inline void set_item5_4(RuntimeObject * value)
	{
		___item5_4 = value;
		Il2CppCodeGenWriteBarrier((&___item5_4), value);
	}

	inline static int32_t get_offset_of_item6_5() { return static_cast<int32_t>(offsetof(Tuple_7_t2257463594, ___item6_5)); }
	inline RuntimeObject * get_item6_5() const { return ___item6_5; }
	inline RuntimeObject ** get_address_of_item6_5() { return &___item6_5; }
	inline void set_item6_5(RuntimeObject * value)
	{
		___item6_5 = value;
		Il2CppCodeGenWriteBarrier((&___item6_5), value);
	}

	inline static int32_t get_offset_of_item7_6() { return static_cast<int32_t>(offsetof(Tuple_7_t2257463594, ___item7_6)); }
	inline RuntimeObject * get_item7_6() const { return ___item7_6; }
	inline RuntimeObject ** get_address_of_item7_6() { return &___item7_6; }
	inline void set_item7_6(RuntimeObject * value)
	{
		___item7_6 = value;
		Il2CppCodeGenWriteBarrier((&___item7_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUPLE_7_T2257463594_H
#ifndef MATERIALIZEOBSERVABLE_1_T1905148853_H
#define MATERIALIZEOBSERVABLE_1_T1905148853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.MaterializeObservable`1<System.Object>
struct  MaterializeObservable_1_t1905148853  : public OperatorObservableBase_1_t1183232841
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.MaterializeObservable`1::source
	RuntimeObject* ___source_1;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(MaterializeObservable_1_t1905148853, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALIZEOBSERVABLE_1_T1905148853_H
#ifndef OFCOMPONENTENUMERABLE_1_T3213154383_H
#define OFCOMPONENTENUMERABLE_1_T3213154383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/AncestorsEnumerable/OfComponentEnumerable`1<System.Object>
struct  OfComponentEnumerable_1_t3213154383 
{
public:
	// Unity.Linq.GameObjectExtensions/AncestorsEnumerable Unity.Linq.GameObjectExtensions/AncestorsEnumerable/OfComponentEnumerable`1::parent
	AncestorsEnumerable_t953073017  ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(OfComponentEnumerable_1_t3213154383, ___parent_0)); }
	inline AncestorsEnumerable_t953073017  get_parent_0() const { return ___parent_0; }
	inline AncestorsEnumerable_t953073017 * get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(AncestorsEnumerable_t953073017  value)
	{
		___parent_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OFCOMPONENTENUMERABLE_1_T3213154383_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef OFCOMPONENTENUMERABLE_1_T80195040_H
#define OFCOMPONENTENUMERABLE_1_T80195040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/OfComponentEnumerable`1<System.Object>
struct  OfComponentEnumerable_1_t80195040 
{
public:
	// Unity.Linq.GameObjectExtensions/AfterSelfEnumerable Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/OfComponentEnumerable`1::parent
	AfterSelfEnumerable_t4038755258  ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(OfComponentEnumerable_1_t80195040, ___parent_0)); }
	inline AfterSelfEnumerable_t4038755258  get_parent_0() const { return ___parent_0; }
	inline AfterSelfEnumerable_t4038755258 * get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(AfterSelfEnumerable_t4038755258  value)
	{
		___parent_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OFCOMPONENTENUMERABLE_1_T80195040_H
#ifndef TUPLE_8_T3974872067_H
#define TUPLE_8_T3974872067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>
struct  Tuple_8_t3974872067  : public RuntimeObject
{
public:
	// T1 UniRx.Tuple`8::item1
	RuntimeObject * ___item1_0;
	// T2 UniRx.Tuple`8::item2
	RuntimeObject * ___item2_1;
	// T3 UniRx.Tuple`8::item3
	RuntimeObject * ___item3_2;
	// T4 UniRx.Tuple`8::item4
	RuntimeObject * ___item4_3;
	// T5 UniRx.Tuple`8::item5
	RuntimeObject * ___item5_4;
	// T6 UniRx.Tuple`8::item6
	RuntimeObject * ___item6_5;
	// T7 UniRx.Tuple`8::item7
	RuntimeObject * ___item7_6;
	// TRest UniRx.Tuple`8::rest
	Tuple_1_t4248013280  ___rest_7;

public:
	inline static int32_t get_offset_of_item1_0() { return static_cast<int32_t>(offsetof(Tuple_8_t3974872067, ___item1_0)); }
	inline RuntimeObject * get_item1_0() const { return ___item1_0; }
	inline RuntimeObject ** get_address_of_item1_0() { return &___item1_0; }
	inline void set_item1_0(RuntimeObject * value)
	{
		___item1_0 = value;
		Il2CppCodeGenWriteBarrier((&___item1_0), value);
	}

	inline static int32_t get_offset_of_item2_1() { return static_cast<int32_t>(offsetof(Tuple_8_t3974872067, ___item2_1)); }
	inline RuntimeObject * get_item2_1() const { return ___item2_1; }
	inline RuntimeObject ** get_address_of_item2_1() { return &___item2_1; }
	inline void set_item2_1(RuntimeObject * value)
	{
		___item2_1 = value;
		Il2CppCodeGenWriteBarrier((&___item2_1), value);
	}

	inline static int32_t get_offset_of_item3_2() { return static_cast<int32_t>(offsetof(Tuple_8_t3974872067, ___item3_2)); }
	inline RuntimeObject * get_item3_2() const { return ___item3_2; }
	inline RuntimeObject ** get_address_of_item3_2() { return &___item3_2; }
	inline void set_item3_2(RuntimeObject * value)
	{
		___item3_2 = value;
		Il2CppCodeGenWriteBarrier((&___item3_2), value);
	}

	inline static int32_t get_offset_of_item4_3() { return static_cast<int32_t>(offsetof(Tuple_8_t3974872067, ___item4_3)); }
	inline RuntimeObject * get_item4_3() const { return ___item4_3; }
	inline RuntimeObject ** get_address_of_item4_3() { return &___item4_3; }
	inline void set_item4_3(RuntimeObject * value)
	{
		___item4_3 = value;
		Il2CppCodeGenWriteBarrier((&___item4_3), value);
	}

	inline static int32_t get_offset_of_item5_4() { return static_cast<int32_t>(offsetof(Tuple_8_t3974872067, ___item5_4)); }
	inline RuntimeObject * get_item5_4() const { return ___item5_4; }
	inline RuntimeObject ** get_address_of_item5_4() { return &___item5_4; }
	inline void set_item5_4(RuntimeObject * value)
	{
		___item5_4 = value;
		Il2CppCodeGenWriteBarrier((&___item5_4), value);
	}

	inline static int32_t get_offset_of_item6_5() { return static_cast<int32_t>(offsetof(Tuple_8_t3974872067, ___item6_5)); }
	inline RuntimeObject * get_item6_5() const { return ___item6_5; }
	inline RuntimeObject ** get_address_of_item6_5() { return &___item6_5; }
	inline void set_item6_5(RuntimeObject * value)
	{
		___item6_5 = value;
		Il2CppCodeGenWriteBarrier((&___item6_5), value);
	}

	inline static int32_t get_offset_of_item7_6() { return static_cast<int32_t>(offsetof(Tuple_8_t3974872067, ___item7_6)); }
	inline RuntimeObject * get_item7_6() const { return ___item7_6; }
	inline RuntimeObject ** get_address_of_item7_6() { return &___item7_6; }
	inline void set_item7_6(RuntimeObject * value)
	{
		___item7_6 = value;
		Il2CppCodeGenWriteBarrier((&___item7_6), value);
	}

	inline static int32_t get_offset_of_rest_7() { return static_cast<int32_t>(offsetof(Tuple_8_t3974872067, ___rest_7)); }
	inline Tuple_1_t4248013280  get_rest_7() const { return ___rest_7; }
	inline Tuple_1_t4248013280 * get_address_of_rest_7() { return &___rest_7; }
	inline void set_rest_7(Tuple_1_t4248013280  value)
	{
		___rest_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUPLE_8_T3974872067_H
#ifndef TUPLE_3_T2373742699_H
#define TUPLE_3_T2373742699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>
struct  Tuple_3_t2373742699 
{
public:
	// T1 UniRx.Tuple`3::item1
	RuntimeObject * ___item1_0;
	// T2 UniRx.Tuple`3::item2
	Unit_t3362249467  ___item2_1;
	// T3 UniRx.Tuple`3::item3
	RuntimeObject * ___item3_2;

public:
	inline static int32_t get_offset_of_item1_0() { return static_cast<int32_t>(offsetof(Tuple_3_t2373742699, ___item1_0)); }
	inline RuntimeObject * get_item1_0() const { return ___item1_0; }
	inline RuntimeObject ** get_address_of_item1_0() { return &___item1_0; }
	inline void set_item1_0(RuntimeObject * value)
	{
		___item1_0 = value;
		Il2CppCodeGenWriteBarrier((&___item1_0), value);
	}

	inline static int32_t get_offset_of_item2_1() { return static_cast<int32_t>(offsetof(Tuple_3_t2373742699, ___item2_1)); }
	inline Unit_t3362249467  get_item2_1() const { return ___item2_1; }
	inline Unit_t3362249467 * get_address_of_item2_1() { return &___item2_1; }
	inline void set_item2_1(Unit_t3362249467  value)
	{
		___item2_1 = value;
	}

	inline static int32_t get_offset_of_item3_2() { return static_cast<int32_t>(offsetof(Tuple_3_t2373742699, ___item3_2)); }
	inline RuntimeObject * get_item3_2() const { return ___item3_2; }
	inline RuntimeObject ** get_address_of_item3_2() { return &___item3_2; }
	inline void set_item3_2(RuntimeObject * value)
	{
		___item3_2 = value;
		Il2CppCodeGenWriteBarrier((&___item3_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUPLE_3_T2373742699_H
#ifndef OFCOMPONENTENUMERABLE_1_T3909910951_H
#define OFCOMPONENTENUMERABLE_1_T3909910951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/OfComponentEnumerable`1<System.Object>
struct  OfComponentEnumerable_1_t3909910951 
{
public:
	// Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/OfComponentEnumerable`1::parent
	BeforeSelfEnumerable_t2845913035  ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(OfComponentEnumerable_1_t3909910951, ___parent_0)); }
	inline BeforeSelfEnumerable_t2845913035  get_parent_0() const { return ___parent_0; }
	inline BeforeSelfEnumerable_t2845913035 * get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(BeforeSelfEnumerable_t2845913035  value)
	{
		___parent_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OFCOMPONENTENUMERABLE_1_T3909910951_H
#ifndef TASKSTATUS_T4183509909_H
#define TASKSTATUS_T4183509909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.LazyTask/TaskStatus
struct  TaskStatus_t4183509909 
{
public:
	// System.Int32 UniRx.LazyTask/TaskStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TaskStatus_t4183509909, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKSTATUS_T4183509909_H
#ifndef OFCOMPONENTENUMERABLE_1_T1188510407_H
#define OFCOMPONENTENUMERABLE_1_T1188510407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/ChildrenEnumerable/OfComponentEnumerable`1<System.Object>
struct  OfComponentEnumerable_1_t1188510407 
{
public:
	// Unity.Linq.GameObjectExtensions/ChildrenEnumerable Unity.Linq.GameObjectExtensions/ChildrenEnumerable/OfComponentEnumerable`1::parent
	ChildrenEnumerable_t145725860  ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(OfComponentEnumerable_1_t1188510407, ___parent_0)); }
	inline ChildrenEnumerable_t145725860  get_parent_0() const { return ___parent_0; }
	inline ChildrenEnumerable_t145725860 * get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(ChildrenEnumerable_t145725860  value)
	{
		___parent_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OFCOMPONENTENUMERABLE_1_T1188510407_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef OFCOMPONENTENUMERABLE_1_T908350185_H
#define OFCOMPONENTENUMERABLE_1_T908350185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/DescendantsEnumerable/OfComponentEnumerable`1<System.Object>
struct  OfComponentEnumerable_1_t908350185 
{
public:
	// Unity.Linq.GameObjectExtensions/DescendantsEnumerable Unity.Linq.GameObjectExtensions/DescendantsEnumerable/OfComponentEnumerable`1::parent
	DescendantsEnumerable_t4167108050  ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(OfComponentEnumerable_1_t908350185, ___parent_0)); }
	inline DescendantsEnumerable_t4167108050  get_parent_0() const { return ___parent_0; }
	inline DescendantsEnumerable_t4167108050 * get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(DescendantsEnumerable_t4167108050  value)
	{
		___parent_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OFCOMPONENTENUMERABLE_1_T908350185_H
#ifndef INVALIDOPERATIONEXCEPTION_T56020091_H
#define INVALIDOPERATIONEXCEPTION_T56020091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t56020091  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T56020091_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef ARGUMENTEXCEPTION_T132251570_H
#define ARGUMENTEXCEPTION_T132251570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t132251570  : public SystemException_t176217640
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t132251570, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T132251570_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef OBSERVABLE_T2677550507_H
#define OBSERVABLE_T2677550507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable
struct  Observable_t2677550507  : public RuntimeObject
{
public:

public:
};

struct Observable_t2677550507_StaticFields
{
public:
	// System.TimeSpan UniRx.Observable::InfiniteTimeSpan
	TimeSpan_t881159249  ___InfiniteTimeSpan_0;
	// System.Collections.Generic.HashSet`1<System.Type> UniRx.Observable::YieldInstructionTypes
	HashSet_1_t1048894234 * ___YieldInstructionTypes_1;
	// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable::<>f__mg$cache0
	Func_3_t1439315263 * ___U3CU3Ef__mgU24cache0_2;
	// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable::<>f__mg$cache1
	Func_3_t1439315263 * ___U3CU3Ef__mgU24cache1_3;
	// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable::<>f__mg$cache2
	Func_3_t1439315263 * ___U3CU3Ef__mgU24cache2_4;
	// System.Func`3<System.Int64,UniRx.Unit,System.Int64> UniRx.Observable::<>f__am$cache0
	Func_3_t2605432362 * ___U3CU3Ef__amU24cache0_5;
	// System.Func`3<System.Int64,UniRx.Unit,System.Int64> UniRx.Observable::<>f__am$cache1
	Func_3_t2605432362 * ___U3CU3Ef__amU24cache1_6;
	// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable::<>f__am$cache2
	Func_3_t1439315263 * ___U3CU3Ef__amU24cache2_7;
	// System.Func`3<UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable::<>f__mg$cache3
	Func_3_t2376649402 * ___U3CU3Ef__mgU24cache3_8;

public:
	inline static int32_t get_offset_of_InfiniteTimeSpan_0() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___InfiniteTimeSpan_0)); }
	inline TimeSpan_t881159249  get_InfiniteTimeSpan_0() const { return ___InfiniteTimeSpan_0; }
	inline TimeSpan_t881159249 * get_address_of_InfiniteTimeSpan_0() { return &___InfiniteTimeSpan_0; }
	inline void set_InfiniteTimeSpan_0(TimeSpan_t881159249  value)
	{
		___InfiniteTimeSpan_0 = value;
	}

	inline static int32_t get_offset_of_YieldInstructionTypes_1() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___YieldInstructionTypes_1)); }
	inline HashSet_1_t1048894234 * get_YieldInstructionTypes_1() const { return ___YieldInstructionTypes_1; }
	inline HashSet_1_t1048894234 ** get_address_of_YieldInstructionTypes_1() { return &___YieldInstructionTypes_1; }
	inline void set_YieldInstructionTypes_1(HashSet_1_t1048894234 * value)
	{
		___YieldInstructionTypes_1 = value;
		Il2CppCodeGenWriteBarrier((&___YieldInstructionTypes_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_2() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___U3CU3Ef__mgU24cache0_2)); }
	inline Func_3_t1439315263 * get_U3CU3Ef__mgU24cache0_2() const { return ___U3CU3Ef__mgU24cache0_2; }
	inline Func_3_t1439315263 ** get_address_of_U3CU3Ef__mgU24cache0_2() { return &___U3CU3Ef__mgU24cache0_2; }
	inline void set_U3CU3Ef__mgU24cache0_2(Func_3_t1439315263 * value)
	{
		___U3CU3Ef__mgU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_3() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___U3CU3Ef__mgU24cache1_3)); }
	inline Func_3_t1439315263 * get_U3CU3Ef__mgU24cache1_3() const { return ___U3CU3Ef__mgU24cache1_3; }
	inline Func_3_t1439315263 ** get_address_of_U3CU3Ef__mgU24cache1_3() { return &___U3CU3Ef__mgU24cache1_3; }
	inline void set_U3CU3Ef__mgU24cache1_3(Func_3_t1439315263 * value)
	{
		___U3CU3Ef__mgU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_4() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___U3CU3Ef__mgU24cache2_4)); }
	inline Func_3_t1439315263 * get_U3CU3Ef__mgU24cache2_4() const { return ___U3CU3Ef__mgU24cache2_4; }
	inline Func_3_t1439315263 ** get_address_of_U3CU3Ef__mgU24cache2_4() { return &___U3CU3Ef__mgU24cache2_4; }
	inline void set_U3CU3Ef__mgU24cache2_4(Func_3_t1439315263 * value)
	{
		___U3CU3Ef__mgU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Func_3_t2605432362 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Func_3_t2605432362 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Func_3_t2605432362 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_6() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___U3CU3Ef__amU24cache1_6)); }
	inline Func_3_t2605432362 * get_U3CU3Ef__amU24cache1_6() const { return ___U3CU3Ef__amU24cache1_6; }
	inline Func_3_t2605432362 ** get_address_of_U3CU3Ef__amU24cache1_6() { return &___U3CU3Ef__amU24cache1_6; }
	inline void set_U3CU3Ef__amU24cache1_6(Func_3_t2605432362 * value)
	{
		___U3CU3Ef__amU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_7() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___U3CU3Ef__amU24cache2_7)); }
	inline Func_3_t1439315263 * get_U3CU3Ef__amU24cache2_7() const { return ___U3CU3Ef__amU24cache2_7; }
	inline Func_3_t1439315263 ** get_address_of_U3CU3Ef__amU24cache2_7() { return &___U3CU3Ef__amU24cache2_7; }
	inline void set_U3CU3Ef__amU24cache2_7(Func_3_t1439315263 * value)
	{
		___U3CU3Ef__amU24cache2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_8() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___U3CU3Ef__mgU24cache3_8)); }
	inline Func_3_t2376649402 * get_U3CU3Ef__mgU24cache3_8() const { return ___U3CU3Ef__mgU24cache3_8; }
	inline Func_3_t2376649402 ** get_address_of_U3CU3Ef__mgU24cache3_8() { return &___U3CU3Ef__mgU24cache3_8; }
	inline void set_U3CU3Ef__mgU24cache3_8(Func_3_t2376649402 * value)
	{
		___U3CU3Ef__mgU24cache3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLE_T2677550507_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef LOGTYPE_T73765434_H
#define LOGTYPE_T73765434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LogType
struct  LogType_t73765434 
{
public:
	// System.Int32 UnityEngine.LogType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogType_t73765434, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGTYPE_T73765434_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef GROUPBYOBSERVABLE_3_T1723459814_H
#define GROUPBYOBSERVABLE_3_T1723459814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.GroupByObservable`3<System.Object,System.Object,System.Object>
struct  GroupByObservable_3_t1723459814  : public OperatorObservableBase_1_t3820056813
{
public:
	// UniRx.IObservable`1<TSource> UniRx.Operators.GroupByObservable`3::source
	RuntimeObject* ___source_1;
	// System.Func`2<TSource,TKey> UniRx.Operators.GroupByObservable`3::keySelector
	Func_2_t2447130374 * ___keySelector_2;
	// System.Func`2<TSource,TElement> UniRx.Operators.GroupByObservable`3::elementSelector
	Func_2_t2447130374 * ___elementSelector_3;
	// System.Nullable`1<System.Int32> UniRx.Operators.GroupByObservable`3::capacity
	Nullable_1_t378540539  ___capacity_4;
	// System.Collections.Generic.IEqualityComparer`1<TKey> UniRx.Operators.GroupByObservable`3::comparer
	RuntimeObject* ___comparer_5;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(GroupByObservable_3_t1723459814, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_keySelector_2() { return static_cast<int32_t>(offsetof(GroupByObservable_3_t1723459814, ___keySelector_2)); }
	inline Func_2_t2447130374 * get_keySelector_2() const { return ___keySelector_2; }
	inline Func_2_t2447130374 ** get_address_of_keySelector_2() { return &___keySelector_2; }
	inline void set_keySelector_2(Func_2_t2447130374 * value)
	{
		___keySelector_2 = value;
		Il2CppCodeGenWriteBarrier((&___keySelector_2), value);
	}

	inline static int32_t get_offset_of_elementSelector_3() { return static_cast<int32_t>(offsetof(GroupByObservable_3_t1723459814, ___elementSelector_3)); }
	inline Func_2_t2447130374 * get_elementSelector_3() const { return ___elementSelector_3; }
	inline Func_2_t2447130374 ** get_address_of_elementSelector_3() { return &___elementSelector_3; }
	inline void set_elementSelector_3(Func_2_t2447130374 * value)
	{
		___elementSelector_3 = value;
		Il2CppCodeGenWriteBarrier((&___elementSelector_3), value);
	}

	inline static int32_t get_offset_of_capacity_4() { return static_cast<int32_t>(offsetof(GroupByObservable_3_t1723459814, ___capacity_4)); }
	inline Nullable_1_t378540539  get_capacity_4() const { return ___capacity_4; }
	inline Nullable_1_t378540539 * get_address_of_capacity_4() { return &___capacity_4; }
	inline void set_capacity_4(Nullable_1_t378540539  value)
	{
		___capacity_4 = value;
	}

	inline static int32_t get_offset_of_comparer_5() { return static_cast<int32_t>(offsetof(GroupByObservable_3_t1723459814, ___comparer_5)); }
	inline RuntimeObject* get_comparer_5() const { return ___comparer_5; }
	inline RuntimeObject** get_address_of_comparer_5() { return &___comparer_5; }
	inline void set_comparer_5(RuntimeObject* value)
	{
		___comparer_5 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPBYOBSERVABLE_3_T1723459814_H
#ifndef FRAMECOUNTTYPE_T3331626185_H
#define FRAMECOUNTTYPE_T3331626185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.FrameCountType
struct  FrameCountType_t3331626185 
{
public:
	// System.Int32 UniRx.FrameCountType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FrameCountType_t3331626185, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMECOUNTTYPE_T3331626185_H
#ifndef THROTTLEFIRSTOBSERVABLE_1_T3958710643_H
#define THROTTLEFIRSTOBSERVABLE_1_T3958710643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ThrottleFirstObservable`1<System.Object>
struct  ThrottleFirstObservable_1_t3958710643  : public OperatorObservableBase_1_t72576253
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.ThrottleFirstObservable`1::source
	RuntimeObject* ___source_1;
	// System.TimeSpan UniRx.Operators.ThrottleFirstObservable`1::dueTime
	TimeSpan_t881159249  ___dueTime_2;
	// UniRx.IScheduler UniRx.Operators.ThrottleFirstObservable`1::scheduler
	RuntimeObject* ___scheduler_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(ThrottleFirstObservable_1_t3958710643, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_dueTime_2() { return static_cast<int32_t>(offsetof(ThrottleFirstObservable_1_t3958710643, ___dueTime_2)); }
	inline TimeSpan_t881159249  get_dueTime_2() const { return ___dueTime_2; }
	inline TimeSpan_t881159249 * get_address_of_dueTime_2() { return &___dueTime_2; }
	inline void set_dueTime_2(TimeSpan_t881159249  value)
	{
		___dueTime_2 = value;
	}

	inline static int32_t get_offset_of_scheduler_3() { return static_cast<int32_t>(offsetof(ThrottleFirstObservable_1_t3958710643, ___scheduler_3)); }
	inline RuntimeObject* get_scheduler_3() const { return ___scheduler_3; }
	inline RuntimeObject** get_address_of_scheduler_3() { return &___scheduler_3; }
	inline void set_scheduler_3(RuntimeObject* value)
	{
		___scheduler_3 = value;
		Il2CppCodeGenWriteBarrier((&___scheduler_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THROTTLEFIRSTOBSERVABLE_1_T3958710643_H
#ifndef THROTTLEOBSERVABLE_1_T3070947169_H
#define THROTTLEOBSERVABLE_1_T3070947169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ThrottleObservable`1<System.Object>
struct  ThrottleObservable_1_t3070947169  : public OperatorObservableBase_1_t72576253
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.ThrottleObservable`1::source
	RuntimeObject* ___source_1;
	// System.TimeSpan UniRx.Operators.ThrottleObservable`1::dueTime
	TimeSpan_t881159249  ___dueTime_2;
	// UniRx.IScheduler UniRx.Operators.ThrottleObservable`1::scheduler
	RuntimeObject* ___scheduler_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(ThrottleObservable_1_t3070947169, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_dueTime_2() { return static_cast<int32_t>(offsetof(ThrottleObservable_1_t3070947169, ___dueTime_2)); }
	inline TimeSpan_t881159249  get_dueTime_2() const { return ___dueTime_2; }
	inline TimeSpan_t881159249 * get_address_of_dueTime_2() { return &___dueTime_2; }
	inline void set_dueTime_2(TimeSpan_t881159249  value)
	{
		___dueTime_2 = value;
	}

	inline static int32_t get_offset_of_scheduler_3() { return static_cast<int32_t>(offsetof(ThrottleObservable_1_t3070947169, ___scheduler_3)); }
	inline RuntimeObject* get_scheduler_3() const { return ___scheduler_3; }
	inline RuntimeObject** get_address_of_scheduler_3() { return &___scheduler_3; }
	inline void set_scheduler_3(RuntimeObject* value)
	{
		___scheduler_3 = value;
		Il2CppCodeGenWriteBarrier((&___scheduler_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THROTTLEOBSERVABLE_1_T3070947169_H
#ifndef THROTTLEOBSERVABLE_1_T3727408309_H
#define THROTTLEOBSERVABLE_1_T3727408309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ThrottleObservable`1<System.Int64>
struct  ThrottleObservable_1_t3727408309  : public OperatorObservableBase_1_t729037393
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.ThrottleObservable`1::source
	RuntimeObject* ___source_1;
	// System.TimeSpan UniRx.Operators.ThrottleObservable`1::dueTime
	TimeSpan_t881159249  ___dueTime_2;
	// UniRx.IScheduler UniRx.Operators.ThrottleObservable`1::scheduler
	RuntimeObject* ___scheduler_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(ThrottleObservable_1_t3727408309, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_dueTime_2() { return static_cast<int32_t>(offsetof(ThrottleObservable_1_t3727408309, ___dueTime_2)); }
	inline TimeSpan_t881159249  get_dueTime_2() const { return ___dueTime_2; }
	inline TimeSpan_t881159249 * get_address_of_dueTime_2() { return &___dueTime_2; }
	inline void set_dueTime_2(TimeSpan_t881159249  value)
	{
		___dueTime_2 = value;
	}

	inline static int32_t get_offset_of_scheduler_3() { return static_cast<int32_t>(offsetof(ThrottleObservable_1_t3727408309, ___scheduler_3)); }
	inline RuntimeObject* get_scheduler_3() const { return ___scheduler_3; }
	inline RuntimeObject** get_address_of_scheduler_3() { return &___scheduler_3; }
	inline void set_scheduler_3(RuntimeObject* value)
	{
		___scheduler_3 = value;
		Il2CppCodeGenWriteBarrier((&___scheduler_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THROTTLEOBSERVABLE_1_T3727408309_H
#ifndef U3CONERRORRETRYU3EC__ANONSTOREY12_2_T1114935737_H
#define U3CONERRORRETRYU3EC__ANONSTOREY12_2_T1114935737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<OnErrorRetry>c__AnonStorey12`2<System.Object,System.Object>
struct  U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737  : public RuntimeObject
{
public:
	// System.TimeSpan UniRx.Observable/<OnErrorRetry>c__AnonStorey12`2::delay
	TimeSpan_t881159249  ___delay_0;
	// UniRx.IObservable`1<TSource> UniRx.Observable/<OnErrorRetry>c__AnonStorey12`2::source
	RuntimeObject* ___source_1;
	// System.Action`1<TException> UniRx.Observable/<OnErrorRetry>c__AnonStorey12`2::onError
	Action_1_t3252573759 * ___onError_2;
	// System.Int32 UniRx.Observable/<OnErrorRetry>c__AnonStorey12`2::retryCount
	int32_t ___retryCount_3;
	// UniRx.IScheduler UniRx.Observable/<OnErrorRetry>c__AnonStorey12`2::delayScheduler
	RuntimeObject* ___delayScheduler_4;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737, ___delay_0)); }
	inline TimeSpan_t881159249  get_delay_0() const { return ___delay_0; }
	inline TimeSpan_t881159249 * get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(TimeSpan_t881159249  value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_onError_2() { return static_cast<int32_t>(offsetof(U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737, ___onError_2)); }
	inline Action_1_t3252573759 * get_onError_2() const { return ___onError_2; }
	inline Action_1_t3252573759 ** get_address_of_onError_2() { return &___onError_2; }
	inline void set_onError_2(Action_1_t3252573759 * value)
	{
		___onError_2 = value;
		Il2CppCodeGenWriteBarrier((&___onError_2), value);
	}

	inline static int32_t get_offset_of_retryCount_3() { return static_cast<int32_t>(offsetof(U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737, ___retryCount_3)); }
	inline int32_t get_retryCount_3() const { return ___retryCount_3; }
	inline int32_t* get_address_of_retryCount_3() { return &___retryCount_3; }
	inline void set_retryCount_3(int32_t value)
	{
		___retryCount_3 = value;
	}

	inline static int32_t get_offset_of_delayScheduler_4() { return static_cast<int32_t>(offsetof(U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737, ___delayScheduler_4)); }
	inline RuntimeObject* get_delayScheduler_4() const { return ___delayScheduler_4; }
	inline RuntimeObject** get_address_of_delayScheduler_4() { return &___delayScheduler_4; }
	inline void set_delayScheduler_4(RuntimeObject* value)
	{
		___delayScheduler_4 = value;
		Il2CppCodeGenWriteBarrier((&___delayScheduler_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONERRORRETRYU3EC__ANONSTOREY12_2_T1114935737_H
#ifndef OBSERVABLEYIELDINSTRUCTION_1_T2400010708_H
#define OBSERVABLEYIELDINSTRUCTION_1_T2400010708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableYieldInstruction`1<UniRx.Unit>
struct  ObservableYieldInstruction_1_t2400010708  : public RuntimeObject
{
public:
	// System.IDisposable UniRx.ObservableYieldInstruction`1::subscription
	RuntimeObject* ___subscription_0;
	// UniRx.CancellationToken UniRx.ObservableYieldInstruction`1::cancel
	CancellationToken_t1265546479  ___cancel_1;
	// System.Boolean UniRx.ObservableYieldInstruction`1::reThrowOnError
	bool ___reThrowOnError_2;
	// T UniRx.ObservableYieldInstruction`1::current
	Unit_t3362249467  ___current_3;
	// T UniRx.ObservableYieldInstruction`1::result
	Unit_t3362249467  ___result_4;
	// System.Boolean UniRx.ObservableYieldInstruction`1::moveNext
	bool ___moveNext_5;
	// System.Boolean UniRx.ObservableYieldInstruction`1::hasResult
	bool ___hasResult_6;
	// System.Exception UniRx.ObservableYieldInstruction`1::error
	Exception_t * ___error_7;

public:
	inline static int32_t get_offset_of_subscription_0() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2400010708, ___subscription_0)); }
	inline RuntimeObject* get_subscription_0() const { return ___subscription_0; }
	inline RuntimeObject** get_address_of_subscription_0() { return &___subscription_0; }
	inline void set_subscription_0(RuntimeObject* value)
	{
		___subscription_0 = value;
		Il2CppCodeGenWriteBarrier((&___subscription_0), value);
	}

	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2400010708, ___cancel_1)); }
	inline CancellationToken_t1265546479  get_cancel_1() const { return ___cancel_1; }
	inline CancellationToken_t1265546479 * get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(CancellationToken_t1265546479  value)
	{
		___cancel_1 = value;
	}

	inline static int32_t get_offset_of_reThrowOnError_2() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2400010708, ___reThrowOnError_2)); }
	inline bool get_reThrowOnError_2() const { return ___reThrowOnError_2; }
	inline bool* get_address_of_reThrowOnError_2() { return &___reThrowOnError_2; }
	inline void set_reThrowOnError_2(bool value)
	{
		___reThrowOnError_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2400010708, ___current_3)); }
	inline Unit_t3362249467  get_current_3() const { return ___current_3; }
	inline Unit_t3362249467 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Unit_t3362249467  value)
	{
		___current_3 = value;
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2400010708, ___result_4)); }
	inline Unit_t3362249467  get_result_4() const { return ___result_4; }
	inline Unit_t3362249467 * get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(Unit_t3362249467  value)
	{
		___result_4 = value;
	}

	inline static int32_t get_offset_of_moveNext_5() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2400010708, ___moveNext_5)); }
	inline bool get_moveNext_5() const { return ___moveNext_5; }
	inline bool* get_address_of_moveNext_5() { return &___moveNext_5; }
	inline void set_moveNext_5(bool value)
	{
		___moveNext_5 = value;
	}

	inline static int32_t get_offset_of_hasResult_6() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2400010708, ___hasResult_6)); }
	inline bool get_hasResult_6() const { return ___hasResult_6; }
	inline bool* get_address_of_hasResult_6() { return &___hasResult_6; }
	inline void set_hasResult_6(bool value)
	{
		___hasResult_6 = value;
	}

	inline static int32_t get_offset_of_error_7() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2400010708, ___error_7)); }
	inline Exception_t * get_error_7() const { return ___error_7; }
	inline Exception_t ** get_address_of_error_7() { return &___error_7; }
	inline void set_error_7(Exception_t * value)
	{
		___error_7 = value;
		Il2CppCodeGenWriteBarrier((&___error_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEYIELDINSTRUCTION_1_T2400010708_H
#ifndef OBSERVABLEYIELDINSTRUCTION_1_T2774328545_H
#define OBSERVABLEYIELDINSTRUCTION_1_T2774328545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableYieldInstruction`1<System.Int64>
struct  ObservableYieldInstruction_1_t2774328545  : public RuntimeObject
{
public:
	// System.IDisposable UniRx.ObservableYieldInstruction`1::subscription
	RuntimeObject* ___subscription_0;
	// UniRx.CancellationToken UniRx.ObservableYieldInstruction`1::cancel
	CancellationToken_t1265546479  ___cancel_1;
	// System.Boolean UniRx.ObservableYieldInstruction`1::reThrowOnError
	bool ___reThrowOnError_2;
	// T UniRx.ObservableYieldInstruction`1::current
	int64_t ___current_3;
	// T UniRx.ObservableYieldInstruction`1::result
	int64_t ___result_4;
	// System.Boolean UniRx.ObservableYieldInstruction`1::moveNext
	bool ___moveNext_5;
	// System.Boolean UniRx.ObservableYieldInstruction`1::hasResult
	bool ___hasResult_6;
	// System.Exception UniRx.ObservableYieldInstruction`1::error
	Exception_t * ___error_7;

public:
	inline static int32_t get_offset_of_subscription_0() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2774328545, ___subscription_0)); }
	inline RuntimeObject* get_subscription_0() const { return ___subscription_0; }
	inline RuntimeObject** get_address_of_subscription_0() { return &___subscription_0; }
	inline void set_subscription_0(RuntimeObject* value)
	{
		___subscription_0 = value;
		Il2CppCodeGenWriteBarrier((&___subscription_0), value);
	}

	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2774328545, ___cancel_1)); }
	inline CancellationToken_t1265546479  get_cancel_1() const { return ___cancel_1; }
	inline CancellationToken_t1265546479 * get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(CancellationToken_t1265546479  value)
	{
		___cancel_1 = value;
	}

	inline static int32_t get_offset_of_reThrowOnError_2() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2774328545, ___reThrowOnError_2)); }
	inline bool get_reThrowOnError_2() const { return ___reThrowOnError_2; }
	inline bool* get_address_of_reThrowOnError_2() { return &___reThrowOnError_2; }
	inline void set_reThrowOnError_2(bool value)
	{
		___reThrowOnError_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2774328545, ___current_3)); }
	inline int64_t get_current_3() const { return ___current_3; }
	inline int64_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int64_t value)
	{
		___current_3 = value;
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2774328545, ___result_4)); }
	inline int64_t get_result_4() const { return ___result_4; }
	inline int64_t* get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(int64_t value)
	{
		___result_4 = value;
	}

	inline static int32_t get_offset_of_moveNext_5() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2774328545, ___moveNext_5)); }
	inline bool get_moveNext_5() const { return ___moveNext_5; }
	inline bool* get_address_of_moveNext_5() { return &___moveNext_5; }
	inline void set_moveNext_5(bool value)
	{
		___moveNext_5 = value;
	}

	inline static int32_t get_offset_of_hasResult_6() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2774328545, ___hasResult_6)); }
	inline bool get_hasResult_6() const { return ___hasResult_6; }
	inline bool* get_address_of_hasResult_6() { return &___hasResult_6; }
	inline void set_hasResult_6(bool value)
	{
		___hasResult_6 = value;
	}

	inline static int32_t get_offset_of_error_7() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2774328545, ___error_7)); }
	inline Exception_t * get_error_7() const { return ___error_7; }
	inline Exception_t ** get_address_of_error_7() { return &___error_7; }
	inline void set_error_7(Exception_t * value)
	{
		___error_7 = value;
		Il2CppCodeGenWriteBarrier((&___error_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEYIELDINSTRUCTION_1_T2774328545_H
#ifndef OBSERVABLEYIELDINSTRUCTION_1_T2117867405_H
#define OBSERVABLEYIELDINSTRUCTION_1_T2117867405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableYieldInstruction`1<System.Object>
struct  ObservableYieldInstruction_1_t2117867405  : public RuntimeObject
{
public:
	// System.IDisposable UniRx.ObservableYieldInstruction`1::subscription
	RuntimeObject* ___subscription_0;
	// UniRx.CancellationToken UniRx.ObservableYieldInstruction`1::cancel
	CancellationToken_t1265546479  ___cancel_1;
	// System.Boolean UniRx.ObservableYieldInstruction`1::reThrowOnError
	bool ___reThrowOnError_2;
	// T UniRx.ObservableYieldInstruction`1::current
	RuntimeObject * ___current_3;
	// T UniRx.ObservableYieldInstruction`1::result
	RuntimeObject * ___result_4;
	// System.Boolean UniRx.ObservableYieldInstruction`1::moveNext
	bool ___moveNext_5;
	// System.Boolean UniRx.ObservableYieldInstruction`1::hasResult
	bool ___hasResult_6;
	// System.Exception UniRx.ObservableYieldInstruction`1::error
	Exception_t * ___error_7;

public:
	inline static int32_t get_offset_of_subscription_0() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2117867405, ___subscription_0)); }
	inline RuntimeObject* get_subscription_0() const { return ___subscription_0; }
	inline RuntimeObject** get_address_of_subscription_0() { return &___subscription_0; }
	inline void set_subscription_0(RuntimeObject* value)
	{
		___subscription_0 = value;
		Il2CppCodeGenWriteBarrier((&___subscription_0), value);
	}

	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2117867405, ___cancel_1)); }
	inline CancellationToken_t1265546479  get_cancel_1() const { return ___cancel_1; }
	inline CancellationToken_t1265546479 * get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(CancellationToken_t1265546479  value)
	{
		___cancel_1 = value;
	}

	inline static int32_t get_offset_of_reThrowOnError_2() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2117867405, ___reThrowOnError_2)); }
	inline bool get_reThrowOnError_2() const { return ___reThrowOnError_2; }
	inline bool* get_address_of_reThrowOnError_2() { return &___reThrowOnError_2; }
	inline void set_reThrowOnError_2(bool value)
	{
		___reThrowOnError_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2117867405, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2117867405, ___result_4)); }
	inline RuntimeObject * get_result_4() const { return ___result_4; }
	inline RuntimeObject ** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(RuntimeObject * value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}

	inline static int32_t get_offset_of_moveNext_5() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2117867405, ___moveNext_5)); }
	inline bool get_moveNext_5() const { return ___moveNext_5; }
	inline bool* get_address_of_moveNext_5() { return &___moveNext_5; }
	inline void set_moveNext_5(bool value)
	{
		___moveNext_5 = value;
	}

	inline static int32_t get_offset_of_hasResult_6() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2117867405, ___hasResult_6)); }
	inline bool get_hasResult_6() const { return ___hasResult_6; }
	inline bool* get_address_of_hasResult_6() { return &___hasResult_6; }
	inline void set_hasResult_6(bool value)
	{
		___hasResult_6 = value;
	}

	inline static int32_t get_offset_of_error_7() { return static_cast<int32_t>(offsetof(ObservableYieldInstruction_1_t2117867405, ___error_7)); }
	inline Exception_t * get_error_7() const { return ___error_7; }
	inline Exception_t ** get_address_of_error_7() { return &___error_7; }
	inline void set_error_7(Exception_t * value)
	{
		___error_7 = value;
		Il2CppCodeGenWriteBarrier((&___error_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEYIELDINSTRUCTION_1_T2117867405_H
#ifndef THROTTLEFIRSTFRAMEOBSERVABLE_1_T1990180597_H
#define THROTTLEFIRSTFRAMEOBSERVABLE_1_T1990180597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ThrottleFirstFrameObservable`1<System.Object>
struct  ThrottleFirstFrameObservable_1_t1990180597  : public OperatorObservableBase_1_t72576253
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.ThrottleFirstFrameObservable`1::source
	RuntimeObject* ___source_1;
	// System.Int32 UniRx.Operators.ThrottleFirstFrameObservable`1::frameCount
	int32_t ___frameCount_2;
	// UniRx.FrameCountType UniRx.Operators.ThrottleFirstFrameObservable`1::frameCountType
	int32_t ___frameCountType_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(ThrottleFirstFrameObservable_1_t1990180597, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_frameCount_2() { return static_cast<int32_t>(offsetof(ThrottleFirstFrameObservable_1_t1990180597, ___frameCount_2)); }
	inline int32_t get_frameCount_2() const { return ___frameCount_2; }
	inline int32_t* get_address_of_frameCount_2() { return &___frameCount_2; }
	inline void set_frameCount_2(int32_t value)
	{
		___frameCount_2 = value;
	}

	inline static int32_t get_offset_of_frameCountType_3() { return static_cast<int32_t>(offsetof(ThrottleFirstFrameObservable_1_t1990180597, ___frameCountType_3)); }
	inline int32_t get_frameCountType_3() const { return ___frameCountType_3; }
	inline int32_t* get_address_of_frameCountType_3() { return &___frameCountType_3; }
	inline void set_frameCountType_3(int32_t value)
	{
		___frameCountType_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THROTTLEFIRSTFRAMEOBSERVABLE_1_T1990180597_H
#ifndef ARGUMENTOUTOFRANGEEXCEPTION_T777629997_H
#define ARGUMENTOUTOFRANGEEXCEPTION_T777629997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t777629997  : public ArgumentException_t132251570
{
public:
	// System.Object System.ArgumentOutOfRangeException::actual_value
	RuntimeObject * ___actual_value_13;

public:
	inline static int32_t get_offset_of_actual_value_13() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t777629997, ___actual_value_13)); }
	inline RuntimeObject * get_actual_value_13() const { return ___actual_value_13; }
	inline RuntimeObject ** get_address_of_actual_value_13() { return &___actual_value_13; }
	inline void set_actual_value_13(RuntimeObject * value)
	{
		___actual_value_13 = value;
		Il2CppCodeGenWriteBarrier((&___actual_value_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTOUTOFRANGEEXCEPTION_T777629997_H
#ifndef THROTTLEFRAMEOBSERVABLE_1_T1027693670_H
#define THROTTLEFRAMEOBSERVABLE_1_T1027693670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ThrottleFrameObservable`1<System.Object>
struct  ThrottleFrameObservable_1_t1027693670  : public OperatorObservableBase_1_t72576253
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.ThrottleFrameObservable`1::source
	RuntimeObject* ___source_1;
	// System.Int32 UniRx.Operators.ThrottleFrameObservable`1::frameCount
	int32_t ___frameCount_2;
	// UniRx.FrameCountType UniRx.Operators.ThrottleFrameObservable`1::frameCountType
	int32_t ___frameCountType_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(ThrottleFrameObservable_1_t1027693670, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_frameCount_2() { return static_cast<int32_t>(offsetof(ThrottleFrameObservable_1_t1027693670, ___frameCount_2)); }
	inline int32_t get_frameCount_2() const { return ___frameCount_2; }
	inline int32_t* get_address_of_frameCount_2() { return &___frameCount_2; }
	inline void set_frameCount_2(int32_t value)
	{
		___frameCount_2 = value;
	}

	inline static int32_t get_offset_of_frameCountType_3() { return static_cast<int32_t>(offsetof(ThrottleFrameObservable_1_t1027693670, ___frameCountType_3)); }
	inline int32_t get_frameCountType_3() const { return ___frameCountType_3; }
	inline int32_t* get_address_of_frameCountType_3() { return &___frameCountType_3; }
	inline void set_frameCountType_3(int32_t value)
	{
		___frameCountType_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THROTTLEFRAMEOBSERVABLE_1_T1027693670_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef LAZYTASK_T1721948414_H
#define LAZYTASK_T1721948414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.LazyTask
struct  LazyTask_t1721948414  : public RuntimeObject
{
public:
	// UniRx.LazyTask/TaskStatus UniRx.LazyTask::<Status>k__BackingField
	int32_t ___U3CStatusU3Ek__BackingField_0;
	// UniRx.BooleanDisposable UniRx.LazyTask::cancellation
	BooleanDisposable_t84760918 * ___cancellation_1;

public:
	inline static int32_t get_offset_of_U3CStatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LazyTask_t1721948414, ___U3CStatusU3Ek__BackingField_0)); }
	inline int32_t get_U3CStatusU3Ek__BackingField_0() const { return ___U3CStatusU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CStatusU3Ek__BackingField_0() { return &___U3CStatusU3Ek__BackingField_0; }
	inline void set_U3CStatusU3Ek__BackingField_0(int32_t value)
	{
		___U3CStatusU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_cancellation_1() { return static_cast<int32_t>(offsetof(LazyTask_t1721948414, ___cancellation_1)); }
	inline BooleanDisposable_t84760918 * get_cancellation_1() const { return ___cancellation_1; }
	inline BooleanDisposable_t84760918 ** get_address_of_cancellation_1() { return &___cancellation_1; }
	inline void set_cancellation_1(BooleanDisposable_t84760918 * value)
	{
		___cancellation_1 = value;
		Il2CppCodeGenWriteBarrier((&___cancellation_1), value);
	}
};

struct LazyTask_t1721948414_StaticFields
{
public:
	// System.Func`2<UniRx.LazyTask,UnityEngine.Coroutine> UniRx.LazyTask::<>f__am$cache0
	Func_2_t2678796183 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(LazyTask_t1721948414_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Func_2_t2678796183 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Func_2_t2678796183 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Func_2_t2678796183 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAZYTASK_T1721948414_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef ARGUMENTNULLEXCEPTION_T1615371798_H
#define ARGUMENTNULLEXCEPTION_T1615371798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t1615371798  : public ArgumentException_t132251570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T1615371798_H
#ifndef OBJECTDISPOSEDEXCEPTION_T21392786_H
#define OBJECTDISPOSEDEXCEPTION_T21392786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ObjectDisposedException
struct  ObjectDisposedException_t21392786  : public InvalidOperationException_t56020091
{
public:
	// System.String System.ObjectDisposedException::obj_name
	String_t* ___obj_name_12;
	// System.String System.ObjectDisposedException::msg
	String_t* ___msg_13;

public:
	inline static int32_t get_offset_of_obj_name_12() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t21392786, ___obj_name_12)); }
	inline String_t* get_obj_name_12() const { return ___obj_name_12; }
	inline String_t** get_address_of_obj_name_12() { return &___obj_name_12; }
	inline void set_obj_name_12(String_t* value)
	{
		___obj_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___obj_name_12), value);
	}

	inline static int32_t get_offset_of_msg_13() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t21392786, ___msg_13)); }
	inline String_t* get_msg_13() const { return ___msg_13; }
	inline String_t** get_address_of_msg_13() { return &___msg_13; }
	inline void set_msg_13(String_t* value)
	{
		___msg_13 = value;
		Il2CppCodeGenWriteBarrier((&___msg_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDISPOSEDEXCEPTION_T21392786_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef ACTION_1_T2821607406_H
#define ACTION_1_T2821607406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct  Action_1_t2821607406  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T2821607406_H
#ifndef ACTION_1_T1729704308_H
#define ACTION_1_T1729704308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>>
struct  Action_1_t1729704308  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T1729704308_H
#ifndef UNITYACTION_4_T682480391_H
#define UNITYACTION_4_T682480391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>
struct  UnityAction_4_t682480391  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_4_T682480391_H
#ifndef FUNC_2_T2651802615_H
#define FUNC_2_T2651802615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Action`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>,UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>>
struct  Func_2_t2651802615  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T2651802615_H
#ifndef ACTION_1_T854947986_H
#define ACTION_1_T854947986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>>
struct  Action_1_t854947986  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T854947986_H
#ifndef LAZYTASK_1_T2287704132_H
#define LAZYTASK_1_T2287704132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.LazyTask`1<System.Object>
struct  LazyTask_1_t2287704132  : public LazyTask_t1721948414
{
public:
	// UniRx.IObservable`1<T> UniRx.LazyTask`1::source
	RuntimeObject* ___source_3;
	// T UniRx.LazyTask`1::result
	RuntimeObject * ___result_4;
	// System.Exception UniRx.LazyTask`1::<Exception>k__BackingField
	Exception_t * ___U3CExceptionU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(LazyTask_1_t2287704132, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((&___source_3), value);
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(LazyTask_1_t2287704132, ___result_4)); }
	inline RuntimeObject * get_result_4() const { return ___result_4; }
	inline RuntimeObject ** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(RuntimeObject * value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LazyTask_1_t2287704132, ___U3CExceptionU3Ek__BackingField_5)); }
	inline Exception_t * get_U3CExceptionU3Ek__BackingField_5() const { return ___U3CExceptionU3Ek__BackingField_5; }
	inline Exception_t ** get_address_of_U3CExceptionU3Ek__BackingField_5() { return &___U3CExceptionU3Ek__BackingField_5; }
	inline void set_U3CExceptionU3Ek__BackingField_5(Exception_t * value)
	{
		___U3CExceptionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAZYTASK_1_T2287704132_H
#ifndef ACTION_2_T11315885_H
#define ACTION_2_T11315885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`2<System.Int32,System.Object>
struct  Action_2_t11315885  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_2_T11315885_H
#ifndef FUNC_2_T852405815_H
#define FUNC_2_T852405815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,UniRx.IObservable`1<UniRx.Unit>>
struct  Func_2_t852405815  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T852405815_H
#ifndef FUNC_2_T3187915266_H
#define FUNC_2_T3187915266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Action`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>,UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>>
struct  Func_2_t3187915266  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T3187915266_H
#ifndef FUNC_2_T2447130374_H
#define FUNC_2_T2447130374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.Object>
struct  Func_2_t2447130374  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T2447130374_H
#ifndef FUNC_2_T663966201_H
#define FUNC_2_T663966201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.EventHandler`1<System.Object>,System.Object>
struct  Func_2_t663966201  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T663966201_H
#ifndef LOGENTRY_T1141507113_H
#define LOGENTRY_T1141507113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Diagnostics.LogEntry
struct  LogEntry_t1141507113 
{
public:
	union
	{
		struct
		{
			// System.String UniRx.Diagnostics.LogEntry::<LoggerName>k__BackingField
			String_t* ___U3CLoggerNameU3Ek__BackingField_0;
			// UnityEngine.LogType UniRx.Diagnostics.LogEntry::<LogType>k__BackingField
			int32_t ___U3CLogTypeU3Ek__BackingField_1;
			// System.String UniRx.Diagnostics.LogEntry::<Message>k__BackingField
			String_t* ___U3CMessageU3Ek__BackingField_2;
			// System.DateTime UniRx.Diagnostics.LogEntry::<Timestamp>k__BackingField
			DateTime_t3738529785  ___U3CTimestampU3Ek__BackingField_3;
			// UnityEngine.Object UniRx.Diagnostics.LogEntry::<Context>k__BackingField
			Object_t631007953 * ___U3CContextU3Ek__BackingField_4;
			// System.Exception UniRx.Diagnostics.LogEntry::<Exception>k__BackingField
			Exception_t * ___U3CExceptionU3Ek__BackingField_5;
			// System.String UniRx.Diagnostics.LogEntry::<StackTrace>k__BackingField
			String_t* ___U3CStackTraceU3Ek__BackingField_6;
			// System.Object UniRx.Diagnostics.LogEntry::<State>k__BackingField
			RuntimeObject * ___U3CStateU3Ek__BackingField_7;
		};
		uint8_t LogEntry_t1141507113__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CLoggerNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CLoggerNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CLoggerNameU3Ek__BackingField_0() const { return ___U3CLoggerNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CLoggerNameU3Ek__BackingField_0() { return &___U3CLoggerNameU3Ek__BackingField_0; }
	inline void set_U3CLoggerNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CLoggerNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoggerNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CLogTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CLogTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CLogTypeU3Ek__BackingField_1() const { return ___U3CLogTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CLogTypeU3Ek__BackingField_1() { return &___U3CLogTypeU3Ek__BackingField_1; }
	inline void set_U3CLogTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CLogTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CMessageU3Ek__BackingField_2)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_2() const { return ___U3CMessageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_2() { return &___U3CMessageU3Ek__BackingField_2; }
	inline void set_U3CMessageU3Ek__BackingField_2(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CTimestampU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CTimestampU3Ek__BackingField_3)); }
	inline DateTime_t3738529785  get_U3CTimestampU3Ek__BackingField_3() const { return ___U3CTimestampU3Ek__BackingField_3; }
	inline DateTime_t3738529785 * get_address_of_U3CTimestampU3Ek__BackingField_3() { return &___U3CTimestampU3Ek__BackingField_3; }
	inline void set_U3CTimestampU3Ek__BackingField_3(DateTime_t3738529785  value)
	{
		___U3CTimestampU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CContextU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CContextU3Ek__BackingField_4)); }
	inline Object_t631007953 * get_U3CContextU3Ek__BackingField_4() const { return ___U3CContextU3Ek__BackingField_4; }
	inline Object_t631007953 ** get_address_of_U3CContextU3Ek__BackingField_4() { return &___U3CContextU3Ek__BackingField_4; }
	inline void set_U3CContextU3Ek__BackingField_4(Object_t631007953 * value)
	{
		___U3CContextU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContextU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CExceptionU3Ek__BackingField_5)); }
	inline Exception_t * get_U3CExceptionU3Ek__BackingField_5() const { return ___U3CExceptionU3Ek__BackingField_5; }
	inline Exception_t ** get_address_of_U3CExceptionU3Ek__BackingField_5() { return &___U3CExceptionU3Ek__BackingField_5; }
	inline void set_U3CExceptionU3Ek__BackingField_5(Exception_t * value)
	{
		___U3CExceptionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CStackTraceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CStackTraceU3Ek__BackingField_6)); }
	inline String_t* get_U3CStackTraceU3Ek__BackingField_6() const { return ___U3CStackTraceU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CStackTraceU3Ek__BackingField_6() { return &___U3CStackTraceU3Ek__BackingField_6; }
	inline void set_U3CStackTraceU3Ek__BackingField_6(String_t* value)
	{
		___U3CStackTraceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStackTraceU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CStateU3Ek__BackingField_7)); }
	inline RuntimeObject * get_U3CStateU3Ek__BackingField_7() const { return ___U3CStateU3Ek__BackingField_7; }
	inline RuntimeObject ** get_address_of_U3CStateU3Ek__BackingField_7() { return &___U3CStateU3Ek__BackingField_7; }
	inline void set_U3CStateU3Ek__BackingField_7(RuntimeObject * value)
	{
		___U3CStateU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStateU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UniRx.Diagnostics.LogEntry
struct LogEntry_t1141507113_marshaled_pinvoke
{
	union
	{
		struct
		{
			char* ___U3CLoggerNameU3Ek__BackingField_0;
			int32_t ___U3CLogTypeU3Ek__BackingField_1;
			char* ___U3CMessageU3Ek__BackingField_2;
			DateTime_t3738529785  ___U3CTimestampU3Ek__BackingField_3;
			Object_t631007953_marshaled_pinvoke ___U3CContextU3Ek__BackingField_4;
			Exception_t * ___U3CExceptionU3Ek__BackingField_5;
			char* ___U3CStackTraceU3Ek__BackingField_6;
			Il2CppIUnknown* ___U3CStateU3Ek__BackingField_7;
		};
		uint8_t LogEntry_t1141507113__padding[1];
	};
};
// Native definition for COM marshalling of UniRx.Diagnostics.LogEntry
struct LogEntry_t1141507113_marshaled_com
{
	union
	{
		struct
		{
			Il2CppChar* ___U3CLoggerNameU3Ek__BackingField_0;
			int32_t ___U3CLogTypeU3Ek__BackingField_1;
			Il2CppChar* ___U3CMessageU3Ek__BackingField_2;
			DateTime_t3738529785  ___U3CTimestampU3Ek__BackingField_3;
			Object_t631007953_marshaled_com* ___U3CContextU3Ek__BackingField_4;
			Exception_t * ___U3CExceptionU3Ek__BackingField_5;
			Il2CppChar* ___U3CStackTraceU3Ek__BackingField_6;
			Il2CppIUnknown* ___U3CStateU3Ek__BackingField_7;
		};
		uint8_t LogEntry_t1141507113__padding[1];
	};
};
#endif // LOGENTRY_T1141507113_H
#ifndef FUNC_3_T3398609381_H
#define FUNC_3_T3398609381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`3<System.Object,System.Object,System.Object>
struct  Func_3_t3398609381  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_3_T3398609381_H
#ifndef FUNC_1_T632984949_H
#define FUNC_1_T632984949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`1<UniRx.IObservable`1<System.Object>>
struct  Func_1_t632984949  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_1_T632984949_H
#ifndef ACTION_1_T3252573759_H
#define ACTION_1_T3252573759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<System.Object>
struct  Action_1_t3252573759  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T3252573759_H
#ifndef DATETIMEOFFSET_T3229287507_H
#define DATETIMEOFFSET_T3229287507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeOffset
struct  DateTimeOffset_t3229287507 
{
public:
	// System.DateTime System.DateTimeOffset::dt
	DateTime_t3738529785  ___dt_2;
	// System.TimeSpan System.DateTimeOffset::utc_offset
	TimeSpan_t881159249  ___utc_offset_3;

public:
	inline static int32_t get_offset_of_dt_2() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507, ___dt_2)); }
	inline DateTime_t3738529785  get_dt_2() const { return ___dt_2; }
	inline DateTime_t3738529785 * get_address_of_dt_2() { return &___dt_2; }
	inline void set_dt_2(DateTime_t3738529785  value)
	{
		___dt_2 = value;
	}

	inline static int32_t get_offset_of_utc_offset_3() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507, ___utc_offset_3)); }
	inline TimeSpan_t881159249  get_utc_offset_3() const { return ___utc_offset_3; }
	inline TimeSpan_t881159249 * get_address_of_utc_offset_3() { return &___utc_offset_3; }
	inline void set_utc_offset_3(TimeSpan_t881159249  value)
	{
		___utc_offset_3 = value;
	}
};

struct DateTimeOffset_t3229287507_StaticFields
{
public:
	// System.DateTimeOffset System.DateTimeOffset::MaxValue
	DateTimeOffset_t3229287507  ___MaxValue_0;
	// System.DateTimeOffset System.DateTimeOffset::MinValue
	DateTimeOffset_t3229287507  ___MinValue_1;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507_StaticFields, ___MaxValue_0)); }
	inline DateTimeOffset_t3229287507  get_MaxValue_0() const { return ___MaxValue_0; }
	inline DateTimeOffset_t3229287507 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(DateTimeOffset_t3229287507  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507_StaticFields, ___MinValue_1)); }
	inline DateTimeOffset_t3229287507  get_MinValue_1() const { return ___MinValue_1; }
	inline DateTimeOffset_t3229287507 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(DateTimeOffset_t3229287507  value)
	{
		___MinValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEOFFSET_T3229287507_H
#ifndef ACTION_1_T3401448761_H
#define ACTION_1_T3401448761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>
struct  Action_1_t3401448761  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T3401448761_H
#ifndef UNITYACTION_3_T1557236713_H
#define UNITYACTION_3_T1557236713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
struct  UnityAction_3_t1557236713  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_3_T1557236713_H
#ifndef ACTION_1_T3456439482_H
#define ACTION_1_T3456439482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.Events.UnityAction`2<System.Object,System.Object>>
struct  Action_1_t3456439482  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T3456439482_H
#ifndef FUNC_2_T1163256665_H
#define FUNC_2_T1163256665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Action`1<UniRx.Tuple`2<System.Object,System.Object>>,UnityEngine.Events.UnityAction`2<System.Object,System.Object>>
struct  Func_2_t1163256665  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T1163256665_H
#ifndef ACTION_1_T522702268_H
#define ACTION_1_T522702268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UniRx.Tuple`2<System.Object,System.Object>>
struct  Action_1_t522702268  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T522702268_H
#ifndef UNITYACTION_2_T3283971887_H
#define UNITYACTION_2_T3283971887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct  UnityAction_2_t3283971887  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_2_T3283971887_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef ACTION_2_T2470008838_H
#define ACTION_2_T2470008838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`2<System.Object,System.Object>
struct  Action_2_t2470008838  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_2_T2470008838_H
#ifndef ACTION_3_T2220624998_H
#define ACTION_3_T2220624998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`3<System.Exception,System.Object,System.Object>
struct  Action_3_t2220624998  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_3_T2220624998_H
#ifndef ACTION_3_T3632554945_H
#define ACTION_3_T3632554945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`3<System.Object,System.Object,System.Object>
struct  Action_3_t3632554945  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_3_T3632554945_H
#ifndef ACTION_1_T3534717062_H
#define ACTION_1_T3534717062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UniRx.Unit>
struct  Action_1_t3534717062  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T3534717062_H
#ifndef ACTION_1_T2159409255_H
#define ACTION_1_T2159409255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct  Action_1_t2159409255  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T2159409255_H
#ifndef ACTION_1_T1860522164_H
#define ACTION_1_T1860522164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>
struct  Action_1_t1860522164  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T1860522164_H
#ifndef ACTION_3_T1712484883_H
#define ACTION_3_T1712484883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`3<System.Single,System.Object,System.Object>
struct  Action_3_t1712484883  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_3_T1712484883_H
#ifndef ACTION_2_T2161070725_H
#define ACTION_2_T2161070725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`2<System.Exception,System.Object>
struct  Action_2_t2161070725  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_2_T2161070725_H
#ifndef TUPLE_4_T342572496_H
#define TUPLE_4_T342572496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple`4<System.Object,System.Boolean,System.Int32,System.DateTime>
struct  Tuple_4_t342572496 
{
public:
	// T1 UniRx.Tuple`4::item1
	RuntimeObject * ___item1_0;
	// T2 UniRx.Tuple`4::item2
	bool ___item2_1;
	// T3 UniRx.Tuple`4::item3
	int32_t ___item3_2;
	// T4 UniRx.Tuple`4::item4
	DateTime_t3738529785  ___item4_3;

public:
	inline static int32_t get_offset_of_item1_0() { return static_cast<int32_t>(offsetof(Tuple_4_t342572496, ___item1_0)); }
	inline RuntimeObject * get_item1_0() const { return ___item1_0; }
	inline RuntimeObject ** get_address_of_item1_0() { return &___item1_0; }
	inline void set_item1_0(RuntimeObject * value)
	{
		___item1_0 = value;
		Il2CppCodeGenWriteBarrier((&___item1_0), value);
	}

	inline static int32_t get_offset_of_item2_1() { return static_cast<int32_t>(offsetof(Tuple_4_t342572496, ___item2_1)); }
	inline bool get_item2_1() const { return ___item2_1; }
	inline bool* get_address_of_item2_1() { return &___item2_1; }
	inline void set_item2_1(bool value)
	{
		___item2_1 = value;
	}

	inline static int32_t get_offset_of_item3_2() { return static_cast<int32_t>(offsetof(Tuple_4_t342572496, ___item3_2)); }
	inline int32_t get_item3_2() const { return ___item3_2; }
	inline int32_t* get_address_of_item3_2() { return &___item3_2; }
	inline void set_item3_2(int32_t value)
	{
		___item3_2 = value;
	}

	inline static int32_t get_offset_of_item4_3() { return static_cast<int32_t>(offsetof(Tuple_4_t342572496, ___item4_3)); }
	inline DateTime_t3738529785  get_item4_3() const { return ___item4_3; }
	inline DateTime_t3738529785 * get_address_of_item4_3() { return &___item4_3; }
	inline void set_item4_3(DateTime_t3738529785  value)
	{
		___item4_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUPLE_4_T342572496_H
#ifndef ACTION_2_T2523487705_H
#define ACTION_2_T2523487705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`2<System.Boolean,System.Object>
struct  Action_2_t2523487705  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_2_T2523487705_H
#ifndef ACTION_4_T309985972_H
#define ACTION_4_T309985972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`4<System.Object,System.Object,System.Object,System.Object>
struct  Action_4_t309985972  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_4_T309985972_H
#ifndef ACTION_4_T1238479323_H
#define ACTION_4_T1238479323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`4<System.Exception,System.Object,System.Object,System.Object>
struct  Action_4_t1238479323  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_4_T1238479323_H
#ifndef ACTION_4_T327971120_H
#define ACTION_4_T327971120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`4<System.Int64,System.Object,System.Object,System.Object>
struct  Action_4_t327971120  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_4_T327971120_H
#ifndef ACTION_2_T2185242338_H
#define ACTION_2_T2185242338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`2<System.Int64,System.Object>
struct  Action_2_t2185242338  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_2_T2185242338_H
#ifndef ACTION_1_T1569734369_H
#define ACTION_1_T1569734369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<System.Single>
struct  Action_1_t1569734369  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T1569734369_H
#ifndef ACTION_1_T2328697118_H
#define ACTION_1_T2328697118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.Vector2>
struct  Action_1_t2328697118  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T2328697118_H
#ifndef ACTION_2_T1614770371_H
#define ACTION_2_T1614770371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`2<UniRx.Unit,System.Object>
struct  Action_2_t1614770371  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_2_T1614770371_H
#ifndef ACTION_T1264377477_H
#define ACTION_T1264377477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action
struct  Action_t1264377477  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T1264377477_H
#ifndef ACTION_1_T1609204844_H
#define ACTION_1_T1609204844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<System.Exception>
struct  Action_1_t1609204844  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T1609204844_H
#ifndef FUNC_1_T1283030885_H
#define FUNC_1_T1283030885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`1<System.Collections.IEnumerator>
struct  Func_1_t1283030885  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_1_T1283030885_H
#ifndef FUNC_2_T4286427857_H
#define FUNC_2_T4286427857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Action,System.Object>
struct  Func_2_t4286427857  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T4286427857_H
#ifndef ACTION_2_T2340848427_H
#define ACTION_2_T2340848427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`2<System.Object,System.Int32>
struct  Action_2_t2340848427  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_2_T2340848427_H
#ifndef EVENTFUNCTION_1_T1764640198_H
#define EVENTFUNCTION_1_T1764640198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct  EventFunction_1_t1764640198  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTFUNCTION_1_T1764640198_H
#ifndef FUNC_2_T1220308448_H
#define FUNC_2_T1220308448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Object,System.Collections.IEnumerator>
struct  Func_2_t1220308448  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T1220308448_H
#ifndef ACTION_1_T3909034899_H
#define ACTION_1_T3909034899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<System.Int64>
struct  Action_1_t3909034899  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T3909034899_H
#ifndef ACTION_1_T3123413348_H
#define ACTION_1_T3123413348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<System.Int32>
struct  Action_1_t3123413348  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T3123413348_H
#ifndef UNITYACTION_1_T682124106_H
#define UNITYACTION_1_T682124106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Boolean>
struct  UnityAction_1_t682124106  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T682124106_H
#ifndef UNITYACTION_1_T3664942305_H
#define UNITYACTION_1_T3664942305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Object>
struct  UnityAction_1_t3664942305  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T3664942305_H
#ifndef UNITYACTION_1_T1982102915_H
#define UNITYACTION_1_T1982102915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<System.Single>
struct  UnityAction_1_t1982102915  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T1982102915_H
#ifndef ACTION_1_T269755560_H
#define ACTION_1_T269755560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<System.Boolean>
struct  Action_1_t269755560  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T269755560_H
#ifndef UNITYACTION_1_T2741065664_H
#define UNITYACTION_1_T2741065664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
struct  UnityAction_1_t2741065664  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_1_T2741065664_H
#ifndef ACTION_1_T1313974708_H
#define ACTION_1_T1313974708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UniRx.Diagnostics.LogEntry>
struct  Action_1_t1313974708  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T1313974708_H
#ifndef TIMESTAMPED_1_T1881506576_H
#define TIMESTAMPED_1_T1881506576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Timestamped`1<System.Object>
struct  Timestamped_1_t1881506576 
{
public:
	// System.DateTimeOffset UniRx.Timestamped`1::_timestamp
	DateTimeOffset_t3229287507  ____timestamp_0;
	// T UniRx.Timestamped`1::_value
	RuntimeObject * ____value_1;

public:
	inline static int32_t get_offset_of__timestamp_0() { return static_cast<int32_t>(offsetof(Timestamped_1_t1881506576, ____timestamp_0)); }
	inline DateTimeOffset_t3229287507  get__timestamp_0() const { return ____timestamp_0; }
	inline DateTimeOffset_t3229287507 * get_address_of__timestamp_0() { return &____timestamp_0; }
	inline void set__timestamp_0(DateTimeOffset_t3229287507  value)
	{
		____timestamp_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(Timestamped_1_t1881506576, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPED_1_T1881506576_H
// UniRx.IObservable`1<System.Object>[]
struct IObservable_1U5BU5D_t3893959211  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Func`2<System.Object,UniRx.IObservable`1<UniRx.Unit>>[]
struct Func_2U5BU5D_t1724110606  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Func_2_t852405815 * m_Items[1];

public:
	inline Func_2_t852405815 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Func_2_t852405815 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Func_2_t852405815 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Func_2_t852405815 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Func_2_t852405815 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Func_2_t852405815 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObservable`1<UniRx.Unit>[]
struct IObservable_1U5BU5D_t3038720744  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Nullable`1<System.Int32>::.ctor(!0)
extern "C"  void Nullable_1__ctor_m3940678751_gshared (Nullable_1_t378540539 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3280774074_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject ** p1, const RuntimeMethod* method);
// UniRx.IObservable`1<T> UniRx.Observable::Take<UniRx.Unit>(UniRx.IObservable`1<T>,System.Int32)
extern "C"  RuntimeObject* Observable_Take_TisUnit_t3362249467_m3583177933_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, int32_t ___count1, const RuntimeMethod* method);
// System.Void System.Func`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_1__ctor_m298111648_gshared (Func_1_t2509852811 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void UniRx.Timestamped`1<System.Object>::.ctor(T,System.DateTimeOffset)
extern "C"  void Timestamped_1__ctor_m21775297_gshared (Timestamped_1_t1881506576 * __this, RuntimeObject * p0, DateTimeOffset_t3229287507  p1, const RuntimeMethod* method);
// System.Void UniRx.Tuple`1<System.Object>::.ctor(T1)
extern "C"  void Tuple_1__ctor_m946716862_gshared (Tuple_1_t4248013280 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void UniRx.Tuple`2<System.Object,System.Object>::.ctor(T1,T2)
extern "C"  void Tuple_2__ctor_m1110087791_gshared (Tuple_2_t350234673 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void UniRx.Tuple`3<System.Object,System.Object,System.Object>::.ctor(T1,T2,T3)
extern "C"  void Tuple_3__ctor_m811173283_gshared (Tuple_3_t3228981166 * __this, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Void UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>::.ctor(T1,T2,T3)
extern "C"  void Tuple_3__ctor_m3344996810_gshared (Tuple_3_t2373742699 * __this, RuntimeObject * p0, Unit_t3362249467  p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Void UniRx.Tuple`4<System.Object,System.Boolean,System.Int32,System.DateTime>::.ctor(T1,T2,T3,T4)
extern "C"  void Tuple_4__ctor_m36515419_gshared (Tuple_4_t342572496 * __this, RuntimeObject * p0, bool p1, int32_t p2, DateTime_t3738529785  p3, const RuntimeMethod* method);
// System.Void UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::.ctor(T1,T2,T3,T4)
extern "C"  void Tuple_4__ctor_m1088025688_gshared (Tuple_4_t2649139811 * __this, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, const RuntimeMethod* method);
// System.Void UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(T1,T2,T3,T4,T5)
extern "C"  void Tuple_5__ctor_m2288603759_gshared (Tuple_5_t2125722268 * __this, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, const RuntimeMethod* method);
// System.Void UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(T1,T2,T3,T4,T5,T6)
extern "C"  void Tuple_6__ctor_m2315075868_gshared (Tuple_6_t2833421365 * __this, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, RuntimeObject * p5, const RuntimeMethod* method);
// System.Void UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(T1,T2,T3,T4,T5,T6,T7)
extern "C"  void Tuple_7__ctor_m3950978026_gshared (Tuple_7_t2257463594 * __this, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, RuntimeObject * p3, RuntimeObject * p4, RuntimeObject * p5, RuntimeObject * p6, const RuntimeMethod* method);
// System.Void Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/OfComponentEnumerable`1<System.Object>::.ctor(Unity.Linq.GameObjectExtensions/AfterSelfEnumerable&)
extern "C"  void OfComponentEnumerable_1__ctor_m3058731097_gshared (OfComponentEnumerable_1_t80195040 * __this, AfterSelfEnumerable_t4038755258 * p0, const RuntimeMethod* method);
// Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/OfComponentEnumerable`1<T> Unity.Linq.GameObjectExtensions/AfterSelfEnumerable::OfComponent<System.Object>()
extern "C"  OfComponentEnumerable_1_t80195040  AfterSelfEnumerable_OfComponent_TisRuntimeObject_m3894075102_gshared (AfterSelfEnumerable_t4038755258 * __this, const RuntimeMethod* method);
// System.Void Unity.Linq.GameObjectExtensions/AncestorsEnumerable/OfComponentEnumerable`1<System.Object>::.ctor(Unity.Linq.GameObjectExtensions/AncestorsEnumerable&)
extern "C"  void OfComponentEnumerable_1__ctor_m24595556_gshared (OfComponentEnumerable_1_t3213154383 * __this, AncestorsEnumerable_t953073017 * p0, const RuntimeMethod* method);
// Unity.Linq.GameObjectExtensions/AncestorsEnumerable/OfComponentEnumerable`1<T> Unity.Linq.GameObjectExtensions/AncestorsEnumerable::OfComponent<System.Object>()
extern "C"  OfComponentEnumerable_1_t3213154383  AncestorsEnumerable_OfComponent_TisRuntimeObject_m1961088017_gshared (AncestorsEnumerable_t953073017 * __this, const RuntimeMethod* method);
// System.Void Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/OfComponentEnumerable`1<System.Object>::.ctor(Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable&)
extern "C"  void OfComponentEnumerable_1__ctor_m2906258269_gshared (OfComponentEnumerable_1_t3909910951 * __this, BeforeSelfEnumerable_t2845913035 * p0, const RuntimeMethod* method);
// Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/OfComponentEnumerable`1<T> Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable::OfComponent<System.Object>()
extern "C"  OfComponentEnumerable_1_t3909910951  BeforeSelfEnumerable_OfComponent_TisRuntimeObject_m1141516106_gshared (BeforeSelfEnumerable_t2845913035 * __this, const RuntimeMethod* method);
// System.Void Unity.Linq.GameObjectExtensions/ChildrenEnumerable/OfComponentEnumerable`1<System.Object>::.ctor(Unity.Linq.GameObjectExtensions/ChildrenEnumerable&)
extern "C"  void OfComponentEnumerable_1__ctor_m1320143178_gshared (OfComponentEnumerable_1_t1188510407 * __this, ChildrenEnumerable_t145725860 * p0, const RuntimeMethod* method);
// Unity.Linq.GameObjectExtensions/ChildrenEnumerable/OfComponentEnumerable`1<T> Unity.Linq.GameObjectExtensions/ChildrenEnumerable::OfComponent<System.Object>()
extern "C"  OfComponentEnumerable_1_t1188510407  ChildrenEnumerable_OfComponent_TisRuntimeObject_m2868525068_gshared (ChildrenEnumerable_t145725860 * __this, const RuntimeMethod* method);
// System.Void Unity.Linq.GameObjectExtensions/DescendantsEnumerable/OfComponentEnumerable`1<System.Object>::.ctor(Unity.Linq.GameObjectExtensions/DescendantsEnumerable&)
extern "C"  void OfComponentEnumerable_1__ctor_m1823402387_gshared (OfComponentEnumerable_1_t908350185 * __this, DescendantsEnumerable_t4167108050 * p0, const RuntimeMethod* method);
// Unity.Linq.GameObjectExtensions/DescendantsEnumerable/OfComponentEnumerable`1<T> Unity.Linq.GameObjectExtensions/DescendantsEnumerable::OfComponent<System.Object>()
extern "C"  OfComponentEnumerable_1_t908350185  DescendantsEnumerable_OfComponent_TisRuntimeObject_m4141729503_gshared (DescendantsEnumerable_t4167108050 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);

// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::get_ConstantTimeOperations()
extern "C"  RuntimeObject* DefaultSchedulers_get_ConstantTimeOperations_m2337137380 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::get_TimeBasedOperations()
extern "C"  RuntimeObject* DefaultSchedulers_get_TimeBasedOperations_m2660764704 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m3628145864 (ArgumentOutOfRangeException_t777629997 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Nullable`1<System.Int32>::.ctor(!0)
#define Nullable_1__ctor_m3940678751(__this, p0, method) ((  void (*) (Nullable_1_t378540539 *, int32_t, const RuntimeMethod*))Nullable_1__ctor_m3940678751_gshared)(__this, p0, method)
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m2249409497 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObjectDisposedException::.ctor(System.String)
extern "C"  void ObjectDisposedException__ctor_m3603759869 (ObjectDisposedException_t21392786 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Object>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m3038145912(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t1229485932 *, Type_t *, RuntimeObject **, const RuntimeMethod*))Dictionary_2_TryGetValue_m3280774074_gshared)(__this, p0, p1, method)
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::ReturnUnit()
extern "C"  RuntimeObject* Observable_ReturnUnit_m3743972880 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m3585316909 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::WhenAll(UniRx.IObservable`1<UniRx.Unit>[])
extern "C"  RuntimeObject* Observable_WhenAll_m331076954 (RuntimeObject * __this /* static, unused */, IObservable_1U5BU5D_t3038720744* ___sources0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.MainThreadDispatcher::UpdateAsObservable()
extern "C"  RuntimeObject* MainThreadDispatcher_UpdateAsObservable_m3818177576 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<T> UniRx.Observable::Take<UniRx.Unit>(UniRx.IObservable`1<T>,System.Int32)
#define Observable_Take_TisUnit_t3362249467_m3583177933(__this /* static, unused */, ___source0, ___count1, method) ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, int32_t, const RuntimeMethod*))Observable_Take_TisUnit_t3362249467_m3583177933_gshared)(__this /* static, unused */, ___source0, ___count1, method)
// UniRx.IObservable`1<UniRx.Unit> UniRx.MainThreadDispatcher::LateUpdateAsObservable()
extern "C"  RuntimeObject* MainThreadDispatcher_LateUpdateAsObservable_m4277804338 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`1<System.Collections.IEnumerator>::.ctor(System.Object,System.IntPtr)
#define Func_1__ctor_m3733315985(__this, p0, p1, method) ((  void (*) (Func_1_t1283030885 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_1__ctor_m298111648_gshared)(__this, p0, p1, method)
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::FromCoroutine(System.Func`1<System.Collections.IEnumerator>,System.Boolean)
extern "C"  RuntimeObject* Observable_FromCoroutine_m1963572360 (RuntimeObject * __this /* static, unused */, Func_1_t1283030885 * ___coroutine0, bool ___publishEveryYield1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Delegate::op_Equality(System.Delegate,System.Delegate)
extern "C"  bool Delegate_op_Equality_m1690449587 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Timestamped`1<System.Object>::.ctor(T,System.DateTimeOffset)
#define Timestamped_1__ctor_m21775297(__this, p0, p1, method) ((  void (*) (Timestamped_1_t1881506576 *, RuntimeObject *, DateTimeOffset_t3229287507 , const RuntimeMethod*))Timestamped_1__ctor_m21775297_gshared)(__this, p0, p1, method)
// System.Void UniRx.Tuple`1<System.Object>::.ctor(T1)
#define Tuple_1__ctor_m946716862(__this, p0, method) ((  void (*) (Tuple_1_t4248013280 *, RuntimeObject *, const RuntimeMethod*))Tuple_1__ctor_m946716862_gshared)(__this, p0, method)
// System.Void UniRx.Tuple`2<System.Object,System.Object>::.ctor(T1,T2)
#define Tuple_2__ctor_m1110087791(__this, p0, p1, method) ((  void (*) (Tuple_2_t350234673 *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))Tuple_2__ctor_m1110087791_gshared)(__this, p0, p1, method)
// System.Void UniRx.Tuple`3<System.Object,System.Object,System.Object>::.ctor(T1,T2,T3)
#define Tuple_3__ctor_m811173283(__this, p0, p1, p2, method) ((  void (*) (Tuple_3_t3228981166 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))Tuple_3__ctor_m811173283_gshared)(__this, p0, p1, p2, method)
// System.Void UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>::.ctor(T1,T2,T3)
#define Tuple_3__ctor_m3344996810(__this, p0, p1, p2, method) ((  void (*) (Tuple_3_t2373742699 *, RuntimeObject *, Unit_t3362249467 , RuntimeObject *, const RuntimeMethod*))Tuple_3__ctor_m3344996810_gshared)(__this, p0, p1, p2, method)
// System.Void UniRx.Tuple`4<System.Object,System.Boolean,System.Int32,System.DateTime>::.ctor(T1,T2,T3,T4)
#define Tuple_4__ctor_m36515419(__this, p0, p1, p2, p3, method) ((  void (*) (Tuple_4_t342572496 *, RuntimeObject *, bool, int32_t, DateTime_t3738529785 , const RuntimeMethod*))Tuple_4__ctor_m36515419_gshared)(__this, p0, p1, p2, p3, method)
// System.Void UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>::.ctor(T1,T2,T3,T4)
#define Tuple_4__ctor_m1088025688(__this, p0, p1, p2, p3, method) ((  void (*) (Tuple_4_t2649139811 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))Tuple_4__ctor_m1088025688_gshared)(__this, p0, p1, p2, p3, method)
// System.Void UniRx.Tuple`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(T1,T2,T3,T4,T5)
#define Tuple_5__ctor_m2288603759(__this, p0, p1, p2, p3, p4, method) ((  void (*) (Tuple_5_t2125722268 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))Tuple_5__ctor_m2288603759_gshared)(__this, p0, p1, p2, p3, p4, method)
// System.Void UniRx.Tuple`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(T1,T2,T3,T4,T5,T6)
#define Tuple_6__ctor_m2315075868(__this, p0, p1, p2, p3, p4, p5, method) ((  void (*) (Tuple_6_t2833421365 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))Tuple_6__ctor_m2315075868_gshared)(__this, p0, p1, p2, p3, p4, p5, method)
// System.Void UniRx.Tuple`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(T1,T2,T3,T4,T5,T6,T7)
#define Tuple_7__ctor_m3950978026(__this, p0, p1, p2, p3, p4, p5, p6, method) ((  void (*) (Tuple_7_t2257463594 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))Tuple_7__ctor_m3950978026_gshared)(__this, p0, p1, p2, p3, p4, p5, p6, method)
// System.Void Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/OfComponentEnumerable`1<System.Object>::.ctor(Unity.Linq.GameObjectExtensions/AfterSelfEnumerable&)
#define OfComponentEnumerable_1__ctor_m3058731097(__this, p0, method) ((  void (*) (OfComponentEnumerable_1_t80195040 *, AfterSelfEnumerable_t4038755258 *, const RuntimeMethod*))OfComponentEnumerable_1__ctor_m3058731097_gshared)(__this, p0, method)
// Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/OfComponentEnumerable`1<T> Unity.Linq.GameObjectExtensions/AfterSelfEnumerable::OfComponent<System.Object>()
#define AfterSelfEnumerable_OfComponent_TisRuntimeObject_m3894075102(__this, method) ((  OfComponentEnumerable_1_t80195040  (*) (AfterSelfEnumerable_t4038755258 *, const RuntimeMethod*))AfterSelfEnumerable_OfComponent_TisRuntimeObject_m3894075102_gshared)(__this, method)
// System.Void Unity.Linq.GameObjectExtensions/AncestorsEnumerable/OfComponentEnumerable`1<System.Object>::.ctor(Unity.Linq.GameObjectExtensions/AncestorsEnumerable&)
#define OfComponentEnumerable_1__ctor_m24595556(__this, p0, method) ((  void (*) (OfComponentEnumerable_1_t3213154383 *, AncestorsEnumerable_t953073017 *, const RuntimeMethod*))OfComponentEnumerable_1__ctor_m24595556_gshared)(__this, p0, method)
// Unity.Linq.GameObjectExtensions/AncestorsEnumerable/OfComponentEnumerable`1<T> Unity.Linq.GameObjectExtensions/AncestorsEnumerable::OfComponent<System.Object>()
#define AncestorsEnumerable_OfComponent_TisRuntimeObject_m1961088017(__this, method) ((  OfComponentEnumerable_1_t3213154383  (*) (AncestorsEnumerable_t953073017 *, const RuntimeMethod*))AncestorsEnumerable_OfComponent_TisRuntimeObject_m1961088017_gshared)(__this, method)
// System.Void Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/OfComponentEnumerable`1<System.Object>::.ctor(Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable&)
#define OfComponentEnumerable_1__ctor_m2906258269(__this, p0, method) ((  void (*) (OfComponentEnumerable_1_t3909910951 *, BeforeSelfEnumerable_t2845913035 *, const RuntimeMethod*))OfComponentEnumerable_1__ctor_m2906258269_gshared)(__this, p0, method)
// Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/OfComponentEnumerable`1<T> Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable::OfComponent<System.Object>()
#define BeforeSelfEnumerable_OfComponent_TisRuntimeObject_m1141516106(__this, method) ((  OfComponentEnumerable_1_t3909910951  (*) (BeforeSelfEnumerable_t2845913035 *, const RuntimeMethod*))BeforeSelfEnumerable_OfComponent_TisRuntimeObject_m1141516106_gshared)(__this, method)
// System.Void Unity.Linq.GameObjectExtensions/ChildrenEnumerable/OfComponentEnumerable`1<System.Object>::.ctor(Unity.Linq.GameObjectExtensions/ChildrenEnumerable&)
#define OfComponentEnumerable_1__ctor_m1320143178(__this, p0, method) ((  void (*) (OfComponentEnumerable_1_t1188510407 *, ChildrenEnumerable_t145725860 *, const RuntimeMethod*))OfComponentEnumerable_1__ctor_m1320143178_gshared)(__this, p0, method)
// Unity.Linq.GameObjectExtensions/ChildrenEnumerable/OfComponentEnumerable`1<T> Unity.Linq.GameObjectExtensions/ChildrenEnumerable::OfComponent<System.Object>()
#define ChildrenEnumerable_OfComponent_TisRuntimeObject_m2868525068(__this, method) ((  OfComponentEnumerable_1_t1188510407  (*) (ChildrenEnumerable_t145725860 *, const RuntimeMethod*))ChildrenEnumerable_OfComponent_TisRuntimeObject_m2868525068_gshared)(__this, method)
// System.Void Unity.Linq.GameObjectExtensions/DescendantsEnumerable/OfComponentEnumerable`1<System.Object>::.ctor(Unity.Linq.GameObjectExtensions/DescendantsEnumerable&)
#define OfComponentEnumerable_1__ctor_m1823402387(__this, p0, method) ((  void (*) (OfComponentEnumerable_1_t908350185 *, DescendantsEnumerable_t4167108050 *, const RuntimeMethod*))OfComponentEnumerable_1__ctor_m1823402387_gshared)(__this, p0, method)
// Unity.Linq.GameObjectExtensions/DescendantsEnumerable/OfComponentEnumerable`1<T> Unity.Linq.GameObjectExtensions/DescendantsEnumerable::OfComponent<System.Object>()
#define DescendantsEnumerable_OfComponent_TisRuntimeObject_m4141729503(__this, method) ((  OfComponentEnumerable_1_t908350185  (*) (DescendantsEnumerable_t4167108050 *, const RuntimeMethod*))DescendantsEnumerable_OfComponent_TisRuntimeObject_m4141729503_gshared)(__this, method)
// UnityEngine.Coroutine UniRx.MainThreadDispatcher::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3829159415 * MainThreadDispatcher_StartCoroutine_m384510185 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___routine0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::GetEventChain(UnityEngine.GameObject,System.Collections.Generic.IList`1<UnityEngine.Transform>)
extern "C"  void ExecuteEvents_GetEventChain_m2404658789 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___root0, RuntimeObject* ___eventChain1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32)
#define List_1_get_Item_m2402360903(__this, p0, method) ((  Transform_t3600365921 * (*) (List_1_t777473367 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count()
#define List_1_get_Count_m3543896146(__this, method) ((  int32_t (*) (List_1_t777473367 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3600365921 * Transform_get_parent_m835071599 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<TSource> UniRx.Observable::Merge<System.Object>(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<TSource>>,System.Int32)
extern "C"  RuntimeObject* Observable_Merge_TisRuntimeObject_m2084972840_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___sources0, int32_t ___maxConcurrent1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_Merge_TisRuntimeObject_m2084972840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___sources0;
		int32_t L_1 = ___maxConcurrent1;
		RuntimeObject* L_2 = DefaultSchedulers_get_ConstantTimeOperations_m2337137380(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, int32_t, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (int32_t)L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::Merge<System.Object>(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<TSource>>,System.Int32,UniRx.IScheduler)
extern "C"  RuntimeObject* Observable_Merge_TisRuntimeObject_m2448690970_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___sources0, int32_t ___maxConcurrent1, RuntimeObject* ___scheduler2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_Merge_TisRuntimeObject_m2448690970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___sources0;
		RuntimeObject* L_1 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_2 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		int32_t L_3 = ___maxConcurrent1;
		RuntimeObject* L_4 = ___scheduler2;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t1572131957_il2cpp_TypeInfo_var);
		RuntimeObject* L_5 = ((Scheduler_t1572131957_StaticFields*)il2cpp_codegen_static_fields_for(Scheduler_t1572131957_il2cpp_TypeInfo_var))->get_CurrentThread_0();
		MergeObservable_1_t1765447768 * L_6 = (MergeObservable_1_t1765447768 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (MergeObservable_1_t1765447768 *, RuntimeObject*, int32_t, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_6, (RuntimeObject*)L_2, (int32_t)L_3, (bool)((((RuntimeObject*)(RuntimeObject*)L_4) == ((RuntimeObject*)(RuntimeObject*)L_5))? 1 : 0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_6;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::Merge<System.Object>(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<TSource>>,UniRx.IScheduler)
extern "C"  RuntimeObject* Observable_Merge_TisRuntimeObject_m1371129573_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___sources0, RuntimeObject* ___scheduler1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_Merge_TisRuntimeObject_m1371129573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___sources0;
		RuntimeObject* L_1 = ___scheduler1;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_2 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		RuntimeObject* L_3 = ___scheduler1;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t1572131957_il2cpp_TypeInfo_var);
		RuntimeObject* L_4 = ((Scheduler_t1572131957_StaticFields*)il2cpp_codegen_static_fields_for(Scheduler_t1572131957_il2cpp_TypeInfo_var))->get_CurrentThread_0();
		MergeObservable_1_t1765447768 * L_5 = (MergeObservable_1_t1765447768 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (MergeObservable_1_t1765447768 *, RuntimeObject*, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_5, (RuntimeObject*)L_2, (bool)((((RuntimeObject*)(RuntimeObject*)L_3) == ((RuntimeObject*)(RuntimeObject*)L_4))? 1 : 0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_5;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::Merge<System.Object>(UniRx.IObservable`1<TSource>[])
extern "C"  RuntimeObject* Observable_Merge_TisRuntimeObject_m2375299036_gshared (RuntimeObject * __this /* static, unused */, IObservable_1U5BU5D_t3893959211* ___sources0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_Merge_TisRuntimeObject_m2375299036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = DefaultSchedulers_get_ConstantTimeOperations_m2337137380(NULL /*static, unused*/, /*hidden argument*/NULL);
		IObservable_1U5BU5D_t3893959211* L_1 = ___sources0;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_2 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, IObservable_1U5BU5D_t3893959211*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (IObservable_1U5BU5D_t3893959211*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::Merge<System.Object>(UniRx.IScheduler,UniRx.IObservable`1<TSource>[])
extern "C"  RuntimeObject* Observable_Merge_TisRuntimeObject_m2201465073_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___scheduler0, IObservable_1U5BU5D_t3893959211* ___sources1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_Merge_TisRuntimeObject_m2201465073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IObservable_1U5BU5D_t3893959211* L_0 = ___sources1;
		RuntimeObject* L_1 = ___scheduler0;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_2 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)(RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		RuntimeObject* L_3 = ___scheduler0;
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t1572131957_il2cpp_TypeInfo_var);
		RuntimeObject* L_4 = ((Scheduler_t1572131957_StaticFields*)il2cpp_codegen_static_fields_for(Scheduler_t1572131957_il2cpp_TypeInfo_var))->get_CurrentThread_0();
		MergeObservable_1_t1765447768 * L_5 = (MergeObservable_1_t1765447768 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (MergeObservable_1_t1765447768 *, RuntimeObject*, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_5, (RuntimeObject*)L_2, (bool)((((RuntimeObject*)(RuntimeObject*)L_3) == ((RuntimeObject*)(RuntimeObject*)L_4))? 1 : 0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_5;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::OnErrorRetry<System.Object,System.Object>(UniRx.IObservable`1<TSource>,System.Action`1<TException>)
extern "C"  RuntimeObject* Observable_OnErrorRetry_TisRuntimeObject_TisRuntimeObject_m2365061993_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Action_1_t3252573759 * ___onError1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_OnErrorRetry_TisRuntimeObject_TisRuntimeObject_m2365061993_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		Action_1_t3252573759 * L_1 = ___onError1;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t881159249_il2cpp_TypeInfo_var);
		TimeSpan_t881159249  L_2 = ((TimeSpan_t881159249_StaticFields*)il2cpp_codegen_static_fields_for(TimeSpan_t881159249_il2cpp_TypeInfo_var))->get_Zero_7();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Action_1_t3252573759 *, TimeSpan_t881159249 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (Action_1_t3252573759 *)L_1, (TimeSpan_t881159249 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::OnErrorRetry<System.Object,System.Object>(UniRx.IObservable`1<TSource>,System.Action`1<TException>,System.Int32)
extern "C"  RuntimeObject* Observable_OnErrorRetry_TisRuntimeObject_TisRuntimeObject_m3057258735_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Action_1_t3252573759 * ___onError1, int32_t ___retryCount2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_OnErrorRetry_TisRuntimeObject_TisRuntimeObject_m3057258735_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		Action_1_t3252573759 * L_1 = ___onError1;
		int32_t L_2 = ___retryCount2;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t881159249_il2cpp_TypeInfo_var);
		TimeSpan_t881159249  L_3 = ((TimeSpan_t881159249_StaticFields*)il2cpp_codegen_static_fields_for(TimeSpan_t881159249_il2cpp_TypeInfo_var))->get_Zero_7();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_4 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Action_1_t3252573759 *, int32_t, TimeSpan_t881159249 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (Action_1_t3252573759 *)L_1, (int32_t)L_2, (TimeSpan_t881159249 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_4;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::OnErrorRetry<System.Object,System.Object>(UniRx.IObservable`1<TSource>,System.Action`1<TException>,System.Int32,System.TimeSpan)
extern "C"  RuntimeObject* Observable_OnErrorRetry_TisRuntimeObject_TisRuntimeObject_m35022389_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Action_1_t3252573759 * ___onError1, int32_t ___retryCount2, TimeSpan_t881159249  ___delay3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_OnErrorRetry_TisRuntimeObject_TisRuntimeObject_m35022389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		Action_1_t3252573759 * L_1 = ___onError1;
		int32_t L_2 = ___retryCount2;
		TimeSpan_t881159249  L_3 = ___delay3;
		RuntimeObject* L_4 = DefaultSchedulers_get_TimeBasedOperations_m2660764704(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_5 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Action_1_t3252573759 *, int32_t, TimeSpan_t881159249 , RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (Action_1_t3252573759 *)L_1, (int32_t)L_2, (TimeSpan_t881159249 )L_3, (RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_5;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::OnErrorRetry<System.Object,System.Object>(UniRx.IObservable`1<TSource>,System.Action`1<TException>,System.Int32,System.TimeSpan,UniRx.IScheduler)
extern "C"  RuntimeObject* Observable_OnErrorRetry_TisRuntimeObject_TisRuntimeObject_m2883033659_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Action_1_t3252573759 * ___onError1, int32_t ___retryCount2, TimeSpan_t881159249  ___delay3, RuntimeObject* ___delayScheduler4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_OnErrorRetry_TisRuntimeObject_TisRuntimeObject_m2883033659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737 * L_0 = (U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737 *)L_0;
		U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737 * L_1 = V_0;
		TimeSpan_t881159249  L_2 = ___delay3;
		NullCheck(L_1);
		L_1->set_delay_0(L_2);
		U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737 * L_3 = V_0;
		RuntimeObject* L_4 = ___source0;
		NullCheck(L_3);
		L_3->set_source_1(L_4);
		U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737 * L_5 = V_0;
		Action_1_t3252573759 * L_6 = ___onError1;
		NullCheck(L_5);
		L_5->set_onError_2(L_6);
		U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737 * L_7 = V_0;
		int32_t L_8 = ___retryCount2;
		NullCheck(L_7);
		L_7->set_retryCount_3(L_8);
		U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737 * L_9 = V_0;
		RuntimeObject* L_10 = ___delayScheduler4;
		NullCheck(L_9);
		L_9->set_delayScheduler_4(L_10);
		U3COnErrorRetryU3Ec__AnonStorey12_2_t1114935737 * L_11 = V_0;
		intptr_t L_12 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2);
		Func_1_t632984949 * L_13 = (Func_1_t632984949 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (Func_1_t632984949 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_13, (RuntimeObject *)L_11, (intptr_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_14 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, Func_1_t632984949 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Func_1_t632984949 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5));
		V_1 = (RuntimeObject*)L_14;
		RuntimeObject* L_15 = V_1;
		return L_15;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::OnErrorRetry<System.Object,System.Object>(UniRx.IObservable`1<TSource>,System.Action`1<TException>,System.TimeSpan)
extern "C"  RuntimeObject* Observable_OnErrorRetry_TisRuntimeObject_TisRuntimeObject_m1898518509_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Action_1_t3252573759 * ___onError1, TimeSpan_t881159249  ___delay2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_OnErrorRetry_TisRuntimeObject_TisRuntimeObject_m1898518509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		Action_1_t3252573759 * L_1 = ___onError1;
		TimeSpan_t881159249  L_2 = ___delay2;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Action_1_t3252573759 *, int32_t, TimeSpan_t881159249 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (Action_1_t3252573759 *)L_1, (int32_t)((int32_t)2147483647LL), (TimeSpan_t881159249 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::OnErrorRetry<System.Object>(UniRx.IObservable`1<TSource>)
extern "C"  RuntimeObject* Observable_OnErrorRetry_TisRuntimeObject_m3009981768_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_OnErrorRetry_TisRuntimeObject_m3009981768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		RuntimeObject* L_0 = ___source0;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (RuntimeObject*)L_1;
		RuntimeObject* L_2 = V_0;
		return L_2;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::Retry<System.Object>(UniRx.IObservable`1<TSource>)
extern "C"  RuntimeObject* Observable_Retry_TisRuntimeObject_m1366679961_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_Retry_TisRuntimeObject_m1366679961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		RuntimeObject* L_2 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::Retry<System.Object>(UniRx.IObservable`1<TSource>,System.Int32)
extern "C"  RuntimeObject* Observable_Retry_TisRuntimeObject_m2603381968_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, int32_t ___retryCount1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_Retry_TisRuntimeObject_m2603381968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		int32_t L_1 = ___retryCount1;
		RuntimeObject* L_2 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::Scan<System.Object>(UniRx.IObservable`1<TSource>,System.Func`3<TSource,TSource,TSource>)
extern "C"  RuntimeObject* Observable_Scan_TisRuntimeObject_m4021382163_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Func_3_t3398609381 * ___accumulator1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		Func_3_t3398609381 * L_1 = ___accumulator1;
		ScanObservable_1_t1685372452 * L_2 = (ScanObservable_1_t1685372452 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ScanObservable_1_t1685372452 *, RuntimeObject*, Func_3_t3398609381 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (Func_3_t3398609381 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::Throttle<System.Int64>(UniRx.IObservable`1<TSource>,System.TimeSpan)
extern "C"  RuntimeObject* Observable_Throttle_TisInt64_t3736567304_m1457699944_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, TimeSpan_t881159249  ___dueTime1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_Throttle_TisInt64_t3736567304_m1457699944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		TimeSpan_t881159249  L_1 = ___dueTime1;
		RuntimeObject* L_2 = DefaultSchedulers_get_TimeBasedOperations_m2660764704(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, TimeSpan_t881159249 , RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (TimeSpan_t881159249 )L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::Throttle<System.Int64>(UniRx.IObservable`1<TSource>,System.TimeSpan,UniRx.IScheduler)
extern "C"  RuntimeObject* Observable_Throttle_TisInt64_t3736567304_m3387314576_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, TimeSpan_t881159249  ___dueTime1, RuntimeObject* ___scheduler2, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		TimeSpan_t881159249  L_1 = ___dueTime1;
		RuntimeObject* L_2 = ___scheduler2;
		ThrottleObservable_1_t3727408309 * L_3 = (ThrottleObservable_1_t3727408309 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ThrottleObservable_1_t3727408309 *, RuntimeObject*, TimeSpan_t881159249 , RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_3, (RuntimeObject*)L_0, (TimeSpan_t881159249 )L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::Throttle<System.Object>(UniRx.IObservable`1<TSource>,System.TimeSpan)
extern "C"  RuntimeObject* Observable_Throttle_TisRuntimeObject_m2844368835_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, TimeSpan_t881159249  ___dueTime1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_Throttle_TisRuntimeObject_m2844368835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		TimeSpan_t881159249  L_1 = ___dueTime1;
		RuntimeObject* L_2 = DefaultSchedulers_get_TimeBasedOperations_m2660764704(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, TimeSpan_t881159249 , RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (TimeSpan_t881159249 )L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::Throttle<System.Object>(UniRx.IObservable`1<TSource>,System.TimeSpan,UniRx.IScheduler)
extern "C"  RuntimeObject* Observable_Throttle_TisRuntimeObject_m3713681759_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, TimeSpan_t881159249  ___dueTime1, RuntimeObject* ___scheduler2, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		TimeSpan_t881159249  L_1 = ___dueTime1;
		RuntimeObject* L_2 = ___scheduler2;
		ThrottleObservable_1_t3070947169 * L_3 = (ThrottleObservable_1_t3070947169 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ThrottleObservable_1_t3070947169 *, RuntimeObject*, TimeSpan_t881159249 , RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_3, (RuntimeObject*)L_0, (TimeSpan_t881159249 )L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::ThrottleFirst<System.Object>(UniRx.IObservable`1<TSource>,System.TimeSpan)
extern "C"  RuntimeObject* Observable_ThrottleFirst_TisRuntimeObject_m1967177714_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, TimeSpan_t881159249  ___dueTime1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_ThrottleFirst_TisRuntimeObject_m1967177714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		TimeSpan_t881159249  L_1 = ___dueTime1;
		RuntimeObject* L_2 = DefaultSchedulers_get_TimeBasedOperations_m2660764704(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, TimeSpan_t881159249 , RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (TimeSpan_t881159249 )L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::ThrottleFirst<System.Object>(UniRx.IObservable`1<TSource>,System.TimeSpan,UniRx.IScheduler)
extern "C"  RuntimeObject* Observable_ThrottleFirst_TisRuntimeObject_m1076323125_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, TimeSpan_t881159249  ___dueTime1, RuntimeObject* ___scheduler2, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		TimeSpan_t881159249  L_1 = ___dueTime1;
		RuntimeObject* L_2 = ___scheduler2;
		ThrottleFirstObservable_1_t3958710643 * L_3 = (ThrottleFirstObservable_1_t3958710643 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ThrottleFirstObservable_1_t3958710643 *, RuntimeObject*, TimeSpan_t881159249 , RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_3, (RuntimeObject*)L_0, (TimeSpan_t881159249 )L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::ThrottleFirstFrame<System.Object>(UniRx.IObservable`1<TSource>,System.Int32,UniRx.FrameCountType)
extern "C"  RuntimeObject* Observable_ThrottleFirstFrame_TisRuntimeObject_m3270737833_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, int32_t ___frameCount1, int32_t ___frameCountType2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_ThrottleFirstFrame_TisRuntimeObject_m3270737833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___frameCount1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_1 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_1, (String_t*)_stringLiteral207938285, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		RuntimeObject* L_2 = ___source0;
		int32_t L_3 = ___frameCount1;
		int32_t L_4 = ___frameCountType2;
		ThrottleFirstFrameObservable_1_t1990180597 * L_5 = (ThrottleFirstFrameObservable_1_t1990180597 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ThrottleFirstFrameObservable_1_t1990180597 *, RuntimeObject*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_5, (RuntimeObject*)L_2, (int32_t)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_5;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Observable::ThrottleFrame<System.Object>(UniRx.IObservable`1<TSource>,System.Int32,UniRx.FrameCountType)
extern "C"  RuntimeObject* Observable_ThrottleFrame_TisRuntimeObject_m1878905285_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, int32_t ___frameCount1, int32_t ___frameCountType2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_ThrottleFrame_TisRuntimeObject_m1878905285_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___frameCount1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_1 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_1, (String_t*)_stringLiteral207938285, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		RuntimeObject* L_2 = ___source0;
		int32_t L_3 = ___frameCount1;
		int32_t L_4 = ___frameCountType2;
		ThrottleFrameObservable_1_t1027693670 * L_5 = (ThrottleFrameObservable_1_t1027693670 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ThrottleFrameObservable_1_t1027693670 *, RuntimeObject*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_5, (RuntimeObject*)L_2, (int32_t)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_5;
	}
}
// UniRx.IObservable`1<TSource> UniRx.Stubs::CatchIgnore<System.Object>(System.Exception)
extern "C"  RuntimeObject* Stubs_CatchIgnore_TisRuntimeObject_m2040818366_gshared (RuntimeObject * __this /* static, unused */, Exception_t * ___ex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Stubs_CatchIgnore_TisRuntimeObject_m2040818366_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// UniRx.IObservable`1<UniRx.EventPattern`1<TEventArgs>> UniRx.Observable::FromEventPattern<System.Object,System.Object>(System.Func`2<System.EventHandler`1<TEventArgs>,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern "C"  RuntimeObject* Observable_FromEventPattern_TisRuntimeObject_TisRuntimeObject_m2508905222_gshared (RuntimeObject * __this /* static, unused */, Func_2_t663966201 * ___conversion0, Action_1_t3252573759 * ___addHandler1, Action_1_t3252573759 * ___removeHandler2, const RuntimeMethod* method)
{
	{
		Func_2_t663966201 * L_0 = ___conversion0;
		Action_1_t3252573759 * L_1 = ___addHandler1;
		Action_1_t3252573759 * L_2 = ___removeHandler2;
		FromEventPatternObservable_2_t3314993416 * L_3 = (FromEventPatternObservable_2_t3314993416 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (FromEventPatternObservable_2_t3314993416 *, Func_2_t663966201 *, Action_1_t3252573759 *, Action_1_t3252573759 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_3, (Func_2_t663966201 *)L_0, (Action_1_t3252573759 *)L_1, (Action_1_t3252573759 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// UniRx.IObservable`1<UniRx.FrameInterval`1<T>> UniRx.Observable::FrameInterval<System.Object>(UniRx.IObservable`1<T>)
extern "C"  RuntimeObject* Observable_FrameInterval_TisRuntimeObject_m3749389141_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		FrameIntervalObservable_1_t2769986100 * L_1 = (FrameIntervalObservable_1_t2769986100 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (FrameIntervalObservable_1_t2769986100 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_1, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// UniRx.IObservable`1<UniRx.IGroupedObservable`2<TKey,TElement>> UniRx.Observable::GroupBy<System.Object,System.Object,System.Object>(UniRx.IObservable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
extern "C"  RuntimeObject* Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m3883557576_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Func_2_t2447130374 * ___keySelector1, Func_2_t2447130374 * ___elementSelector2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m3883557576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3411193022_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (RuntimeObject*)L_0;
		RuntimeObject* L_1 = ___source0;
		Func_2_t2447130374 * L_2 = ___keySelector1;
		Func_2_t2447130374 * L_3 = ___elementSelector2;
		RuntimeObject* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_5 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t2447130374 *, Func_2_t2447130374 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_1, (Func_2_t2447130374 *)L_2, (Func_2_t2447130374 *)L_3, (RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_5;
	}
}
// UniRx.IObservable`1<UniRx.IGroupedObservable`2<TKey,TElement>> UniRx.Observable::GroupBy<System.Object,System.Object,System.Object>(UniRx.IObservable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  RuntimeObject* Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m1864551740_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Func_2_t2447130374 * ___keySelector1, Func_2_t2447130374 * ___elementSelector2, RuntimeObject* ___comparer3, const RuntimeMethod* method)
{
	Nullable_1_t378540539  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RuntimeObject* L_0 = ___source0;
		Func_2_t2447130374 * L_1 = ___keySelector1;
		Func_2_t2447130374 * L_2 = ___elementSelector2;
		il2cpp_codegen_initobj((&V_0), sizeof(Nullable_1_t378540539 ));
		Nullable_1_t378540539  L_3 = V_0;
		RuntimeObject* L_4 = ___comparer3;
		GroupByObservable_3_t1723459814 * L_5 = (GroupByObservable_3_t1723459814 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (GroupByObservable_3_t1723459814 *, RuntimeObject*, Func_2_t2447130374 *, Func_2_t2447130374 *, Nullable_1_t378540539 , RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_5, (RuntimeObject*)L_0, (Func_2_t2447130374 *)L_1, (Func_2_t2447130374 *)L_2, (Nullable_1_t378540539 )L_3, (RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_5;
	}
}
// UniRx.IObservable`1<UniRx.IGroupedObservable`2<TKey,TElement>> UniRx.Observable::GroupBy<System.Object,System.Object,System.Object>(UniRx.IObservable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Int32)
extern "C"  RuntimeObject* Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m1594666584_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Func_2_t2447130374 * ___keySelector1, Func_2_t2447130374 * ___elementSelector2, int32_t ___capacity3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m1594666584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3411193022_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (RuntimeObject*)L_0;
		RuntimeObject* L_1 = ___source0;
		Func_2_t2447130374 * L_2 = ___keySelector1;
		Func_2_t2447130374 * L_3 = ___elementSelector2;
		int32_t L_4 = ___capacity3;
		RuntimeObject* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_6 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t2447130374 *, Func_2_t2447130374 *, int32_t, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_1, (Func_2_t2447130374 *)L_2, (Func_2_t2447130374 *)L_3, (int32_t)L_4, (RuntimeObject*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_6;
	}
}
// UniRx.IObservable`1<UniRx.IGroupedObservable`2<TKey,TElement>> UniRx.Observable::GroupBy<System.Object,System.Object,System.Object>(UniRx.IObservable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  RuntimeObject* Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m56460662_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Func_2_t2447130374 * ___keySelector1, Func_2_t2447130374 * ___elementSelector2, int32_t ___capacity3, RuntimeObject* ___comparer4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m56460662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		Func_2_t2447130374 * L_1 = ___keySelector1;
		Func_2_t2447130374 * L_2 = ___elementSelector2;
		int32_t L_3 = ___capacity3;
		Nullable_1_t378540539  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Nullable_1__ctor_m3940678751((&L_4), (int32_t)L_3, /*hidden argument*/Nullable_1__ctor_m3940678751_RuntimeMethod_var);
		RuntimeObject* L_5 = ___comparer4;
		GroupByObservable_3_t1723459814 * L_6 = (GroupByObservable_3_t1723459814 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (GroupByObservable_3_t1723459814 *, RuntimeObject*, Func_2_t2447130374 *, Func_2_t2447130374 *, Nullable_1_t378540539 , RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_6, (RuntimeObject*)L_0, (Func_2_t2447130374 *)L_1, (Func_2_t2447130374 *)L_2, (Nullable_1_t378540539 )L_4, (RuntimeObject*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_6;
	}
}
// UniRx.IObservable`1<UniRx.IGroupedObservable`2<TKey,TSource>> UniRx.Observable::GroupBy<System.Object,System.Object>(UniRx.IObservable`1<TSource>,System.Func`2<TSource,TKey>)
extern "C"  RuntimeObject* Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_m2801338715_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Func_2_t2447130374 * ___keySelector1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_m2801338715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		Func_2_t2447130374 * L_1 = ___keySelector1;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Func_2_t2447130374 * L_2 = ((Stubs_1_t2084668772_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Identity_1();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t2447130374 *, Func_2_t2447130374 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (Func_2_t2447130374 *)L_1, (Func_2_t2447130374 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// UniRx.IObservable`1<UniRx.IGroupedObservable`2<TKey,TSource>> UniRx.Observable::GroupBy<System.Object,System.Object>(UniRx.IObservable`1<TSource>,System.Func`2<TSource,TKey>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  RuntimeObject* Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_m1316155870_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Func_2_t2447130374 * ___keySelector1, RuntimeObject* ___comparer2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_m1316155870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		Func_2_t2447130374 * L_1 = ___keySelector1;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Func_2_t2447130374 * L_2 = ((Stubs_1_t2084668772_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Identity_1();
		RuntimeObject* L_3 = ___comparer2;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_4 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t2447130374 *, Func_2_t2447130374 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (Func_2_t2447130374 *)L_1, (Func_2_t2447130374 *)L_2, (RuntimeObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_4;
	}
}
// UniRx.IObservable`1<UniRx.IGroupedObservable`2<TKey,TSource>> UniRx.Observable::GroupBy<System.Object,System.Object>(UniRx.IObservable`1<TSource>,System.Func`2<TSource,TKey>,System.Int32)
extern "C"  RuntimeObject* Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_m3471062564_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Func_2_t2447130374 * ___keySelector1, int32_t ___capacity2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_m3471062564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		Func_2_t2447130374 * L_1 = ___keySelector1;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Func_2_t2447130374 * L_2 = ((Stubs_1_t2084668772_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Identity_1();
		int32_t L_3 = ___capacity2;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_4 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t2447130374 *, Func_2_t2447130374 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (Func_2_t2447130374 *)L_1, (Func_2_t2447130374 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_4;
	}
}
// UniRx.IObservable`1<UniRx.IGroupedObservable`2<TKey,TSource>> UniRx.Observable::GroupBy<System.Object,System.Object>(UniRx.IObservable`1<TSource>,System.Func`2<TSource,TKey>,System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  RuntimeObject* Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_m3516009247_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Func_2_t2447130374 * ___keySelector1, int32_t ___capacity2, RuntimeObject* ___comparer3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_GroupBy_TisRuntimeObject_TisRuntimeObject_m3516009247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		Func_2_t2447130374 * L_1 = ___keySelector1;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Func_2_t2447130374 * L_2 = ((Stubs_1_t2084668772_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Identity_1();
		int32_t L_3 = ___capacity2;
		RuntimeObject* L_4 = ___comparer3;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_5 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t2447130374 *, Func_2_t2447130374 *, int32_t, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (Func_2_t2447130374 *)L_1, (Func_2_t2447130374 *)L_2, (int32_t)L_3, (RuntimeObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_5;
	}
}
// UniRx.IObservable`1<UniRx.Notification`1<T>> UniRx.Observable::Materialize<System.Object>(UniRx.IObservable`1<T>)
extern "C"  RuntimeObject* Observable_Materialize_TisRuntimeObject_m1243367395_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		MaterializeObservable_1_t1905148853 * L_1 = (MaterializeObservable_1_t1905148853 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (MaterializeObservable_1_t1905148853 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_1, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// UniRx.IObservable`1<UniRx.Pair`1<T>> UniRx.Observable::Pairwise<System.Object>(UniRx.IObservable`1<T>)
extern "C"  RuntimeObject* Observable_Pairwise_TisRuntimeObject_m3640286535_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		PairwiseObservable_1_t1009914496 * L_1 = (PairwiseObservable_1_t1009914496 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (PairwiseObservable_1_t1009914496 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_1, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// UniRx.IObservable`1<UniRx.TimeInterval`1<T>> UniRx.Observable::FrameTimeInterval<System.Object>(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  RuntimeObject* Observable_FrameTimeInterval_TisRuntimeObject_m1665441749_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, bool ___ignoreTimeScale1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		bool L_1 = ___ignoreTimeScale1;
		FrameTimeIntervalObservable_1_t3088423318 * L_2 = (FrameTimeIntervalObservable_1_t3088423318 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (FrameTimeIntervalObservable_1_t3088423318 *, RuntimeObject*, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.IObservable`1<UniRx.TimeInterval`1<TSource>> UniRx.Observable::TimeInterval<System.Object>(UniRx.IObservable`1<TSource>)
extern "C"  RuntimeObject* Observable_TimeInterval_TisRuntimeObject_m4203739597_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_TimeInterval_TisRuntimeObject_m4203739597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		RuntimeObject* L_1 = DefaultSchedulers_get_TimeBasedOperations_m2660764704(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_2 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// UniRx.IObservable`1<UniRx.TimeInterval`1<TSource>> UniRx.Observable::TimeInterval<System.Object>(UniRx.IObservable`1<TSource>,UniRx.IScheduler)
extern "C"  RuntimeObject* Observable_TimeInterval_TisRuntimeObject_m1928241860_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, RuntimeObject* ___scheduler1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		RuntimeObject* L_1 = ___scheduler1;
		TimeIntervalObservable_1_t696285847 * L_2 = (TimeIntervalObservable_1_t696285847 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (TimeIntervalObservable_1_t696285847 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.IObservable`1<UniRx.Timestamped`1<TSource>> UniRx.Observable::Timestamp<System.Object>(UniRx.IObservable`1<TSource>)
extern "C"  RuntimeObject* Observable_Timestamp_TisRuntimeObject_m3943482726_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_Timestamp_TisRuntimeObject_m3943482726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		RuntimeObject* L_1 = DefaultSchedulers_get_TimeBasedOperations_m2660764704(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_2 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// UniRx.IObservable`1<UniRx.Timestamped`1<TSource>> UniRx.Observable::Timestamp<System.Object>(UniRx.IObservable`1<TSource>,UniRx.IScheduler)
extern "C"  RuntimeObject* Observable_Timestamp_TisRuntimeObject_m1261798180_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, RuntimeObject* ___scheduler1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		RuntimeObject* L_1 = ___scheduler1;
		TimestampObservable_1_t1502464332 * L_2 = (TimestampObservable_1_t1502464332 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (TimestampObservable_1_t1502464332 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.IObservable`1<UniRx.Tuple`2<T0,T1>> UniRx.UnityEventExtensions::AsObservable<System.Object,System.Object>(UnityEngine.Events.UnityEvent`2<T0,T1>)
extern "C"  RuntimeObject* UnityEventExtensions_AsObservable_TisRuntimeObject_TisRuntimeObject_m4002206433_gshared (RuntimeObject * __this /* static, unused */, UnityEvent_2_t614268397 * ___unityEvent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventExtensions_AsObservable_TisRuntimeObject_TisRuntimeObject_m4002206433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CAsObservableU3Ec__AnonStorey3_2_t3918319924 * V_0 = NULL;
	{
		U3CAsObservableU3Ec__AnonStorey3_2_t3918319924 * L_0 = (U3CAsObservableU3Ec__AnonStorey3_2_t3918319924 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CAsObservableU3Ec__AnonStorey3_2_t3918319924 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3CAsObservableU3Ec__AnonStorey3_2_t3918319924 *)L_0;
		U3CAsObservableU3Ec__AnonStorey3_2_t3918319924 * L_1 = V_0;
		UnityEvent_2_t614268397 * L_2 = ___unityEvent0;
		NullCheck(L_1);
		L_1->set_unityEvent_0(L_2);
		intptr_t L_3 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2);
		Func_2_t1163256665 * L_4 = (Func_2_t1163256665 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (Func_2_t1163256665 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_4, (RuntimeObject *)NULL, (intptr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		U3CAsObservableU3Ec__AnonStorey3_2_t3918319924 * L_5 = V_0;
		intptr_t L_6 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5);
		Action_1_t3456439482 * L_7 = (Action_1_t3456439482 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (Action_1_t3456439482 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_7, (RuntimeObject *)L_5, (intptr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		U3CAsObservableU3Ec__AnonStorey3_2_t3918319924 * L_8 = V_0;
		intptr_t L_9 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 8);
		Action_1_t3456439482 * L_10 = (Action_1_t3456439482 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (Action_1_t3456439482 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_10, (RuntimeObject *)L_8, (intptr_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_11 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, Func_2_t1163256665 *, Action_1_t3456439482 *, Action_1_t3456439482 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (Func_2_t1163256665 *)L_4, (Action_1_t3456439482 *)L_7, (Action_1_t3456439482 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 9));
		return L_11;
	}
}
// UniRx.IObservable`1<UniRx.Tuple`3<T0,T1,T2>> UniRx.UnityEventExtensions::AsObservable<System.Object,System.Object,System.Object>(UnityEngine.Events.UnityEvent`3<T0,T1,T2>)
extern "C"  RuntimeObject* UnityEventExtensions_AsObservable_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m2902013217_gshared (RuntimeObject * __this /* static, unused */, UnityEvent_3_t2404744798 * ___unityEvent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventExtensions_AsObservable_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m2902013217_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CAsObservableU3Ec__AnonStorey5_3_t4293553857 * V_0 = NULL;
	{
		U3CAsObservableU3Ec__AnonStorey5_3_t4293553857 * L_0 = (U3CAsObservableU3Ec__AnonStorey5_3_t4293553857 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CAsObservableU3Ec__AnonStorey5_3_t4293553857 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3CAsObservableU3Ec__AnonStorey5_3_t4293553857 *)L_0;
		U3CAsObservableU3Ec__AnonStorey5_3_t4293553857 * L_1 = V_0;
		UnityEvent_3_t2404744798 * L_2 = ___unityEvent0;
		NullCheck(L_1);
		L_1->set_unityEvent_0(L_2);
		intptr_t L_3 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2);
		Func_2_t3187915266 * L_4 = (Func_2_t3187915266 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (Func_2_t3187915266 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_4, (RuntimeObject *)NULL, (intptr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		U3CAsObservableU3Ec__AnonStorey5_3_t4293553857 * L_5 = V_0;
		intptr_t L_6 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5);
		Action_1_t1729704308 * L_7 = (Action_1_t1729704308 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (Action_1_t1729704308 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_7, (RuntimeObject *)L_5, (intptr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		U3CAsObservableU3Ec__AnonStorey5_3_t4293553857 * L_8 = V_0;
		intptr_t L_9 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 8);
		Action_1_t1729704308 * L_10 = (Action_1_t1729704308 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (Action_1_t1729704308 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_10, (RuntimeObject *)L_8, (intptr_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_11 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, Func_2_t3187915266 *, Action_1_t1729704308 *, Action_1_t1729704308 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (Func_2_t3187915266 *)L_4, (Action_1_t1729704308 *)L_7, (Action_1_t1729704308 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 9));
		return L_11;
	}
}
// UniRx.IObservable`1<UniRx.Tuple`4<T0,T1,T2,T3>> UniRx.UnityEventExtensions::AsObservable<System.Object,System.Object,System.Object,System.Object>(UnityEngine.Events.UnityEvent`4<T0,T1,T2,T3>)
extern "C"  RuntimeObject* UnityEventExtensions_AsObservable_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m3617384406_gshared (RuntimeObject * __this /* static, unused */, UnityEvent_4_t4085588227 * ___unityEvent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventExtensions_AsObservable_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m3617384406_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CAsObservableU3Ec__AnonStorey7_4_t135207142 * V_0 = NULL;
	{
		U3CAsObservableU3Ec__AnonStorey7_4_t135207142 * L_0 = (U3CAsObservableU3Ec__AnonStorey7_4_t135207142 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CAsObservableU3Ec__AnonStorey7_4_t135207142 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3CAsObservableU3Ec__AnonStorey7_4_t135207142 *)L_0;
		U3CAsObservableU3Ec__AnonStorey7_4_t135207142 * L_1 = V_0;
		UnityEvent_4_t4085588227 * L_2 = ___unityEvent0;
		NullCheck(L_1);
		L_1->set_unityEvent_0(L_2);
		intptr_t L_3 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2);
		Func_2_t2651802615 * L_4 = (Func_2_t2651802615 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (Func_2_t2651802615 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_4, (RuntimeObject *)NULL, (intptr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		U3CAsObservableU3Ec__AnonStorey7_4_t135207142 * L_5 = V_0;
		intptr_t L_6 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5);
		Action_1_t854947986 * L_7 = (Action_1_t854947986 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (Action_1_t854947986 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_7, (RuntimeObject *)L_5, (intptr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		U3CAsObservableU3Ec__AnonStorey7_4_t135207142 * L_8 = V_0;
		intptr_t L_9 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 8);
		Action_1_t854947986 * L_10 = (Action_1_t854947986 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 6));
		((  void (*) (Action_1_t854947986 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7)->methodPointer)(L_10, (RuntimeObject *)L_8, (intptr_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 7));
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_11 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, Func_2_t2651802615 *, Action_1_t854947986 *, Action_1_t854947986 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (Func_2_t2651802615 *)L_4, (Action_1_t854947986 *)L_7, (Action_1_t854947986 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 9));
		return L_11;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.AsyncMessageBroker::PublishAsync<System.Object>(T)
extern "C"  RuntimeObject* AsyncMessageBroker_PublishAsync_TisRuntimeObject_m2308285229_gshared (AsyncMessageBroker_t3406290114 * __this, RuntimeObject * ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncMessageBroker_PublishAsync_TisRuntimeObject_m2308285229_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ImmutableList_1_t1553399856 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	RuntimeObject * V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	Func_2U5BU5D_t1724110606* V_4 = NULL;
	IObservable_1U5BU5D_t3038720744* V_5 = NULL;
	int32_t V_6 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t1229485932 * L_0 = (Dictionary_2_t1229485932 *)__this->get_notifiers_2();
		V_1 = (RuntimeObject *)L_0;
		RuntimeObject * L_1 = V_1;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, (RuntimeObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = (bool)__this->get_isDisposed_1();
			if (!L_2)
			{
				goto IL_0023;
			}
		}

IL_0018:
		{
			ObjectDisposedException_t21392786 * L_3 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
			ObjectDisposedException__ctor_m3603759869(L_3, (String_t*)_stringLiteral232837026, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
		}

IL_0023:
		{
			Dictionary_2_t1229485932 * L_4 = (Dictionary_2_t1229485932 *)__this->get_notifiers_2();
			RuntimeTypeHandle_t3027515415  L_5 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_6 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )L_5, /*hidden argument*/NULL);
			NullCheck((Dictionary_2_t1229485932 *)L_4);
			bool L_7 = Dictionary_2_TryGetValue_m3038145912((Dictionary_2_t1229485932 *)L_4, (Type_t *)L_6, (RuntimeObject **)(&V_2), /*hidden argument*/Dictionary_2_TryGetValue_m3038145912_RuntimeMethod_var);
			if (!L_7)
			{
				goto IL_004b;
			}
		}

IL_003f:
		{
			RuntimeObject * L_8 = V_2;
			V_0 = (ImmutableList_1_t1553399856 *)((ImmutableList_1_t1553399856 *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
			goto IL_0056;
		}

IL_004b:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
			RuntimeObject* L_9 = Observable_ReturnUnit_m3743972880(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_3 = (RuntimeObject*)L_9;
			IL2CPP_LEAVE(0xA6, FINALLY_005b);
		}

IL_0056:
		{
			IL2CPP_LEAVE(0x62, FINALLY_005b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		RuntimeObject * L_10 = V_1;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0xA6, IL_00a6)
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0062:
	{
		ImmutableList_1_t1553399856 * L_11 = V_0;
		NullCheck((ImmutableList_1_t1553399856 *)L_11);
		Func_2U5BU5D_t1724110606* L_12 = ((  Func_2U5BU5D_t1724110606* (*) (ImmutableList_1_t1553399856 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((ImmutableList_1_t1553399856 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		V_4 = (Func_2U5BU5D_t1724110606*)L_12;
		Func_2U5BU5D_t1724110606* L_13 = V_4;
		NullCheck(L_13);
		V_5 = (IObservable_1U5BU5D_t3038720744*)((IObservable_1U5BU5D_t3038720744*)SZArrayNew(IObservable_1U5BU5D_t3038720744_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length))))));
		V_6 = (int32_t)0;
		goto IL_0093;
	}

IL_007d:
	{
		IObservable_1U5BU5D_t3038720744* L_14 = V_5;
		int32_t L_15 = V_6;
		Func_2U5BU5D_t1724110606* L_16 = V_4;
		int32_t L_17 = V_6;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		Func_2_t852405815 * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		RuntimeObject * L_20 = ___message0;
		NullCheck((Func_2_t852405815 *)L_19);
		RuntimeObject* L_21 = ((  RuntimeObject* (*) (Func_2_t852405815 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)((Func_2_t852405815 *)L_19, (RuntimeObject *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_21);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (RuntimeObject*)L_21);
		int32_t L_22 = V_6;
		V_6 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0093:
	{
		int32_t L_23 = V_6;
		Func_2U5BU5D_t1724110606* L_24 = V_4;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_24)->max_length)))))))
		{
			goto IL_007d;
		}
	}
	{
		IObservable_1U5BU5D_t3038720744* L_25 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_26 = Observable_WhenAll_m331076954(NULL /*static, unused*/, (IObservable_1U5BU5D_t3038720744*)L_25, /*hidden argument*/NULL);
		return L_26;
	}

IL_00a6:
	{
		RuntimeObject* L_27 = V_3;
		return L_27;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::<ObserveOnMainThread`1>m__7<System.Object>(T)
extern "C"  RuntimeObject* Observable_U3CObserveOnMainThread_1U3Em__7_TisRuntimeObject_m3402388184_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ____0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_U3CObserveOnMainThread_1U3Em__7_TisRuntimeObject_m3402388184_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t3684499304_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = MainThreadDispatcher_UpdateAsObservable_m3818177576(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = Observable_Take_TisUnit_t3362249467_m3583177933(NULL /*static, unused*/, (RuntimeObject*)L_0, (int32_t)1, /*hidden argument*/Observable_Take_TisUnit_t3362249467_m3583177933_RuntimeMethod_var);
		return L_1;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::<ObserveOnMainThread`1>m__9<System.Object>(T)
extern "C"  RuntimeObject* Observable_U3CObserveOnMainThread_1U3Em__9_TisRuntimeObject_m3367388466_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ____0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_U3CObserveOnMainThread_1U3Em__9_TisRuntimeObject_m3367388466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t3684499304_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = MainThreadDispatcher_LateUpdateAsObservable_m4277804338(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = Observable_Take_TisUnit_t3362249467_m3583177933(NULL /*static, unused*/, (RuntimeObject*)L_0, (int32_t)1, /*hidden argument*/Observable_Take_TisUnit_t3362249467_m3583177933_RuntimeMethod_var);
		return L_1;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::AsSingleUnitObservable<System.Object>(UniRx.IObservable`1<T>)
extern "C"  RuntimeObject* Observable_AsSingleUnitObservable_TisRuntimeObject_m639466183_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		AsSingleUnitObservableObservable_1_t2095035369 * L_1 = (AsSingleUnitObservableObservable_1_t2095035369 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (AsSingleUnitObservableObservable_1_t2095035369 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_1, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::AsUnitObservable<System.Object>(UniRx.IObservable`1<T>)
extern "C"  RuntimeObject* Observable_AsUnitObservable_TisRuntimeObject_m1872500995_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		AsUnitObservableObservable_1_t1939971722 * L_1 = (AsUnitObservableObservable_1_t1939971722 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (AsUnitObservableObservable_1_t1939971722 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_1, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::ForEachAsync<System.Object>(UniRx.IObservable`1<T>,System.Action`1<T>)
extern "C"  RuntimeObject* Observable_ForEachAsync_TisRuntimeObject_m2890463848_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Action_1_t3252573759 * ___onNext1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		Action_1_t3252573759 * L_1 = ___onNext1;
		ForEachAsyncObservable_1_t3073042236 * L_2 = (ForEachAsyncObservable_1_t3073042236 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ForEachAsyncObservable_1_t3073042236 *, RuntimeObject*, Action_1_t3252573759 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (Action_1_t3252573759 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::ForEachAsync<System.Object>(UniRx.IObservable`1<T>,System.Action`2<T,System.Int32>)
extern "C"  RuntimeObject* Observable_ForEachAsync_TisRuntimeObject_m3834473043_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Action_2_t2340848427 * ___onNext1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		Action_2_t2340848427 * L_1 = ___onNext1;
		ForEachAsyncObservable_1_t3073042236 * L_2 = (ForEachAsyncObservable_1_t3073042236 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ForEachAsyncObservable_1_t3073042236 *, RuntimeObject*, Action_2_t2340848427 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (Action_2_t2340848427 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::FromEvent<System.Object>(System.Func`2<System.Action,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern "C"  RuntimeObject* Observable_FromEvent_TisRuntimeObject_m3498578029_gshared (RuntimeObject * __this /* static, unused */, Func_2_t4286427857 * ___conversion0, Action_1_t3252573759 * ___addHandler1, Action_1_t3252573759 * ___removeHandler2, const RuntimeMethod* method)
{
	{
		Func_2_t4286427857 * L_0 = ___conversion0;
		Action_1_t3252573759 * L_1 = ___addHandler1;
		Action_1_t3252573759 * L_2 = ___removeHandler2;
		FromEventObservable_1_t3138708542 * L_3 = (FromEventObservable_1_t3138708542 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (FromEventObservable_1_t3138708542 *, Func_2_t4286427857 *, Action_1_t3252573759 *, Action_1_t3252573759 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_3, (Func_2_t4286427857 *)L_0, (Action_1_t3252573759 *)L_1, (Action_1_t3252573759 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::SelectMany<System.Object>(UniRx.IObservable`1<T>,System.Collections.IEnumerator,System.Boolean)
extern "C"  RuntimeObject* Observable_SelectMany_TisRuntimeObject_m2392913521_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, RuntimeObject* ___coroutine1, bool ___publishEveryYield2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_SelectMany_TisRuntimeObject_m2392913521_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSelectManyU3Ec__AnonStorey25_1_t3053759039 * V_0 = NULL;
	{
		U3CSelectManyU3Ec__AnonStorey25_1_t3053759039 * L_0 = (U3CSelectManyU3Ec__AnonStorey25_1_t3053759039 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CSelectManyU3Ec__AnonStorey25_1_t3053759039 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3CSelectManyU3Ec__AnonStorey25_1_t3053759039 *)L_0;
		U3CSelectManyU3Ec__AnonStorey25_1_t3053759039 * L_1 = V_0;
		RuntimeObject* L_2 = ___coroutine1;
		NullCheck(L_1);
		L_1->set_coroutine_0(L_2);
		RuntimeObject* L_3 = ___source0;
		U3CSelectManyU3Ec__AnonStorey25_1_t3053759039 * L_4 = V_0;
		intptr_t L_5 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2);
		Func_1_t1283030885 * L_6 = (Func_1_t1283030885 *)il2cpp_codegen_object_new(Func_1_t1283030885_il2cpp_TypeInfo_var);
		Func_1__ctor_m3733315985(L_6, (RuntimeObject *)L_4, (intptr_t)L_5, /*hidden argument*/Func_1__ctor_m3733315985_RuntimeMethod_var);
		bool L_7 = ___publishEveryYield2;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_8 = Observable_FromCoroutine_m1963572360(NULL /*static, unused*/, (Func_1_t1283030885 *)L_6, (bool)L_7, /*hidden argument*/NULL);
		RuntimeObject* L_9 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_3, (RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_9;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::SelectMany<System.Object>(UniRx.IObservable`1<T>,System.Func`1<System.Collections.IEnumerator>,System.Boolean)
extern "C"  RuntimeObject* Observable_SelectMany_TisRuntimeObject_m3078398865_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Func_1_t1283030885 * ___selector1, bool ___publishEveryYield2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_SelectMany_TisRuntimeObject_m3078398865_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSelectManyU3Ec__AnonStorey26_1_t1327606335 * V_0 = NULL;
	{
		U3CSelectManyU3Ec__AnonStorey26_1_t1327606335 * L_0 = (U3CSelectManyU3Ec__AnonStorey26_1_t1327606335 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CSelectManyU3Ec__AnonStorey26_1_t1327606335 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3CSelectManyU3Ec__AnonStorey26_1_t1327606335 *)L_0;
		U3CSelectManyU3Ec__AnonStorey26_1_t1327606335 * L_1 = V_0;
		Func_1_t1283030885 * L_2 = ___selector1;
		NullCheck(L_1);
		L_1->set_selector_0(L_2);
		RuntimeObject* L_3 = ___source0;
		U3CSelectManyU3Ec__AnonStorey26_1_t1327606335 * L_4 = V_0;
		intptr_t L_5 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2);
		Func_1_t1283030885 * L_6 = (Func_1_t1283030885 *)il2cpp_codegen_object_new(Func_1_t1283030885_il2cpp_TypeInfo_var);
		Func_1__ctor_m3733315985(L_6, (RuntimeObject *)L_4, (intptr_t)L_5, /*hidden argument*/Func_1__ctor_m3733315985_RuntimeMethod_var);
		bool L_7 = ___publishEveryYield2;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_8 = Observable_FromCoroutine_m1963572360(NULL /*static, unused*/, (Func_1_t1283030885 *)L_6, (bool)L_7, /*hidden argument*/NULL);
		RuntimeObject* L_9 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_3, (RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_9;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::SelectMany<System.Object>(UniRx.IObservable`1<T>,System.Func`2<T,System.Collections.IEnumerator>)
extern "C"  RuntimeObject* Observable_SelectMany_TisRuntimeObject_m3243795571_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Func_2_t1220308448 * ___selector1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_SelectMany_TisRuntimeObject_m3243795571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSelectManyU3Ec__AnonStorey27_1_t3896420927 * V_0 = NULL;
	{
		U3CSelectManyU3Ec__AnonStorey27_1_t3896420927 * L_0 = (U3CSelectManyU3Ec__AnonStorey27_1_t3896420927 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CSelectManyU3Ec__AnonStorey27_1_t3896420927 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3CSelectManyU3Ec__AnonStorey27_1_t3896420927 *)L_0;
		U3CSelectManyU3Ec__AnonStorey27_1_t3896420927 * L_1 = V_0;
		Func_2_t1220308448 * L_2 = ___selector1;
		NullCheck(L_1);
		L_1->set_selector_0(L_2);
		RuntimeObject* L_3 = ___source0;
		U3CSelectManyU3Ec__AnonStorey27_1_t3896420927 * L_4 = V_0;
		intptr_t L_5 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2);
		Func_2_t852405815 * L_6 = (Func_2_t852405815 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (Func_2_t852405815 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_6, (RuntimeObject *)L_4, (intptr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_7 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t852405815 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_3, (Func_2_t852405815 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 5));
		return L_7;
	}
}
// UniRx.IObservable`1<UniRx.Unit> UniRx.Observable::SelectMany<UniRx.Unit>(UniRx.IObservable`1<T>,System.Func`1<System.Collections.IEnumerator>,System.Boolean)
extern "C"  RuntimeObject* Observable_SelectMany_TisUnit_t3362249467_m1014653926_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Func_1_t1283030885 * ___selector1, bool ___publishEveryYield2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_SelectMany_TisUnit_t3362249467_m1014653926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSelectManyU3Ec__AnonStorey26_1_t1609749638 * V_0 = NULL;
	{
		U3CSelectManyU3Ec__AnonStorey26_1_t1609749638 * L_0 = (U3CSelectManyU3Ec__AnonStorey26_1_t1609749638 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CSelectManyU3Ec__AnonStorey26_1_t1609749638 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3CSelectManyU3Ec__AnonStorey26_1_t1609749638 *)L_0;
		U3CSelectManyU3Ec__AnonStorey26_1_t1609749638 * L_1 = V_0;
		Func_1_t1283030885 * L_2 = ___selector1;
		NullCheck(L_1);
		L_1->set_selector_0(L_2);
		RuntimeObject* L_3 = ___source0;
		U3CSelectManyU3Ec__AnonStorey26_1_t1609749638 * L_4 = V_0;
		intptr_t L_5 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2);
		Func_1_t1283030885 * L_6 = (Func_1_t1283030885 *)il2cpp_codegen_object_new(Func_1_t1283030885_il2cpp_TypeInfo_var);
		Func_1__ctor_m3733315985(L_6, (RuntimeObject *)L_4, (intptr_t)L_5, /*hidden argument*/Func_1__ctor_m3733315985_RuntimeMethod_var);
		bool L_7 = ___publishEveryYield2;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_8 = Observable_FromCoroutine_m1963572360(NULL /*static, unused*/, (Func_1_t1283030885 *)L_6, (bool)L_7, /*hidden argument*/NULL);
		RuntimeObject* L_9 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_3, (RuntimeObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_9;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::Create<System.Object>(System.Action`1<T>)
extern "C"  RuntimeObject* Observer_Create_TisRuntimeObject_m1223195334_gshared (RuntimeObject * __this /* static, unused */, Action_1_t3252573759 * ___onNext0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observer_Create_TisRuntimeObject_m1223195334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t3252573759 * L_0 = ___onNext0;
		IL2CPP_RUNTIME_CLASS_INIT(Stubs_t4137823816_il2cpp_TypeInfo_var);
		Action_1_t1609204844 * L_1 = ((Stubs_t4137823816_StaticFields*)il2cpp_codegen_static_fields_for(Stubs_t4137823816_il2cpp_TypeInfo_var))->get_Throw_1();
		Action_t1264377477 * L_2 = ((Stubs_t4137823816_StaticFields*)il2cpp_codegen_static_fields_for(Stubs_t4137823816_il2cpp_TypeInfo_var))->get_Nop_0();
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, Action_1_t3252573759 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Action_1_t3252573759 *)L_0, (Action_1_t1609204844 *)L_1, (Action_t1264377477 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::Create<System.Object>(System.Action`1<T>,System.Action)
extern "C"  RuntimeObject* Observer_Create_TisRuntimeObject_m2421516608_gshared (RuntimeObject * __this /* static, unused */, Action_1_t3252573759 * ___onNext0, Action_t1264377477 * ___onCompleted1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observer_Create_TisRuntimeObject_m2421516608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t3252573759 * L_0 = ___onNext0;
		IL2CPP_RUNTIME_CLASS_INIT(Stubs_t4137823816_il2cpp_TypeInfo_var);
		Action_1_t1609204844 * L_1 = ((Stubs_t4137823816_StaticFields*)il2cpp_codegen_static_fields_for(Stubs_t4137823816_il2cpp_TypeInfo_var))->get_Throw_1();
		Action_t1264377477 * L_2 = ___onCompleted1;
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, Action_1_t3252573759 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Action_1_t3252573759 *)L_0, (Action_1_t1609204844 *)L_1, (Action_t1264377477 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::Create<System.Object>(System.Action`1<T>,System.Action`1<System.Exception>)
extern "C"  RuntimeObject* Observer_Create_TisRuntimeObject_m710702794_gshared (RuntimeObject * __this /* static, unused */, Action_1_t3252573759 * ___onNext0, Action_1_t1609204844 * ___onError1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observer_Create_TisRuntimeObject_m710702794_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t3252573759 * L_0 = ___onNext0;
		Action_1_t1609204844 * L_1 = ___onError1;
		IL2CPP_RUNTIME_CLASS_INIT(Stubs_t4137823816_il2cpp_TypeInfo_var);
		Action_t1264377477 * L_2 = ((Stubs_t4137823816_StaticFields*)il2cpp_codegen_static_fields_for(Stubs_t4137823816_il2cpp_TypeInfo_var))->get_Nop_0();
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, Action_1_t3252573759 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (Action_1_t3252573759 *)L_0, (Action_1_t1609204844 *)L_1, (Action_t1264377477 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_3;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::Create<System.Object>(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  RuntimeObject* Observer_Create_TisRuntimeObject_m3649204800_gshared (RuntimeObject * __this /* static, unused */, Action_1_t3252573759 * ___onNext0, Action_1_t1609204844 * ___onError1, Action_t1264377477 * ___onCompleted2, const RuntimeMethod* method)
{
	{
		Action_1_t3252573759 * L_0 = ___onNext0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Action_1_t3252573759 * L_1 = ((Stubs_1_t2084668772_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Ignore_0();
		bool L_2 = Delegate_op_Equality_m1690449587(NULL /*static, unused*/, (Delegate_t1188392813 *)L_0, (Delegate_t1188392813 *)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1609204844 * L_3 = ___onError1;
		Action_t1264377477 * L_4 = ___onCompleted2;
		EmptyOnNextAnonymousObserver_1_t2179921185 * L_5 = (EmptyOnNextAnonymousObserver_1_t2179921185 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (EmptyOnNextAnonymousObserver_1_t2179921185 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_5, (Action_1_t1609204844 *)L_3, (Action_t1264377477 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_5;
	}

IL_0018:
	{
		Action_1_t3252573759 * L_6 = ___onNext0;
		Action_1_t1609204844 * L_7 = ___onError1;
		Action_t1264377477 * L_8 = ___onCompleted2;
		AnonymousObserver_1_t66846250 * L_9 = (AnonymousObserver_1_t66846250 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (AnonymousObserver_1_t66846250 *, Action_1_t3252573759 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_9, (Action_1_t3252573759 *)L_6, (Action_1_t1609204844 *)L_7, (Action_t1264377477 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_9;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateAutoDetachObserver<System.Object>(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  RuntimeObject* Observer_CreateAutoDetachObserver_TisRuntimeObject_m2691736049_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___observer0, RuntimeObject* ___disposable1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject* L_1 = ___disposable1;
		AutoDetachObserver_1_t1556989284 * L_2 = (AutoDetachObserver_1_t1556989284 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (AutoDetachObserver_1_t1556989284 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeObserver<System.Boolean>(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  RuntimeObject* Observer_CreateSubscribeObserver_TisBoolean_t97287965_m3811671641_gshared (RuntimeObject * __this /* static, unused */, Action_1_t269755560 * ___onNext0, Action_1_t1609204844 * ___onError1, Action_t1264377477 * ___onCompleted2, const RuntimeMethod* method)
{
	{
		Action_1_t269755560 * L_0 = ___onNext0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Action_1_t269755560 * L_1 = ((Stubs_1_t3396817869_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Ignore_0();
		bool L_2 = Delegate_op_Equality_m1690449587(NULL /*static, unused*/, (Delegate_t1188392813 *)L_0, (Delegate_t1188392813 *)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1609204844 * L_3 = ___onError1;
		Action_t1264377477 * L_4 = ___onCompleted2;
		Subscribe__1_t4252920902 * L_5 = (Subscribe__1_t4252920902 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (Subscribe__1_t4252920902 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_5, (Action_1_t1609204844 *)L_3, (Action_t1264377477 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_5;
	}

IL_0018:
	{
		Action_1_t269755560 * L_6 = ___onNext0;
		Action_1_t1609204844 * L_7 = ___onError1;
		Action_t1264377477 * L_8 = ___onCompleted2;
		Subscribe_1_t610984217 * L_9 = (Subscribe_1_t610984217 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (Subscribe_1_t610984217 *, Action_1_t269755560 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_9, (Action_1_t269755560 *)L_6, (Action_1_t1609204844 *)L_7, (Action_t1264377477 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_9;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeObserver<System.Int32>(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  RuntimeObject* Observer_CreateSubscribeObserver_TisInt32_t2950945753_m152975164_gshared (RuntimeObject * __this /* static, unused */, Action_1_t3123413348 * ___onNext0, Action_1_t1609204844 * ___onError1, Action_t1264377477 * ___onCompleted2, const RuntimeMethod* method)
{
	{
		Action_1_t3123413348 * L_0 = ___onNext0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Action_1_t3123413348 * L_1 = ((Stubs_1_t1955508361_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Ignore_0();
		bool L_2 = Delegate_op_Equality_m1690449587(NULL /*static, unused*/, (Delegate_t1188392813 *)L_0, (Delegate_t1188392813 *)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1609204844 * L_3 = ___onError1;
		Action_t1264377477 * L_4 = ___onCompleted2;
		Subscribe__1_t2811611394 * L_5 = (Subscribe__1_t2811611394 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (Subscribe__1_t2811611394 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_5, (Action_1_t1609204844 *)L_3, (Action_t1264377477 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_5;
	}

IL_0018:
	{
		Action_1_t3123413348 * L_6 = ___onNext0;
		Action_1_t1609204844 * L_7 = ___onError1;
		Action_t1264377477 * L_8 = ___onCompleted2;
		Subscribe_1_t3464642005 * L_9 = (Subscribe_1_t3464642005 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (Subscribe_1_t3464642005 *, Action_1_t3123413348 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_9, (Action_1_t3123413348 *)L_6, (Action_1_t1609204844 *)L_7, (Action_t1264377477 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_9;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeObserver<System.Int64>(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  RuntimeObject* Observer_CreateSubscribeObserver_TisInt64_t3736567304_m2328455708_gshared (RuntimeObject * __this /* static, unused */, Action_1_t3909034899 * ___onNext0, Action_1_t1609204844 * ___onError1, Action_t1264377477 * ___onCompleted2, const RuntimeMethod* method)
{
	{
		Action_1_t3909034899 * L_0 = ___onNext0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Action_1_t3909034899 * L_1 = ((Stubs_1_t2741129912_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Ignore_0();
		bool L_2 = Delegate_op_Equality_m1690449587(NULL /*static, unused*/, (Delegate_t1188392813 *)L_0, (Delegate_t1188392813 *)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1609204844 * L_3 = ___onError1;
		Action_t1264377477 * L_4 = ___onCompleted2;
		Subscribe__1_t3597232945 * L_5 = (Subscribe__1_t3597232945 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (Subscribe__1_t3597232945 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_5, (Action_1_t1609204844 *)L_3, (Action_t1264377477 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_5;
	}

IL_0018:
	{
		Action_1_t3909034899 * L_6 = ___onNext0;
		Action_1_t1609204844 * L_7 = ___onError1;
		Action_t1264377477 * L_8 = ___onCompleted2;
		Subscribe_1_t4250263556 * L_9 = (Subscribe_1_t4250263556 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (Subscribe_1_t4250263556 *, Action_1_t3909034899 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_9, (Action_1_t3909034899 *)L_6, (Action_1_t1609204844 *)L_7, (Action_t1264377477 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_9;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeObserver<System.Object>(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  RuntimeObject* Observer_CreateSubscribeObserver_TisRuntimeObject_m3460277300_gshared (RuntimeObject * __this /* static, unused */, Action_1_t3252573759 * ___onNext0, Action_1_t1609204844 * ___onError1, Action_t1264377477 * ___onCompleted2, const RuntimeMethod* method)
{
	{
		Action_1_t3252573759 * L_0 = ___onNext0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Action_1_t3252573759 * L_1 = ((Stubs_1_t2084668772_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Ignore_0();
		bool L_2 = Delegate_op_Equality_m1690449587(NULL /*static, unused*/, (Delegate_t1188392813 *)L_0, (Delegate_t1188392813 *)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1609204844 * L_3 = ___onError1;
		Action_t1264377477 * L_4 = ___onCompleted2;
		Subscribe__1_t2940771805 * L_5 = (Subscribe__1_t2940771805 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (Subscribe__1_t2940771805 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_5, (Action_1_t1609204844 *)L_3, (Action_t1264377477 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_5;
	}

IL_0018:
	{
		Action_1_t3252573759 * L_6 = ___onNext0;
		Action_1_t1609204844 * L_7 = ___onError1;
		Action_t1264377477 * L_8 = ___onCompleted2;
		Subscribe_1_t3593802416 * L_9 = (Subscribe_1_t3593802416 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (Subscribe_1_t3593802416 *, Action_1_t3252573759 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_9, (Action_1_t3252573759 *)L_6, (Action_1_t1609204844 *)L_7, (Action_t1264377477 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_9;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeObserver<System.Single>(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  RuntimeObject* Observer_CreateSubscribeObserver_TisSingle_t1397266774_m3184849810_gshared (RuntimeObject * __this /* static, unused */, Action_1_t1569734369 * ___onNext0, Action_1_t1609204844 * ___onError1, Action_t1264377477 * ___onCompleted2, const RuntimeMethod* method)
{
	{
		Action_1_t1569734369 * L_0 = ___onNext0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Action_1_t1569734369 * L_1 = ((Stubs_1_t401829382_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Ignore_0();
		bool L_2 = Delegate_op_Equality_m1690449587(NULL /*static, unused*/, (Delegate_t1188392813 *)L_0, (Delegate_t1188392813 *)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1609204844 * L_3 = ___onError1;
		Action_t1264377477 * L_4 = ___onCompleted2;
		Subscribe__1_t1257932415 * L_5 = (Subscribe__1_t1257932415 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (Subscribe__1_t1257932415 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_5, (Action_1_t1609204844 *)L_3, (Action_t1264377477 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_5;
	}

IL_0018:
	{
		Action_1_t1569734369 * L_6 = ___onNext0;
		Action_1_t1609204844 * L_7 = ___onError1;
		Action_t1264377477 * L_8 = ___onCompleted2;
		Subscribe_1_t1910963026 * L_9 = (Subscribe_1_t1910963026 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (Subscribe_1_t1910963026 *, Action_1_t1569734369 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_9, (Action_1_t1569734369 *)L_6, (Action_1_t1609204844 *)L_7, (Action_t1264377477 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_9;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeObserver<UniRx.CollectionAddEvent`1<System.Object>>(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  RuntimeObject* Observer_CreateSubscribeObserver_TisCollectionAddEvent_1_t1688054569_m3352528763_gshared (RuntimeObject * __this /* static, unused */, Action_1_t1860522164 * ___onNext0, Action_1_t1609204844 * ___onError1, Action_t1264377477 * ___onCompleted2, const RuntimeMethod* method)
{
	{
		Action_1_t1860522164 * L_0 = ___onNext0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Action_1_t1860522164 * L_1 = ((Stubs_1_t692617177_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Ignore_0();
		bool L_2 = Delegate_op_Equality_m1690449587(NULL /*static, unused*/, (Delegate_t1188392813 *)L_0, (Delegate_t1188392813 *)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1609204844 * L_3 = ___onError1;
		Action_t1264377477 * L_4 = ___onCompleted2;
		Subscribe__1_t1548720210 * L_5 = (Subscribe__1_t1548720210 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (Subscribe__1_t1548720210 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_5, (Action_1_t1609204844 *)L_3, (Action_t1264377477 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_5;
	}

IL_0018:
	{
		Action_1_t1860522164 * L_6 = ___onNext0;
		Action_1_t1609204844 * L_7 = ___onError1;
		Action_t1264377477 * L_8 = ___onCompleted2;
		Subscribe_1_t2201750821 * L_9 = (Subscribe_1_t2201750821 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (Subscribe_1_t2201750821 *, Action_1_t1860522164 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_9, (Action_1_t1860522164 *)L_6, (Action_1_t1609204844 *)L_7, (Action_t1264377477 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_9;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeObserver<UniRx.CollectionRemoveEvent`1<System.Object>>(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  RuntimeObject* Observer_CreateSubscribeObserver_TisCollectionRemoveEvent_1_t1986941660_m1193707439_gshared (RuntimeObject * __this /* static, unused */, Action_1_t2159409255 * ___onNext0, Action_1_t1609204844 * ___onError1, Action_t1264377477 * ___onCompleted2, const RuntimeMethod* method)
{
	{
		Action_1_t2159409255 * L_0 = ___onNext0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Action_1_t2159409255 * L_1 = ((Stubs_1_t991504268_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Ignore_0();
		bool L_2 = Delegate_op_Equality_m1690449587(NULL /*static, unused*/, (Delegate_t1188392813 *)L_0, (Delegate_t1188392813 *)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1609204844 * L_3 = ___onError1;
		Action_t1264377477 * L_4 = ___onCompleted2;
		Subscribe__1_t1847607301 * L_5 = (Subscribe__1_t1847607301 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (Subscribe__1_t1847607301 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_5, (Action_1_t1609204844 *)L_3, (Action_t1264377477 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_5;
	}

IL_0018:
	{
		Action_1_t2159409255 * L_6 = ___onNext0;
		Action_1_t1609204844 * L_7 = ___onError1;
		Action_t1264377477 * L_8 = ___onCompleted2;
		Subscribe_1_t2500637912 * L_9 = (Subscribe_1_t2500637912 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (Subscribe_1_t2500637912 *, Action_1_t2159409255 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_9, (Action_1_t2159409255 *)L_6, (Action_1_t1609204844 *)L_7, (Action_t1264377477 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_9;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeObserver<UniRx.Diagnostics.LogEntry>(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  RuntimeObject* Observer_CreateSubscribeObserver_TisLogEntry_t1141507113_m3144647857_gshared (RuntimeObject * __this /* static, unused */, Action_1_t1313974708 * ___onNext0, Action_1_t1609204844 * ___onError1, Action_t1264377477 * ___onCompleted2, const RuntimeMethod* method)
{
	{
		Action_1_t1313974708 * L_0 = ___onNext0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Action_1_t1313974708 * L_1 = ((Stubs_1_t146069721_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Ignore_0();
		bool L_2 = Delegate_op_Equality_m1690449587(NULL /*static, unused*/, (Delegate_t1188392813 *)L_0, (Delegate_t1188392813 *)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1609204844 * L_3 = ___onError1;
		Action_t1264377477 * L_4 = ___onCompleted2;
		Subscribe__1_t1002172754 * L_5 = (Subscribe__1_t1002172754 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (Subscribe__1_t1002172754 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_5, (Action_1_t1609204844 *)L_3, (Action_t1264377477 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_5;
	}

IL_0018:
	{
		Action_1_t1313974708 * L_6 = ___onNext0;
		Action_1_t1609204844 * L_7 = ___onError1;
		Action_t1264377477 * L_8 = ___onCompleted2;
		Subscribe_1_t1655203365 * L_9 = (Subscribe_1_t1655203365 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (Subscribe_1_t1655203365 *, Action_1_t1313974708 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_9, (Action_1_t1313974708 *)L_6, (Action_1_t1609204844 *)L_7, (Action_t1264377477 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_9;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeObserver<UniRx.Unit>(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  RuntimeObject* Observer_CreateSubscribeObserver_TisUnit_t3362249467_m2865797124_gshared (RuntimeObject * __this /* static, unused */, Action_1_t3534717062 * ___onNext0, Action_1_t1609204844 * ___onError1, Action_t1264377477 * ___onCompleted2, const RuntimeMethod* method)
{
	{
		Action_1_t3534717062 * L_0 = ___onNext0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Action_1_t3534717062 * L_1 = ((Stubs_1_t2366812075_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Ignore_0();
		bool L_2 = Delegate_op_Equality_m1690449587(NULL /*static, unused*/, (Delegate_t1188392813 *)L_0, (Delegate_t1188392813 *)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t1609204844 * L_3 = ___onError1;
		Action_t1264377477 * L_4 = ___onCompleted2;
		Subscribe__1_t3222915108 * L_5 = (Subscribe__1_t3222915108 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (Subscribe__1_t3222915108 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_5, (Action_1_t1609204844 *)L_3, (Action_t1264377477 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_5;
	}

IL_0018:
	{
		Action_1_t3534717062 * L_6 = ___onNext0;
		Action_1_t1609204844 * L_7 = ___onError1;
		Action_t1264377477 * L_8 = ___onCompleted2;
		Subscribe_1_t3875945719 * L_9 = (Subscribe_1_t3875945719 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (Subscribe_1_t3875945719 *, Action_1_t3534717062 *, Action_1_t1609204844 *, Action_t1264377477 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_9, (Action_1_t3534717062 *)L_6, (Action_1_t1609204844 *)L_7, (Action_t1264377477 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_9;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeWithState2Observer<System.Object,System.Object,System.Object>(TState1,TState2,System.Action`3<T,TState1,TState2>,System.Action`3<System.Exception,TState1,TState2>,System.Action`2<TState1,TState2>)
extern "C"  RuntimeObject* Observer_CreateSubscribeWithState2Observer_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m230903370_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___state10, RuntimeObject * ___state21, Action_3_t3632554945 * ___onNext2, Action_3_t2220624998 * ___onError3, Action_2_t2470008838 * ___onCompleted4, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___state10;
		RuntimeObject * L_1 = ___state21;
		Action_3_t3632554945 * L_2 = ___onNext2;
		Action_3_t2220624998 * L_3 = ___onError3;
		Action_2_t2470008838 * L_4 = ___onCompleted4;
		Subscribe_3_t2075922912 * L_5 = (Subscribe_3_t2075922912 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (Subscribe_3_t2075922912 *, RuntimeObject *, RuntimeObject *, Action_3_t3632554945 *, Action_3_t2220624998 *, Action_2_t2470008838 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_5, (RuntimeObject *)L_0, (RuntimeObject *)L_1, (Action_3_t3632554945 *)L_2, (Action_3_t2220624998 *)L_3, (Action_2_t2470008838 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_5;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeWithState2Observer<System.Single,System.Object,System.Object>(TState1,TState2,System.Action`3<T,TState1,TState2>,System.Action`3<System.Exception,TState1,TState2>,System.Action`2<TState1,TState2>)
extern "C"  RuntimeObject* Observer_CreateSubscribeWithState2Observer_TisSingle_t1397266774_TisRuntimeObject_TisRuntimeObject_m2095356281_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___state10, RuntimeObject * ___state21, Action_3_t1712484883 * ___onNext2, Action_3_t2220624998 * ___onError3, Action_2_t2470008838 * ___onCompleted4, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___state10;
		RuntimeObject * L_1 = ___state21;
		Action_3_t1712484883 * L_2 = ___onNext2;
		Action_3_t2220624998 * L_3 = ___onError3;
		Action_2_t2470008838 * L_4 = ___onCompleted4;
		Subscribe_3_t155852850 * L_5 = (Subscribe_3_t155852850 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (Subscribe_3_t155852850 *, RuntimeObject *, RuntimeObject *, Action_3_t1712484883 *, Action_3_t2220624998 *, Action_2_t2470008838 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_5, (RuntimeObject *)L_0, (RuntimeObject *)L_1, (Action_3_t1712484883 *)L_2, (Action_3_t2220624998 *)L_3, (Action_2_t2470008838 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_5;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeWithState3Observer<System.Int64,System.Object,System.Object,System.Object>(TState1,TState2,TState3,System.Action`4<T,TState1,TState2,TState3>,System.Action`4<System.Exception,TState1,TState2,TState3>,System.Action`3<TState1,TState2,TState3>)
extern "C"  RuntimeObject* Observer_CreateSubscribeWithState3Observer_TisInt64_t3736567304_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m3633397664_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___state10, RuntimeObject * ___state21, RuntimeObject * ___state32, Action_4_t327971120 * ___onNext3, Action_4_t1238479323 * ___onError4, Action_3_t3632554945 * ___onCompleted5, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___state10;
		RuntimeObject * L_1 = ___state21;
		RuntimeObject * L_2 = ___state32;
		Action_4_t327971120 * L_3 = ___onNext3;
		Action_4_t1238479323 * L_4 = ___onError4;
		Action_3_t3632554945 * L_5 = ___onCompleted5;
		Subscribe_4_t2932287952 * L_6 = (Subscribe_4_t2932287952 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (Subscribe_4_t2932287952 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, Action_4_t327971120 *, Action_4_t1238479323 *, Action_3_t3632554945 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_6, (RuntimeObject *)L_0, (RuntimeObject *)L_1, (RuntimeObject *)L_2, (Action_4_t327971120 *)L_3, (Action_4_t1238479323 *)L_4, (Action_3_t3632554945 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_6;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeWithState3Observer<System.Object,System.Object,System.Object,System.Object>(TState1,TState2,TState3,System.Action`4<T,TState1,TState2,TState3>,System.Action`4<System.Exception,TState1,TState2,TState3>,System.Action`3<TState1,TState2,TState3>)
extern "C"  RuntimeObject* Observer_CreateSubscribeWithState3Observer_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m1815022162_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___state10, RuntimeObject * ___state21, RuntimeObject * ___state32, Action_4_t309985972 * ___onNext3, Action_4_t1238479323 * ___onError4, Action_3_t3632554945 * ___onCompleted5, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___state10;
		RuntimeObject * L_1 = ___state21;
		RuntimeObject * L_2 = ___state32;
		Action_4_t309985972 * L_3 = ___onNext3;
		Action_4_t1238479323 * L_4 = ___onError4;
		Action_3_t3632554945 * L_5 = ___onCompleted5;
		Subscribe_4_t2914302804 * L_6 = (Subscribe_4_t2914302804 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (Subscribe_4_t2914302804 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, Action_4_t309985972 *, Action_4_t1238479323 *, Action_3_t3632554945 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_6, (RuntimeObject *)L_0, (RuntimeObject *)L_1, (RuntimeObject *)L_2, (Action_4_t309985972 *)L_3, (Action_4_t1238479323 *)L_4, (Action_3_t3632554945 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_6;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeWithStateObserver<System.Boolean,System.Object>(TState,System.Action`2<T,TState>,System.Action`2<System.Exception,TState>,System.Action`1<TState>)
extern "C"  RuntimeObject* Observer_CreateSubscribeWithStateObserver_TisBoolean_t97287965_TisRuntimeObject_m567169209_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___state0, Action_2_t2523487705 * ___onNext1, Action_2_t2161070725 * ___onError2, Action_1_t3252573759 * ___onCompleted3, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___state0;
		Action_2_t2523487705 * L_1 = ___onNext1;
		Action_2_t2161070725 * L_2 = ___onError2;
		Action_1_t3252573759 * L_3 = ___onCompleted3;
		Subscribe_2_t1258558231 * L_4 = (Subscribe_2_t1258558231 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (Subscribe_2_t1258558231 *, RuntimeObject *, Action_2_t2523487705 *, Action_2_t2161070725 *, Action_1_t3252573759 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_4, (RuntimeObject *)L_0, (Action_2_t2523487705 *)L_1, (Action_2_t2161070725 *)L_2, (Action_1_t3252573759 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_4;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeWithStateObserver<System.Int32,System.Object>(TState,System.Action`2<T,TState>,System.Action`2<System.Exception,TState>,System.Action`1<TState>)
extern "C"  RuntimeObject* Observer_CreateSubscribeWithStateObserver_TisInt32_t2950945753_TisRuntimeObject_m940479209_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___state0, Action_2_t11315885 * ___onNext1, Action_2_t2161070725 * ___onError2, Action_1_t3252573759 * ___onCompleted3, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___state0;
		Action_2_t11315885 * L_1 = ___onNext1;
		Action_2_t2161070725 * L_2 = ___onError2;
		Action_1_t3252573759 * L_3 = ___onCompleted3;
		Subscribe_2_t3041353707 * L_4 = (Subscribe_2_t3041353707 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (Subscribe_2_t3041353707 *, RuntimeObject *, Action_2_t11315885 *, Action_2_t2161070725 *, Action_1_t3252573759 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_4, (RuntimeObject *)L_0, (Action_2_t11315885 *)L_1, (Action_2_t2161070725 *)L_2, (Action_1_t3252573759 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_4;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeWithStateObserver<System.Int64,System.Object>(TState,System.Action`2<T,TState>,System.Action`2<System.Exception,TState>,System.Action`1<TState>)
extern "C"  RuntimeObject* Observer_CreateSubscribeWithStateObserver_TisInt64_t3736567304_TisRuntimeObject_m2078608357_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___state0, Action_2_t2185242338 * ___onNext1, Action_2_t2161070725 * ___onError2, Action_1_t3252573759 * ___onCompleted3, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___state0;
		Action_2_t2185242338 * L_1 = ___onNext1;
		Action_2_t2161070725 * L_2 = ___onError2;
		Action_1_t3252573759 * L_3 = ___onCompleted3;
		Subscribe_2_t920312864 * L_4 = (Subscribe_2_t920312864 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (Subscribe_2_t920312864 *, RuntimeObject *, Action_2_t2185242338 *, Action_2_t2161070725 *, Action_1_t3252573759 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_4, (RuntimeObject *)L_0, (Action_2_t2185242338 *)L_1, (Action_2_t2161070725 *)L_2, (Action_1_t3252573759 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_4;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeWithStateObserver<System.Object,System.Object>(TState,System.Action`2<T,TState>,System.Action`2<System.Exception,TState>,System.Action`1<TState>)
extern "C"  RuntimeObject* Observer_CreateSubscribeWithStateObserver_TisRuntimeObject_TisRuntimeObject_m84369023_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___state0, Action_2_t2470008838 * ___onNext1, Action_2_t2161070725 * ___onError2, Action_1_t3252573759 * ___onCompleted3, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___state0;
		Action_2_t2470008838 * L_1 = ___onNext1;
		Action_2_t2161070725 * L_2 = ___onError2;
		Action_1_t3252573759 * L_3 = ___onCompleted3;
		Subscribe_2_t1205079364 * L_4 = (Subscribe_2_t1205079364 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (Subscribe_2_t1205079364 *, RuntimeObject *, Action_2_t2470008838 *, Action_2_t2161070725 *, Action_1_t3252573759 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_4, (RuntimeObject *)L_0, (Action_2_t2470008838 *)L_1, (Action_2_t2161070725 *)L_2, (Action_1_t3252573759 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_4;
	}
}
// UniRx.IObserver`1<T> UniRx.Observer::CreateSubscribeWithStateObserver<UniRx.Unit,System.Object>(TState,System.Action`2<T,TState>,System.Action`2<System.Exception,TState>,System.Action`1<TState>)
extern "C"  RuntimeObject* Observer_CreateSubscribeWithStateObserver_TisUnit_t3362249467_TisRuntimeObject_m4126535447_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___state0, Action_2_t1614770371 * ___onNext1, Action_2_t2161070725 * ___onError2, Action_1_t3252573759 * ___onCompleted3, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___state0;
		Action_2_t1614770371 * L_1 = ___onNext1;
		Action_2_t2161070725 * L_2 = ___onError2;
		Action_1_t3252573759 * L_3 = ___onCompleted3;
		Subscribe_2_t349840897 * L_4 = (Subscribe_2_t349840897 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (Subscribe_2_t349840897 *, RuntimeObject *, Action_2_t1614770371 *, Action_2_t2161070725 *, Action_1_t3252573759 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_4, (RuntimeObject *)L_0, (Action_2_t1614770371 *)L_1, (Action_2_t2161070725 *)L_2, (Action_1_t3252573759 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_4;
	}
}
// UniRx.IObserver`1<T> UniRx.ObserverExtensions::Synchronize<System.Object>(UniRx.IObserver`1<T>)
extern "C"  RuntimeObject* ObserverExtensions_Synchronize_TisRuntimeObject_m747448223_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___observer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObserverExtensions_Synchronize_TisRuntimeObject_m747448223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject * L_1 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_1, /*hidden argument*/NULL);
		SynchronizedObserver_1_t3807712003 * L_2 = (SynchronizedObserver_1_t3807712003 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (SynchronizedObserver_1_t3807712003 *, RuntimeObject*, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.IObserver`1<T> UniRx.ObserverExtensions::Synchronize<System.Object>(UniRx.IObserver`1<T>,System.Object)
extern "C"  RuntimeObject* ObserverExtensions_Synchronize_TisRuntimeObject_m203049172_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___observer0, RuntimeObject * ___gate1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___observer0;
		RuntimeObject * L_1 = ___gate1;
		SynchronizedObserver_1_t3807712003 * L_2 = (SynchronizedObserver_1_t3807712003 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (SynchronizedObserver_1_t3807712003 *, RuntimeObject*, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.ISubject`1<T> UniRx.SubjectExtensions::Synchronize<System.Object>(UniRx.ISubject`1<T>)
extern "C"  RuntimeObject* SubjectExtensions_Synchronize_TisRuntimeObject_m1422148723_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___subject0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___subject0;
		RuntimeObject* L_1 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		RuntimeObject* L_2 = ___subject0;
		AnonymousSubject_1_t1300284131 * L_3 = (AnonymousSubject_1_t1300284131 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (AnonymousSubject_1_t1300284131 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_3, (RuntimeObject*)L_1, (RuntimeObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_3;
	}
}
// UniRx.ISubject`1<T> UniRx.SubjectExtensions::Synchronize<System.Object>(UniRx.ISubject`1<T>,System.Object)
extern "C"  RuntimeObject* SubjectExtensions_Synchronize_TisRuntimeObject_m3267319213_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___subject0, RuntimeObject * ___gate1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___subject0;
		RuntimeObject * L_1 = ___gate1;
		RuntimeObject* L_2 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		RuntimeObject* L_3 = ___subject0;
		AnonymousSubject_1_t1300284131 * L_4 = (AnonymousSubject_1_t1300284131 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (AnonymousSubject_1_t1300284131 *, RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_4, (RuntimeObject*)L_2, (RuntimeObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_4;
	}
}
// UniRx.LazyTask`1<T> UniRx.LazyTask::FromResult<System.Object>(T)
extern "C"  LazyTask_1_t2287704132 * LazyTask_FromResult_TisRuntimeObject_m4000011225_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		LazyTask_1_t2287704132 * L_1 = ((  LazyTask_1_t2287704132 * (*) (RuntimeObject * /* static, unused */, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// UniRx.LazyTask`1<T> UniRx.LazyTaskExtensions::ToLazyTask<System.Object>(UniRx.IObservable`1<T>)
extern "C"  LazyTask_1_t2287704132 * LazyTaskExtensions_ToLazyTask_TisRuntimeObject_m2267124002_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		LazyTask_1_t2287704132 * L_1 = (LazyTask_1_t2287704132 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (LazyTask_1_t2287704132 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_1, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// UniRx.Notification`1<T> UniRx.Notification::CreateOnCompleted<System.Object>()
extern "C"  Notification_1_t4190762752 * Notification_CreateOnCompleted_TisRuntimeObject_m2730857082_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		OnCompletedNotification_t2296134618 * L_0 = (OnCompletedNotification_t2296134618 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (OnCompletedNotification_t2296134618 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_0;
	}
}
// UniRx.Notification`1<T> UniRx.Notification::CreateOnError<System.Object>(System.Exception)
extern "C"  Notification_1_t4190762752 * Notification_CreateOnError_TisRuntimeObject_m3870854833_gshared (RuntimeObject * __this /* static, unused */, Exception_t * ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Notification_CreateOnError_TisRuntimeObject_m3870854833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Exception_t * L_0 = ___error0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, (String_t*)_stringLiteral95342995, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Exception_t * L_2 = ___error0;
		OnErrorNotification_t1865595679 * L_3 = (OnErrorNotification_t1865595679 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (OnErrorNotification_t1865595679 *, Exception_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_3, (Exception_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// UniRx.Notification`1<T> UniRx.Notification::CreateOnNext<System.Object>(T)
extern "C"  Notification_1_t4190762752 * Notification_CreateOnNext_TisRuntimeObject_m853166994_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		OnNextNotification_t3334954344 * L_1 = (OnNextNotification_t3334954344 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (OnNextNotification_t3334954344 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_1, (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// UniRx.ObservableYieldInstruction`1<T> UniRx.Observable::ToYieldInstruction<System.Int64>(UniRx.IObservable`1<T>)
extern "C"  ObservableYieldInstruction_1_t2774328545 * Observable_ToYieldInstruction_TisInt64_t3736567304_m3018093371_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_ToYieldInstruction_TisInt64_t3736567304_m3018093371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		IL2CPP_RUNTIME_CLASS_INIT(CancellationToken_t1265546479_il2cpp_TypeInfo_var);
		CancellationToken_t1265546479  L_1 = ((CancellationToken_t1265546479_StaticFields*)il2cpp_codegen_static_fields_for(CancellationToken_t1265546479_il2cpp_TypeInfo_var))->get_None_2();
		ObservableYieldInstruction_1_t2774328545 * L_2 = (ObservableYieldInstruction_1_t2774328545 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ObservableYieldInstruction_1_t2774328545 *, RuntimeObject*, bool, CancellationToken_t1265546479 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (bool)1, (CancellationToken_t1265546479 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.ObservableYieldInstruction`1<T> UniRx.Observable::ToYieldInstruction<System.Object>(UniRx.IObservable`1<T>)
extern "C"  ObservableYieldInstruction_1_t2117867405 * Observable_ToYieldInstruction_TisRuntimeObject_m3235847277_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_ToYieldInstruction_TisRuntimeObject_m3235847277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		IL2CPP_RUNTIME_CLASS_INIT(CancellationToken_t1265546479_il2cpp_TypeInfo_var);
		CancellationToken_t1265546479  L_1 = ((CancellationToken_t1265546479_StaticFields*)il2cpp_codegen_static_fields_for(CancellationToken_t1265546479_il2cpp_TypeInfo_var))->get_None_2();
		ObservableYieldInstruction_1_t2117867405 * L_2 = (ObservableYieldInstruction_1_t2117867405 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ObservableYieldInstruction_1_t2117867405 *, RuntimeObject*, bool, CancellationToken_t1265546479 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (bool)1, (CancellationToken_t1265546479 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.ObservableYieldInstruction`1<T> UniRx.Observable::ToYieldInstruction<System.Object>(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  ObservableYieldInstruction_1_t2117867405 * Observable_ToYieldInstruction_TisRuntimeObject_m3857052964_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, bool ___throwOnError1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_ToYieldInstruction_TisRuntimeObject_m3857052964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		bool L_1 = ___throwOnError1;
		IL2CPP_RUNTIME_CLASS_INIT(CancellationToken_t1265546479_il2cpp_TypeInfo_var);
		CancellationToken_t1265546479  L_2 = ((CancellationToken_t1265546479_StaticFields*)il2cpp_codegen_static_fields_for(CancellationToken_t1265546479_il2cpp_TypeInfo_var))->get_None_2();
		ObservableYieldInstruction_1_t2117867405 * L_3 = (ObservableYieldInstruction_1_t2117867405 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ObservableYieldInstruction_1_t2117867405 *, RuntimeObject*, bool, CancellationToken_t1265546479 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_3, (RuntimeObject*)L_0, (bool)L_1, (CancellationToken_t1265546479 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// UniRx.ObservableYieldInstruction`1<T> UniRx.Observable::ToYieldInstruction<System.Object>(UniRx.IObservable`1<T>,System.Boolean,UniRx.CancellationToken)
extern "C"  ObservableYieldInstruction_1_t2117867405 * Observable_ToYieldInstruction_TisRuntimeObject_m1398800608_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, bool ___throwOnError1, CancellationToken_t1265546479  ___cancel2, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		bool L_1 = ___throwOnError1;
		CancellationToken_t1265546479  L_2 = ___cancel2;
		ObservableYieldInstruction_1_t2117867405 * L_3 = (ObservableYieldInstruction_1_t2117867405 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ObservableYieldInstruction_1_t2117867405 *, RuntimeObject*, bool, CancellationToken_t1265546479 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_3, (RuntimeObject*)L_0, (bool)L_1, (CancellationToken_t1265546479 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// UniRx.ObservableYieldInstruction`1<T> UniRx.Observable::ToYieldInstruction<System.Object>(UniRx.IObservable`1<T>,UniRx.CancellationToken)
extern "C"  ObservableYieldInstruction_1_t2117867405 * Observable_ToYieldInstruction_TisRuntimeObject_m2607151189_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, CancellationToken_t1265546479  ___cancel1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		CancellationToken_t1265546479  L_1 = ___cancel1;
		ObservableYieldInstruction_1_t2117867405 * L_2 = (ObservableYieldInstruction_1_t2117867405 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ObservableYieldInstruction_1_t2117867405 *, RuntimeObject*, bool, CancellationToken_t1265546479 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (bool)1, (CancellationToken_t1265546479 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.ObservableYieldInstruction`1<T> UniRx.Observable::ToYieldInstruction<UniRx.Unit>(UniRx.IObservable`1<T>)
extern "C"  ObservableYieldInstruction_1_t2400010708 * Observable_ToYieldInstruction_TisUnit_t3362249467_m2489366239_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_ToYieldInstruction_TisUnit_t3362249467_m2489366239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		IL2CPP_RUNTIME_CLASS_INIT(CancellationToken_t1265546479_il2cpp_TypeInfo_var);
		CancellationToken_t1265546479  L_1 = ((CancellationToken_t1265546479_StaticFields*)il2cpp_codegen_static_fields_for(CancellationToken_t1265546479_il2cpp_TypeInfo_var))->get_None_2();
		ObservableYieldInstruction_1_t2400010708 * L_2 = (ObservableYieldInstruction_1_t2400010708 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ObservableYieldInstruction_1_t2400010708 *, RuntimeObject*, bool, CancellationToken_t1265546479 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (bool)1, (CancellationToken_t1265546479 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.ObservableYieldInstruction`1<T> UniRx.Observable::ToYieldInstruction<UniRx.Unit>(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  ObservableYieldInstruction_1_t2400010708 * Observable_ToYieldInstruction_TisUnit_t3362249467_m2382445313_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, bool ___throwOnError1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_ToYieldInstruction_TisUnit_t3362249467_m2382445313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		bool L_1 = ___throwOnError1;
		IL2CPP_RUNTIME_CLASS_INIT(CancellationToken_t1265546479_il2cpp_TypeInfo_var);
		CancellationToken_t1265546479  L_2 = ((CancellationToken_t1265546479_StaticFields*)il2cpp_codegen_static_fields_for(CancellationToken_t1265546479_il2cpp_TypeInfo_var))->get_None_2();
		ObservableYieldInstruction_1_t2400010708 * L_3 = (ObservableYieldInstruction_1_t2400010708 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ObservableYieldInstruction_1_t2400010708 *, RuntimeObject*, bool, CancellationToken_t1265546479 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_3, (RuntimeObject*)L_0, (bool)L_1, (CancellationToken_t1265546479 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// UniRx.ObservableYieldInstruction`1<T> UniRx.Observable::ToYieldInstruction<UniRx.Unit>(UniRx.IObservable`1<T>,System.Boolean,UniRx.CancellationToken)
extern "C"  ObservableYieldInstruction_1_t2400010708 * Observable_ToYieldInstruction_TisUnit_t3362249467_m2993341421_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, bool ___throwOnError1, CancellationToken_t1265546479  ___cancel2, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		bool L_1 = ___throwOnError1;
		CancellationToken_t1265546479  L_2 = ___cancel2;
		ObservableYieldInstruction_1_t2400010708 * L_3 = (ObservableYieldInstruction_1_t2400010708 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ObservableYieldInstruction_1_t2400010708 *, RuntimeObject*, bool, CancellationToken_t1265546479 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_3, (RuntimeObject*)L_0, (bool)L_1, (CancellationToken_t1265546479 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// UniRx.ObservableYieldInstruction`1<T> UniRx.Observable::ToYieldInstruction<UniRx.Unit>(UniRx.IObservable`1<T>,UniRx.CancellationToken)
extern "C"  ObservableYieldInstruction_1_t2400010708 * Observable_ToYieldInstruction_TisUnit_t3362249467_m172663249_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, CancellationToken_t1265546479  ___cancel1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		CancellationToken_t1265546479  L_1 = ___cancel1;
		ObservableYieldInstruction_1_t2400010708 * L_2 = (ObservableYieldInstruction_1_t2400010708 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ObservableYieldInstruction_1_t2400010708 *, RuntimeObject*, bool, CancellationToken_t1265546479 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (bool)1, (CancellationToken_t1265546479 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.ReactiveCollection`1<T> UniRx.ReactiveCollectionExtensions::ToReactiveCollection<System.Object>(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  ReactiveCollection_1_t4090598151 * ReactiveCollectionExtensions_ToReactiveCollection_TisRuntimeObject_m3990410617_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		ReactiveCollection_1_t4090598151 * L_1 = (ReactiveCollection_1_t4090598151 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ReactiveCollection_1_t4090598151 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_1, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// UniRx.ReactiveCommand`1<T> UniRx.ReactiveCommandExtensions::ToReactiveCommand<System.Object>(UniRx.IObservable`1<System.Boolean>,System.Boolean)
extern "C"  ReactiveCommand_1_t1068238646 * ReactiveCommandExtensions_ToReactiveCommand_TisRuntimeObject_m908196869_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___canExecuteSource0, bool ___initialValue1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___canExecuteSource0;
		bool L_1 = ___initialValue1;
		ReactiveCommand_1_t1068238646 * L_2 = (ReactiveCommand_1_t1068238646 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ReactiveCommand_1_t1068238646 *, RuntimeObject*, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.ReactiveDictionary`2<TKey,TValue> UniRx.ReactiveDictionaryExtensions::ToReactiveDictionary<System.Object,System.Object>(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  ReactiveDictionary_2_t820484097 * ReactiveDictionaryExtensions_ToReactiveDictionary_TisRuntimeObject_TisRuntimeObject_m874116791_gshared (RuntimeObject * __this /* static, unused */, Dictionary_2_t132545152 * ___dictionary0, const RuntimeMethod* method)
{
	{
		Dictionary_2_t132545152 * L_0 = ___dictionary0;
		ReactiveDictionary_2_t820484097 * L_1 = (ReactiveDictionary_2_t820484097 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ReactiveDictionary_2_t820484097 *, Dictionary_2_t132545152 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_1, (Dictionary_2_t132545152 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// UniRx.ReactiveProperty`1<T> UniRx.ReactivePropertyExtensions::ToReactiveProperty<System.Boolean>(UniRx.IObservable`1<T>)
extern "C"  ReactiveProperty_1_t3626980155 * ReactivePropertyExtensions_ToReactiveProperty_TisBoolean_t97287965_m2504507275_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		ReactiveProperty_1_t3626980155 * L_1 = (ReactiveProperty_1_t3626980155 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ReactiveProperty_1_t3626980155 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_1, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// UniRx.ReactiveProperty`1<T> UniRx.ReactivePropertyExtensions::ToReactiveProperty<System.Object>(UniRx.IObservable`1<T>)
extern "C"  ReactiveProperty_1_t2314831058 * ReactivePropertyExtensions_ToReactiveProperty_TisRuntimeObject_m4036877109_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		ReactiveProperty_1_t2314831058 * L_1 = (ReactiveProperty_1_t2314831058 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ReactiveProperty_1_t2314831058 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_1, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// UniRx.ReactiveProperty`1<T> UniRx.ReactivePropertyExtensions::ToReactiveProperty<System.Object>(UniRx.IObservable`1<T>,T)
extern "C"  ReactiveProperty_1_t2314831058 * ReactivePropertyExtensions_ToReactiveProperty_TisRuntimeObject_m5014214_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, RuntimeObject * ___initialValue1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		RuntimeObject * L_1 = ___initialValue1;
		ReactiveProperty_1_t2314831058 * L_2 = (ReactiveProperty_1_t2314831058 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ReactiveProperty_1_t2314831058 *, RuntimeObject*, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.ReadOnlyReactiveProperty`1<T> UniRx.ReactivePropertyExtensions::ToReadOnlyReactiveProperty<System.Object>(UniRx.IObservable`1<T>)
extern "C"  ReadOnlyReactiveProperty_1_t3751549484 * ReactivePropertyExtensions_ToReadOnlyReactiveProperty_TisRuntimeObject_m2174218473_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		ReadOnlyReactiveProperty_1_t3751549484 * L_1 = (ReadOnlyReactiveProperty_1_t3751549484 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ReadOnlyReactiveProperty_1_t3751549484 *, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_1, (RuntimeObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// UniRx.ReadOnlyReactiveProperty`1<T> UniRx.ReactivePropertyExtensions::ToReadOnlyReactiveProperty<System.Object>(UniRx.IObservable`1<T>,T)
extern "C"  ReadOnlyReactiveProperty_1_t3751549484 * ReactivePropertyExtensions_ToReadOnlyReactiveProperty_TisRuntimeObject_m1881612071_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, RuntimeObject * ___initialValue1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		RuntimeObject * L_1 = ___initialValue1;
		ReadOnlyReactiveProperty_1_t3751549484 * L_2 = (ReadOnlyReactiveProperty_1_t3751549484 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ReadOnlyReactiveProperty_1_t3751549484 *, RuntimeObject*, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.ReadOnlyReactiveProperty`1<T> UniRx.ReactivePropertyExtensions::ToSequentialReadOnlyReactiveProperty<System.Object>(UniRx.IObservable`1<T>)
extern "C"  ReadOnlyReactiveProperty_1_t3751549484 * ReactivePropertyExtensions_ToSequentialReadOnlyReactiveProperty_TisRuntimeObject_m3136719491_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		ReadOnlyReactiveProperty_1_t3751549484 * L_1 = (ReadOnlyReactiveProperty_1_t3751549484 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ReadOnlyReactiveProperty_1_t3751549484 *, RuntimeObject*, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_1, (RuntimeObject*)L_0, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// UniRx.ReadOnlyReactiveProperty`1<T> UniRx.ReactivePropertyExtensions::ToSequentialReadOnlyReactiveProperty<System.Object>(UniRx.IObservable`1<T>,T)
extern "C"  ReadOnlyReactiveProperty_1_t3751549484 * ReactivePropertyExtensions_ToSequentialReadOnlyReactiveProperty_TisRuntimeObject_m540868783_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, RuntimeObject * ___initialValue1, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = ___source0;
		RuntimeObject * L_1 = ___initialValue1;
		ReadOnlyReactiveProperty_1_t3751549484 * L_2 = (ReadOnlyReactiveProperty_1_t3751549484 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (ReadOnlyReactiveProperty_1_t3751549484 *, RuntimeObject*, RuntimeObject *, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_2, (RuntimeObject*)L_0, (RuntimeObject *)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.Timestamped`1<T> UniRx.Timestamped::Create<System.Object>(T,System.DateTimeOffset)
extern "C"  Timestamped_1_t1881506576  Timestamped_Create_TisRuntimeObject_m3285665478_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___value0, DateTimeOffset_t3229287507  ___timestamp1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		DateTimeOffset_t3229287507  L_1 = ___timestamp1;
		Timestamped_1_t1881506576  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Timestamped_1__ctor_m21775297((&L_2), (RuntimeObject *)L_0, (DateTimeOffset_t3229287507 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.Tuple`1<T1> UniRx.Tuple::Create<System.Object>(T1)
extern "C"  Tuple_1_t4248013280  Tuple_Create_TisRuntimeObject_m3132630252_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item10, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___item10;
		Tuple_1_t4248013280  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Tuple_1__ctor_m946716862((&L_1), (RuntimeObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_1;
	}
}
// UniRx.Tuple`2<T1,T2> UniRx.Tuple::Create<System.Object,System.Object>(T1,T2)
extern "C"  Tuple_2_t350234673  Tuple_Create_TisRuntimeObject_TisRuntimeObject_m646415138_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item10, RuntimeObject * ___item21, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___item10;
		RuntimeObject * L_1 = ___item21;
		Tuple_2_t350234673  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Tuple_2__ctor_m1110087791((&L_2), (RuntimeObject *)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// UniRx.Tuple`3<T1,T2,T3> UniRx.Tuple::Create<System.Object,System.Object,System.Object>(T1,T2,T3)
extern "C"  Tuple_3_t3228981166  Tuple_Create_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m2652884778_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item10, RuntimeObject * ___item21, RuntimeObject * ___item32, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___item10;
		RuntimeObject * L_1 = ___item21;
		RuntimeObject * L_2 = ___item32;
		Tuple_3_t3228981166  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Tuple_3__ctor_m811173283((&L_3), (RuntimeObject *)L_0, (RuntimeObject *)L_1, (RuntimeObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// UniRx.Tuple`3<T1,T2,T3> UniRx.Tuple::Create<System.Object,UniRx.Unit,System.Object>(T1,T2,T3)
extern "C"  Tuple_3_t2373742699  Tuple_Create_TisRuntimeObject_TisUnit_t3362249467_TisRuntimeObject_m2804475518_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item10, Unit_t3362249467  ___item21, RuntimeObject * ___item32, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___item10;
		Unit_t3362249467  L_1 = ___item21;
		RuntimeObject * L_2 = ___item32;
		Tuple_3_t2373742699  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Tuple_3__ctor_m3344996810((&L_3), (RuntimeObject *)L_0, (Unit_t3362249467 )L_1, (RuntimeObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_3;
	}
}
// UniRx.Tuple`4<T1,T2,T3,T4> UniRx.Tuple::Create<System.Object,System.Boolean,System.Int32,System.DateTime>(T1,T2,T3,T4)
extern "C"  Tuple_4_t342572496  Tuple_Create_TisRuntimeObject_TisBoolean_t97287965_TisInt32_t2950945753_TisDateTime_t3738529785_m4173303459_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item10, bool ___item21, int32_t ___item32, DateTime_t3738529785  ___item43, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___item10;
		bool L_1 = ___item21;
		int32_t L_2 = ___item32;
		DateTime_t3738529785  L_3 = ___item43;
		Tuple_4_t342572496  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Tuple_4__ctor_m36515419((&L_4), (RuntimeObject *)L_0, (bool)L_1, (int32_t)L_2, (DateTime_t3738529785 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_4;
	}
}
// UniRx.Tuple`4<T1,T2,T3,T4> UniRx.Tuple::Create<System.Object,System.Object,System.Object,System.Object>(T1,T2,T3,T4)
extern "C"  Tuple_4_t2649139811  Tuple_Create_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m3557837930_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item10, RuntimeObject * ___item21, RuntimeObject * ___item32, RuntimeObject * ___item43, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___item10;
		RuntimeObject * L_1 = ___item21;
		RuntimeObject * L_2 = ___item32;
		RuntimeObject * L_3 = ___item43;
		Tuple_4_t2649139811  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Tuple_4__ctor_m1088025688((&L_4), (RuntimeObject *)L_0, (RuntimeObject *)L_1, (RuntimeObject *)L_2, (RuntimeObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_4;
	}
}
// UniRx.Tuple`5<T1,T2,T3,T4,T5> UniRx.Tuple::Create<System.Object,System.Object,System.Object,System.Object,System.Object>(T1,T2,T3,T4,T5)
extern "C"  Tuple_5_t2125722268  Tuple_Create_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m1331433468_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item10, RuntimeObject * ___item21, RuntimeObject * ___item32, RuntimeObject * ___item43, RuntimeObject * ___item54, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___item10;
		RuntimeObject * L_1 = ___item21;
		RuntimeObject * L_2 = ___item32;
		RuntimeObject * L_3 = ___item43;
		RuntimeObject * L_4 = ___item54;
		Tuple_5_t2125722268  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Tuple_5__ctor_m2288603759((&L_5), (RuntimeObject *)L_0, (RuntimeObject *)L_1, (RuntimeObject *)L_2, (RuntimeObject *)L_3, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_5;
	}
}
// UniRx.Tuple`6<T1,T2,T3,T4,T5,T6> UniRx.Tuple::Create<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>(T1,T2,T3,T4,T5,T6)
extern "C"  Tuple_6_t2833421365  Tuple_Create_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m859454359_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item10, RuntimeObject * ___item21, RuntimeObject * ___item32, RuntimeObject * ___item43, RuntimeObject * ___item54, RuntimeObject * ___item65, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___item10;
		RuntimeObject * L_1 = ___item21;
		RuntimeObject * L_2 = ___item32;
		RuntimeObject * L_3 = ___item43;
		RuntimeObject * L_4 = ___item54;
		RuntimeObject * L_5 = ___item65;
		Tuple_6_t2833421365  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Tuple_6__ctor_m2315075868((&L_6), (RuntimeObject *)L_0, (RuntimeObject *)L_1, (RuntimeObject *)L_2, (RuntimeObject *)L_3, (RuntimeObject *)L_4, (RuntimeObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_6;
	}
}
// UniRx.Tuple`7<T1,T2,T3,T4,T5,T6,T7> UniRx.Tuple::Create<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>(T1,T2,T3,T4,T5,T6,T7)
extern "C"  Tuple_7_t2257463594  Tuple_Create_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m523673331_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item10, RuntimeObject * ___item21, RuntimeObject * ___item32, RuntimeObject * ___item43, RuntimeObject * ___item54, RuntimeObject * ___item65, RuntimeObject * ___item76, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___item10;
		RuntimeObject * L_1 = ___item21;
		RuntimeObject * L_2 = ___item32;
		RuntimeObject * L_3 = ___item43;
		RuntimeObject * L_4 = ___item54;
		RuntimeObject * L_5 = ___item65;
		RuntimeObject * L_6 = ___item76;
		Tuple_7_t2257463594  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Tuple_7__ctor_m3950978026((&L_7), (RuntimeObject *)L_0, (RuntimeObject *)L_1, (RuntimeObject *)L_2, (RuntimeObject *)L_3, (RuntimeObject *)L_4, (RuntimeObject *)L_5, (RuntimeObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_7;
	}
}
// UniRx.Tuple`8<T1,T2,T3,T4,T5,T6,T7,UniRx.Tuple`1<T8>> UniRx.Tuple::Create<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>(T1,T2,T3,T4,T5,T6,T7,T8)
extern "C"  Tuple_8_t3974872067 * Tuple_Create_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m4037542499_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___item10, RuntimeObject * ___item21, RuntimeObject * ___item32, RuntimeObject * ___item43, RuntimeObject * ___item54, RuntimeObject * ___item65, RuntimeObject * ___item76, RuntimeObject * ___item87, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___item10;
		RuntimeObject * L_1 = ___item21;
		RuntimeObject * L_2 = ___item32;
		RuntimeObject * L_3 = ___item43;
		RuntimeObject * L_4 = ___item54;
		RuntimeObject * L_5 = ___item65;
		RuntimeObject * L_6 = ___item76;
		RuntimeObject * L_7 = ___item87;
		Tuple_1_t4248013280  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Tuple_1__ctor_m946716862((&L_8), (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		Tuple_8_t3974872067 * L_9 = (Tuple_8_t3974872067 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (Tuple_8_t3974872067 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, Tuple_1_t4248013280 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(L_9, (RuntimeObject *)L_0, (RuntimeObject *)L_1, (RuntimeObject *)L_2, (RuntimeObject *)L_3, (RuntimeObject *)L_4, (RuntimeObject *)L_5, (RuntimeObject *)L_6, (Tuple_1_t4248013280 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		return L_9;
	}
}
// Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/OfComponentEnumerable`1<T> Unity.Linq.GameObjectExtensions/AfterSelfEnumerable::OfComponent<System.Object>()
extern "C"  OfComponentEnumerable_1_t80195040  AfterSelfEnumerable_OfComponent_TisRuntimeObject_m3894075102_gshared (AfterSelfEnumerable_t4038755258 * __this, const RuntimeMethod* method)
{
	{
		OfComponentEnumerable_1_t80195040  L_0;
		memset(&L_0, 0, sizeof(L_0));
		OfComponentEnumerable_1__ctor_m3058731097((&L_0), (AfterSelfEnumerable_t4038755258 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_0;
	}
}
extern "C"  OfComponentEnumerable_1_t80195040  AfterSelfEnumerable_OfComponent_TisRuntimeObject_m3894075102_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AfterSelfEnumerable_t4038755258 * _thisAdjusted = reinterpret_cast<AfterSelfEnumerable_t4038755258 *>(__this + 1);
	return AfterSelfEnumerable_OfComponent_TisRuntimeObject_m3894075102(_thisAdjusted, method);
}
// Unity.Linq.GameObjectExtensions/AncestorsEnumerable/OfComponentEnumerable`1<T> Unity.Linq.GameObjectExtensions/AncestorsEnumerable::OfComponent<System.Object>()
extern "C"  OfComponentEnumerable_1_t3213154383  AncestorsEnumerable_OfComponent_TisRuntimeObject_m1961088017_gshared (AncestorsEnumerable_t953073017 * __this, const RuntimeMethod* method)
{
	{
		OfComponentEnumerable_1_t3213154383  L_0;
		memset(&L_0, 0, sizeof(L_0));
		OfComponentEnumerable_1__ctor_m24595556((&L_0), (AncestorsEnumerable_t953073017 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_0;
	}
}
extern "C"  OfComponentEnumerable_1_t3213154383  AncestorsEnumerable_OfComponent_TisRuntimeObject_m1961088017_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AncestorsEnumerable_t953073017 * _thisAdjusted = reinterpret_cast<AncestorsEnumerable_t953073017 *>(__this + 1);
	return AncestorsEnumerable_OfComponent_TisRuntimeObject_m1961088017(_thisAdjusted, method);
}
// Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/OfComponentEnumerable`1<T> Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable::OfComponent<System.Object>()
extern "C"  OfComponentEnumerable_1_t3909910951  BeforeSelfEnumerable_OfComponent_TisRuntimeObject_m1141516106_gshared (BeforeSelfEnumerable_t2845913035 * __this, const RuntimeMethod* method)
{
	{
		OfComponentEnumerable_1_t3909910951  L_0;
		memset(&L_0, 0, sizeof(L_0));
		OfComponentEnumerable_1__ctor_m2906258269((&L_0), (BeforeSelfEnumerable_t2845913035 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_0;
	}
}
extern "C"  OfComponentEnumerable_1_t3909910951  BeforeSelfEnumerable_OfComponent_TisRuntimeObject_m1141516106_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	BeforeSelfEnumerable_t2845913035 * _thisAdjusted = reinterpret_cast<BeforeSelfEnumerable_t2845913035 *>(__this + 1);
	return BeforeSelfEnumerable_OfComponent_TisRuntimeObject_m1141516106(_thisAdjusted, method);
}
// Unity.Linq.GameObjectExtensions/ChildrenEnumerable/OfComponentEnumerable`1<T> Unity.Linq.GameObjectExtensions/ChildrenEnumerable::OfComponent<System.Object>()
extern "C"  OfComponentEnumerable_1_t1188510407  ChildrenEnumerable_OfComponent_TisRuntimeObject_m2868525068_gshared (ChildrenEnumerable_t145725860 * __this, const RuntimeMethod* method)
{
	{
		OfComponentEnumerable_1_t1188510407  L_0;
		memset(&L_0, 0, sizeof(L_0));
		OfComponentEnumerable_1__ctor_m1320143178((&L_0), (ChildrenEnumerable_t145725860 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_0;
	}
}
extern "C"  OfComponentEnumerable_1_t1188510407  ChildrenEnumerable_OfComponent_TisRuntimeObject_m2868525068_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ChildrenEnumerable_t145725860 * _thisAdjusted = reinterpret_cast<ChildrenEnumerable_t145725860 *>(__this + 1);
	return ChildrenEnumerable_OfComponent_TisRuntimeObject_m2868525068(_thisAdjusted, method);
}
// Unity.Linq.GameObjectExtensions/DescendantsEnumerable/OfComponentEnumerable`1<T> Unity.Linq.GameObjectExtensions/DescendantsEnumerable::OfComponent<System.Object>()
extern "C"  OfComponentEnumerable_1_t908350185  DescendantsEnumerable_OfComponent_TisRuntimeObject_m4141729503_gshared (DescendantsEnumerable_t4167108050 * __this, const RuntimeMethod* method)
{
	{
		OfComponentEnumerable_1_t908350185  L_0;
		memset(&L_0, 0, sizeof(L_0));
		OfComponentEnumerable_1__ctor_m1823402387((&L_0), (DescendantsEnumerable_t4167108050 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_0;
	}
}
extern "C"  OfComponentEnumerable_1_t908350185  DescendantsEnumerable_OfComponent_TisRuntimeObject_m4141729503_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	DescendantsEnumerable_t4167108050 * _thisAdjusted = reinterpret_cast<DescendantsEnumerable_t4167108050 *>(__this + 1);
	return DescendantsEnumerable_OfComponent_TisRuntimeObject_m4141729503(_thisAdjusted, method);
}
// UnityEngine.Coroutine UniRx.Observable::StartAsCoroutine<System.Int32>(UniRx.IObservable`1<T>,System.Action`1<T>,System.Action`1<System.Exception>,UniRx.CancellationToken)
extern "C"  Coroutine_t3829159415 * Observable_StartAsCoroutine_TisInt32_t2950945753_m1004123127_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Action_1_t3123413348 * ___onResult1, Action_1_t1609204844 * ___onError2, CancellationToken_t1265546479  ___cancel3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_StartAsCoroutine_TisInt32_t2950945753_m1004123127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		Action_1_t3123413348 * L_1 = ___onResult1;
		Action_1_t1609204844 * L_2 = ___onError2;
		CancellationToken_t1265546479  L_3 = ___cancel3;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_4 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Action_1_t3123413348 *, Action_1_t1609204844 *, CancellationToken_t1265546479 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (Action_1_t3123413348 *)L_1, (Action_1_t1609204844 *)L_2, (CancellationToken_t1265546479 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t3684499304_il2cpp_TypeInfo_var);
		Coroutine_t3829159415 * L_5 = MainThreadDispatcher_StartCoroutine_m384510185(NULL /*static, unused*/, (RuntimeObject*)L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Coroutine UniRx.Observable::StartAsCoroutine<System.Int32>(UniRx.IObservable`1<T>,System.Action`1<T>,UniRx.CancellationToken)
extern "C"  Coroutine_t3829159415 * Observable_StartAsCoroutine_TisInt32_t2950945753_m1977739596_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Action_1_t3123413348 * ___onResult1, CancellationToken_t1265546479  ___cancel2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_StartAsCoroutine_TisInt32_t2950945753_m1977739596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		Action_1_t3123413348 * L_1 = ___onResult1;
		IL2CPP_RUNTIME_CLASS_INIT(Stubs_t4137823816_il2cpp_TypeInfo_var);
		Action_1_t1609204844 * L_2 = ((Stubs_t4137823816_StaticFields*)il2cpp_codegen_static_fields_for(Stubs_t4137823816_il2cpp_TypeInfo_var))->get_Throw_1();
		CancellationToken_t1265546479  L_3 = ___cancel2;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		Coroutine_t3829159415 * L_4 = ((  Coroutine_t3829159415 * (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Action_1_t3123413348 *, Action_1_t1609204844 *, CancellationToken_t1265546479 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (Action_1_t3123413348 *)L_1, (Action_1_t1609204844 *)L_2, (CancellationToken_t1265546479 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_4;
	}
}
// UnityEngine.Coroutine UniRx.Observable::StartAsCoroutine<System.Object>(UniRx.IObservable`1<T>,System.Action`1<System.Exception>,UniRx.CancellationToken)
extern "C"  Coroutine_t3829159415 * Observable_StartAsCoroutine_TisRuntimeObject_m2276369549_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Action_1_t1609204844 * ___onError1, CancellationToken_t1265546479  ___cancel2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_StartAsCoroutine_TisRuntimeObject_m2276369549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Action_1_t3252573759 * L_1 = ((Stubs_1_t2084668772_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Ignore_0();
		Action_1_t1609204844 * L_2 = ___onError1;
		CancellationToken_t1265546479  L_3 = ___cancel2;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		Coroutine_t3829159415 * L_4 = ((  Coroutine_t3829159415 * (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Action_1_t3252573759 *, Action_1_t1609204844 *, CancellationToken_t1265546479 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (Action_1_t3252573759 *)L_1, (Action_1_t1609204844 *)L_2, (CancellationToken_t1265546479 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_4;
	}
}
// UnityEngine.Coroutine UniRx.Observable::StartAsCoroutine<System.Object>(UniRx.IObservable`1<T>,System.Action`1<T>,System.Action`1<System.Exception>,UniRx.CancellationToken)
extern "C"  Coroutine_t3829159415 * Observable_StartAsCoroutine_TisRuntimeObject_m4187518785_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Action_1_t3252573759 * ___onResult1, Action_1_t1609204844 * ___onError2, CancellationToken_t1265546479  ___cancel3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_StartAsCoroutine_TisRuntimeObject_m4187518785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		Action_1_t3252573759 * L_1 = ___onResult1;
		Action_1_t1609204844 * L_2 = ___onError2;
		CancellationToken_t1265546479  L_3 = ___cancel3;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		RuntimeObject* L_4 = ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Action_1_t3252573759 *, Action_1_t1609204844 *, CancellationToken_t1265546479 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (Action_1_t3252573759 *)L_1, (Action_1_t1609204844 *)L_2, (CancellationToken_t1265546479 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t3684499304_il2cpp_TypeInfo_var);
		Coroutine_t3829159415 * L_5 = MainThreadDispatcher_StartCoroutine_m384510185(NULL /*static, unused*/, (RuntimeObject*)L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Coroutine UniRx.Observable::StartAsCoroutine<System.Object>(UniRx.IObservable`1<T>,System.Action`1<T>,UniRx.CancellationToken)
extern "C"  Coroutine_t3829159415 * Observable_StartAsCoroutine_TisRuntimeObject_m2348039341_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, Action_1_t3252573759 * ___onResult1, CancellationToken_t1265546479  ___cancel2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_StartAsCoroutine_TisRuntimeObject_m2348039341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		Action_1_t3252573759 * L_1 = ___onResult1;
		IL2CPP_RUNTIME_CLASS_INIT(Stubs_t4137823816_il2cpp_TypeInfo_var);
		Action_1_t1609204844 * L_2 = ((Stubs_t4137823816_StaticFields*)il2cpp_codegen_static_fields_for(Stubs_t4137823816_il2cpp_TypeInfo_var))->get_Throw_1();
		CancellationToken_t1265546479  L_3 = ___cancel2;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		Coroutine_t3829159415 * L_4 = ((  Coroutine_t3829159415 * (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Action_1_t3252573759 *, Action_1_t1609204844 *, CancellationToken_t1265546479 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (Action_1_t3252573759 *)L_1, (Action_1_t1609204844 *)L_2, (CancellationToken_t1265546479 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_4;
	}
}
// UnityEngine.Coroutine UniRx.Observable::StartAsCoroutine<System.Object>(UniRx.IObservable`1<T>,UniRx.CancellationToken)
extern "C"  Coroutine_t3829159415 * Observable_StartAsCoroutine_TisRuntimeObject_m447298803_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* ___source0, CancellationToken_t1265546479  ___cancel1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Observable_StartAsCoroutine_TisRuntimeObject_m447298803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___source0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		Action_1_t3252573759 * L_1 = ((Stubs_1_t2084668772_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Ignore_0();
		IL2CPP_RUNTIME_CLASS_INIT(Stubs_t4137823816_il2cpp_TypeInfo_var);
		Action_1_t1609204844 * L_2 = ((Stubs_t4137823816_StaticFields*)il2cpp_codegen_static_fields_for(Stubs_t4137823816_il2cpp_TypeInfo_var))->get_Throw_1();
		CancellationToken_t1265546479  L_3 = ___cancel1;
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t2677550507_il2cpp_TypeInfo_var);
		Coroutine_t3829159415 * L_4 = ((  Coroutine_t3829159415 * (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Action_1_t3252573759 *, Action_1_t1609204844 *, CancellationToken_t1265546479 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, (RuntimeObject*)L_0, (Action_1_t3252573759 *)L_1, (Action_1_t1609204844 *)L_2, (CancellationToken_t1265546479 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_4;
	}
}
// UnityEngine.Events.UnityAction`1<T> UniRx.UnityEventExtensions::<AsObservable`1>m__1<System.Boolean>(System.Action`1<T>)
extern "C"  UnityAction_1_t682124106 * UnityEventExtensions_U3CAsObservable_1U3Em__1_TisBoolean_t97287965_m3659169748_gshared (RuntimeObject * __this /* static, unused */, Action_1_t269755560 * ___h0, const RuntimeMethod* method)
{
	{
		Action_1_t269755560 * L_0 = ___h0;
		intptr_t L_1 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0);
		UnityAction_1_t682124106 * L_2 = (UnityAction_1_t682124106 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (UnityAction_1_t682124106 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_2, (RuntimeObject *)L_0, (intptr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_2;
	}
}
// UnityEngine.Events.UnityAction`1<T> UniRx.UnityEventExtensions::<AsObservable`1>m__1<System.Object>(System.Action`1<T>)
extern "C"  UnityAction_1_t3664942305 * UnityEventExtensions_U3CAsObservable_1U3Em__1_TisRuntimeObject_m3025295511_gshared (RuntimeObject * __this /* static, unused */, Action_1_t3252573759 * ___h0, const RuntimeMethod* method)
{
	{
		Action_1_t3252573759 * L_0 = ___h0;
		intptr_t L_1 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0);
		UnityAction_1_t3664942305 * L_2 = (UnityAction_1_t3664942305 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (UnityAction_1_t3664942305 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_2, (RuntimeObject *)L_0, (intptr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_2;
	}
}
// UnityEngine.Events.UnityAction`1<T> UniRx.UnityEventExtensions::<AsObservable`1>m__1<System.Single>(System.Action`1<T>)
extern "C"  UnityAction_1_t1982102915 * UnityEventExtensions_U3CAsObservable_1U3Em__1_TisSingle_t1397266774_m3155033615_gshared (RuntimeObject * __this /* static, unused */, Action_1_t1569734369 * ___h0, const RuntimeMethod* method)
{
	{
		Action_1_t1569734369 * L_0 = ___h0;
		intptr_t L_1 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0);
		UnityAction_1_t1982102915 * L_2 = (UnityAction_1_t1982102915 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (UnityAction_1_t1982102915 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_2, (RuntimeObject *)L_0, (intptr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_2;
	}
}
// UnityEngine.Events.UnityAction`1<T> UniRx.UnityEventExtensions::<AsObservable`1>m__1<UnityEngine.Vector2>(System.Action`1<T>)
extern "C"  UnityAction_1_t2741065664 * UnityEventExtensions_U3CAsObservable_1U3Em__1_TisVector2_t2156229523_m1188584721_gshared (RuntimeObject * __this /* static, unused */, Action_1_t2328697118 * ___h0, const RuntimeMethod* method)
{
	{
		Action_1_t2328697118 * L_0 = ___h0;
		intptr_t L_1 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0);
		UnityAction_1_t2741065664 * L_2 = (UnityAction_1_t2741065664 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 1));
		((  void (*) (UnityAction_1_t2741065664 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(L_2, (RuntimeObject *)L_0, (intptr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return L_2;
	}
}
// UnityEngine.Events.UnityAction`2<T0,T1> UniRx.UnityEventExtensions::<AsObservable`2>m__2<System.Object,System.Object>(System.Action`1<UniRx.Tuple`2<T0,T1>>)
extern "C"  UnityAction_2_t3283971887 * UnityEventExtensions_U3CAsObservable_2U3Em__2_TisRuntimeObject_TisRuntimeObject_m2370642707_gshared (RuntimeObject * __this /* static, unused */, Action_1_t522702268 * ___h0, const RuntimeMethod* method)
{
	U3CAsObservableU3Ec__AnonStorey2_2_t1336485635 * V_0 = NULL;
	{
		U3CAsObservableU3Ec__AnonStorey2_2_t1336485635 * L_0 = (U3CAsObservableU3Ec__AnonStorey2_2_t1336485635 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CAsObservableU3Ec__AnonStorey2_2_t1336485635 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3CAsObservableU3Ec__AnonStorey2_2_t1336485635 *)L_0;
		U3CAsObservableU3Ec__AnonStorey2_2_t1336485635 * L_1 = V_0;
		Action_1_t522702268 * L_2 = ___h0;
		NullCheck(L_1);
		L_1->set_h_0(L_2);
		U3CAsObservableU3Ec__AnonStorey2_2_t1336485635 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2);
		UnityAction_2_t3283971887 * L_5 = (UnityAction_2_t3283971887 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (UnityAction_2_t3283971887 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_5, (RuntimeObject *)L_3, (intptr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_5;
	}
}
// UnityEngine.Events.UnityAction`3<T0,T1,T2> UniRx.UnityEventExtensions::<AsObservable`3>m__3<System.Object,System.Object,System.Object>(System.Action`1<UniRx.Tuple`3<T0,T1,T2>>)
extern "C"  UnityAction_3_t1557236713 * UnityEventExtensions_U3CAsObservable_3U3Em__3_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m1624642617_gshared (RuntimeObject * __this /* static, unused */, Action_1_t3401448761 * ___h0, const RuntimeMethod* method)
{
	U3CAsObservableU3Ec__AnonStorey4_3_t3097154774 * V_0 = NULL;
	{
		U3CAsObservableU3Ec__AnonStorey4_3_t3097154774 * L_0 = (U3CAsObservableU3Ec__AnonStorey4_3_t3097154774 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CAsObservableU3Ec__AnonStorey4_3_t3097154774 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3CAsObservableU3Ec__AnonStorey4_3_t3097154774 *)L_0;
		U3CAsObservableU3Ec__AnonStorey4_3_t3097154774 * L_1 = V_0;
		Action_1_t3401448761 * L_2 = ___h0;
		NullCheck(L_1);
		L_1->set_h_0(L_2);
		U3CAsObservableU3Ec__AnonStorey4_3_t3097154774 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2);
		UnityAction_3_t1557236713 * L_5 = (UnityAction_3_t1557236713 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (UnityAction_3_t1557236713 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_5, (RuntimeObject *)L_3, (intptr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_5;
	}
}
// UnityEngine.Events.UnityAction`4<T0,T1,T2,T3> UniRx.UnityEventExtensions::<AsObservable`4>m__4<System.Object,System.Object,System.Object,System.Object>(System.Action`1<UniRx.Tuple`4<T0,T1,T2,T3>>)
extern "C"  UnityAction_4_t682480391 * UnityEventExtensions_U3CAsObservable_4U3Em__4_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_TisRuntimeObject_m3058473618_gshared (RuntimeObject * __this /* static, unused */, Action_1_t2821607406 * ___h0, const RuntimeMethod* method)
{
	U3CAsObservableU3Ec__AnonStorey6_4_t2752624349 * V_0 = NULL;
	{
		U3CAsObservableU3Ec__AnonStorey6_4_t2752624349 * L_0 = (U3CAsObservableU3Ec__AnonStorey6_4_t2752624349 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		((  void (*) (U3CAsObservableU3Ec__AnonStorey6_4_t2752624349 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (U3CAsObservableU3Ec__AnonStorey6_4_t2752624349 *)L_0;
		U3CAsObservableU3Ec__AnonStorey6_4_t2752624349 * L_1 = V_0;
		Action_1_t2821607406 * L_2 = ___h0;
		NullCheck(L_1);
		L_1->set_h_0(L_2);
		U3CAsObservableU3Ec__AnonStorey6_4_t2752624349 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2);
		UnityAction_4_t682480391 * L_5 = (UnityAction_4_t682480391 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 3));
		((  void (*) (UnityAction_4_t682480391 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->methodPointer)(L_5, (RuntimeObject *)L_3, (intptr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_5;
	}
}
// UnityEngine.GameObject Unity.Linq.GameObjectExtensions::GetGameObject<System.Object>(T)
extern "C"  GameObject_t1113636619 * GameObjectExtensions_GetGameObject_TisRuntimeObject_m291751742_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameObjectExtensions_GetGameObject_TisRuntimeObject_m291751742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	Component_t1923634451 * V_1 = NULL;
	{
		RuntimeObject * L_0 = ___obj0;
		V_0 = (GameObject_t1113636619 *)((GameObject_t1113636619 *)IsInst((RuntimeObject*)L_0, GameObject_t1113636619_il2cpp_TypeInfo_var));
		GameObject_t1113636619 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1810815630(NULL /*static, unused*/, (Object_t631007953 *)L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		RuntimeObject * L_3 = ___obj0;
		V_1 = (Component_t1923634451 *)((Component_t1923634451 *)IsInst((RuntimeObject*)L_3, Component_t1923634451_il2cpp_TypeInfo_var));
		Component_t1923634451 * L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m1810815630(NULL /*static, unused*/, (Object_t631007953 *)L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0032;
		}
	}
	{
		return (GameObject_t1113636619 *)NULL;
	}

IL_0032:
	{
		Component_t1923634451 * L_6 = V_1;
		NullCheck((Component_t1923634451 *)L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142((Component_t1923634451 *)L_6, /*hidden argument*/NULL);
		V_0 = (GameObject_t1113636619 *)L_7;
	}

IL_0039:
	{
		GameObject_t1113636619 * L_8 = V_0;
		return L_8;
	}
}
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
extern "C"  GameObject_t1113636619 * ExecuteEvents_ExecuteHierarchy_TisRuntimeObject_m3266560969_gshared (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___root0, BaseEventData_t3903027533 * ___eventData1, EventFunction_1_t1764640198 * ___callbackFunction2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_ExecuteHierarchy_TisRuntimeObject_m3266560969_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Transform_t3600365921 * V_1 = NULL;
	GameObject_t1113636619 * V_2 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___root0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t3484638744_il2cpp_TypeInfo_var);
		List_1_t777473367 * L_1 = ((ExecuteEvents_t3484638744_StaticFields*)il2cpp_codegen_static_fields_for(ExecuteEvents_t3484638744_il2cpp_TypeInfo_var))->get_s_InternalTransformList_18();
		ExecuteEvents_GetEventChain_m2404658789(NULL /*static, unused*/, (GameObject_t1113636619 *)L_0, (RuntimeObject*)L_1, /*hidden argument*/NULL);
		V_0 = (int32_t)0;
		goto IL_0043;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t3484638744_il2cpp_TypeInfo_var);
		List_1_t777473367 * L_2 = ((ExecuteEvents_t3484638744_StaticFields*)il2cpp_codegen_static_fields_for(ExecuteEvents_t3484638744_il2cpp_TypeInfo_var))->get_s_InternalTransformList_18();
		int32_t L_3 = V_0;
		NullCheck((List_1_t777473367 *)L_2);
		Transform_t3600365921 * L_4 = List_1_get_Item_m2402360903((List_1_t777473367 *)L_2, (int32_t)L_3, /*hidden argument*/List_1_get_Item_m2402360903_RuntimeMethod_var);
		V_1 = (Transform_t3600365921 *)L_4;
		Transform_t3600365921 * L_5 = V_1;
		NullCheck((Component_t1923634451 *)L_5);
		GameObject_t1113636619 * L_6 = Component_get_gameObject_m442555142((Component_t1923634451 *)L_5, /*hidden argument*/NULL);
		BaseEventData_t3903027533 * L_7 = ___eventData1;
		EventFunction_1_t1764640198 * L_8 = ___callbackFunction2;
		bool L_9 = ((  bool (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, BaseEventData_t3903027533 *, EventFunction_1_t1764640198 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (GameObject_t1113636619 *)L_6, (BaseEventData_t3903027533 *)L_7, (EventFunction_1_t1764640198 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_9)
		{
			goto IL_003e;
		}
	}
	{
		Transform_t3600365921 * L_10 = V_1;
		NullCheck((Component_t1923634451 *)L_10);
		GameObject_t1113636619 * L_11 = Component_get_gameObject_m442555142((Component_t1923634451 *)L_10, /*hidden argument*/NULL);
		V_2 = (GameObject_t1113636619 *)L_11;
		goto IL_005a;
	}

IL_003e:
	{
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_0043:
	{
		int32_t L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t3484638744_il2cpp_TypeInfo_var);
		List_1_t777473367 * L_14 = ((ExecuteEvents_t3484638744_StaticFields*)il2cpp_codegen_static_fields_for(ExecuteEvents_t3484638744_il2cpp_TypeInfo_var))->get_s_InternalTransformList_18();
		NullCheck((List_1_t777473367 *)L_14);
		int32_t L_15 = List_1_get_Count_m3543896146((List_1_t777473367 *)L_14, /*hidden argument*/List_1_get_Count_m3543896146_RuntimeMethod_var);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0013;
		}
	}
	{
		V_2 = (GameObject_t1113636619 *)NULL;
		goto IL_005a;
	}

IL_005a:
	{
		GameObject_t1113636619 * L_16 = V_2;
		return L_16;
	}
}
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
extern "C"  GameObject_t1113636619 * ExecuteEvents_GetEventHandler_TisRuntimeObject_m3687647312_gshared (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___root0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_GetEventHandler_TisRuntimeObject_m3687647312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	Transform_t3600365921 * V_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___root0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, (Object_t631007953 *)L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		V_0 = (GameObject_t1113636619 *)NULL;
		goto IL_0058;
	}

IL_0014:
	{
		GameObject_t1113636619 * L_2 = ___root0;
		NullCheck((GameObject_t1113636619 *)L_2);
		Transform_t3600365921 * L_3 = GameObject_get_transform_m1369836730((GameObject_t1113636619 *)L_2, /*hidden argument*/NULL);
		V_1 = (Transform_t3600365921 *)L_3;
		goto IL_0045;
	}

IL_0020:
	{
		Transform_t3600365921 * L_4 = V_1;
		NullCheck((Component_t1923634451 *)L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142((Component_t1923634451 *)L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t3484638744_il2cpp_TypeInfo_var);
		bool L_6 = ((  bool (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (GameObject_t1113636619 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		Transform_t3600365921 * L_7 = V_1;
		NullCheck((Component_t1923634451 *)L_7);
		GameObject_t1113636619 * L_8 = Component_get_gameObject_m442555142((Component_t1923634451 *)L_7, /*hidden argument*/NULL);
		V_0 = (GameObject_t1113636619 *)L_8;
		goto IL_0058;
	}

IL_003d:
	{
		Transform_t3600365921 * L_9 = V_1;
		NullCheck((Transform_t3600365921 *)L_9);
		Transform_t3600365921 * L_10 = Transform_get_parent_m835071599((Transform_t3600365921 *)L_9, /*hidden argument*/NULL);
		V_1 = (Transform_t3600365921 *)L_10;
	}

IL_0045:
	{
		Transform_t3600365921 * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, (Object_t631007953 *)L_11, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0020;
		}
	}
	{
		V_0 = (GameObject_t1113636619 *)NULL;
		goto IL_0058;
	}

IL_0058:
	{
		GameObject_t1113636619 * L_13 = V_0;
		return L_13;
	}
}
