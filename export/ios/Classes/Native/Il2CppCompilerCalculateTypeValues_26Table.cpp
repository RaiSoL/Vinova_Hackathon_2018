﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UniRx.Operators.FromEventObservable
struct FromEventObservable_t3684970068;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t811551750;
// System.Action
struct Action_t1264377477;
// System.Action`1<System.Action>
struct Action_1_t1436845072;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef FROMEVENT_T3269215805_H
#define FROMEVENT_T3269215805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FromEventObservable/FromEvent
struct  FromEvent_t3269215805  : public RuntimeObject
{
public:
	// UniRx.Operators.FromEventObservable UniRx.Operators.FromEventObservable/FromEvent::parent
	FromEventObservable_t3684970068 * ___parent_0;
	// UniRx.IObserver`1<UniRx.Unit> UniRx.Operators.FromEventObservable/FromEvent::observer
	RuntimeObject* ___observer_1;
	// System.Action UniRx.Operators.FromEventObservable/FromEvent::handler
	Action_t1264377477 * ___handler_2;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(FromEvent_t3269215805, ___parent_0)); }
	inline FromEventObservable_t3684970068 * get_parent_0() const { return ___parent_0; }
	inline FromEventObservable_t3684970068 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(FromEventObservable_t3684970068 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_observer_1() { return static_cast<int32_t>(offsetof(FromEvent_t3269215805, ___observer_1)); }
	inline RuntimeObject* get_observer_1() const { return ___observer_1; }
	inline RuntimeObject** get_address_of_observer_1() { return &___observer_1; }
	inline void set_observer_1(RuntimeObject* value)
	{
		___observer_1 = value;
		Il2CppCodeGenWriteBarrier((&___observer_1), value);
	}

	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(FromEvent_t3269215805, ___handler_2)); }
	inline Action_t1264377477 * get_handler_2() const { return ___handler_2; }
	inline Action_t1264377477 ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(Action_t1264377477 * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier((&___handler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMEVENT_T3269215805_H
#ifndef OPERATOROBSERVABLEBASE_1_T354719556_H
#define OPERATOROBSERVABLEBASE_1_T354719556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1<UniRx.Unit>
struct  OperatorObservableBase_1_t354719556  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t354719556, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVABLEBASE_1_T354719556_H
#ifndef FROMEVENTOBSERVABLE_T3684970068_H
#define FROMEVENTOBSERVABLE_T3684970068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FromEventObservable
struct  FromEventObservable_t3684970068  : public OperatorObservableBase_1_t354719556
{
public:
	// System.Action`1<System.Action> UniRx.Operators.FromEventObservable::addHandler
	Action_1_t1436845072 * ___addHandler_1;
	// System.Action`1<System.Action> UniRx.Operators.FromEventObservable::removeHandler
	Action_1_t1436845072 * ___removeHandler_2;

public:
	inline static int32_t get_offset_of_addHandler_1() { return static_cast<int32_t>(offsetof(FromEventObservable_t3684970068, ___addHandler_1)); }
	inline Action_1_t1436845072 * get_addHandler_1() const { return ___addHandler_1; }
	inline Action_1_t1436845072 ** get_address_of_addHandler_1() { return &___addHandler_1; }
	inline void set_addHandler_1(Action_1_t1436845072 * value)
	{
		___addHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___addHandler_1), value);
	}

	inline static int32_t get_offset_of_removeHandler_2() { return static_cast<int32_t>(offsetof(FromEventObservable_t3684970068, ___removeHandler_2)); }
	inline Action_1_t1436845072 * get_removeHandler_2() const { return ___removeHandler_2; }
	inline Action_1_t1436845072 ** get_address_of_removeHandler_2() { return &___removeHandler_2; }
	inline void set_removeHandler_2(Action_1_t1436845072 * value)
	{
		___removeHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___removeHandler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMEVENTOBSERVABLE_T3684970068_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2600[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2601[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2602[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2603[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2604[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2605[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2606[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2607[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2608[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2609[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2610[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2612[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2613[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2614[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2615[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2616[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2617[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2618[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2619[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2621[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2623[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2625[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2626[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2627[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2629[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2630[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2631[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2632[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2633[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2634[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2635[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2636[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2637[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2638[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2639[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2640[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2641[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2642[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2643[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2644[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2645[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2646[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2647[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2648[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2649[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2650[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2651[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2652[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2653[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2654[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2655[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2656[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2657[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2659[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2662[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2663[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2664[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2666[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2667[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2668[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2670[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2672[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2673[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2674[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2675[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (FromEventObservable_t3684970068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2676[2] = 
{
	FromEventObservable_t3684970068::get_offset_of_addHandler_1(),
	FromEventObservable_t3684970068::get_offset_of_removeHandler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (FromEvent_t3269215805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2677[3] = 
{
	FromEvent_t3269215805::get_offset_of_parent_0(),
	FromEvent_t3269215805::get_offset_of_observer_1(),
	FromEvent_t3269215805::get_offset_of_handler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2678[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2679[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2680[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2682[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2683[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2685[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2686[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2689[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2690[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2693[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2694[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2696[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2697[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[4] = 
{
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
