﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UniRx.InternalUtil.SchedulerQueue
struct SchedulerQueue_t1565164295;
// System.Diagnostics.Stopwatch
struct Stopwatch_t305734070;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.String
struct String_t;
// UniRx.Subject`1<System.String>
struct Subject_1_t1936107076;
// System.IDisposable
struct IDisposable_t3640265483;
// System.Exception
struct Exception_t;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// UniRx.IProgress`1<System.Single>
struct IProgress_1_t3968412695;
// System.Collections.Generic.IEqualityComparer`1<System.Boolean>
struct IEqualityComparer_1_t2204619983;
// UniRx.Subject`1<System.Boolean>
struct Subject_1_t185944352;
// UniRx.Scheduler/<Schedule>c__AnonStorey4
struct U3CScheduleU3Ec__AnonStorey4_t481864779;
// System.Collections.Generic.IEqualityComparer`1<System.Double>
struct IEqualityComparer_1_t2701997381;
// UniRx.Subject`1<System.Double>
struct Subject_1_t683321750;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t763310475;
// UniRx.Subject`1<System.Int32>
struct Subject_1_t3039602140;
// System.Collections.Generic.IEqualityComparer`1<System.Int64>
struct IEqualityComparer_1_t1548932026;
// UniRx.Subject`1<System.Int64>
struct Subject_1_t3825223691;
// System.Collections.Generic.IEqualityComparer`1<System.Byte>
struct IEqualityComparer_1_t3241628394;
// UniRx.Subject`1<System.Byte>
struct Subject_1_t1222952763;
// System.Collections.Generic.IEqualityComparer`1<System.Single>
struct IEqualityComparer_1_t3504598792;
// UniRx.Subject`1<System.Single>
struct Subject_1_t1485923161;
// UniRx.Subject`1<UniRx.Diagnostics.LogEntry>
struct Subject_1_t1230163500;
// System.Action`1<UniRx.Diagnostics.LogEntry>
struct Action_1_t1313974708;
// System.Action`1<System.Action`1<System.DateTimeOffset>>
struct Action_1_t3574222697;
// UniRx.IScheduler
struct IScheduler_t411218504;
// UniRx.CompositeDisposable
struct CompositeDisposable_t3924054141;
// System.Action
struct Action_t1264377477;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// UniRx.BooleanDisposable
struct BooleanDisposable_t84760918;
// System.Collections.Generic.HashSet`1<System.Threading.Timer>
struct HashSet_1_t3576587796;
// System.Threading.Timer
struct Timer_t716671026;
// UniRx.InternalUtil.AsyncLock
struct AsyncLock_t301382185;
// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t4102667663;
// System.Action`1<System.Action>
struct Action_1_t1436845072;
// UniRx.Scheduler/<Schedule>c__AnonStorey0
struct U3CScheduleU3Ec__AnonStorey0_t1246538827;
// System.Action`1<System.Action`1<System.TimeSpan>>
struct Action_1_t1226094439;
// UniRx.Scheduler/<Schedule>c__AnonStorey2
struct U3CScheduleU3Ec__AnonStorey2_t1628875851;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.AnimationCurve>
struct IEqualityComparer_1_t859119088;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UniRx.Subject`1<UnityEngine.AnimationCurve>
struct Subject_1_t3135410753;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UniRx.ICancelable
struct ICancelable_t3440398893;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Rect>
struct IEqualityComparer_1_t172844581;
// UniRx.Subject`1<UnityEngine.Rect>
struct Subject_1_t2449136246;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Color>
struct IEqualityComparer_1_t368051046;
// UniRx.Subject`1<UnityEngine.Color>
struct Subject_1_t2644342711;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector4>
struct IEqualityComparer_1_t1131393659;
// UniRx.Subject`1<UnityEngine.Vector4>
struct Subject_1_t3407685324;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector3>
struct IEqualityComparer_1_t1534678186;
// UniRx.Subject`1<UnityEngine.Vector3>
struct Subject_1_t3810969851;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector2>
struct IEqualityComparer_1_t4263561541;
// UniRx.Subject`1<UnityEngine.Vector2>
struct Subject_1_t2244885910;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Bounds>
struct IEqualityComparer_1_t79202632;
// UniRx.Subject`1<UnityEngine.Bounds>
struct Subject_1_t2355494297;
// UnityEngine.Object
struct Object_t631007953;

struct Object_t631007953_marshaled_com;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CURRENTTHREADSCHEDULER_T2387236140_H
#define CURRENTTHREADSCHEDULER_T2387236140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/CurrentThreadScheduler
struct  CurrentThreadScheduler_t2387236140  : public RuntimeObject
{
public:

public:
};

struct CurrentThreadScheduler_t2387236140_ThreadStaticFields
{
public:
	// UniRx.InternalUtil.SchedulerQueue UniRx.Scheduler/CurrentThreadScheduler::s_threadLocalQueue
	SchedulerQueue_t1565164295 * ___s_threadLocalQueue_0;
	// System.Diagnostics.Stopwatch UniRx.Scheduler/CurrentThreadScheduler::s_clock
	Stopwatch_t305734070 * ___s_clock_1;

public:
	inline static int32_t get_offset_of_s_threadLocalQueue_0() { return static_cast<int32_t>(offsetof(CurrentThreadScheduler_t2387236140_ThreadStaticFields, ___s_threadLocalQueue_0)); }
	inline SchedulerQueue_t1565164295 * get_s_threadLocalQueue_0() const { return ___s_threadLocalQueue_0; }
	inline SchedulerQueue_t1565164295 ** get_address_of_s_threadLocalQueue_0() { return &___s_threadLocalQueue_0; }
	inline void set_s_threadLocalQueue_0(SchedulerQueue_t1565164295 * value)
	{
		___s_threadLocalQueue_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_threadLocalQueue_0), value);
	}

	inline static int32_t get_offset_of_s_clock_1() { return static_cast<int32_t>(offsetof(CurrentThreadScheduler_t2387236140_ThreadStaticFields, ___s_clock_1)); }
	inline Stopwatch_t305734070 * get_s_clock_1() const { return ___s_clock_1; }
	inline Stopwatch_t305734070 ** get_address_of_s_clock_1() { return &___s_clock_1; }
	inline void set_s_clock_1(Stopwatch_t305734070 * value)
	{
		___s_clock_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_clock_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTTHREADSCHEDULER_T2387236140_H
#ifndef REACTIVEPROPERTY_1_T1082175583_H
#define REACTIVEPROPERTY_1_T1082175583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<System.String>
struct  ReactiveProperty_1_t1082175583  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	String_t* ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t1936107076 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1082175583, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1082175583, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1082175583, ___value_3)); }
	inline String_t* get_value_3() const { return ___value_3; }
	inline String_t** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(String_t* value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1082175583, ___publisher_4)); }
	inline Subject_1_t1936107076 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t1936107076 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t1936107076 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1082175583, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1082175583, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t1082175583_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1082175583_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T1082175583_H
#ifndef U3CASOBSERVABLEU3EC__ANONSTOREY1_T2816429619_H
#define U3CASOBSERVABLEU3EC__ANONSTOREY1_T2816429619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.AsyncOperationExtensions/<AsObservable>c__AnonStorey1
struct  U3CAsObservableU3Ec__AnonStorey1_t2816429619  : public RuntimeObject
{
public:
	// UnityEngine.AsyncOperation UniRx.AsyncOperationExtensions/<AsObservable>c__AnonStorey1::asyncOperation
	AsyncOperation_t1445031843 * ___asyncOperation_0;
	// UniRx.IProgress`1<System.Single> UniRx.AsyncOperationExtensions/<AsObservable>c__AnonStorey1::progress
	RuntimeObject* ___progress_1;

public:
	inline static int32_t get_offset_of_asyncOperation_0() { return static_cast<int32_t>(offsetof(U3CAsObservableU3Ec__AnonStorey1_t2816429619, ___asyncOperation_0)); }
	inline AsyncOperation_t1445031843 * get_asyncOperation_0() const { return ___asyncOperation_0; }
	inline AsyncOperation_t1445031843 ** get_address_of_asyncOperation_0() { return &___asyncOperation_0; }
	inline void set_asyncOperation_0(AsyncOperation_t1445031843 * value)
	{
		___asyncOperation_0 = value;
		Il2CppCodeGenWriteBarrier((&___asyncOperation_0), value);
	}

	inline static int32_t get_offset_of_progress_1() { return static_cast<int32_t>(offsetof(U3CAsObservableU3Ec__AnonStorey1_t2816429619, ___progress_1)); }
	inline RuntimeObject* get_progress_1() const { return ___progress_1; }
	inline RuntimeObject** get_address_of_progress_1() { return &___progress_1; }
	inline void set_progress_1(RuntimeObject* value)
	{
		___progress_1 = value;
		Il2CppCodeGenWriteBarrier((&___progress_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CASOBSERVABLEU3EC__ANONSTOREY1_T2816429619_H
#ifndef ASYNCOPERATIONEXTENSIONS_T103671278_H
#define ASYNCOPERATIONEXTENSIONS_T103671278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.AsyncOperationExtensions
struct  AsyncOperationExtensions_t103671278  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCOPERATIONEXTENSIONS_T103671278_H
#ifndef AOTSAFEEXTENSIONS_T3690086409_H
#define AOTSAFEEXTENSIONS_T3690086409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.AotSafeExtensions
struct  AotSafeExtensions_t3690086409  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AOTSAFEEXTENSIONS_T3690086409_H
#ifndef TIMESTAMPED_T555880859_H
#define TIMESTAMPED_T555880859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Timestamped
struct  Timestamped_t555880859  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMPED_T555880859_H
#ifndef REACTIVEPROPERTY_1_T3626980155_H
#define REACTIVEPROPERTY_1_T3626980155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<System.Boolean>
struct  ReactiveProperty_1_t3626980155  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	bool ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t185944352 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t3626980155, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t3626980155, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t3626980155, ___value_3)); }
	inline bool get_value_3() const { return ___value_3; }
	inline bool* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(bool value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t3626980155, ___publisher_4)); }
	inline Subject_1_t185944352 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t185944352 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t185944352 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t3626980155, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t3626980155, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t3626980155_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t3626980155_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T3626980155_H
#ifndef TUPLE_T1757971014_H
#define TUPLE_T1757971014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple
struct  Tuple_t1757971014  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUPLE_T1757971014_H
#ifndef OPTIMIZEDOBSERVABLEEXTENSIONS_T1647498745_H
#define OPTIMIZEDOBSERVABLEEXTENSIONS_T1647498745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.OptimizedObservableExtensions
struct  OptimizedObservableExtensions_t1647498745  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIMIZEDOBSERVABLEEXTENSIONS_T1647498745_H
#ifndef SUBJECTEXTENSIONS_T2134319452_H
#define SUBJECTEXTENSIONS_T2134319452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.SubjectExtensions
struct  SubjectExtensions_t2134319452  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBJECTEXTENSIONS_T2134319452_H
#ifndef U3CSCHEDULEU3EC__ANONSTOREY5_T849780234_H
#define U3CSCHEDULEU3EC__ANONSTOREY5_T849780234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/<Schedule>c__AnonStorey4/<Schedule>c__AnonStorey5
struct  U3CScheduleU3Ec__AnonStorey5_t849780234  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Scheduler/<Schedule>c__AnonStorey4/<Schedule>c__AnonStorey5::isAdded
	bool ___isAdded_0;
	// System.IDisposable UniRx.Scheduler/<Schedule>c__AnonStorey4/<Schedule>c__AnonStorey5::d
	RuntimeObject* ___d_1;
	// System.Boolean UniRx.Scheduler/<Schedule>c__AnonStorey4/<Schedule>c__AnonStorey5::isDone
	bool ___isDone_2;
	// UniRx.Scheduler/<Schedule>c__AnonStorey4 UniRx.Scheduler/<Schedule>c__AnonStorey4/<Schedule>c__AnonStorey5::<>f__ref$4
	U3CScheduleU3Ec__AnonStorey4_t481864779 * ___U3CU3Ef__refU244_3;

public:
	inline static int32_t get_offset_of_isAdded_0() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey5_t849780234, ___isAdded_0)); }
	inline bool get_isAdded_0() const { return ___isAdded_0; }
	inline bool* get_address_of_isAdded_0() { return &___isAdded_0; }
	inline void set_isAdded_0(bool value)
	{
		___isAdded_0 = value;
	}

	inline static int32_t get_offset_of_d_1() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey5_t849780234, ___d_1)); }
	inline RuntimeObject* get_d_1() const { return ___d_1; }
	inline RuntimeObject** get_address_of_d_1() { return &___d_1; }
	inline void set_d_1(RuntimeObject* value)
	{
		___d_1 = value;
		Il2CppCodeGenWriteBarrier((&___d_1), value);
	}

	inline static int32_t get_offset_of_isDone_2() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey5_t849780234, ___isDone_2)); }
	inline bool get_isDone_2() const { return ___isDone_2; }
	inline bool* get_address_of_isDone_2() { return &___isDone_2; }
	inline void set_isDone_2(bool value)
	{
		___isDone_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU244_3() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey5_t849780234, ___U3CU3Ef__refU244_3)); }
	inline U3CScheduleU3Ec__AnonStorey4_t481864779 * get_U3CU3Ef__refU244_3() const { return ___U3CU3Ef__refU244_3; }
	inline U3CScheduleU3Ec__AnonStorey4_t481864779 ** get_address_of_U3CU3Ef__refU244_3() { return &___U3CU3Ef__refU244_3; }
	inline void set_U3CU3Ef__refU244_3(U3CScheduleU3Ec__AnonStorey4_t481864779 * value)
	{
		___U3CU3Ef__refU244_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU244_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCHEDULEU3EC__ANONSTOREY5_T849780234_H
#ifndef REACTIVEPROPERTY_1_T4124357553_H
#define REACTIVEPROPERTY_1_T4124357553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<System.Double>
struct  ReactiveProperty_1_t4124357553  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	double ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t683321750 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t4124357553, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t4124357553, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t4124357553, ___value_3)); }
	inline double get_value_3() const { return ___value_3; }
	inline double* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(double value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t4124357553, ___publisher_4)); }
	inline Subject_1_t683321750 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t683321750 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t683321750 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t4124357553, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t4124357553, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t4124357553_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t4124357553_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T4124357553_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef REACTIVEPROPERTY_1_T2185670647_H
#define REACTIVEPROPERTY_1_T2185670647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<System.Int32>
struct  ReactiveProperty_1_t2185670647  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	int32_t ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t3039602140 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2185670647, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2185670647, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2185670647, ___value_3)); }
	inline int32_t get_value_3() const { return ___value_3; }
	inline int32_t* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(int32_t value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2185670647, ___publisher_4)); }
	inline Subject_1_t3039602140 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t3039602140 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t3039602140 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2185670647, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2185670647, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t2185670647_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2185670647_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T2185670647_H
#ifndef REACTIVEPROPERTY_1_T2971292198_H
#define REACTIVEPROPERTY_1_T2971292198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<System.Int64>
struct  ReactiveProperty_1_t2971292198  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	int64_t ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t3825223691 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2971292198, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2971292198, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2971292198, ___value_3)); }
	inline int64_t get_value_3() const { return ___value_3; }
	inline int64_t* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(int64_t value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2971292198, ___publisher_4)); }
	inline Subject_1_t3825223691 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t3825223691 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t3825223691 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2971292198, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2971292198, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t2971292198_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2971292198_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T2971292198_H
#ifndef REACTIVEPROPERTY_1_T369021270_H
#define REACTIVEPROPERTY_1_T369021270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<System.Byte>
struct  ReactiveProperty_1_t369021270  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	uint8_t ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t1222952763 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t369021270, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t369021270, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t369021270, ___value_3)); }
	inline uint8_t get_value_3() const { return ___value_3; }
	inline uint8_t* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(uint8_t value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t369021270, ___publisher_4)); }
	inline Subject_1_t1222952763 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t1222952763 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t1222952763 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t369021270, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t369021270, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t369021270_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t369021270_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T369021270_H
#ifndef REACTIVEPROPERTY_1_T631991668_H
#define REACTIVEPROPERTY_1_T631991668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<System.Single>
struct  ReactiveProperty_1_t631991668  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	float ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t1485923161 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t631991668, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t631991668, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t631991668, ___value_3)); }
	inline float get_value_3() const { return ___value_3; }
	inline float* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(float value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t631991668, ___publisher_4)); }
	inline Subject_1_t1485923161 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t1485923161 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t1485923161 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t631991668, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t631991668, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t631991668_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t631991668_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T631991668_H
#ifndef UNITYDEBUGSINK_T4250252982_H
#define UNITYDEBUGSINK_T4250252982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Diagnostics.UnityDebugSink
struct  UnityDebugSink_t4250252982  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYDEBUGSINK_T4250252982_H
#ifndef OBSERVABLELOGGER_T2665118834_H
#define OBSERVABLELOGGER_T2665118834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Diagnostics.ObservableLogger
struct  ObservableLogger_t2665118834  : public RuntimeObject
{
public:

public:
};

struct ObservableLogger_t2665118834_StaticFields
{
public:
	// UniRx.Subject`1<UniRx.Diagnostics.LogEntry> UniRx.Diagnostics.ObservableLogger::logPublisher
	Subject_1_t1230163500 * ___logPublisher_0;
	// UniRx.Diagnostics.ObservableLogger UniRx.Diagnostics.ObservableLogger::Listener
	ObservableLogger_t2665118834 * ___Listener_1;

public:
	inline static int32_t get_offset_of_logPublisher_0() { return static_cast<int32_t>(offsetof(ObservableLogger_t2665118834_StaticFields, ___logPublisher_0)); }
	inline Subject_1_t1230163500 * get_logPublisher_0() const { return ___logPublisher_0; }
	inline Subject_1_t1230163500 ** get_address_of_logPublisher_0() { return &___logPublisher_0; }
	inline void set_logPublisher_0(Subject_1_t1230163500 * value)
	{
		___logPublisher_0 = value;
		Il2CppCodeGenWriteBarrier((&___logPublisher_0), value);
	}

	inline static int32_t get_offset_of_Listener_1() { return static_cast<int32_t>(offsetof(ObservableLogger_t2665118834_StaticFields, ___Listener_1)); }
	inline ObservableLogger_t2665118834 * get_Listener_1() const { return ___Listener_1; }
	inline ObservableLogger_t2665118834 ** get_address_of_Listener_1() { return &___Listener_1; }
	inline void set_Listener_1(ObservableLogger_t2665118834 * value)
	{
		___Listener_1 = value;
		Il2CppCodeGenWriteBarrier((&___Listener_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLELOGGER_T2665118834_H
#ifndef OBSERVABLEDEBUGEXTENSIONS_T2967164862_H
#define OBSERVABLEDEBUGEXTENSIONS_T2967164862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Diagnostics.ObservableDebugExtensions
struct  ObservableDebugExtensions_t2967164862  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEDEBUGEXTENSIONS_T2967164862_H
#ifndef LOGGER_T3861635025_H
#define LOGGER_T3861635025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Diagnostics.Logger
struct  Logger_t3861635025  : public RuntimeObject
{
public:
	// System.String UniRx.Diagnostics.Logger::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_2;
	// System.Action`1<UniRx.Diagnostics.LogEntry> UniRx.Diagnostics.Logger::logPublisher
	Action_1_t1313974708 * ___logPublisher_3;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Logger_t3861635025, ___U3CNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CNameU3Ek__BackingField_2() const { return ___U3CNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_2() { return &___U3CNameU3Ek__BackingField_2; }
	inline void set_U3CNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_logPublisher_3() { return static_cast<int32_t>(offsetof(Logger_t3861635025, ___logPublisher_3)); }
	inline Action_1_t1313974708 * get_logPublisher_3() const { return ___logPublisher_3; }
	inline Action_1_t1313974708 ** get_address_of_logPublisher_3() { return &___logPublisher_3; }
	inline void set_logPublisher_3(Action_1_t1313974708 * value)
	{
		___logPublisher_3 = value;
		Il2CppCodeGenWriteBarrier((&___logPublisher_3), value);
	}
};

struct Logger_t3861635025_StaticFields
{
public:
	// System.Boolean UniRx.Diagnostics.Logger::isInitialized
	bool ___isInitialized_0;
	// System.Boolean UniRx.Diagnostics.Logger::isDebugBuild
	bool ___isDebugBuild_1;

public:
	inline static int32_t get_offset_of_isInitialized_0() { return static_cast<int32_t>(offsetof(Logger_t3861635025_StaticFields, ___isInitialized_0)); }
	inline bool get_isInitialized_0() const { return ___isInitialized_0; }
	inline bool* get_address_of_isInitialized_0() { return &___isInitialized_0; }
	inline void set_isInitialized_0(bool value)
	{
		___isInitialized_0 = value;
	}

	inline static int32_t get_offset_of_isDebugBuild_1() { return static_cast<int32_t>(offsetof(Logger_t3861635025_StaticFields, ___isDebugBuild_1)); }
	inline bool get_isDebugBuild_1() const { return ___isDebugBuild_1; }
	inline bool* get_address_of_isDebugBuild_1() { return &___isDebugBuild_1; }
	inline void set_isDebugBuild_1(bool value)
	{
		___isDebugBuild_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGER_T3861635025_H
#ifndef LOGENTRYEXTENSIONS_T2577791660_H
#define LOGENTRYEXTENSIONS_T2577791660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Diagnostics.LogEntryExtensions
struct  LogEntryExtensions_t2577791660  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGENTRYEXTENSIONS_T2577791660_H
#ifndef U3CSCHEDULEU3EC__ANONSTOREY4_T481864779_H
#define U3CSCHEDULEU3EC__ANONSTOREY4_T481864779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/<Schedule>c__AnonStorey4
struct  U3CScheduleU3Ec__AnonStorey4_t481864779  : public RuntimeObject
{
public:
	// System.Action`1<System.Action`1<System.DateTimeOffset>> UniRx.Scheduler/<Schedule>c__AnonStorey4::action
	Action_1_t3574222697 * ___action_0;
	// UniRx.IScheduler UniRx.Scheduler/<Schedule>c__AnonStorey4::scheduler
	RuntimeObject* ___scheduler_1;
	// System.Object UniRx.Scheduler/<Schedule>c__AnonStorey4::gate
	RuntimeObject * ___gate_2;
	// UniRx.CompositeDisposable UniRx.Scheduler/<Schedule>c__AnonStorey4::group
	CompositeDisposable_t3924054141 * ___group_3;
	// System.Action UniRx.Scheduler/<Schedule>c__AnonStorey4::recursiveAction
	Action_t1264377477 * ___recursiveAction_4;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey4_t481864779, ___action_0)); }
	inline Action_1_t3574222697 * get_action_0() const { return ___action_0; }
	inline Action_1_t3574222697 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t3574222697 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_scheduler_1() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey4_t481864779, ___scheduler_1)); }
	inline RuntimeObject* get_scheduler_1() const { return ___scheduler_1; }
	inline RuntimeObject** get_address_of_scheduler_1() { return &___scheduler_1; }
	inline void set_scheduler_1(RuntimeObject* value)
	{
		___scheduler_1 = value;
		Il2CppCodeGenWriteBarrier((&___scheduler_1), value);
	}

	inline static int32_t get_offset_of_gate_2() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey4_t481864779, ___gate_2)); }
	inline RuntimeObject * get_gate_2() const { return ___gate_2; }
	inline RuntimeObject ** get_address_of_gate_2() { return &___gate_2; }
	inline void set_gate_2(RuntimeObject * value)
	{
		___gate_2 = value;
		Il2CppCodeGenWriteBarrier((&___gate_2), value);
	}

	inline static int32_t get_offset_of_group_3() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey4_t481864779, ___group_3)); }
	inline CompositeDisposable_t3924054141 * get_group_3() const { return ___group_3; }
	inline CompositeDisposable_t3924054141 ** get_address_of_group_3() { return &___group_3; }
	inline void set_group_3(CompositeDisposable_t3924054141 * value)
	{
		___group_3 = value;
		Il2CppCodeGenWriteBarrier((&___group_3), value);
	}

	inline static int32_t get_offset_of_recursiveAction_4() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey4_t481864779, ___recursiveAction_4)); }
	inline Action_t1264377477 * get_recursiveAction_4() const { return ___recursiveAction_4; }
	inline Action_t1264377477 ** get_address_of_recursiveAction_4() { return &___recursiveAction_4; }
	inline void set_recursiveAction_4(Action_t1264377477 * value)
	{
		___recursiveAction_4 = value;
		Il2CppCodeGenWriteBarrier((&___recursiveAction_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCHEDULEU3EC__ANONSTOREY4_T481864779_H
#ifndef IGNORETIMESCALEMAINTHREADSCHEDULER_T115692742_H
#define IGNORETIMESCALEMAINTHREADSCHEDULER_T115692742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler
struct  IgnoreTimeScaleMainThreadScheduler_t115692742  : public RuntimeObject
{
public:
	// System.Action`1<System.Object> UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler::scheduleAction
	Action_1_t3252573759 * ___scheduleAction_0;

public:
	inline static int32_t get_offset_of_scheduleAction_0() { return static_cast<int32_t>(offsetof(IgnoreTimeScaleMainThreadScheduler_t115692742, ___scheduleAction_0)); }
	inline Action_1_t3252573759 * get_scheduleAction_0() const { return ___scheduleAction_0; }
	inline Action_1_t3252573759 ** get_address_of_scheduleAction_0() { return &___scheduleAction_0; }
	inline void set_scheduleAction_0(Action_1_t3252573759 * value)
	{
		___scheduleAction_0 = value;
		Il2CppCodeGenWriteBarrier((&___scheduleAction_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGNORETIMESCALEMAINTHREADSCHEDULER_T115692742_H
#ifndef MAINTHREADSCHEDULER_T794692167_H
#define MAINTHREADSCHEDULER_T794692167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/MainThreadScheduler
struct  MainThreadScheduler_t794692167  : public RuntimeObject
{
public:
	// System.Action`1<System.Object> UniRx.Scheduler/MainThreadScheduler::scheduleAction
	Action_1_t3252573759 * ___scheduleAction_0;

public:
	inline static int32_t get_offset_of_scheduleAction_0() { return static_cast<int32_t>(offsetof(MainThreadScheduler_t794692167, ___scheduleAction_0)); }
	inline Action_1_t3252573759 * get_scheduleAction_0() const { return ___scheduleAction_0; }
	inline Action_1_t3252573759 ** get_address_of_scheduleAction_0() { return &___scheduleAction_0; }
	inline void set_scheduleAction_0(Action_1_t3252573759 * value)
	{
		___scheduleAction_0 = value;
		Il2CppCodeGenWriteBarrier((&___scheduleAction_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINTHREADSCHEDULER_T794692167_H
#ifndef U3CSCHEDULEU3EC__ANONSTOREY0_T2014915017_H
#define U3CSCHEDULEU3EC__ANONSTOREY0_T2014915017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/ThreadPoolScheduler/<Schedule>c__AnonStorey0
struct  U3CScheduleU3Ec__AnonStorey0_t2014915017  : public RuntimeObject
{
public:
	// UniRx.BooleanDisposable UniRx.Scheduler/ThreadPoolScheduler/<Schedule>c__AnonStorey0::d
	BooleanDisposable_t84760918 * ___d_0;
	// System.Action UniRx.Scheduler/ThreadPoolScheduler/<Schedule>c__AnonStorey0::action
	Action_t1264377477 * ___action_1;

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey0_t2014915017, ___d_0)); }
	inline BooleanDisposable_t84760918 * get_d_0() const { return ___d_0; }
	inline BooleanDisposable_t84760918 ** get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(BooleanDisposable_t84760918 * value)
	{
		___d_0 = value;
		Il2CppCodeGenWriteBarrier((&___d_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey0_t2014915017, ___action_1)); }
	inline Action_t1264377477 * get_action_1() const { return ___action_1; }
	inline Action_t1264377477 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_t1264377477 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCHEDULEU3EC__ANONSTOREY0_T2014915017_H
#ifndef PERIODICTIMER_T753797134_H
#define PERIODICTIMER_T753797134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer
struct  PeriodicTimer_t753797134  : public RuntimeObject
{
public:
	// System.Action UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer::_action
	Action_t1264377477 * ____action_1;
	// System.Threading.Timer UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer::_timer
	Timer_t716671026 * ____timer_2;
	// UniRx.InternalUtil.AsyncLock UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer::_gate
	AsyncLock_t301382185 * ____gate_3;

public:
	inline static int32_t get_offset_of__action_1() { return static_cast<int32_t>(offsetof(PeriodicTimer_t753797134, ____action_1)); }
	inline Action_t1264377477 * get__action_1() const { return ____action_1; }
	inline Action_t1264377477 ** get_address_of__action_1() { return &____action_1; }
	inline void set__action_1(Action_t1264377477 * value)
	{
		____action_1 = value;
		Il2CppCodeGenWriteBarrier((&____action_1), value);
	}

	inline static int32_t get_offset_of__timer_2() { return static_cast<int32_t>(offsetof(PeriodicTimer_t753797134, ____timer_2)); }
	inline Timer_t716671026 * get__timer_2() const { return ____timer_2; }
	inline Timer_t716671026 ** get_address_of__timer_2() { return &____timer_2; }
	inline void set__timer_2(Timer_t716671026 * value)
	{
		____timer_2 = value;
		Il2CppCodeGenWriteBarrier((&____timer_2), value);
	}

	inline static int32_t get_offset_of__gate_3() { return static_cast<int32_t>(offsetof(PeriodicTimer_t753797134, ____gate_3)); }
	inline AsyncLock_t301382185 * get__gate_3() const { return ____gate_3; }
	inline AsyncLock_t301382185 ** get_address_of__gate_3() { return &____gate_3; }
	inline void set__gate_3(AsyncLock_t301382185 * value)
	{
		____gate_3 = value;
		Il2CppCodeGenWriteBarrier((&____gate_3), value);
	}
};

struct PeriodicTimer_t753797134_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.Threading.Timer> UniRx.Scheduler/ThreadPoolScheduler/PeriodicTimer::s_timers
	HashSet_1_t3576587796 * ___s_timers_0;

public:
	inline static int32_t get_offset_of_s_timers_0() { return static_cast<int32_t>(offsetof(PeriodicTimer_t753797134_StaticFields, ___s_timers_0)); }
	inline HashSet_1_t3576587796 * get_s_timers_0() const { return ___s_timers_0; }
	inline HashSet_1_t3576587796 ** get_address_of_s_timers_0() { return &___s_timers_0; }
	inline void set_s_timers_0(HashSet_1_t3576587796 * value)
	{
		___s_timers_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_timers_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERIODICTIMER_T753797134_H
#ifndef TIMER_T996616921_H
#define TIMER_T996616921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/ThreadPoolScheduler/Timer
struct  Timer_t996616921  : public RuntimeObject
{
public:
	// UniRx.SingleAssignmentDisposable UniRx.Scheduler/ThreadPoolScheduler/Timer::_disposable
	SingleAssignmentDisposable_t4102667663 * ____disposable_1;
	// System.Action UniRx.Scheduler/ThreadPoolScheduler/Timer::_action
	Action_t1264377477 * ____action_2;
	// System.Threading.Timer UniRx.Scheduler/ThreadPoolScheduler/Timer::_timer
	Timer_t716671026 * ____timer_3;
	// System.Boolean UniRx.Scheduler/ThreadPoolScheduler/Timer::_hasAdded
	bool ____hasAdded_4;
	// System.Boolean UniRx.Scheduler/ThreadPoolScheduler/Timer::_hasRemoved
	bool ____hasRemoved_5;

public:
	inline static int32_t get_offset_of__disposable_1() { return static_cast<int32_t>(offsetof(Timer_t996616921, ____disposable_1)); }
	inline SingleAssignmentDisposable_t4102667663 * get__disposable_1() const { return ____disposable_1; }
	inline SingleAssignmentDisposable_t4102667663 ** get_address_of__disposable_1() { return &____disposable_1; }
	inline void set__disposable_1(SingleAssignmentDisposable_t4102667663 * value)
	{
		____disposable_1 = value;
		Il2CppCodeGenWriteBarrier((&____disposable_1), value);
	}

	inline static int32_t get_offset_of__action_2() { return static_cast<int32_t>(offsetof(Timer_t996616921, ____action_2)); }
	inline Action_t1264377477 * get__action_2() const { return ____action_2; }
	inline Action_t1264377477 ** get_address_of__action_2() { return &____action_2; }
	inline void set__action_2(Action_t1264377477 * value)
	{
		____action_2 = value;
		Il2CppCodeGenWriteBarrier((&____action_2), value);
	}

	inline static int32_t get_offset_of__timer_3() { return static_cast<int32_t>(offsetof(Timer_t996616921, ____timer_3)); }
	inline Timer_t716671026 * get__timer_3() const { return ____timer_3; }
	inline Timer_t716671026 ** get_address_of__timer_3() { return &____timer_3; }
	inline void set__timer_3(Timer_t716671026 * value)
	{
		____timer_3 = value;
		Il2CppCodeGenWriteBarrier((&____timer_3), value);
	}

	inline static int32_t get_offset_of__hasAdded_4() { return static_cast<int32_t>(offsetof(Timer_t996616921, ____hasAdded_4)); }
	inline bool get__hasAdded_4() const { return ____hasAdded_4; }
	inline bool* get_address_of__hasAdded_4() { return &____hasAdded_4; }
	inline void set__hasAdded_4(bool value)
	{
		____hasAdded_4 = value;
	}

	inline static int32_t get_offset_of__hasRemoved_5() { return static_cast<int32_t>(offsetof(Timer_t996616921, ____hasRemoved_5)); }
	inline bool get__hasRemoved_5() const { return ____hasRemoved_5; }
	inline bool* get_address_of__hasRemoved_5() { return &____hasRemoved_5; }
	inline void set__hasRemoved_5(bool value)
	{
		____hasRemoved_5 = value;
	}
};

struct Timer_t996616921_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.Threading.Timer> UniRx.Scheduler/ThreadPoolScheduler/Timer::s_timers
	HashSet_1_t3576587796 * ___s_timers_0;

public:
	inline static int32_t get_offset_of_s_timers_0() { return static_cast<int32_t>(offsetof(Timer_t996616921_StaticFields, ___s_timers_0)); }
	inline HashSet_1_t3576587796 * get_s_timers_0() const { return ___s_timers_0; }
	inline HashSet_1_t3576587796 ** get_address_of_s_timers_0() { return &___s_timers_0; }
	inline void set_s_timers_0(HashSet_1_t3576587796 * value)
	{
		___s_timers_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_timers_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMER_T996616921_H
#ifndef THREADPOOLSCHEDULER_T964667149_H
#define THREADPOOLSCHEDULER_T964667149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/ThreadPoolScheduler
struct  ThreadPoolScheduler_t964667149  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADPOOLSCHEDULER_T964667149_H
#ifndef DEFAULTSCHEDULERS_T4171041390_H
#define DEFAULTSCHEDULERS_T4171041390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/DefaultSchedulers
struct  DefaultSchedulers_t4171041390  : public RuntimeObject
{
public:

public:
};

struct DefaultSchedulers_t4171041390_StaticFields
{
public:
	// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::constantTime
	RuntimeObject* ___constantTime_0;
	// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::tailRecursion
	RuntimeObject* ___tailRecursion_1;
	// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::iteration
	RuntimeObject* ___iteration_2;
	// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::timeBasedOperations
	RuntimeObject* ___timeBasedOperations_3;
	// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::asyncConversions
	RuntimeObject* ___asyncConversions_4;

public:
	inline static int32_t get_offset_of_constantTime_0() { return static_cast<int32_t>(offsetof(DefaultSchedulers_t4171041390_StaticFields, ___constantTime_0)); }
	inline RuntimeObject* get_constantTime_0() const { return ___constantTime_0; }
	inline RuntimeObject** get_address_of_constantTime_0() { return &___constantTime_0; }
	inline void set_constantTime_0(RuntimeObject* value)
	{
		___constantTime_0 = value;
		Il2CppCodeGenWriteBarrier((&___constantTime_0), value);
	}

	inline static int32_t get_offset_of_tailRecursion_1() { return static_cast<int32_t>(offsetof(DefaultSchedulers_t4171041390_StaticFields, ___tailRecursion_1)); }
	inline RuntimeObject* get_tailRecursion_1() const { return ___tailRecursion_1; }
	inline RuntimeObject** get_address_of_tailRecursion_1() { return &___tailRecursion_1; }
	inline void set_tailRecursion_1(RuntimeObject* value)
	{
		___tailRecursion_1 = value;
		Il2CppCodeGenWriteBarrier((&___tailRecursion_1), value);
	}

	inline static int32_t get_offset_of_iteration_2() { return static_cast<int32_t>(offsetof(DefaultSchedulers_t4171041390_StaticFields, ___iteration_2)); }
	inline RuntimeObject* get_iteration_2() const { return ___iteration_2; }
	inline RuntimeObject** get_address_of_iteration_2() { return &___iteration_2; }
	inline void set_iteration_2(RuntimeObject* value)
	{
		___iteration_2 = value;
		Il2CppCodeGenWriteBarrier((&___iteration_2), value);
	}

	inline static int32_t get_offset_of_timeBasedOperations_3() { return static_cast<int32_t>(offsetof(DefaultSchedulers_t4171041390_StaticFields, ___timeBasedOperations_3)); }
	inline RuntimeObject* get_timeBasedOperations_3() const { return ___timeBasedOperations_3; }
	inline RuntimeObject** get_address_of_timeBasedOperations_3() { return &___timeBasedOperations_3; }
	inline void set_timeBasedOperations_3(RuntimeObject* value)
	{
		___timeBasedOperations_3 = value;
		Il2CppCodeGenWriteBarrier((&___timeBasedOperations_3), value);
	}

	inline static int32_t get_offset_of_asyncConversions_4() { return static_cast<int32_t>(offsetof(DefaultSchedulers_t4171041390_StaticFields, ___asyncConversions_4)); }
	inline RuntimeObject* get_asyncConversions_4() const { return ___asyncConversions_4; }
	inline RuntimeObject** get_address_of_asyncConversions_4() { return &___asyncConversions_4; }
	inline void set_asyncConversions_4(RuntimeObject* value)
	{
		___asyncConversions_4 = value;
		Il2CppCodeGenWriteBarrier((&___asyncConversions_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSCHEDULERS_T4171041390_H
#ifndef IMMEDIATESCHEDULER_T2366437439_H
#define IMMEDIATESCHEDULER_T2366437439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/ImmediateScheduler
struct  ImmediateScheduler_t2366437439  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMMEDIATESCHEDULER_T2366437439_H
#ifndef TRAMPOLINE_T854539875_H
#define TRAMPOLINE_T854539875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/CurrentThreadScheduler/Trampoline
struct  Trampoline_t854539875  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAMPOLINE_T854539875_H
#ifndef FIXEDUPDATEMAINTHREADSCHEDULER_T1759960949_H
#define FIXEDUPDATEMAINTHREADSCHEDULER_T1759960949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/FixedUpdateMainThreadScheduler
struct  FixedUpdateMainThreadScheduler_t1759960949  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDUPDATEMAINTHREADSCHEDULER_T1759960949_H
#ifndef ENDOFFRAMEMAINTHREADSCHEDULER_T3512639697_H
#define ENDOFFRAMEMAINTHREADSCHEDULER_T3512639697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/EndOfFrameMainThreadScheduler
struct  EndOfFrameMainThreadScheduler_t3512639697  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDOFFRAMEMAINTHREADSCHEDULER_T3512639697_H
#ifndef U3CSCHEDULEU3EC__ANONSTOREY0_T1246538827_H
#define U3CSCHEDULEU3EC__ANONSTOREY0_T1246538827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/<Schedule>c__AnonStorey0
struct  U3CScheduleU3Ec__AnonStorey0_t1246538827  : public RuntimeObject
{
public:
	// System.Action`1<System.Action> UniRx.Scheduler/<Schedule>c__AnonStorey0::action
	Action_1_t1436845072 * ___action_0;
	// UniRx.IScheduler UniRx.Scheduler/<Schedule>c__AnonStorey0::scheduler
	RuntimeObject* ___scheduler_1;
	// System.Object UniRx.Scheduler/<Schedule>c__AnonStorey0::gate
	RuntimeObject * ___gate_2;
	// UniRx.CompositeDisposable UniRx.Scheduler/<Schedule>c__AnonStorey0::group
	CompositeDisposable_t3924054141 * ___group_3;
	// System.Action UniRx.Scheduler/<Schedule>c__AnonStorey0::recursiveAction
	Action_t1264377477 * ___recursiveAction_4;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey0_t1246538827, ___action_0)); }
	inline Action_1_t1436845072 * get_action_0() const { return ___action_0; }
	inline Action_1_t1436845072 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t1436845072 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_scheduler_1() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey0_t1246538827, ___scheduler_1)); }
	inline RuntimeObject* get_scheduler_1() const { return ___scheduler_1; }
	inline RuntimeObject** get_address_of_scheduler_1() { return &___scheduler_1; }
	inline void set_scheduler_1(RuntimeObject* value)
	{
		___scheduler_1 = value;
		Il2CppCodeGenWriteBarrier((&___scheduler_1), value);
	}

	inline static int32_t get_offset_of_gate_2() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey0_t1246538827, ___gate_2)); }
	inline RuntimeObject * get_gate_2() const { return ___gate_2; }
	inline RuntimeObject ** get_address_of_gate_2() { return &___gate_2; }
	inline void set_gate_2(RuntimeObject * value)
	{
		___gate_2 = value;
		Il2CppCodeGenWriteBarrier((&___gate_2), value);
	}

	inline static int32_t get_offset_of_group_3() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey0_t1246538827, ___group_3)); }
	inline CompositeDisposable_t3924054141 * get_group_3() const { return ___group_3; }
	inline CompositeDisposable_t3924054141 ** get_address_of_group_3() { return &___group_3; }
	inline void set_group_3(CompositeDisposable_t3924054141 * value)
	{
		___group_3 = value;
		Il2CppCodeGenWriteBarrier((&___group_3), value);
	}

	inline static int32_t get_offset_of_recursiveAction_4() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey0_t1246538827, ___recursiveAction_4)); }
	inline Action_t1264377477 * get_recursiveAction_4() const { return ___recursiveAction_4; }
	inline Action_t1264377477 ** get_address_of_recursiveAction_4() { return &___recursiveAction_4; }
	inline void set_recursiveAction_4(Action_t1264377477 * value)
	{
		___recursiveAction_4 = value;
		Il2CppCodeGenWriteBarrier((&___recursiveAction_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCHEDULEU3EC__ANONSTOREY0_T1246538827_H
#ifndef U3CSCHEDULEU3EC__ANONSTOREY1_T2063910649_H
#define U3CSCHEDULEU3EC__ANONSTOREY1_T2063910649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/<Schedule>c__AnonStorey0/<Schedule>c__AnonStorey1
struct  U3CScheduleU3Ec__AnonStorey1_t2063910649  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Scheduler/<Schedule>c__AnonStorey0/<Schedule>c__AnonStorey1::isAdded
	bool ___isAdded_0;
	// System.IDisposable UniRx.Scheduler/<Schedule>c__AnonStorey0/<Schedule>c__AnonStorey1::d
	RuntimeObject* ___d_1;
	// System.Boolean UniRx.Scheduler/<Schedule>c__AnonStorey0/<Schedule>c__AnonStorey1::isDone
	bool ___isDone_2;
	// UniRx.Scheduler/<Schedule>c__AnonStorey0 UniRx.Scheduler/<Schedule>c__AnonStorey0/<Schedule>c__AnonStorey1::<>f__ref$0
	U3CScheduleU3Ec__AnonStorey0_t1246538827 * ___U3CU3Ef__refU240_3;

public:
	inline static int32_t get_offset_of_isAdded_0() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey1_t2063910649, ___isAdded_0)); }
	inline bool get_isAdded_0() const { return ___isAdded_0; }
	inline bool* get_address_of_isAdded_0() { return &___isAdded_0; }
	inline void set_isAdded_0(bool value)
	{
		___isAdded_0 = value;
	}

	inline static int32_t get_offset_of_d_1() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey1_t2063910649, ___d_1)); }
	inline RuntimeObject* get_d_1() const { return ___d_1; }
	inline RuntimeObject** get_address_of_d_1() { return &___d_1; }
	inline void set_d_1(RuntimeObject* value)
	{
		___d_1 = value;
		Il2CppCodeGenWriteBarrier((&___d_1), value);
	}

	inline static int32_t get_offset_of_isDone_2() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey1_t2063910649, ___isDone_2)); }
	inline bool get_isDone_2() const { return ___isDone_2; }
	inline bool* get_address_of_isDone_2() { return &___isDone_2; }
	inline void set_isDone_2(bool value)
	{
		___isDone_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_3() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey1_t2063910649, ___U3CU3Ef__refU240_3)); }
	inline U3CScheduleU3Ec__AnonStorey0_t1246538827 * get_U3CU3Ef__refU240_3() const { return ___U3CU3Ef__refU240_3; }
	inline U3CScheduleU3Ec__AnonStorey0_t1246538827 ** get_address_of_U3CU3Ef__refU240_3() { return &___U3CU3Ef__refU240_3; }
	inline void set_U3CU3Ef__refU240_3(U3CScheduleU3Ec__AnonStorey0_t1246538827 * value)
	{
		___U3CU3Ef__refU240_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCHEDULEU3EC__ANONSTOREY1_T2063910649_H
#ifndef U3CSCHEDULEU3EC__ANONSTOREY2_T1628875851_H
#define U3CSCHEDULEU3EC__ANONSTOREY2_T1628875851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/<Schedule>c__AnonStorey2
struct  U3CScheduleU3Ec__AnonStorey2_t1628875851  : public RuntimeObject
{
public:
	// System.Action`1<System.Action`1<System.TimeSpan>> UniRx.Scheduler/<Schedule>c__AnonStorey2::action
	Action_1_t1226094439 * ___action_0;
	// UniRx.IScheduler UniRx.Scheduler/<Schedule>c__AnonStorey2::scheduler
	RuntimeObject* ___scheduler_1;
	// System.Object UniRx.Scheduler/<Schedule>c__AnonStorey2::gate
	RuntimeObject * ___gate_2;
	// UniRx.CompositeDisposable UniRx.Scheduler/<Schedule>c__AnonStorey2::group
	CompositeDisposable_t3924054141 * ___group_3;
	// System.Action UniRx.Scheduler/<Schedule>c__AnonStorey2::recursiveAction
	Action_t1264377477 * ___recursiveAction_4;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey2_t1628875851, ___action_0)); }
	inline Action_1_t1226094439 * get_action_0() const { return ___action_0; }
	inline Action_1_t1226094439 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t1226094439 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_scheduler_1() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey2_t1628875851, ___scheduler_1)); }
	inline RuntimeObject* get_scheduler_1() const { return ___scheduler_1; }
	inline RuntimeObject** get_address_of_scheduler_1() { return &___scheduler_1; }
	inline void set_scheduler_1(RuntimeObject* value)
	{
		___scheduler_1 = value;
		Il2CppCodeGenWriteBarrier((&___scheduler_1), value);
	}

	inline static int32_t get_offset_of_gate_2() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey2_t1628875851, ___gate_2)); }
	inline RuntimeObject * get_gate_2() const { return ___gate_2; }
	inline RuntimeObject ** get_address_of_gate_2() { return &___gate_2; }
	inline void set_gate_2(RuntimeObject * value)
	{
		___gate_2 = value;
		Il2CppCodeGenWriteBarrier((&___gate_2), value);
	}

	inline static int32_t get_offset_of_group_3() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey2_t1628875851, ___group_3)); }
	inline CompositeDisposable_t3924054141 * get_group_3() const { return ___group_3; }
	inline CompositeDisposable_t3924054141 ** get_address_of_group_3() { return &___group_3; }
	inline void set_group_3(CompositeDisposable_t3924054141 * value)
	{
		___group_3 = value;
		Il2CppCodeGenWriteBarrier((&___group_3), value);
	}

	inline static int32_t get_offset_of_recursiveAction_4() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey2_t1628875851, ___recursiveAction_4)); }
	inline Action_t1264377477 * get_recursiveAction_4() const { return ___recursiveAction_4; }
	inline Action_t1264377477 ** get_address_of_recursiveAction_4() { return &___recursiveAction_4; }
	inline void set_recursiveAction_4(Action_t1264377477 * value)
	{
		___recursiveAction_4 = value;
		Il2CppCodeGenWriteBarrier((&___recursiveAction_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCHEDULEU3EC__ANONSTOREY2_T1628875851_H
#ifndef U3CSCHEDULEU3EC__ANONSTOREY3_T342939487_H
#define U3CSCHEDULEU3EC__ANONSTOREY3_T342939487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/<Schedule>c__AnonStorey2/<Schedule>c__AnonStorey3
struct  U3CScheduleU3Ec__AnonStorey3_t342939487  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Scheduler/<Schedule>c__AnonStorey2/<Schedule>c__AnonStorey3::isAdded
	bool ___isAdded_0;
	// System.IDisposable UniRx.Scheduler/<Schedule>c__AnonStorey2/<Schedule>c__AnonStorey3::d
	RuntimeObject* ___d_1;
	// System.Boolean UniRx.Scheduler/<Schedule>c__AnonStorey2/<Schedule>c__AnonStorey3::isDone
	bool ___isDone_2;
	// UniRx.Scheduler/<Schedule>c__AnonStorey2 UniRx.Scheduler/<Schedule>c__AnonStorey2/<Schedule>c__AnonStorey3::<>f__ref$2
	U3CScheduleU3Ec__AnonStorey2_t1628875851 * ___U3CU3Ef__refU242_3;

public:
	inline static int32_t get_offset_of_isAdded_0() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey3_t342939487, ___isAdded_0)); }
	inline bool get_isAdded_0() const { return ___isAdded_0; }
	inline bool* get_address_of_isAdded_0() { return &___isAdded_0; }
	inline void set_isAdded_0(bool value)
	{
		___isAdded_0 = value;
	}

	inline static int32_t get_offset_of_d_1() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey3_t342939487, ___d_1)); }
	inline RuntimeObject* get_d_1() const { return ___d_1; }
	inline RuntimeObject** get_address_of_d_1() { return &___d_1; }
	inline void set_d_1(RuntimeObject* value)
	{
		___d_1 = value;
		Il2CppCodeGenWriteBarrier((&___d_1), value);
	}

	inline static int32_t get_offset_of_isDone_2() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey3_t342939487, ___isDone_2)); }
	inline bool get_isDone_2() const { return ___isDone_2; }
	inline bool* get_address_of_isDone_2() { return &___isDone_2; }
	inline void set_isDone_2(bool value)
	{
		___isDone_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU242_3() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey3_t342939487, ___U3CU3Ef__refU242_3)); }
	inline U3CScheduleU3Ec__AnonStorey2_t1628875851 * get_U3CU3Ef__refU242_3() const { return ___U3CU3Ef__refU242_3; }
	inline U3CScheduleU3Ec__AnonStorey2_t1628875851 ** get_address_of_U3CU3Ef__refU242_3() { return &___U3CU3Ef__refU242_3; }
	inline void set_U3CU3Ef__refU242_3(U3CScheduleU3Ec__AnonStorey2_t1628875851 * value)
	{
		___U3CU3Ef__refU242_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU242_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCHEDULEU3EC__ANONSTOREY3_T342939487_H
#ifndef REACTIVEPROPERTY_1_T2281479260_H
#define REACTIVEPROPERTY_1_T2281479260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<UnityEngine.AnimationCurve>
struct  ReactiveProperty_1_t2281479260  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	AnimationCurve_t3046754366 * ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t3135410753 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2281479260, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2281479260, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2281479260, ___value_3)); }
	inline AnimationCurve_t3046754366 * get_value_3() const { return ___value_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(AnimationCurve_t3046754366 * value)
	{
		___value_3 = value;
		Il2CppCodeGenWriteBarrier((&___value_3), value);
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2281479260, ___publisher_4)); }
	inline Subject_1_t3135410753 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t3135410753 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t3135410753 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2281479260, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2281479260, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t2281479260_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2281479260_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T2281479260_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef ANIMATIONCURVEREACTIVEPROPERTY_T44630468_H
#define ANIMATIONCURVEREACTIVEPROPERTY_T44630468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.AnimationCurveReactiveProperty
struct  AnimationCurveReactiveProperty_t44630468  : public ReactiveProperty_1_t2281479260
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCURVEREACTIVEPROPERTY_T44630468_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef CANCELLATIONTOKEN_T1265546479_H
#define CANCELLATIONTOKEN_T1265546479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CancellationToken
struct  CancellationToken_t1265546479 
{
public:
	// UniRx.ICancelable UniRx.CancellationToken::source
	RuntimeObject* ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(CancellationToken_t1265546479, ___source_0)); }
	inline RuntimeObject* get_source_0() const { return ___source_0; }
	inline RuntimeObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(RuntimeObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}
};

struct CancellationToken_t1265546479_StaticFields
{
public:
	// UniRx.CancellationToken UniRx.CancellationToken::Empty
	CancellationToken_t1265546479  ___Empty_1;
	// UniRx.CancellationToken UniRx.CancellationToken::None
	CancellationToken_t1265546479  ___None_2;

public:
	inline static int32_t get_offset_of_Empty_1() { return static_cast<int32_t>(offsetof(CancellationToken_t1265546479_StaticFields, ___Empty_1)); }
	inline CancellationToken_t1265546479  get_Empty_1() const { return ___Empty_1; }
	inline CancellationToken_t1265546479 * get_address_of_Empty_1() { return &___Empty_1; }
	inline void set_Empty_1(CancellationToken_t1265546479  value)
	{
		___Empty_1 = value;
	}

	inline static int32_t get_offset_of_None_2() { return static_cast<int32_t>(offsetof(CancellationToken_t1265546479_StaticFields, ___None_2)); }
	inline CancellationToken_t1265546479  get_None_2() const { return ___None_2; }
	inline CancellationToken_t1265546479 * get_address_of_None_2() { return &___None_2; }
	inline void set_None_2(CancellationToken_t1265546479  value)
	{
		___None_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UniRx.CancellationToken
struct CancellationToken_t1265546479_marshaled_pinvoke
{
	RuntimeObject* ___source_0;
};
// Native definition for COM marshalling of UniRx.CancellationToken
struct CancellationToken_t1265546479_marshaled_com
{
	RuntimeObject* ___source_0;
};
#endif // CANCELLATIONTOKEN_T1265546479_H
#ifndef BYTEREACTIVEPROPERTY_T1600986847_H
#define BYTEREACTIVEPROPERTY_T1600986847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ByteReactiveProperty
struct  ByteReactiveProperty_t1600986847  : public ReactiveProperty_1_t369021270
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEREACTIVEPROPERTY_T1600986847_H
#ifndef LONGREACTIVEPROPERTY_T2823960147_H
#define LONGREACTIVEPROPERTY_T2823960147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.LongReactiveProperty
struct  LongReactiveProperty_t2823960147  : public ReactiveProperty_1_t2971292198
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGREACTIVEPROPERTY_T2823960147_H
#ifndef DOUBLEREACTIVEPROPERTY_T3731678748_H
#define DOUBLEREACTIVEPROPERTY_T3731678748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.DoubleReactiveProperty
struct  DoubleReactiveProperty_t3731678748  : public ReactiveProperty_1_t4124357553
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEREACTIVEPROPERTY_T3731678748_H
#ifndef STRINGREACTIVEPROPERTY_T3302058057_H
#define STRINGREACTIVEPROPERTY_T3302058057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.StringReactiveProperty
struct  StringReactiveProperty_t3302058057  : public ReactiveProperty_1_t1082175583
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGREACTIVEPROPERTY_T3302058057_H
#ifndef BOOLREACTIVEPROPERTY_T700220569_H
#define BOOLREACTIVEPROPERTY_T700220569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.BoolReactiveProperty
struct  BoolReactiveProperty_t700220569  : public ReactiveProperty_1_t3626980155
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLREACTIVEPROPERTY_T700220569_H
#ifndef INTREACTIVEPROPERTY_T3243525985_H
#define INTREACTIVEPROPERTY_T3243525985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.IntReactiveProperty
struct  IntReactiveProperty_t3243525985  : public ReactiveProperty_1_t2185670647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTREACTIVEPROPERTY_T3243525985_H
#ifndef UNIT_T3362249467_H
#define UNIT_T3362249467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Unit
struct  Unit_t3362249467 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Unit_t3362249467__padding[1];
	};

public:
};

struct Unit_t3362249467_StaticFields
{
public:
	// UniRx.Unit UniRx.Unit::default
	Unit_t3362249467  ___default_0;

public:
	inline static int32_t get_offset_of_default_0() { return static_cast<int32_t>(offsetof(Unit_t3362249467_StaticFields, ___default_0)); }
	inline Unit_t3362249467  get_default_0() const { return ___default_0; }
	inline Unit_t3362249467 * get_address_of_default_0() { return &___default_0; }
	inline void set_default_0(Unit_t3362249467  value)
	{
		___default_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIT_T3362249467_H
#ifndef FLOATREACTIVEPROPERTY_T2159343209_H
#define FLOATREACTIVEPROPERTY_T2159343209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.FloatReactiveProperty
struct  FloatReactiveProperty_t2159343209  : public ReactiveProperty_1_t631991668
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATREACTIVEPROPERTY_T2159343209_H
#ifndef U3CPERIODICACTIONU3EC__ITERATOR2_T2292180132_H
#define U3CPERIODICACTIONU3EC__ITERATOR2_T2292180132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/EndOfFrameMainThreadScheduler/<PeriodicAction>c__Iterator2
struct  U3CPeriodicActionU3Ec__Iterator2_t2292180132  : public RuntimeObject
{
public:
	// System.TimeSpan UniRx.Scheduler/EndOfFrameMainThreadScheduler/<PeriodicAction>c__Iterator2::period
	TimeSpan_t881159249  ___period_0;
	// UniRx.ICancelable UniRx.Scheduler/EndOfFrameMainThreadScheduler/<PeriodicAction>c__Iterator2::cancellation
	RuntimeObject* ___cancellation_1;
	// System.Action UniRx.Scheduler/EndOfFrameMainThreadScheduler/<PeriodicAction>c__Iterator2::action
	Action_t1264377477 * ___action_2;
	// System.Single UniRx.Scheduler/EndOfFrameMainThreadScheduler/<PeriodicAction>c__Iterator2::<elapsed>__1
	float ___U3CelapsedU3E__1_3;
	// System.Single UniRx.Scheduler/EndOfFrameMainThreadScheduler/<PeriodicAction>c__Iterator2::<dt>__1
	float ___U3CdtU3E__1_4;
	// System.Object UniRx.Scheduler/EndOfFrameMainThreadScheduler/<PeriodicAction>c__Iterator2::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean UniRx.Scheduler/EndOfFrameMainThreadScheduler/<PeriodicAction>c__Iterator2::$disposing
	bool ___U24disposing_6;
	// System.Int32 UniRx.Scheduler/EndOfFrameMainThreadScheduler/<PeriodicAction>c__Iterator2::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_period_0() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2292180132, ___period_0)); }
	inline TimeSpan_t881159249  get_period_0() const { return ___period_0; }
	inline TimeSpan_t881159249 * get_address_of_period_0() { return &___period_0; }
	inline void set_period_0(TimeSpan_t881159249  value)
	{
		___period_0 = value;
	}

	inline static int32_t get_offset_of_cancellation_1() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2292180132, ___cancellation_1)); }
	inline RuntimeObject* get_cancellation_1() const { return ___cancellation_1; }
	inline RuntimeObject** get_address_of_cancellation_1() { return &___cancellation_1; }
	inline void set_cancellation_1(RuntimeObject* value)
	{
		___cancellation_1 = value;
		Il2CppCodeGenWriteBarrier((&___cancellation_1), value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2292180132, ___action_2)); }
	inline Action_t1264377477 * get_action_2() const { return ___action_2; }
	inline Action_t1264377477 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t1264377477 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier((&___action_2), value);
	}

	inline static int32_t get_offset_of_U3CelapsedU3E__1_3() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2292180132, ___U3CelapsedU3E__1_3)); }
	inline float get_U3CelapsedU3E__1_3() const { return ___U3CelapsedU3E__1_3; }
	inline float* get_address_of_U3CelapsedU3E__1_3() { return &___U3CelapsedU3E__1_3; }
	inline void set_U3CelapsedU3E__1_3(float value)
	{
		___U3CelapsedU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CdtU3E__1_4() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2292180132, ___U3CdtU3E__1_4)); }
	inline float get_U3CdtU3E__1_4() const { return ___U3CdtU3E__1_4; }
	inline float* get_address_of_U3CdtU3E__1_4() { return &___U3CdtU3E__1_4; }
	inline void set_U3CdtU3E__1_4(float value)
	{
		___U3CdtU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2292180132, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2292180132, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2292180132, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPERIODICACTIONU3EC__ITERATOR2_T2292180132_H
#ifndef REACTIVEPROPERTY_1_T1595204753_H
#define REACTIVEPROPERTY_1_T1595204753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<UnityEngine.Rect>
struct  ReactiveProperty_1_t1595204753  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	Rect_t2360479859  ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t2449136246 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1595204753, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1595204753, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1595204753, ___value_3)); }
	inline Rect_t2360479859  get_value_3() const { return ___value_3; }
	inline Rect_t2360479859 * get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(Rect_t2360479859  value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1595204753, ___publisher_4)); }
	inline Subject_1_t2449136246 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t2449136246 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t2449136246 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1595204753, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1595204753, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t1595204753_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1595204753_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T1595204753_H
#ifndef REACTIVEPROPERTY_1_T1790411218_H
#define REACTIVEPROPERTY_1_T1790411218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<UnityEngine.Color>
struct  ReactiveProperty_1_t1790411218  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	Color_t2555686324  ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t2644342711 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1790411218, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1790411218, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1790411218, ___value_3)); }
	inline Color_t2555686324  get_value_3() const { return ___value_3; }
	inline Color_t2555686324 * get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(Color_t2555686324  value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1790411218, ___publisher_4)); }
	inline Subject_1_t2644342711 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t2644342711 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t2644342711 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1790411218, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1790411218, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t1790411218_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1790411218_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T1790411218_H
#ifndef U3CDELAYACTIONU3EC__ITERATOR0_T3203061179_H
#define U3CDELAYACTIONU3EC__ITERATOR0_T3203061179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator0
struct  U3CDelayActionU3Ec__Iterator0_t3203061179  : public RuntimeObject
{
public:
	// System.TimeSpan UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator0::dueTime
	TimeSpan_t881159249  ___dueTime_0;
	// UniRx.ICancelable UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator0::cancellation
	RuntimeObject* ___cancellation_1;
	// System.Action UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator0::action
	Action_t1264377477 * ___action_2;
	// System.Object UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 UniRx.Scheduler/MainThreadScheduler/<DelayAction>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_dueTime_0() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator0_t3203061179, ___dueTime_0)); }
	inline TimeSpan_t881159249  get_dueTime_0() const { return ___dueTime_0; }
	inline TimeSpan_t881159249 * get_address_of_dueTime_0() { return &___dueTime_0; }
	inline void set_dueTime_0(TimeSpan_t881159249  value)
	{
		___dueTime_0 = value;
	}

	inline static int32_t get_offset_of_cancellation_1() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator0_t3203061179, ___cancellation_1)); }
	inline RuntimeObject* get_cancellation_1() const { return ___cancellation_1; }
	inline RuntimeObject** get_address_of_cancellation_1() { return &___cancellation_1; }
	inline void set_cancellation_1(RuntimeObject* value)
	{
		___cancellation_1 = value;
		Il2CppCodeGenWriteBarrier((&___cancellation_1), value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator0_t3203061179, ___action_2)); }
	inline Action_t1264377477 * get_action_2() const { return ___action_2; }
	inline Action_t1264377477 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t1264377477 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier((&___action_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator0_t3203061179, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator0_t3203061179, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator0_t3203061179, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYACTIONU3EC__ITERATOR0_T3203061179_H
#ifndef U3CPERIODICACTIONU3EC__ITERATOR1_T1273408431_H
#define U3CPERIODICACTIONU3EC__ITERATOR1_T1273408431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1
struct  U3CPeriodicActionU3Ec__Iterator1_t1273408431  : public RuntimeObject
{
public:
	// System.TimeSpan UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1::period
	TimeSpan_t881159249  ___period_0;
	// UniRx.ICancelable UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1::cancellation
	RuntimeObject* ___cancellation_1;
	// System.Action UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1::action
	Action_t1264377477 * ___action_2;
	// System.Single UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1::<seconds>__1
	float ___U3CsecondsU3E__1_3;
	// UnityEngine.WaitForSeconds UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1::<yieldInstruction>__1
	WaitForSeconds_t1699091251 * ___U3CyieldInstructionU3E__1_4;
	// System.Object UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_period_0() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t1273408431, ___period_0)); }
	inline TimeSpan_t881159249  get_period_0() const { return ___period_0; }
	inline TimeSpan_t881159249 * get_address_of_period_0() { return &___period_0; }
	inline void set_period_0(TimeSpan_t881159249  value)
	{
		___period_0 = value;
	}

	inline static int32_t get_offset_of_cancellation_1() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t1273408431, ___cancellation_1)); }
	inline RuntimeObject* get_cancellation_1() const { return ___cancellation_1; }
	inline RuntimeObject** get_address_of_cancellation_1() { return &___cancellation_1; }
	inline void set_cancellation_1(RuntimeObject* value)
	{
		___cancellation_1 = value;
		Il2CppCodeGenWriteBarrier((&___cancellation_1), value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t1273408431, ___action_2)); }
	inline Action_t1264377477 * get_action_2() const { return ___action_2; }
	inline Action_t1264377477 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t1264377477 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier((&___action_2), value);
	}

	inline static int32_t get_offset_of_U3CsecondsU3E__1_3() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t1273408431, ___U3CsecondsU3E__1_3)); }
	inline float get_U3CsecondsU3E__1_3() const { return ___U3CsecondsU3E__1_3; }
	inline float* get_address_of_U3CsecondsU3E__1_3() { return &___U3CsecondsU3E__1_3; }
	inline void set_U3CsecondsU3E__1_3(float value)
	{
		___U3CsecondsU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CyieldInstructionU3E__1_4() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t1273408431, ___U3CyieldInstructionU3E__1_4)); }
	inline WaitForSeconds_t1699091251 * get_U3CyieldInstructionU3E__1_4() const { return ___U3CyieldInstructionU3E__1_4; }
	inline WaitForSeconds_t1699091251 ** get_address_of_U3CyieldInstructionU3E__1_4() { return &___U3CyieldInstructionU3E__1_4; }
	inline void set_U3CyieldInstructionU3E__1_4(WaitForSeconds_t1699091251 * value)
	{
		___U3CyieldInstructionU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CyieldInstructionU3E__1_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t1273408431, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t1273408431, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t1273408431, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPERIODICACTIONU3EC__ITERATOR1_T1273408431_H
#ifndef U3CDELAYACTIONU3EC__ITERATOR0_T749594046_H
#define U3CDELAYACTIONU3EC__ITERATOR0_T749594046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator0
struct  U3CDelayActionU3Ec__Iterator0_t749594046  : public RuntimeObject
{
public:
	// System.TimeSpan UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator0::dueTime
	TimeSpan_t881159249  ___dueTime_0;
	// UniRx.ICancelable UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator0::cancellation
	RuntimeObject* ___cancellation_1;
	// System.Action UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator0::action
	Action_t1264377477 * ___action_2;
	// System.Single UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator0::<elapsed>__1
	float ___U3CelapsedU3E__1_3;
	// System.Single UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator0::<dt>__1
	float ___U3CdtU3E__1_4;
	// System.Object UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<DelayAction>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_dueTime_0() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator0_t749594046, ___dueTime_0)); }
	inline TimeSpan_t881159249  get_dueTime_0() const { return ___dueTime_0; }
	inline TimeSpan_t881159249 * get_address_of_dueTime_0() { return &___dueTime_0; }
	inline void set_dueTime_0(TimeSpan_t881159249  value)
	{
		___dueTime_0 = value;
	}

	inline static int32_t get_offset_of_cancellation_1() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator0_t749594046, ___cancellation_1)); }
	inline RuntimeObject* get_cancellation_1() const { return ___cancellation_1; }
	inline RuntimeObject** get_address_of_cancellation_1() { return &___cancellation_1; }
	inline void set_cancellation_1(RuntimeObject* value)
	{
		___cancellation_1 = value;
		Il2CppCodeGenWriteBarrier((&___cancellation_1), value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator0_t749594046, ___action_2)); }
	inline Action_t1264377477 * get_action_2() const { return ___action_2; }
	inline Action_t1264377477 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t1264377477 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier((&___action_2), value);
	}

	inline static int32_t get_offset_of_U3CelapsedU3E__1_3() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator0_t749594046, ___U3CelapsedU3E__1_3)); }
	inline float get_U3CelapsedU3E__1_3() const { return ___U3CelapsedU3E__1_3; }
	inline float* get_address_of_U3CelapsedU3E__1_3() { return &___U3CelapsedU3E__1_3; }
	inline void set_U3CelapsedU3E__1_3(float value)
	{
		___U3CelapsedU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CdtU3E__1_4() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator0_t749594046, ___U3CdtU3E__1_4)); }
	inline float get_U3CdtU3E__1_4() const { return ___U3CdtU3E__1_4; }
	inline float* get_address_of_U3CdtU3E__1_4() { return &___U3CdtU3E__1_4; }
	inline void set_U3CdtU3E__1_4(float value)
	{
		___U3CdtU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator0_t749594046, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator0_t749594046, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator0_t749594046, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYACTIONU3EC__ITERATOR0_T749594046_H
#ifndef U3CPERIODICACTIONU3EC__ITERATOR1_T2287198350_H
#define U3CPERIODICACTIONU3EC__ITERATOR1_T2287198350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1
struct  U3CPeriodicActionU3Ec__Iterator1_t2287198350  : public RuntimeObject
{
public:
	// System.TimeSpan UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1::period
	TimeSpan_t881159249  ___period_0;
	// UniRx.ICancelable UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1::cancellation
	RuntimeObject* ___cancellation_1;
	// System.Action UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1::action
	Action_t1264377477 * ___action_2;
	// System.Single UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1::<elapsed>__1
	float ___U3CelapsedU3E__1_3;
	// System.Single UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1::<dt>__1
	float ___U3CdtU3E__1_4;
	// System.Object UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_period_0() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t2287198350, ___period_0)); }
	inline TimeSpan_t881159249  get_period_0() const { return ___period_0; }
	inline TimeSpan_t881159249 * get_address_of_period_0() { return &___period_0; }
	inline void set_period_0(TimeSpan_t881159249  value)
	{
		___period_0 = value;
	}

	inline static int32_t get_offset_of_cancellation_1() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t2287198350, ___cancellation_1)); }
	inline RuntimeObject* get_cancellation_1() const { return ___cancellation_1; }
	inline RuntimeObject** get_address_of_cancellation_1() { return &___cancellation_1; }
	inline void set_cancellation_1(RuntimeObject* value)
	{
		___cancellation_1 = value;
		Il2CppCodeGenWriteBarrier((&___cancellation_1), value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t2287198350, ___action_2)); }
	inline Action_t1264377477 * get_action_2() const { return ___action_2; }
	inline Action_t1264377477 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t1264377477 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier((&___action_2), value);
	}

	inline static int32_t get_offset_of_U3CelapsedU3E__1_3() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t2287198350, ___U3CelapsedU3E__1_3)); }
	inline float get_U3CelapsedU3E__1_3() const { return ___U3CelapsedU3E__1_3; }
	inline float* get_address_of_U3CelapsedU3E__1_3() { return &___U3CelapsedU3E__1_3; }
	inline void set_U3CelapsedU3E__1_3(float value)
	{
		___U3CelapsedU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CdtU3E__1_4() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t2287198350, ___U3CdtU3E__1_4)); }
	inline float get_U3CdtU3E__1_4() const { return ___U3CdtU3E__1_4; }
	inline float* get_address_of_U3CdtU3E__1_4() { return &___U3CdtU3E__1_4; }
	inline void set_U3CdtU3E__1_4(float value)
	{
		___U3CdtU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t2287198350, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t2287198350, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator1_t2287198350, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPERIODICACTIONU3EC__ITERATOR1_T2287198350_H
#ifndef U3CDELAYACTIONU3EC__ITERATOR1_T3218606553_H
#define U3CDELAYACTIONU3EC__ITERATOR1_T3218606553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/FixedUpdateMainThreadScheduler/<DelayAction>c__Iterator1
struct  U3CDelayActionU3Ec__Iterator1_t3218606553  : public RuntimeObject
{
public:
	// System.TimeSpan UniRx.Scheduler/FixedUpdateMainThreadScheduler/<DelayAction>c__Iterator1::dueTime
	TimeSpan_t881159249  ___dueTime_0;
	// UniRx.ICancelable UniRx.Scheduler/FixedUpdateMainThreadScheduler/<DelayAction>c__Iterator1::cancellation
	RuntimeObject* ___cancellation_1;
	// System.Action UniRx.Scheduler/FixedUpdateMainThreadScheduler/<DelayAction>c__Iterator1::action
	Action_t1264377477 * ___action_2;
	// System.Single UniRx.Scheduler/FixedUpdateMainThreadScheduler/<DelayAction>c__Iterator1::<startTime>__1
	float ___U3CstartTimeU3E__1_3;
	// System.Single UniRx.Scheduler/FixedUpdateMainThreadScheduler/<DelayAction>c__Iterator1::<dt>__1
	float ___U3CdtU3E__1_4;
	// System.Single UniRx.Scheduler/FixedUpdateMainThreadScheduler/<DelayAction>c__Iterator1::<elapsed>__2
	float ___U3CelapsedU3E__2_5;
	// System.Object UniRx.Scheduler/FixedUpdateMainThreadScheduler/<DelayAction>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean UniRx.Scheduler/FixedUpdateMainThreadScheduler/<DelayAction>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 UniRx.Scheduler/FixedUpdateMainThreadScheduler/<DelayAction>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_dueTime_0() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t3218606553, ___dueTime_0)); }
	inline TimeSpan_t881159249  get_dueTime_0() const { return ___dueTime_0; }
	inline TimeSpan_t881159249 * get_address_of_dueTime_0() { return &___dueTime_0; }
	inline void set_dueTime_0(TimeSpan_t881159249  value)
	{
		___dueTime_0 = value;
	}

	inline static int32_t get_offset_of_cancellation_1() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t3218606553, ___cancellation_1)); }
	inline RuntimeObject* get_cancellation_1() const { return ___cancellation_1; }
	inline RuntimeObject** get_address_of_cancellation_1() { return &___cancellation_1; }
	inline void set_cancellation_1(RuntimeObject* value)
	{
		___cancellation_1 = value;
		Il2CppCodeGenWriteBarrier((&___cancellation_1), value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t3218606553, ___action_2)); }
	inline Action_t1264377477 * get_action_2() const { return ___action_2; }
	inline Action_t1264377477 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t1264377477 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier((&___action_2), value);
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E__1_3() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t3218606553, ___U3CstartTimeU3E__1_3)); }
	inline float get_U3CstartTimeU3E__1_3() const { return ___U3CstartTimeU3E__1_3; }
	inline float* get_address_of_U3CstartTimeU3E__1_3() { return &___U3CstartTimeU3E__1_3; }
	inline void set_U3CstartTimeU3E__1_3(float value)
	{
		___U3CstartTimeU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CdtU3E__1_4() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t3218606553, ___U3CdtU3E__1_4)); }
	inline float get_U3CdtU3E__1_4() const { return ___U3CdtU3E__1_4; }
	inline float* get_address_of_U3CdtU3E__1_4() { return &___U3CdtU3E__1_4; }
	inline void set_U3CdtU3E__1_4(float value)
	{
		___U3CdtU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedU3E__2_5() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t3218606553, ___U3CelapsedU3E__2_5)); }
	inline float get_U3CelapsedU3E__2_5() const { return ___U3CelapsedU3E__2_5; }
	inline float* get_address_of_U3CelapsedU3E__2_5() { return &___U3CelapsedU3E__2_5; }
	inline void set_U3CelapsedU3E__2_5(float value)
	{
		___U3CelapsedU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t3218606553, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t3218606553, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t3218606553, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYACTIONU3EC__ITERATOR1_T3218606553_H
#ifndef U3CPERIODICACTIONU3EC__ITERATOR2_T2169154091_H
#define U3CPERIODICACTIONU3EC__ITERATOR2_T2169154091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/FixedUpdateMainThreadScheduler/<PeriodicAction>c__Iterator2
struct  U3CPeriodicActionU3Ec__Iterator2_t2169154091  : public RuntimeObject
{
public:
	// System.TimeSpan UniRx.Scheduler/FixedUpdateMainThreadScheduler/<PeriodicAction>c__Iterator2::period
	TimeSpan_t881159249  ___period_0;
	// UniRx.ICancelable UniRx.Scheduler/FixedUpdateMainThreadScheduler/<PeriodicAction>c__Iterator2::cancellation
	RuntimeObject* ___cancellation_1;
	// System.Action UniRx.Scheduler/FixedUpdateMainThreadScheduler/<PeriodicAction>c__Iterator2::action
	Action_t1264377477 * ___action_2;
	// System.Single UniRx.Scheduler/FixedUpdateMainThreadScheduler/<PeriodicAction>c__Iterator2::<startTime>__1
	float ___U3CstartTimeU3E__1_3;
	// System.Single UniRx.Scheduler/FixedUpdateMainThreadScheduler/<PeriodicAction>c__Iterator2::<dt>__1
	float ___U3CdtU3E__1_4;
	// System.Single UniRx.Scheduler/FixedUpdateMainThreadScheduler/<PeriodicAction>c__Iterator2::<ft>__2
	float ___U3CftU3E__2_5;
	// System.Single UniRx.Scheduler/FixedUpdateMainThreadScheduler/<PeriodicAction>c__Iterator2::<elapsed>__2
	float ___U3CelapsedU3E__2_6;
	// System.Object UniRx.Scheduler/FixedUpdateMainThreadScheduler/<PeriodicAction>c__Iterator2::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean UniRx.Scheduler/FixedUpdateMainThreadScheduler/<PeriodicAction>c__Iterator2::$disposing
	bool ___U24disposing_8;
	// System.Int32 UniRx.Scheduler/FixedUpdateMainThreadScheduler/<PeriodicAction>c__Iterator2::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_period_0() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2169154091, ___period_0)); }
	inline TimeSpan_t881159249  get_period_0() const { return ___period_0; }
	inline TimeSpan_t881159249 * get_address_of_period_0() { return &___period_0; }
	inline void set_period_0(TimeSpan_t881159249  value)
	{
		___period_0 = value;
	}

	inline static int32_t get_offset_of_cancellation_1() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2169154091, ___cancellation_1)); }
	inline RuntimeObject* get_cancellation_1() const { return ___cancellation_1; }
	inline RuntimeObject** get_address_of_cancellation_1() { return &___cancellation_1; }
	inline void set_cancellation_1(RuntimeObject* value)
	{
		___cancellation_1 = value;
		Il2CppCodeGenWriteBarrier((&___cancellation_1), value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2169154091, ___action_2)); }
	inline Action_t1264377477 * get_action_2() const { return ___action_2; }
	inline Action_t1264377477 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t1264377477 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier((&___action_2), value);
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E__1_3() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2169154091, ___U3CstartTimeU3E__1_3)); }
	inline float get_U3CstartTimeU3E__1_3() const { return ___U3CstartTimeU3E__1_3; }
	inline float* get_address_of_U3CstartTimeU3E__1_3() { return &___U3CstartTimeU3E__1_3; }
	inline void set_U3CstartTimeU3E__1_3(float value)
	{
		___U3CstartTimeU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CdtU3E__1_4() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2169154091, ___U3CdtU3E__1_4)); }
	inline float get_U3CdtU3E__1_4() const { return ___U3CdtU3E__1_4; }
	inline float* get_address_of_U3CdtU3E__1_4() { return &___U3CdtU3E__1_4; }
	inline void set_U3CdtU3E__1_4(float value)
	{
		___U3CdtU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CftU3E__2_5() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2169154091, ___U3CftU3E__2_5)); }
	inline float get_U3CftU3E__2_5() const { return ___U3CftU3E__2_5; }
	inline float* get_address_of_U3CftU3E__2_5() { return &___U3CftU3E__2_5; }
	inline void set_U3CftU3E__2_5(float value)
	{
		___U3CftU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedU3E__2_6() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2169154091, ___U3CelapsedU3E__2_6)); }
	inline float get_U3CelapsedU3E__2_6() const { return ___U3CelapsedU3E__2_6; }
	inline float* get_address_of_U3CelapsedU3E__2_6() { return &___U3CelapsedU3E__2_6; }
	inline void set_U3CelapsedU3E__2_6(float value)
	{
		___U3CelapsedU3E__2_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2169154091, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2169154091, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CPeriodicActionU3Ec__Iterator2_t2169154091, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPERIODICACTIONU3EC__ITERATOR2_T2169154091_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef U3CDELAYACTIONU3EC__ITERATOR1_T1566613126_H
#define U3CDELAYACTIONU3EC__ITERATOR1_T1566613126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/EndOfFrameMainThreadScheduler/<DelayAction>c__Iterator1
struct  U3CDelayActionU3Ec__Iterator1_t1566613126  : public RuntimeObject
{
public:
	// System.TimeSpan UniRx.Scheduler/EndOfFrameMainThreadScheduler/<DelayAction>c__Iterator1::dueTime
	TimeSpan_t881159249  ___dueTime_0;
	// UniRx.ICancelable UniRx.Scheduler/EndOfFrameMainThreadScheduler/<DelayAction>c__Iterator1::cancellation
	RuntimeObject* ___cancellation_1;
	// System.Action UniRx.Scheduler/EndOfFrameMainThreadScheduler/<DelayAction>c__Iterator1::action
	Action_t1264377477 * ___action_2;
	// System.Single UniRx.Scheduler/EndOfFrameMainThreadScheduler/<DelayAction>c__Iterator1::<elapsed>__1
	float ___U3CelapsedU3E__1_3;
	// System.Single UniRx.Scheduler/EndOfFrameMainThreadScheduler/<DelayAction>c__Iterator1::<dt>__1
	float ___U3CdtU3E__1_4;
	// System.Object UniRx.Scheduler/EndOfFrameMainThreadScheduler/<DelayAction>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean UniRx.Scheduler/EndOfFrameMainThreadScheduler/<DelayAction>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 UniRx.Scheduler/EndOfFrameMainThreadScheduler/<DelayAction>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_dueTime_0() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t1566613126, ___dueTime_0)); }
	inline TimeSpan_t881159249  get_dueTime_0() const { return ___dueTime_0; }
	inline TimeSpan_t881159249 * get_address_of_dueTime_0() { return &___dueTime_0; }
	inline void set_dueTime_0(TimeSpan_t881159249  value)
	{
		___dueTime_0 = value;
	}

	inline static int32_t get_offset_of_cancellation_1() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t1566613126, ___cancellation_1)); }
	inline RuntimeObject* get_cancellation_1() const { return ___cancellation_1; }
	inline RuntimeObject** get_address_of_cancellation_1() { return &___cancellation_1; }
	inline void set_cancellation_1(RuntimeObject* value)
	{
		___cancellation_1 = value;
		Il2CppCodeGenWriteBarrier((&___cancellation_1), value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t1566613126, ___action_2)); }
	inline Action_t1264377477 * get_action_2() const { return ___action_2; }
	inline Action_t1264377477 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t1264377477 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier((&___action_2), value);
	}

	inline static int32_t get_offset_of_U3CelapsedU3E__1_3() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t1566613126, ___U3CelapsedU3E__1_3)); }
	inline float get_U3CelapsedU3E__1_3() const { return ___U3CelapsedU3E__1_3; }
	inline float* get_address_of_U3CelapsedU3E__1_3() { return &___U3CelapsedU3E__1_3; }
	inline void set_U3CelapsedU3E__1_3(float value)
	{
		___U3CelapsedU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CdtU3E__1_4() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t1566613126, ___U3CdtU3E__1_4)); }
	inline float get_U3CdtU3E__1_4() const { return ___U3CdtU3E__1_4; }
	inline float* get_address_of_U3CdtU3E__1_4() { return &___U3CdtU3E__1_4; }
	inline void set_U3CdtU3E__1_4(float value)
	{
		___U3CdtU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t1566613126, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t1566613126, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CDelayActionU3Ec__Iterator1_t1566613126, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYACTIONU3EC__ITERATOR1_T1566613126_H
#ifndef REACTIVEPROPERTY_1_T2553753831_H
#define REACTIVEPROPERTY_1_T2553753831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<UnityEngine.Vector4>
struct  ReactiveProperty_1_t2553753831  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	Vector4_t3319028937  ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t3407685324 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2553753831, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2553753831, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2553753831, ___value_3)); }
	inline Vector4_t3319028937  get_value_3() const { return ___value_3; }
	inline Vector4_t3319028937 * get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(Vector4_t3319028937  value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2553753831, ___publisher_4)); }
	inline Subject_1_t3407685324 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t3407685324 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t3407685324 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2553753831, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2553753831, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t2553753831_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2553753831_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T2553753831_H
#ifndef REACTIVEPROPERTY_1_T2957038358_H
#define REACTIVEPROPERTY_1_T2957038358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<UnityEngine.Vector3>
struct  ReactiveProperty_1_t2957038358  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	Vector3_t3722313464  ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t3810969851 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2957038358, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2957038358, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2957038358, ___value_3)); }
	inline Vector3_t3722313464  get_value_3() const { return ___value_3; }
	inline Vector3_t3722313464 * get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(Vector3_t3722313464  value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2957038358, ___publisher_4)); }
	inline Subject_1_t3810969851 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t3810969851 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t3810969851 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2957038358, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2957038358, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t2957038358_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t2957038358_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T2957038358_H
#ifndef REACTIVEPROPERTY_1_T1390954417_H
#define REACTIVEPROPERTY_1_T1390954417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<UnityEngine.Vector2>
struct  ReactiveProperty_1_t1390954417  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	Vector2_t2156229523  ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t2244885910 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1390954417, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1390954417, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1390954417, ___value_3)); }
	inline Vector2_t2156229523  get_value_3() const { return ___value_3; }
	inline Vector2_t2156229523 * get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(Vector2_t2156229523  value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1390954417, ___publisher_4)); }
	inline Subject_1_t2244885910 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t2244885910 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t2244885910 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1390954417, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1390954417, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t1390954417_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1390954417_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T1390954417_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef LOGTYPE_T73765434_H
#define LOGTYPE_T73765434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LogType
struct  LogType_t73765434 
{
public:
	// System.Int32 UnityEngine.LogType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogType_t73765434, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGTYPE_T73765434_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RECTREACTIVEPROPERTY_T1528095736_H
#define RECTREACTIVEPROPERTY_T1528095736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.RectReactiveProperty
struct  RectReactiveProperty_t1528095736  : public ReactiveProperty_1_t1595204753
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTREACTIVEPROPERTY_T1528095736_H
#ifndef COLORREACTIVEPROPERTY_T3266332558_H
#define COLORREACTIVEPROPERTY_T3266332558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ColorReactiveProperty
struct  ColorReactiveProperty_t3266332558  : public ReactiveProperty_1_t1790411218
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORREACTIVEPROPERTY_T3266332558_H
#ifndef VECTOR4REACTIVEPROPERTY_T1508944816_H
#define VECTOR4REACTIVEPROPERTY_T1508944816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Vector4ReactiveProperty
struct  Vector4ReactiveProperty_t1508944816  : public ReactiveProperty_1_t2553753831
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4REACTIVEPROPERTY_T1508944816_H
#ifndef VECTOR2REACTIVEPROPERTY_T1480206898_H
#define VECTOR2REACTIVEPROPERTY_T1480206898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Vector2ReactiveProperty
struct  Vector2ReactiveProperty_t1480206898  : public ReactiveProperty_1_t1390954417
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2REACTIVEPROPERTY_T1480206898_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef REACTIVEPROPERTY_1_T1501562804_H
#define REACTIVEPROPERTY_1_T1501562804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<UnityEngine.Bounds>
struct  ReactiveProperty_1_t1501562804  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	Bounds_t2266837910  ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t2355494297 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1501562804, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1501562804, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1501562804, ___value_3)); }
	inline Bounds_t2266837910  get_value_3() const { return ___value_3; }
	inline Bounds_t2266837910 * get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(Bounds_t2266837910  value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1501562804, ___publisher_4)); }
	inline Subject_1_t2355494297 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t2355494297 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t2355494297 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1501562804, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1501562804, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t1501562804_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1501562804_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T1501562804_H
#ifndef VECTOR3REACTIVEPROPERTY_T1509712485_H
#define VECTOR3REACTIVEPROPERTY_T1509712485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Vector3ReactiveProperty
struct  Vector3ReactiveProperty_t1509712485  : public ReactiveProperty_1_t2957038358
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3REACTIVEPROPERTY_T1509712485_H
#ifndef BOUNDSREACTIVEPROPERTY_T3308566424_H
#define BOUNDSREACTIVEPROPERTY_T3308566424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.BoundsReactiveProperty
struct  BoundsReactiveProperty_t3308566424  : public ReactiveProperty_1_t1501562804
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDSREACTIVEPROPERTY_T3308566424_H
#ifndef LOGENTRY_T1141507113_H
#define LOGENTRY_T1141507113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Diagnostics.LogEntry
struct  LogEntry_t1141507113 
{
public:
	union
	{
		struct
		{
			// System.String UniRx.Diagnostics.LogEntry::<LoggerName>k__BackingField
			String_t* ___U3CLoggerNameU3Ek__BackingField_0;
			// UnityEngine.LogType UniRx.Diagnostics.LogEntry::<LogType>k__BackingField
			int32_t ___U3CLogTypeU3Ek__BackingField_1;
			// System.String UniRx.Diagnostics.LogEntry::<Message>k__BackingField
			String_t* ___U3CMessageU3Ek__BackingField_2;
			// System.DateTime UniRx.Diagnostics.LogEntry::<Timestamp>k__BackingField
			DateTime_t3738529785  ___U3CTimestampU3Ek__BackingField_3;
			// UnityEngine.Object UniRx.Diagnostics.LogEntry::<Context>k__BackingField
			Object_t631007953 * ___U3CContextU3Ek__BackingField_4;
			// System.Exception UniRx.Diagnostics.LogEntry::<Exception>k__BackingField
			Exception_t * ___U3CExceptionU3Ek__BackingField_5;
			// System.String UniRx.Diagnostics.LogEntry::<StackTrace>k__BackingField
			String_t* ___U3CStackTraceU3Ek__BackingField_6;
			// System.Object UniRx.Diagnostics.LogEntry::<State>k__BackingField
			RuntimeObject * ___U3CStateU3Ek__BackingField_7;
		};
		uint8_t LogEntry_t1141507113__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CLoggerNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CLoggerNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CLoggerNameU3Ek__BackingField_0() const { return ___U3CLoggerNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CLoggerNameU3Ek__BackingField_0() { return &___U3CLoggerNameU3Ek__BackingField_0; }
	inline void set_U3CLoggerNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CLoggerNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoggerNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CLogTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CLogTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CLogTypeU3Ek__BackingField_1() const { return ___U3CLogTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CLogTypeU3Ek__BackingField_1() { return &___U3CLogTypeU3Ek__BackingField_1; }
	inline void set_U3CLogTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CLogTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CMessageU3Ek__BackingField_2)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_2() const { return ___U3CMessageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_2() { return &___U3CMessageU3Ek__BackingField_2; }
	inline void set_U3CMessageU3Ek__BackingField_2(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CTimestampU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CTimestampU3Ek__BackingField_3)); }
	inline DateTime_t3738529785  get_U3CTimestampU3Ek__BackingField_3() const { return ___U3CTimestampU3Ek__BackingField_3; }
	inline DateTime_t3738529785 * get_address_of_U3CTimestampU3Ek__BackingField_3() { return &___U3CTimestampU3Ek__BackingField_3; }
	inline void set_U3CTimestampU3Ek__BackingField_3(DateTime_t3738529785  value)
	{
		___U3CTimestampU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CContextU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CContextU3Ek__BackingField_4)); }
	inline Object_t631007953 * get_U3CContextU3Ek__BackingField_4() const { return ___U3CContextU3Ek__BackingField_4; }
	inline Object_t631007953 ** get_address_of_U3CContextU3Ek__BackingField_4() { return &___U3CContextU3Ek__BackingField_4; }
	inline void set_U3CContextU3Ek__BackingField_4(Object_t631007953 * value)
	{
		___U3CContextU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContextU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CExceptionU3Ek__BackingField_5)); }
	inline Exception_t * get_U3CExceptionU3Ek__BackingField_5() const { return ___U3CExceptionU3Ek__BackingField_5; }
	inline Exception_t ** get_address_of_U3CExceptionU3Ek__BackingField_5() { return &___U3CExceptionU3Ek__BackingField_5; }
	inline void set_U3CExceptionU3Ek__BackingField_5(Exception_t * value)
	{
		___U3CExceptionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CStackTraceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CStackTraceU3Ek__BackingField_6)); }
	inline String_t* get_U3CStackTraceU3Ek__BackingField_6() const { return ___U3CStackTraceU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CStackTraceU3Ek__BackingField_6() { return &___U3CStackTraceU3Ek__BackingField_6; }
	inline void set_U3CStackTraceU3Ek__BackingField_6(String_t* value)
	{
		___U3CStackTraceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStackTraceU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LogEntry_t1141507113, ___U3CStateU3Ek__BackingField_7)); }
	inline RuntimeObject * get_U3CStateU3Ek__BackingField_7() const { return ___U3CStateU3Ek__BackingField_7; }
	inline RuntimeObject ** get_address_of_U3CStateU3Ek__BackingField_7() { return &___U3CStateU3Ek__BackingField_7; }
	inline void set_U3CStateU3Ek__BackingField_7(RuntimeObject * value)
	{
		___U3CStateU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStateU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UniRx.Diagnostics.LogEntry
struct LogEntry_t1141507113_marshaled_pinvoke
{
	union
	{
		struct
		{
			char* ___U3CLoggerNameU3Ek__BackingField_0;
			int32_t ___U3CLogTypeU3Ek__BackingField_1;
			char* ___U3CMessageU3Ek__BackingField_2;
			DateTime_t3738529785  ___U3CTimestampU3Ek__BackingField_3;
			Object_t631007953_marshaled_pinvoke ___U3CContextU3Ek__BackingField_4;
			Exception_t * ___U3CExceptionU3Ek__BackingField_5;
			char* ___U3CStackTraceU3Ek__BackingField_6;
			Il2CppIUnknown* ___U3CStateU3Ek__BackingField_7;
		};
		uint8_t LogEntry_t1141507113__padding[1];
	};
};
// Native definition for COM marshalling of UniRx.Diagnostics.LogEntry
struct LogEntry_t1141507113_marshaled_com
{
	union
	{
		struct
		{
			Il2CppChar* ___U3CLoggerNameU3Ek__BackingField_0;
			int32_t ___U3CLogTypeU3Ek__BackingField_1;
			Il2CppChar* ___U3CMessageU3Ek__BackingField_2;
			DateTime_t3738529785  ___U3CTimestampU3Ek__BackingField_3;
			Object_t631007953_marshaled_com* ___U3CContextU3Ek__BackingField_4;
			Exception_t * ___U3CExceptionU3Ek__BackingField_5;
			Il2CppChar* ___U3CStackTraceU3Ek__BackingField_6;
			Il2CppIUnknown* ___U3CStateU3Ek__BackingField_7;
		};
		uint8_t LogEntry_t1141507113__padding[1];
	};
};
#endif // LOGENTRY_T1141507113_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (CurrentThreadScheduler_t2387236140), -1, 0, sizeof(CurrentThreadScheduler_t2387236140_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2900[2] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (Trampoline_t854539875), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (ImmediateScheduler_t2366437439), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (DefaultSchedulers_t4171041390), -1, sizeof(DefaultSchedulers_t4171041390_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2903[5] = 
{
	DefaultSchedulers_t4171041390_StaticFields::get_offset_of_constantTime_0(),
	DefaultSchedulers_t4171041390_StaticFields::get_offset_of_tailRecursion_1(),
	DefaultSchedulers_t4171041390_StaticFields::get_offset_of_iteration_2(),
	DefaultSchedulers_t4171041390_StaticFields::get_offset_of_timeBasedOperations_3(),
	DefaultSchedulers_t4171041390_StaticFields::get_offset_of_asyncConversions_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (ThreadPoolScheduler_t964667149), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (Timer_t996616921), -1, sizeof(Timer_t996616921_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2905[6] = 
{
	Timer_t996616921_StaticFields::get_offset_of_s_timers_0(),
	Timer_t996616921::get_offset_of__disposable_1(),
	Timer_t996616921::get_offset_of__action_2(),
	Timer_t996616921::get_offset_of__timer_3(),
	Timer_t996616921::get_offset_of__hasAdded_4(),
	Timer_t996616921::get_offset_of__hasRemoved_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (PeriodicTimer_t753797134), -1, sizeof(PeriodicTimer_t753797134_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2906[4] = 
{
	PeriodicTimer_t753797134_StaticFields::get_offset_of_s_timers_0(),
	PeriodicTimer_t753797134::get_offset_of__action_1(),
	PeriodicTimer_t753797134::get_offset_of__timer_2(),
	PeriodicTimer_t753797134::get_offset_of__gate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (U3CScheduleU3Ec__AnonStorey0_t2014915017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2907[2] = 
{
	U3CScheduleU3Ec__AnonStorey0_t2014915017::get_offset_of_d_0(),
	U3CScheduleU3Ec__AnonStorey0_t2014915017::get_offset_of_action_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2908[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (MainThreadScheduler_t794692167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2909[1] = 
{
	MainThreadScheduler_t794692167::get_offset_of_scheduleAction_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2910[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (U3CDelayActionU3Ec__Iterator0_t3203061179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2911[6] = 
{
	U3CDelayActionU3Ec__Iterator0_t3203061179::get_offset_of_dueTime_0(),
	U3CDelayActionU3Ec__Iterator0_t3203061179::get_offset_of_cancellation_1(),
	U3CDelayActionU3Ec__Iterator0_t3203061179::get_offset_of_action_2(),
	U3CDelayActionU3Ec__Iterator0_t3203061179::get_offset_of_U24current_3(),
	U3CDelayActionU3Ec__Iterator0_t3203061179::get_offset_of_U24disposing_4(),
	U3CDelayActionU3Ec__Iterator0_t3203061179::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (U3CPeriodicActionU3Ec__Iterator1_t1273408431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2912[8] = 
{
	U3CPeriodicActionU3Ec__Iterator1_t1273408431::get_offset_of_period_0(),
	U3CPeriodicActionU3Ec__Iterator1_t1273408431::get_offset_of_cancellation_1(),
	U3CPeriodicActionU3Ec__Iterator1_t1273408431::get_offset_of_action_2(),
	U3CPeriodicActionU3Ec__Iterator1_t1273408431::get_offset_of_U3CsecondsU3E__1_3(),
	U3CPeriodicActionU3Ec__Iterator1_t1273408431::get_offset_of_U3CyieldInstructionU3E__1_4(),
	U3CPeriodicActionU3Ec__Iterator1_t1273408431::get_offset_of_U24current_5(),
	U3CPeriodicActionU3Ec__Iterator1_t1273408431::get_offset_of_U24disposing_6(),
	U3CPeriodicActionU3Ec__Iterator1_t1273408431::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (IgnoreTimeScaleMainThreadScheduler_t115692742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2913[1] = 
{
	IgnoreTimeScaleMainThreadScheduler_t115692742::get_offset_of_scheduleAction_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2914[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (U3CDelayActionU3Ec__Iterator0_t749594046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2915[8] = 
{
	U3CDelayActionU3Ec__Iterator0_t749594046::get_offset_of_dueTime_0(),
	U3CDelayActionU3Ec__Iterator0_t749594046::get_offset_of_cancellation_1(),
	U3CDelayActionU3Ec__Iterator0_t749594046::get_offset_of_action_2(),
	U3CDelayActionU3Ec__Iterator0_t749594046::get_offset_of_U3CelapsedU3E__1_3(),
	U3CDelayActionU3Ec__Iterator0_t749594046::get_offset_of_U3CdtU3E__1_4(),
	U3CDelayActionU3Ec__Iterator0_t749594046::get_offset_of_U24current_5(),
	U3CDelayActionU3Ec__Iterator0_t749594046::get_offset_of_U24disposing_6(),
	U3CDelayActionU3Ec__Iterator0_t749594046::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (U3CPeriodicActionU3Ec__Iterator1_t2287198350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2916[8] = 
{
	U3CPeriodicActionU3Ec__Iterator1_t2287198350::get_offset_of_period_0(),
	U3CPeriodicActionU3Ec__Iterator1_t2287198350::get_offset_of_cancellation_1(),
	U3CPeriodicActionU3Ec__Iterator1_t2287198350::get_offset_of_action_2(),
	U3CPeriodicActionU3Ec__Iterator1_t2287198350::get_offset_of_U3CelapsedU3E__1_3(),
	U3CPeriodicActionU3Ec__Iterator1_t2287198350::get_offset_of_U3CdtU3E__1_4(),
	U3CPeriodicActionU3Ec__Iterator1_t2287198350::get_offset_of_U24current_5(),
	U3CPeriodicActionU3Ec__Iterator1_t2287198350::get_offset_of_U24disposing_6(),
	U3CPeriodicActionU3Ec__Iterator1_t2287198350::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (FixedUpdateMainThreadScheduler_t1759960949), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2918[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (U3CDelayActionU3Ec__Iterator1_t3218606553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[9] = 
{
	U3CDelayActionU3Ec__Iterator1_t3218606553::get_offset_of_dueTime_0(),
	U3CDelayActionU3Ec__Iterator1_t3218606553::get_offset_of_cancellation_1(),
	U3CDelayActionU3Ec__Iterator1_t3218606553::get_offset_of_action_2(),
	U3CDelayActionU3Ec__Iterator1_t3218606553::get_offset_of_U3CstartTimeU3E__1_3(),
	U3CDelayActionU3Ec__Iterator1_t3218606553::get_offset_of_U3CdtU3E__1_4(),
	U3CDelayActionU3Ec__Iterator1_t3218606553::get_offset_of_U3CelapsedU3E__2_5(),
	U3CDelayActionU3Ec__Iterator1_t3218606553::get_offset_of_U24current_6(),
	U3CDelayActionU3Ec__Iterator1_t3218606553::get_offset_of_U24disposing_7(),
	U3CDelayActionU3Ec__Iterator1_t3218606553::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (U3CPeriodicActionU3Ec__Iterator2_t2169154091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2920[10] = 
{
	U3CPeriodicActionU3Ec__Iterator2_t2169154091::get_offset_of_period_0(),
	U3CPeriodicActionU3Ec__Iterator2_t2169154091::get_offset_of_cancellation_1(),
	U3CPeriodicActionU3Ec__Iterator2_t2169154091::get_offset_of_action_2(),
	U3CPeriodicActionU3Ec__Iterator2_t2169154091::get_offset_of_U3CstartTimeU3E__1_3(),
	U3CPeriodicActionU3Ec__Iterator2_t2169154091::get_offset_of_U3CdtU3E__1_4(),
	U3CPeriodicActionU3Ec__Iterator2_t2169154091::get_offset_of_U3CftU3E__2_5(),
	U3CPeriodicActionU3Ec__Iterator2_t2169154091::get_offset_of_U3CelapsedU3E__2_6(),
	U3CPeriodicActionU3Ec__Iterator2_t2169154091::get_offset_of_U24current_7(),
	U3CPeriodicActionU3Ec__Iterator2_t2169154091::get_offset_of_U24disposing_8(),
	U3CPeriodicActionU3Ec__Iterator2_t2169154091::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (EndOfFrameMainThreadScheduler_t3512639697), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2922[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (U3CDelayActionU3Ec__Iterator1_t1566613126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2923[8] = 
{
	U3CDelayActionU3Ec__Iterator1_t1566613126::get_offset_of_dueTime_0(),
	U3CDelayActionU3Ec__Iterator1_t1566613126::get_offset_of_cancellation_1(),
	U3CDelayActionU3Ec__Iterator1_t1566613126::get_offset_of_action_2(),
	U3CDelayActionU3Ec__Iterator1_t1566613126::get_offset_of_U3CelapsedU3E__1_3(),
	U3CDelayActionU3Ec__Iterator1_t1566613126::get_offset_of_U3CdtU3E__1_4(),
	U3CDelayActionU3Ec__Iterator1_t1566613126::get_offset_of_U24current_5(),
	U3CDelayActionU3Ec__Iterator1_t1566613126::get_offset_of_U24disposing_6(),
	U3CDelayActionU3Ec__Iterator1_t1566613126::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (U3CPeriodicActionU3Ec__Iterator2_t2292180132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2924[8] = 
{
	U3CPeriodicActionU3Ec__Iterator2_t2292180132::get_offset_of_period_0(),
	U3CPeriodicActionU3Ec__Iterator2_t2292180132::get_offset_of_cancellation_1(),
	U3CPeriodicActionU3Ec__Iterator2_t2292180132::get_offset_of_action_2(),
	U3CPeriodicActionU3Ec__Iterator2_t2292180132::get_offset_of_U3CelapsedU3E__1_3(),
	U3CPeriodicActionU3Ec__Iterator2_t2292180132::get_offset_of_U3CdtU3E__1_4(),
	U3CPeriodicActionU3Ec__Iterator2_t2292180132::get_offset_of_U24current_5(),
	U3CPeriodicActionU3Ec__Iterator2_t2292180132::get_offset_of_U24disposing_6(),
	U3CPeriodicActionU3Ec__Iterator2_t2292180132::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (U3CScheduleU3Ec__AnonStorey0_t1246538827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2925[5] = 
{
	U3CScheduleU3Ec__AnonStorey0_t1246538827::get_offset_of_action_0(),
	U3CScheduleU3Ec__AnonStorey0_t1246538827::get_offset_of_scheduler_1(),
	U3CScheduleU3Ec__AnonStorey0_t1246538827::get_offset_of_gate_2(),
	U3CScheduleU3Ec__AnonStorey0_t1246538827::get_offset_of_group_3(),
	U3CScheduleU3Ec__AnonStorey0_t1246538827::get_offset_of_recursiveAction_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (U3CScheduleU3Ec__AnonStorey1_t2063910649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2926[4] = 
{
	U3CScheduleU3Ec__AnonStorey1_t2063910649::get_offset_of_isAdded_0(),
	U3CScheduleU3Ec__AnonStorey1_t2063910649::get_offset_of_d_1(),
	U3CScheduleU3Ec__AnonStorey1_t2063910649::get_offset_of_isDone_2(),
	U3CScheduleU3Ec__AnonStorey1_t2063910649::get_offset_of_U3CU3Ef__refU240_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (U3CScheduleU3Ec__AnonStorey2_t1628875851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2927[5] = 
{
	U3CScheduleU3Ec__AnonStorey2_t1628875851::get_offset_of_action_0(),
	U3CScheduleU3Ec__AnonStorey2_t1628875851::get_offset_of_scheduler_1(),
	U3CScheduleU3Ec__AnonStorey2_t1628875851::get_offset_of_gate_2(),
	U3CScheduleU3Ec__AnonStorey2_t1628875851::get_offset_of_group_3(),
	U3CScheduleU3Ec__AnonStorey2_t1628875851::get_offset_of_recursiveAction_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (U3CScheduleU3Ec__AnonStorey3_t342939487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2928[4] = 
{
	U3CScheduleU3Ec__AnonStorey3_t342939487::get_offset_of_isAdded_0(),
	U3CScheduleU3Ec__AnonStorey3_t342939487::get_offset_of_d_1(),
	U3CScheduleU3Ec__AnonStorey3_t342939487::get_offset_of_isDone_2(),
	U3CScheduleU3Ec__AnonStorey3_t342939487::get_offset_of_U3CU3Ef__refU242_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (U3CScheduleU3Ec__AnonStorey4_t481864779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2929[5] = 
{
	U3CScheduleU3Ec__AnonStorey4_t481864779::get_offset_of_action_0(),
	U3CScheduleU3Ec__AnonStorey4_t481864779::get_offset_of_scheduler_1(),
	U3CScheduleU3Ec__AnonStorey4_t481864779::get_offset_of_gate_2(),
	U3CScheduleU3Ec__AnonStorey4_t481864779::get_offset_of_group_3(),
	U3CScheduleU3Ec__AnonStorey4_t481864779::get_offset_of_recursiveAction_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (U3CScheduleU3Ec__AnonStorey5_t849780234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2930[4] = 
{
	U3CScheduleU3Ec__AnonStorey5_t849780234::get_offset_of_isAdded_0(),
	U3CScheduleU3Ec__AnonStorey5_t849780234::get_offset_of_d_1(),
	U3CScheduleU3Ec__AnonStorey5_t849780234::get_offset_of_isDone_2(),
	U3CScheduleU3Ec__AnonStorey5_t849780234::get_offset_of_U3CU3Ef__refU244_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2935[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2936[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2937[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2938[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2942[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2943[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2944[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2945[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (SubjectExtensions_t2134319452), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2947[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (OptimizedObservableExtensions_t1647498745), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2955[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (Tuple_t1757971014), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2960[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2961[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2962[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2963[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2964[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2965[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2966[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2967[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (Unit_t3362249467)+ sizeof (RuntimeObject), sizeof(Unit_t3362249467 ), sizeof(Unit_t3362249467_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2968[1] = 
{
	Unit_t3362249467_StaticFields::get_offset_of_default_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2969[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2970[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (Timestamped_t555880859), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (AotSafeExtensions_t3690086409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2973[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (AsyncOperationExtensions_t103671278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (U3CAsObservableU3Ec__AnonStorey1_t2816429619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2975[2] = 
{
	U3CAsObservableU3Ec__AnonStorey1_t2816429619::get_offset_of_asyncOperation_0(),
	U3CAsObservableU3Ec__AnonStorey1_t2816429619::get_offset_of_progress_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2976[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2977[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (CancellationToken_t1265546479)+ sizeof (RuntimeObject), -1, sizeof(CancellationToken_t1265546479_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2978[3] = 
{
	CancellationToken_t1265546479::get_offset_of_source_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CancellationToken_t1265546479_StaticFields::get_offset_of_Empty_1(),
	CancellationToken_t1265546479_StaticFields::get_offset_of_None_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { sizeof (LogEntry_t1141507113)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2979[8] = 
{
	LogEntry_t1141507113::get_offset_of_U3CLoggerNameU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LogEntry_t1141507113::get_offset_of_U3CLogTypeU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LogEntry_t1141507113::get_offset_of_U3CMessageU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LogEntry_t1141507113::get_offset_of_U3CTimestampU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LogEntry_t1141507113::get_offset_of_U3CContextU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LogEntry_t1141507113::get_offset_of_U3CExceptionU3Ek__BackingField_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LogEntry_t1141507113::get_offset_of_U3CStackTraceU3Ek__BackingField_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LogEntry_t1141507113::get_offset_of_U3CStateU3Ek__BackingField_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (LogEntryExtensions_t2577791660), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { sizeof (Logger_t3861635025), -1, sizeof(Logger_t3861635025_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2981[4] = 
{
	Logger_t3861635025_StaticFields::get_offset_of_isInitialized_0(),
	Logger_t3861635025_StaticFields::get_offset_of_isDebugBuild_1(),
	Logger_t3861635025::get_offset_of_U3CNameU3Ek__BackingField_2(),
	Logger_t3861635025::get_offset_of_logPublisher_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (ObservableDebugExtensions_t2967164862), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (ObservableLogger_t2665118834), -1, sizeof(ObservableLogger_t2665118834_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2983[2] = 
{
	ObservableLogger_t2665118834_StaticFields::get_offset_of_logPublisher_0(),
	ObservableLogger_t2665118834_StaticFields::get_offset_of_Listener_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (UnityDebugSink_t4250252982), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2985[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (IntReactiveProperty_t3243525985), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (LongReactiveProperty_t2823960147), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { sizeof (ByteReactiveProperty_t1600986847), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (FloatReactiveProperty_t2159343209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (DoubleReactiveProperty_t3731678748), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { sizeof (StringReactiveProperty_t3302058057), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (BoolReactiveProperty_t700220569), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { sizeof (Vector2ReactiveProperty_t1480206898), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { sizeof (Vector3ReactiveProperty_t1509712485), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { sizeof (Vector4ReactiveProperty_t1508944816), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (ColorReactiveProperty_t3266332558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { sizeof (RectReactiveProperty_t1528095736), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { sizeof (AnimationCurveReactiveProperty_t44630468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (BoundsReactiveProperty_t3308566424), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
