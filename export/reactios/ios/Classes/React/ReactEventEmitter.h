//
//  Header.h
//  Unity-iPhone
//
//  Created by Huynh Tan Ngan on 2/1/18.
//
// CalendarManager.h
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface ReactEventEmitter : RCTEventEmitter <RCTBridgeModule>
- (void)searchVideo:(NSString *)query;
@end
