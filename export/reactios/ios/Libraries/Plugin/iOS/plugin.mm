
extern "C" {
    void _FetchMovieCMethod(int page) {
        double delayInSeconds = 5.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"FetchMovie"
             object:nil];
        });
    }
}
